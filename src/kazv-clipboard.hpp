#include <QString>
#include <QtQml>
#include <QUrl>
#include <QTemporaryDir>
#include <QClipboard>

class KazvClipboard : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
    Q_PROPERTY(bool hasImage READ hasImage WRITE setHasImage NOTIFY hasImageChanged)
    Q_PROPERTY(bool hasUrls READ hasUrls WRITE setHasUrls NOTIFY hasUrlsChanged)
public:
    explicit KazvClipboard(QObject *parent = 0);
    bool hasImage();
    bool hasUrls();
    void setHasImage(bool hasImage);
    void setHasUrls(bool hasUrls);
public Q_SLOTS:
    void updateHasImage();
    void updateHasUrls();
    QList<QUrl> urls();
    QUrl saveClipboardImage();
Q_SIGNALS:
    void hasImageChanged();
    void hasUrlsChanged();
private:
    bool m_hasImage;
    bool m_hasUrls;
    QTemporaryDir m_tmpDir;
    int m_tmpFilesCount;
    QClipboard *m_clipboard;

};
