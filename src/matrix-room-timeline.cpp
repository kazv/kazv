/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <lager/lenses/optional.hpp>

#include <cursorutil.hpp>

#include "matrix-room-timeline.hpp"
#include "matrix-event.hpp"

#include "helper.hpp"

MatrixRoomTimeline::MatrixRoomTimeline(Kazv::Room room, QObject *parent)
    : QObject(parent)
    , m_room(room)
    , m_timeline(m_room.timelineEvents())
    , LAGER_QT(count)(m_timeline.xform(containerSize))
    , LAGER_QT(eventIds)(m_timeline.xform(zug::map(
                                              [](auto container) {
                                                  return zug::into(
                                                      QStringList{},
                                                      zug::map([](Kazv::Event event) {
                                                                   return QString::fromStdString(event.id());
                                                               }),
                                                      std::move(container));
                                              })))
{
}

MatrixRoomTimeline::~MatrixRoomTimeline() = default;

MatrixEvent *MatrixRoomTimeline::at(int index) const
{
    return new MatrixEvent(m_timeline[index][lager::lenses::or_default]);
}
