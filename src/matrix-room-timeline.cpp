/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <cmath>

#include <lager/lenses/optional.hpp>

#include <QTimer>
#include <QJsonValue>
#include <cursorutil.hpp>

#include "matrix-room-timeline.hpp"
#include "matrix-event.hpp"

#include "helper.hpp"

#include "kazv-log.hpp"

using namespace Kazv;

MatrixRoomTimeline::MatrixRoomTimeline(lager::reader<immer::flex_vector<std::string>> eventIds, lager::reader<immer::map<std::string, Kazv::Event>> messagesMap, lager::reader<immer::flex_vector<LocalEchoDesc>> localEchoes, lager::reader<immer::map<std::string, std::string>> timelineGaps, Kazv::Room room, QObject *parent)
    : KazvAbstractListModel(parent)
    , m_room(room)
    , m_timelineEventIds(eventIds)
    , m_messagesMap(messagesMap)
    , m_localEchoes(localEchoes)
    , m_timelineGaps(timelineGaps)
    , LAGER_QT(gaps)(m_timelineGaps.map([](auto g) {
        QJsonObject res;
        for (const auto &[k, _v] : g) {
            res.insert(QString::fromStdString(k), QJsonValue(true));
        }
        return res;
    }))
{
    initCountCursor(lager::with(
        m_timelineEventIds.xform(containerSize),
        m_localEchoes.xform(containerSize)
    ).map([](const auto &tlSize, const auto &localSize) { return tlSize + localSize; }), InitLater);
}

MatrixRoomTimeline::MatrixRoomTimeline(Kazv::Room room, QObject *parent)
    : MatrixRoomTimeline(
        room.timelineEventIds(),
        room.messagesMap(),
        room.localEchoes(),
        room.timelineGaps(),
        room,
        parent
    )
{
}

MatrixRoomTimeline::~MatrixRoomTimeline() = default;

MatrixEvent *MatrixRoomTimeline::at(int index) const
{
    return new MatrixEvent(lager::with(m_timelineEventIds, m_messagesMap, m_localEchoes, LAGER_QT(count))
        .map([index](const auto &tlEventIds, const auto &messagesMap, const auto &localEchoes, const auto &count) -> std::variant<Kazv::Event, Kazv::LocalEchoDesc> {
            auto rIndex = count - index - 1;
            if (static_cast<unsigned int>(rIndex) < tlEventIds.size()) {
                return messagesMap[tlEventIds[rIndex]];
            } else {
                auto remainingIndex = rIndex - tlEventIds.size();
                if (remainingIndex < localEchoes.size()) {
                    return localEchoes[remainingIndex];
                } else {
                    return Event();
                }
            }
        }), m_room);
}

int MatrixRoomTimeline::indexOfEvent(const QString &eventId) const
{
    auto eventMap = m_messagesMap.get();
    auto eventIdStd = eventId.toStdString();
    if (eventMap.count(eventIdStd) == 0) {
        return -1;
    }
    auto eventIds = m_timelineEventIds.get();
    auto it = std::lower_bound(eventIds.begin(), eventIds.end(), eventIdStd,
        [eventMap](const auto &idA, const auto &idB) {
            auto a = std::make_pair(eventMap[idA].originServerTs(), idA);
            auto b = std::make_pair(eventMap[idB].originServerTs(), idB);
            return a < b;
        }
    );
    if (it == eventIds.end()) {
        return -1;
    } else {
        // suppose there are A events and B local echoes
        // forward index:
        // [0, 1, ..., A-1] [A, A+1, ..., A+B-1]
        // reverse index:
        // [A+B-1, A+B-2, ..., B] [B-1, B-2 , 0]
        // forward = it.index()
        // forward + reverse = A+B-1 = count() - 1
        return count() - it.index() - 1;
    }
}
