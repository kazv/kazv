/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <memory>

class QtRandAdapter
{
public:
    QtRandAdapter();
    ~QtRandAdapter();

    QtRandAdapter(const QtRandAdapter &) = delete;
    QtRandAdapter(QtRandAdapter &&);

    QtRandAdapter &operator=(const QtRandAdapter &) = delete;
    QtRandAdapter &operator=(QtRandAdapter &&);

    unsigned int operator()();

private:
    struct Private;
    std::unique_ptr<Private> m_d;
};
