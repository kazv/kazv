/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <string>

inline const std::string accountDataEventType = "im.ponies.user_emotes";
inline const std::string roomStateEventType = "im.ponies.room_emotes";
inline const std::string imagePackRoomsEventType = "im.ponies.emote_rooms";
