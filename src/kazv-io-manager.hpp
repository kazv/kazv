/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include "kazv-io-job.hpp"
#include "matrix-room.hpp"
#include "upload-job-model.hpp"
#include "matrix-room-list.hpp"
#include "matrix-sdk.hpp"

#include <QObject>
#include <QtQml>
#include <QUrl>
#include <QString>
#include <KIO/TransferJob>
#include <QPointer>

#include <memory>

struct KazvIOManagerPrivate;

class KazvIOManager : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    std::unique_ptr<KazvIOManagerPrivate> m_d;

    Q_PROPERTY(QString cacheDirectory READ cacheDirectory WRITE setCacheDirectory NOTIFY cacheDirectoryChanged)

    QPointer<KazvIODownloadJob> makeDownloadJob(
        const QUrl &serverUrl, const QUrl &localFileUrl,
        const bool showPrograssBar, const QString &hash,
        const QString &key = QStringLiteral(""),
        const QString &iv = QStringLiteral("")) const;

public:
    explicit KazvIOManager(QObject *parent = 0);
    ~KazvIOManager() override;

    /**
     * Cache the file specified by fileUrl into a temporary folder
     * Multiple calls do not repeat downloads
     * Returns the local url of the cached file.
     */
    Q_INVOKABLE QUrl cacheFile(const QUrl &fileUrl, const QString &id, const QString &hash = QStringLiteral(""),
        const QString &key = QStringLiteral(""), const QString &iv = QStringLiteral(""));
    Q_INVOKABLE KazvIOBaseJob *startNewDownloadJob(const QUrl &serverUrl, const QUrl &localFileName,
        const QString &jobId, const QString &hash,
        const QString &key = QStringLiteral(""), const QString &iv = QStringLiteral(""));
    Q_INVOKABLE KazvIOBaseJob *startNewUploadJob(
        const QUrl &serverUrl, const QUrl &localFileName, const QString &token,
        const QString &roomId, MatrixRoomList *roomList, const bool encrypted,
        const QString &relType, const QString &relatedTo);
    Q_INVOKABLE KazvIOBaseJob *startNewRoomlessUploadJob(const QUrl &serverUrl, const QUrl &localFileName, const QString &token);
    Q_INVOKABLE KazvIOBaseJob *getCacheJob(const QString &jobId) const;
    Q_INVOKABLE KazvIOBaseJob *getDownloadJob(const QString &jobId);
    Q_INVOKABLE void deleteDownloadJob(const QString &jobId);
    Q_INVOKABLE UploadJobModel *getUploadJobs(const QString &roomId);
    Q_INVOKABLE void deleteUploadJob(const QString &roomId, KazvIOBaseJob *job);
    Q_INVOKABLE void deleteRoomlessUploadJob(KazvIOBaseJob *job);
    Q_INVOKABLE void deleteModelIfEmpty(const QString &roomId);
    Q_INVOKABLE void clearJobs();

    QString cacheDirectory() const;
    void setCacheDirectory(QString cacheDirectory);

Q_SIGNALS:
    void cacheDirectoryChanged();
};
