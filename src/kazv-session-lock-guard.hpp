/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <memory>
#include <filesystem>

class KazvSessionLockGuard
{
public:
    KazvSessionLockGuard(const std::filesystem::path &sessionDir);
    ~KazvSessionLockGuard();

private:
    struct Private;
    std::unique_ptr<Private> m_d;
};
