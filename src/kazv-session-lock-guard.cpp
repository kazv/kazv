/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <kazv-platform.hpp>
#include <cstring>
#if !KAZV_IS_WINDOWS
#include <fcntl.h>
#include <unistd.h>
#endif
#include "kazv-session-lock-guard.hpp"
#include "kazv-log.hpp"

struct KazvSessionLockGuard::Private
{
    std::string lockFilePath;
    int fd{-1};

    void cleanup()
    {
#if !KAZV_IS_WINDOWS
        qCDebug(kazvLog) << "KazvSessionLockGuard::Private::cleanup" << QString::fromStdString(lockFilePath);
        if (fd != -1) {
            qCDebug(kazvLog) << "unlocking session";
            lockf(fd, F_ULOCK, 0);
            close(fd);
            fd = -1;
        }
#endif
    }

    Private(const std::filesystem::path &sessionDir)
        : lockFilePath((sessionDir / "lock").string())
    {
#if !KAZV_IS_WINDOWS
        qCDebug(kazvLog) << "KazvSessionLockGuard::Private::Private" << QString::fromStdString(lockFilePath);
        fd = open(lockFilePath.data(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
        if (fd == -1) {
            throw std::runtime_error{std::string("Cannot open lock file: ") + std::strerror(errno)};
        }

        auto res = lockf(fd, F_TLOCK, 0);
        if (res == -1) {
            cleanup();
            throw std::runtime_error{std::string("Cannot grab lock file: ") + std::strerror(errno)};
        }
#endif
    }

    ~Private()
    {
        cleanup();
    }
};

KazvSessionLockGuard::KazvSessionLockGuard(const std::filesystem::path &sessionDir)
    : m_d(std::make_unique<Private>(sessionDir))
{
}

KazvSessionLockGuard::~KazvSessionLockGuard() = default;
