#include "kazv-clipboard.hpp"

#include <QImage>
#include <QGuiApplication>
#include <QDateTime>
#include <QStringList>
#include <QMimeDatabase>

using namespace Qt::Literals::StringLiterals;

KazvClipboard::KazvClipboard(QObject *parent)
    : QObject(parent)
    , m_tmpDir()
    , m_tmpFilesCount(0)
    , m_clipboard(QGuiApplication::clipboard())
{
    QObject::connect(m_clipboard, &QClipboard::dataChanged,
                     this, &KazvClipboard::updateHasImage);
    QObject::connect(m_clipboard, &QClipboard::dataChanged,
                     this, &KazvClipboard::updateHasUrls);
    this->updateHasImage();
    this->updateHasUrls();
}

bool KazvClipboard::hasImage()
{
    return m_hasImage;
}

bool KazvClipboard::hasUrls()
{
    return m_hasUrls;
}

void KazvClipboard::setHasImage(bool hasImage)
{
    m_hasImage = hasImage;
    Q_EMIT hasImageChanged();
}

void KazvClipboard::setHasUrls(bool hasUrls)
{
    m_hasUrls = hasUrls;
    Q_EMIT hasUrlsChanged();
}

void KazvClipboard::updateHasImage()
{
    bool hasImage = m_clipboard->mimeData()->hasImage();
    setHasImage(hasImage);
}

void KazvClipboard::updateHasUrls()
{
    bool hasUrls = m_clipboard->mimeData()->hasUrls();
    setHasUrls(hasUrls);
}

QList<QUrl> KazvClipboard::urls()
{
    return m_clipboard->mimeData()->urls();
}

QUrl KazvClipboard::saveClipboardImage()
{
    auto *clipboard = QGuiApplication::clipboard();
    m_tmpFilesCount++;
    auto name = u"%1"_s.arg(m_tmpFilesCount);
    auto mimes = clipboard->mimeData()->formats().filter(u"image/"_s);
    QString type;
    if (mimes.size() < 1) {
        qDebug() << "Saving clipboard image, but no image MIME found";
        type = u""_s;
    } else {
        type = QMimeDatabase().mimeTypeForName(mimes.at(0)).preferredSuffix();
    }
    auto path = m_tmpDir.filePath(type.isEmpty() ? name : name.append(u'.').append(type));
    clipboard->image().save(path);
    return QUrl::fromLocalFile(path);
}
