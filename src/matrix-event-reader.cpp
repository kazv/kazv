/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <QDateTime>

#include "matrix-event-reader.hpp"

using namespace Kazv;

MatrixEventReader::MatrixEventReader(lager::reader<Kazv::Event> memberEvent, lager::reader<Kazv::Timestamp> timestamp, QObject *parent)
    : MatrixRoomMember(memberEvent, parent)
    , LAGER_QT(timestamp)(timestamp.map([](auto ts) {
        return static_cast<qint64>(ts);
    }))
    , LAGER_QT(formattedTime)(timestamp
        .map([](Timestamp ts) {
            auto locale = QLocale::system();
            return locale.toString(
                QDateTime::fromMSecsSinceEpoch(ts),
                QLocale::ShortFormat
            );
        }))
{
}

MatrixEventReader::~MatrixEventReader() = default;
