/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>

#include <lager/reader.hpp>
#include <lager/extra/qt.hpp>

#include <base/event.hpp>
#include "kazv-abstract-list-model.hpp"
#include "matrix-sticker-pack-source.hpp"
#include "meta-types.hpp"
Q_MOC_INCLUDE("matrix-sticker.hpp")
Q_MOC_INCLUDE("matrix-event.hpp")

class MatrixSticker;
class MatrixEvent;

class MatrixStickerPack : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<MatrixStickerPackSource> m_source;
    lager::reader<Kazv::Event> m_event;
    lager::reader<Kazv::json> m_images;

public:
    explicit MatrixStickerPack(lager::reader<MatrixStickerPackSource> source, QObject *parent = 0);
    ~MatrixStickerPack() override;

    Q_INVOKABLE MatrixSticker *at(int index) const;

    LAGER_QT_READER(QString, packName);
    LAGER_QT_READER(bool, isAccountData);
    LAGER_QT_READER(bool, isState);
    LAGER_QT_READER(QString, roomId);
    LAGER_QT_READER(QString, stateKey);

    Q_INVOKABLE bool hasShortCode(const QString &shortCode) const;

    /**
     * Add a sticker to the pack and return the source of the
     * modified pack.
     *
     * @param shortCode The short code of the sticker.
     * @param event The event of the sticker.
     * @return The MatrixStickerPackSource of the modified pack.
     */
    Q_INVOKABLE QVariant addSticker(const QString &shortCode, MatrixEvent *event) const;
};
