/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "matrix-event-reader-list-model.hpp"
#include "matrix-event-reader.hpp"

using namespace Kazv;

MatrixEventReaderListModel::MatrixEventReaderListModel(lager::reader<immer::flex_vector<std::tuple<Kazv::Event, Kazv::Timestamp>>> readers, QObject *parent)
    : MatrixRoomMemberListModel(
        readers.xform(containerMap(EventList{}, zug::map([](const auto &item) {
            return std::get<0>(item);
        }))),
        QString(),
        lager::make_constant(Kazv::Event()),
        parent
    )
    , m_readers(readers)
{
}

MatrixEventReaderListModel::~MatrixEventReaderListModel() = default;

MatrixEventReader *MatrixEventReaderListModel::at(int index) const
{
    auto reader = m_readers[index][lager::lenses::or_default].make();
    return new MatrixEventReader(
        reader.map([](const auto &r) {
            return std::get<0>(r);
        }),
        reader.map([](const auto &r) {
            return std::get<1>(r);
        }));
}
