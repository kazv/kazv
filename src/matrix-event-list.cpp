/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "matrix-event.hpp"
#include "helper.hpp"
#include "matrix-event-list.hpp"

using namespace Kazv;

MatrixEventList::MatrixEventList(lager::reader<EventList> eventList, QObject *parent)
    : KazvAbstractListModel(eventList.xform(containerSize), InitNow, parent)
    , m_eventList(eventList)
{
}

MatrixEventList::~MatrixEventList() = default;

MatrixEvent *MatrixEventList::at(int index) const
{
    return new MatrixEvent(m_eventList[index][lager::lenses::or_default]);
}
