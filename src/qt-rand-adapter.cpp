/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <QRandomGenerator>

#include "qt-rand-adapter.hpp"

struct QtRandAdapter::Private
{
    QRandomGenerator generator;
};

QtRandAdapter::QtRandAdapter()
    : m_d(new Private{QRandomGenerator::securelySeeded()})
{
}

QtRandAdapter::~QtRandAdapter() = default;

QtRandAdapter::QtRandAdapter(QtRandAdapter &&) = default;
QtRandAdapter &QtRandAdapter::operator=(QtRandAdapter &&) = default;

unsigned int QtRandAdapter::operator()()
{
    return m_d->generator();
}
