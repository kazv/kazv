/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <nlohmann/json.hpp>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

inline void to_json(nlohmann::json& j, const QJsonObject& qj);
inline void from_json(const nlohmann::json& j, QJsonObject& qj);
inline void to_json(nlohmann::json& j, const QJsonArray& qj);
inline void from_json(const nlohmann::json& j, QJsonArray& qj);
inline void to_json(nlohmann::json& j, const QJsonValue& qj);
inline void to_json(nlohmann::json& j, const QJsonValue& qj);


inline void to_json(nlohmann::json& j, const QJsonValue& qj) {
    if (qj.isArray()) {
        j = qj.toArray();
    } else if (qj.isObject()) {
        j = qj.toObject();
    } else if (qj.isBool()) {
        j = qj.toBool();
    } else if (qj.isDouble()) {
        j = qj.toDouble();
    } else if (qj.isString()) {
        j = qj.toString().toStdString();
    }
}

inline void from_json(const nlohmann::json& j, QJsonValue& qj) {
    if (j.is_array()) {
        qj = QJsonValue(j.get<QJsonArray>());
    } else if (j.is_object()) {
        qj = QJsonValue(j.get<QJsonObject>());
    } else if (j.is_boolean()) {
        qj = QJsonValue(j.get<bool>());
    } else if (j.is_number()) {
        qj = QJsonValue(j.get<double>());
    } else if (j.is_string()) {
        qj = QJsonValue(QString::fromStdString(j.get<std::string>()));
    } else if (j.is_null()) {
        qj = QJsonValue();
    }
}

inline void to_json(nlohmann::json& j, const QJsonObject& qj) {
    for (auto it = qj.constBegin(); it != qj.constEnd(); ++it) {
        j[it.key().toStdString()] = it.value();
    }
}

inline void from_json(const nlohmann::json& j, QJsonObject& qj) {
    for (const auto &[k, v]: j.items()) {
        qj.insert(QString::fromStdString(k), v.get<QJsonValue>());
    }
}

inline void to_json(nlohmann::json& j, const QJsonArray& qj) {
    for (const auto &i: qj) {
        j.push_back(i);
    }
}

inline void from_json(const nlohmann::json& j, QJsonArray& qj) {
    for (const auto &i: j) {
        qj.push_back(i.get<QJsonValue>());
    }
}
