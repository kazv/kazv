/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "upload-job-model.hpp"
#include "kazv-io-job.hpp"

struct UploadJobModelPrivate {
    QList<QPointer<KazvIOUploadJob>> uploadJobs;
};

UploadJobModel::UploadJobModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_d(new UploadJobModelPrivate)
{
}

UploadJobModel::~UploadJobModel() = default;

QVariant UploadJobModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_d->uploadJobs.size() || role != JobRole) {
        return QVariant();
    } else {
        return QVariant::fromValue(m_d->uploadJobs[index.row()].data());
    }
}

int UploadJobModel::rowCount(const QModelIndex & /* parent */) const
{
    return m_d->uploadJobs.size();
}

QHash<int, QByteArray> UploadJobModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[JobRole] = "kazvIOJob";
    return roles;
}

void UploadJobModel::addJob(QPointer<KazvIOUploadJob> job)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_d->uploadJobs.push_back(job.data());
    connect(job.data(), &KazvIOBaseJob::result, this,
        [this, job](KazvIOBaseJob::ErrorCode ec) {
            if (!ec) {
                this->removeJob(job.data());
            }
        });
    endInsertRows();
}

void UploadJobModel::removeJob(QPointer<KazvIOUploadJob> job)
{
    const int index = m_d->uploadJobs.indexOf(job);
    beginRemoveRows(QModelIndex(), index, index);
    m_d->uploadJobs.removeOne(job);
    job->deleteLater();
    endRemoveRows();
}

void UploadJobModel::clearJobs()
{
    beginRemoveRows(QModelIndex(), 0, m_d->uploadJobs.size() - 1);
    std::for_each(m_d->uploadJobs.begin(), m_d->uploadJobs.end(), [this](auto job) {
        job->cancel();
        m_d->uploadJobs.removeOne(job);
        job->deleteLater();
    });
    endRemoveRows();
}
