### Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
###
### This file is part of kazv.
###
### kazv is free software: you can redistribute it and/or modify
### it under the terms of the GNU Affero General Public License as
### published by the Free Software Foundation, either version 3 of the
### License, or (at your option) any later version.
###
### kazv is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU Affero General Public License for more details.
###
### You should have received a copy of the GNU Affero General Public License
### along with kazv.  If not, see <https://www.gnu.org/licenses/>.

app-title = { -kt-app-name }

global-drawer-title = { -kt-app-name }
global-drawer-action-switch-account = 切换账号
global-drawer-action-hard-logout = 登出

empty-room-page-title = 没有选中房间
empty-room-page-description = 当前没有选中的房间。

login-page-title = 登录
login-page-userid-prompt = 用户 id：
login-page-userid-input-placeholder = 例如： @foo:example.org
login-page-password-prompt = 密码：
login-page-login-button = 登录
login-page-close-button = 关闭
login-page-existing-sessions-prompt = 从已有会话中选一个：
login-page-alternative-password-login-prompt = 或者用用户 id 和密码启动新会话：
login-page-restore-session-button = 恢复会话
login-page-error-prompt = 用户名或密码错误 

main-page-title = { -kt-app-name } - { $userId }
main-page-recent-tab-title = 最近
main-page-people-tab-title = 人们
main-page-rooms-tab-title = 房间

room-list-view-room-item-title = { $name } ({ $roomId })
room-list-view-room-item-fav-action = 设为最爱
room-list-view-room-item-fav-action-notification = 把 { $name } 设为了最爱

send-message-box-input-placeholder = 在此输入您的讯息...

## 状态事件
## 通用参数：
## gender = 发送者的性别（male/female/neutral）
## stateKeyUser = state key 用户的名字
## stateKeyUserGender = state key 用户的性别
member-state-joined-room = 加入了房间。
member-state-changed-name-and-avatar = 修改了名字和头像。
member-state-changed-name = 修改了名字。
member-state-changed-avatar = 修改了头像。
member-state-invited = 把 { $stateKeyUser } 邀请到了本房间。
member-state-left = 离开了房间。
member-state-kicked = 踢出了 { $stateKeyUser }。
member-state-banned = 封禁了 { $stateKeyUser }。
member-state-unbanned = 解封了 { $stateKeyUser }。

state-room-created = 创建了房间。
state-room-name-changed = 把房间名字改成了 { $newName }。
state-room-topic-changed = 把房间话题改成了 { $newTopic }。
state-room-avatar-changed = 修改了房间头像。
state-room-pinned-events-changed = 修改了房间的置顶讯息。
state-room-alias-changed = 修改了房间的别名。
state-room-join-rules-changed = 修改了房间的加入规则。
state-room-power-levels-changed = 修改了房间的权限。
state-room-encryption-activated = 对本房间启用了加密。

event-message-image-sent = 发送了图片「{ $body }」。
event-message-file-sent = 发送了文件「{ $body }」。
event-message-video-sent = 发送了视频「{ $body }」。
event-message-audio-sent = 发送了音频「{ $body }」。
event-message-audio-play-audio = 播放音频
