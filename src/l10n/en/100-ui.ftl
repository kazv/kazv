### Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
###
### This file is part of kazv.
###
### kazv is free software: you can redistribute it and/or modify
### it under the terms of the GNU Affero General Public License as
### published by the Free Software Foundation, either version 3 of the
### License, or (at your option) any later version.
###
### kazv is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU Affero General Public License for more details.
###
### You should have received a copy of the GNU Affero General Public License
### along with kazv.  If not, see <https://www.gnu.org/licenses/>.

app-title = { -kt-app-name }

global-drawer-title = { -kt-app-name }
global-drawer-action-switch-account = Switch account
global-drawer-action-hard-logout = Logout

empty-room-page-title = No rooms selected
empty-room-page-description = There is no room selected now.

login-page-title = Log in
login-page-userid-prompt = User id:
login-page-userid-input-placeholder = E.g.: @foo:example.org
login-page-password-prompt = Password:
login-page-login-button = Log in
login-page-close-button = Close
login-page-existing-sessions-prompt = Choose from one of the existing sessions:
login-page-alternative-password-login-prompt = Or start a new session with user id and password:
login-page-restore-session-button = Restore session
login-page-error-prompt = incorrect username or password

main-page-title = { -kt-app-name } - { $userId }
main-page-recent-tab-title = Recent
main-page-people-tab-title = People
main-page-rooms-tab-title = Rooms

room-list-view-room-item-title = { $name } ({ $roomId })
room-list-view-room-item-fav-action = Set as favourite
room-list-view-room-item-fav-action-notification = Set { $name } as favourite

send-message-box-input-placeholder = Type your message here...

## State events
## Common parameters:
## gender = gender of the sender (male/female/neutral)
## stateKeyUser = name of the state key user
## stateKeyUserGender = gender of the state key user
member-state-joined-room = joined the room.
member-state-changed-name-and-avatar = changed { $gender ->
        [male] his
        [female] her
       *[neutral] their
    } name and avatar.
member-state-changed-name = changed { $gender ->
        [male] his
        [female] her
       *[neutral] their
    } name.
member-state-changed-avatar = changed { $gender ->
        [male] his
        [female] her
       *[neutral] their
    } avatar.
member-state-invited = invited { $stateKeyUser } to the room.
member-state-left = left the room.
member-state-kicked = kicked { $stateKeyUser }.
member-state-banned = banned { $stateKeyUser }.
member-state-unbanned = unbanned { $stateKeyUser }.

state-room-created = created the room.
state-room-name-changed = changed the name of the room to { $newName }.
state-room-topic-changed = changed the topic of the room to { $newTopic }.
state-room-avatar-changed = changed the avatar of the room.
state-room-pinned-events-changed = changed the pinned events of the room.
state-room-alias-changed = changed the aliases of the room.
state-room-join-rules-changed = changed join rules of the room.
state-room-power-levels-changed = changed power levels of the room.
state-room-encryption-activated = enabled encryption for this room.

event-message-image-sent = sent an image "{ $body }".
event-message-file-sent = sent a file "{ $body }".
event-message-video-sent = sent a video "{ $body }".
event-message-audio-sent = sent an audio "{ $body }".
event-message-audio-play-audio = Play audio
