### This file is part of kazv.
### SPDX-FileCopyrightText: 2022 xploreinfinity
### SPDX-License-Identifier: AGPL-3.0-or-later

app-title = { -kt-app-name }

global-drawer-title = { -kt-app-name }
global-drawer-action-switch-account = खाता  बदले
global-drawer-action-hard-logout = लॉग आउट
global-drawer-action-save-session = इस सेशन को सेव करें

empty-room-page-title = कोई कमरे नहीं चुने गए है
empty-room-page-description = अभी कोई कमरा नहीं चुना गया है।

login-page-title = लॉग इन करें
login-page-userid-prompt = यूज़र आईडी:
login-page-userid-input-placeholder = उदाहरण: @foo:example.org
login-page-password-prompt = पासवर्ड:
login-page-login-button = लॉग इन करें
login-page-close-button = बंद करे
login-page-existing-sessions-prompt = मौजूदा सेशन में से किसी एक को चुनें:
login-page-alternative-password-login-prompt = या यूजर आईडी और पासवर्ड के साथ एक नया सेशन शुरू करें:
login-page-restore-session-button = सेशन पुनर्स्थापित करें
login-page-request-failed-prompt = लॉगिन विफल। एरर कोड: { $errorCode }। एरर संदेश: { $errorMsg }।
login-page-discover-failed-prompt = यह यूज़र  जिस सर्वर पर है उसका पता लगाने में असमर्थ, या सर्वर अनुपलब्ध है। एरर कोड: { $errorCode }। एरर संदेश: { $errorMsg }।

## main-page-title = { -kt-app-name } - { $userId }
main-page-recent-tab-title = रीसेंट
main-page-people-tab-title = लोग
main-page-rooms-tab-title = कमरे

## room-list-view-room-item-title = { $name } ({ $roomId })
## room-list-view-room-item-fav-action = Set as favourite
## room-list-view-room-item-fav-action-notification = Set { $name } as favourite

send-message-box-input-placeholder = अपना संदेश यहां लिखें...

## State events
## Common parameters:
## gender = gender of the sender (male/female/neutral)
## stateKeyUser = name of the state key user
## stateKeyUserGender = gender of the state key user
member-state-joined-room = ने कमरे में प्रवेश किया
member-state-changed-name-and-avatar = ने अपना नाम और अवतार बदल दिया है।
member-state-changed-name = ने अपना नाम बदल दिया है।
member-state-changed-avatar = ने अपना अवतार बदल दिया है।
member-state-invited = ने  { $stateKeyUser } को इस कमरे में आमंत्रित किया।
member-state-left = कमरे से चला गया।
member-state-kicked = ने { $stateKeyUser } को इस कमरे से बाहर निकाल दिया है ।
member-state-banned = ने { $stateKeyUser } पर प्रतिबंध लगा दिया है।
member-state-unbanned = ने { $stateKeyUser } पर  से प्रतिबंध हटा दिया है।

state-room-created = ने यह कमरा  बनाया।
state-room-name-changed = ने इस कमरे का नाम बदलकर { $newName } कर दिया है ।
state-room-topic-changed = द्वारा इस कमरे का विषय बदल कर  { $newTopic } कर दिया है।
state-room-avatar-changed = द्वारा इस कमरे का अवतार बदल गया है।
state-room-pinned-events-changed = द्वारा इस कमरे के पिन किए गए ईवेंट बदल गए हैं।
state-room-alias-changed = द्वारा इस कमरे के उपनाम बदल गए हैं।
state-room-join-rules-changed = द्वारा इस कमरे में शामिल होने के नियम बदल गए हैं।
state-room-power-levels-changed = ने इस कमरे का पावर लेवल बदल दिया है।
state-room-encryption-activated = ने इस कमरे के लिए एन्क्रिप्शन शुरू किया।

event-message-image-sent = ने भेजी तस्वीर "{ $body }"।
event-message-file-sent = ने भेजी फाइल "{ $body }"।
event-message-video-sent = ने भेजा वीडियो "{ $body }"।
event-message-audio-sent = ने भेजा ऑडियो "{ $body }"।
event-message-audio-play-audio = ऑडियो चलाएं
