/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <lager/setter.hpp>

#include "matrix-room.hpp"
#include "matrix-room-timeline.hpp"
#include "matrix-room-member.hpp"

#include "helper.hpp"

MatrixRoom::MatrixRoom(Kazv::Room room, QObject *parent)
    : QObject(parent)
    , m_room(room)
    , m_memberNames(m_room.members())
    , LAGER_QT(roomId)(m_room.roomId().xform(strToQt))
    , LAGER_QT(name)(m_room.name().xform(strToQt))
    , LAGER_QT(avatarMxcUri)(m_room.avatarMxcUri().xform(strToQt))
    , LAGER_QT(localDraft)(lager::with_setter(
                               m_room.localDraft().xform(strToQt).make(),
                               [this](QString localDraft) {
                                   m_room.setLocalDraft(localDraft.toStdString());
                               },
                               lager::automatic_tag{}))
    , LAGER_QT(memberNames)(m_memberNames.xform(
                                zug::map(
                                    [](auto container) {
                                        return zug::into(QStringList{}, strToQt, std::move(container));
                                    })))
{
}

MatrixRoom::~MatrixRoom() = default;

MatrixRoomTimeline *MatrixRoom::timeline() const
{
    return new MatrixRoomTimeline(m_room);
}

void MatrixRoom::sendTextMessage(QString text) const
{
    m_room.sendTextMessage(text.toStdString());
}

MatrixRoomMember *MatrixRoom::memberAt(int index) const
{
    return new MatrixRoomMember(m_room.memberEventByCursor(
                                    m_memberNames[index][lager::lenses::or_default]));
}

MatrixRoomMember *MatrixRoom::member(QString userId) const
{
    return new MatrixRoomMember(m_room.memberEventFor(userId.toStdString()));
}
