/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "kazv-path-config.hpp"
#include <QCoreApplication>
#include <QStandardPaths>

QString appDir()
{
    return QCoreApplication::applicationDirPath();
}

QString kazvUserDataDir()
{
    static QString dir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    return dir;
}
