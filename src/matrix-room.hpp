/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QObject>
#include <QQmlEngine>

#include <lager/extra/qt.hpp>

#include <client/room/room.hpp>

class MatrixRoomTimeline;
class MatrixRoomMember;

class MatrixRoom : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    Kazv::Room m_room;
    lager::reader<immer::flex_vector<std::string>> m_memberNames;

public:
    explicit MatrixRoom(Kazv::Room room, QObject *parent = 0);
    ~MatrixRoom() override;

    LAGER_QT_READER(QString, roomId);
    LAGER_QT_READER(QString, name);
    LAGER_QT_READER(QString, avatarMxcUri);
    LAGER_QT_CURSOR(QString, localDraft);

    LAGER_QT_READER(QStringList, memberNames);

    Q_INVOKABLE MatrixRoomMember *memberAt(int index) const;

    Q_INVOKABLE MatrixRoomMember *member(QString userId) const;

    Q_INVOKABLE MatrixRoomTimeline *timeline() const;

    Q_INVOKABLE void sendTextMessage(QString text) const;
};
