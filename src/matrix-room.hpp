/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QUrl>
#include <QTimer>

#include <lager/extra/qt.hpp>

#include <client/room/room.hpp>
Q_MOC_INCLUDE("matrix-room-timeline.hpp")
Q_MOC_INCLUDE("matrix-room-pinned-events-timeline.hpp")
Q_MOC_INCLUDE("matrix-sticker-pack-list.hpp")

class MatrixRoomTimeline;
class MatrixRoomPinnedEventsTimeline;
class MatrixRoomMember;
class MatrixPromise;
class MatrixRoomMemberListModel;
class MatrixEvent;
class MatrixStickerPackList;

nlohmann::json makeTextMessageJson(const QString &text, const QString &relType, const QString &relatedTo, Kazv::Event replyToEvent);

class MatrixRoom : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    Kazv::Room m_room;
    lager::reader<Kazv::Event> m_userGivenNicknameEvent;
    lager::reader<std::string> m_selfUserId;
    lager::reader<immer::flex_vector<std::string>> m_memberNames;
    lager::reader<Kazv::PowerLevelsDesc> m_powerLevels;

public:
    enum Membership {
        Invite = Kazv::RoomMembership::Invite,
        Join = Kazv::RoomMembership::Join,
        Leave = Kazv::RoomMembership::Leave,
    };
    Q_ENUM(Membership);

    explicit MatrixRoom(Kazv::Room room, lager::reader<std::string> selfUserId, lager::reader<Kazv::Event> userGivenNicknameEvent = lager::make_constant(Kazv::Event()), QObject *parent = 0);
    ~MatrixRoom() override;

    LAGER_QT_READER(QString, roomId);
    LAGER_QT_READER(QString, name);
    LAGER_QT_READER(QString, topic);
    LAGER_QT_READER(QStringList, heroNames);
    LAGER_QT_READER(QVariant, heroEvents);
    LAGER_QT_READER(QString, avatarMxcUri);
    LAGER_QT_READER(QString, roomOrHeroAvatarMxcUri);
    LAGER_QT_READER(QString, localDraft);
    LAGER_QT_READER(bool, encrypted);
    LAGER_QT_READER(bool, tombstoned);

    LAGER_QT_READER(QStringList, memberNames);
    LAGER_QT_READER(QStringList, tagIds);
    LAGER_QT_READER(Membership, membership);
    LAGER_QT_READER(int, unreadNotificationCount);
    LAGER_QT_READER(QString, localReadMarker);

    Q_INVOKABLE MatrixRoomMember *memberAt(int index) const;

    Q_INVOKABLE MatrixRoomMember *member(QString userId) const;

    Q_INVOKABLE MatrixEvent *state(const QString &type, const QString &stateKey) const;

    Q_INVOKABLE MatrixRoomTimeline *timeline() const;

    Q_INVOKABLE MatrixRoomPinnedEventsTimeline *pinnedEventsTimeline() const;

    Q_INVOKABLE MatrixPromise *pinEvents(const QStringList &eventIds) const;

    Q_INVOKABLE MatrixPromise *unpinEvents(const QStringList &eventIds) const;

    Q_INVOKABLE MatrixEvent *messageById(QString eventId) const;

    Q_INVOKABLE MatrixEvent *localEchoById(QString txnId) const;

    Q_INVOKABLE void sendMessage(const QJsonObject &eventJson, const QString &relType, const QString &relatedTo) const;

    Q_INVOKABLE void sendTextMessage(QString text, const QString &relType, QString relatedTo) const;

    Q_INVOKABLE void sendMediaFileMessage(QString fileName, QString mimeType,
        qint64 fileSize, QString mxcUri,
        const QString &relType, const QString &relatedTo) const;

    Q_INVOKABLE void sendEncryptedFileMessage(const QString &fileName, const QString& mimeType,
        const qint64 fileSize, const QString& mxcUri,
        const QString &key, const QString &iv, const QByteArray &hash,
        const QString &relType, const QString &relatedTo) const;

    Q_INVOKABLE void sendReaction(QString text, QString relatedTo) const;

    Q_INVOKABLE void resendMessage(QString txnId) const;

    Q_INVOKABLE MatrixPromise *sendStateEvent(const QJsonObject &eventJson) const;

    Q_INVOKABLE MatrixPromise *setSelfName(const QString &name) const;

    Q_INVOKABLE MatrixPromise *redactEvent(QString eventId, QString reason) const;

    Q_INVOKABLE MatrixPromise *removeLocalEcho(QString txnId) const;

    Q_INVOKABLE MatrixRoomMemberListModel *typingUsers() const;

    Q_INVOKABLE void setTyping(bool typing);

    Q_INVOKABLE void setLocalDraft(QString localDraft);

    Q_INVOKABLE void updateLocalDraftNow();

    Q_INVOKABLE MatrixPromise *addOrSetTag(QString tagId) const;

    Q_INVOKABLE MatrixPromise *removeTag(QString tagId) const;

    Q_INVOKABLE MatrixPromise *paginateBackFrom(QString eventId) const;

    Q_INVOKABLE MatrixPromise *leaveRoom() const;

    Q_INVOKABLE MatrixPromise *forgetRoom() const;

    Q_INVOKABLE MatrixRoomMemberListModel *members() const;

    Q_INVOKABLE MatrixRoomMemberListModel *bannedMembers() const;

    Q_INVOKABLE qint64 userPowerLevel(const QString &userId) const;

    Q_INVOKABLE MatrixPromise *setUserPowerLevel(const QString &userId, qint64 powerLevel) const;

    Q_INVOKABLE MatrixPromise *unsetUserPowerLevel(const QString &userId) const;

    Q_INVOKABLE MatrixPromise *getStateEvent(const QString &type, const QString &stateKey) const;

    Q_INVOKABLE MatrixPromise *ensureStateEvent(const QString &type, const QString &stateKey) const;

    Q_INVOKABLE MatrixPromise *refreshState() const;

    Q_INVOKABLE MatrixPromise *kickUser(const QString &userId, const QString &reason) const;

    Q_INVOKABLE MatrixPromise *banUser(const QString &userId, const QString &reason = QStringLiteral()) const;

    Q_INVOKABLE MatrixPromise *unbanUser(const QString &userId) const;

    Q_INVOKABLE MatrixPromise *inviteUser(const QString &userId) const;

    Q_INVOKABLE MatrixPromise *postReadReceipt(const QString &eventId) const;

    Q_INVOKABLE MatrixPromise *setName(const QString &newName) const;

    Q_INVOKABLE MatrixPromise *setTopic(const QString &newTopic) const;

    /**
     * Get the sticker pack list defined in this room.
     *
     * @return A list of sticker packs defined in this room.
     */
    Q_INVOKABLE MatrixStickerPackList *stickerPackList() const;

Q_SIGNALS:
    void powerLevelsChanged();

protected:
    nlohmann::json makeMediaFileMessageJson(QString fileName, QString mimeType,
        qint64 fileSize, QString mxcUri,
        const QString &relType, const QString &relatedTo) const;
    nlohmann::json makeEncryptedFileMessageJson(const QString &fileName, const QString &mimeType,
        const qint64 fileSize, const QString &mxcUri,
        const QString &key, const QString &iv, const QByteArray &hash,
        const QString &relType, const QString &relatedTo) const;

private Q_SLOTS:
    void setTypingImpl();

private:
    std::function<void()> m_setTypingThrottled;
    std::function<void()> m_updateLocalDraftDebounced;
    std::optional<QString> m_internalLocalDraft;
};
