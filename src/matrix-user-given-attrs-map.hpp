/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <QObject>
#include <QQmlEngine>
#include <QJsonObject>
#include <lager/reader.hpp>
#include <lager/extra/qt.hpp>
#include <event.hpp>
#include <client.hpp>
Q_MOC_INCLUDE("matrix-promise.hpp")
Q_MOC_INCLUDE(<QJsonValue>)

class QJsonValue;
class MatrixPromise;

class MatrixUserGivenAttrsMap : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::Event> m_event;
    std::function<Kazv::Client::PromiseT(Kazv::Event)> m_setter;

public:
    explicit MatrixUserGivenAttrsMap(
        lager::reader<Kazv::Event> event,
        std::function<Kazv::Client::PromiseT(Kazv::json)> setter,
        QObject *parent = 0
    );
    ~MatrixUserGivenAttrsMap() override;

    LAGER_QT_READER(QJsonObject, map);

    /**
     * Set the user given attribute of the matrix object with @c id
     * and return the modified json.
     *
     * @param id The id of the matrix object.
     * @param data The value to set for the matrix object. Use null to unset.
     * @return The modified event content.
     */
    Kazv::json set(const QString &id, const QJsonValue &data) const;

    Q_INVOKABLE MatrixPromise *setAndUpload(const QString &id, const QJsonValue &data);
};
