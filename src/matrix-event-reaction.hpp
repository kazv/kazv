/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <lager/extra/qt.hpp>
#include <room/room.hpp>
#include "matrix-event-list.hpp"

class MatrixEventReaction : public MatrixEventList
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

public:
    explicit MatrixEventReaction(lager::reader<Kazv::EventList> events, lager::reader<QString> key, QObject *parent = nullptr);

    ~MatrixEventReaction() override;

    LAGER_QT_READER(QString, key);
};
