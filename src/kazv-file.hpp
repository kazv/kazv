/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <aes-256-ctr.hpp>

#include <QObject>
#include <QFile>
#include <QString>
#include <QByteArray>
#include <QSaveFile>

#include <memory>
#include <optional>

struct KazvFilePrivate;

class KazvFile : public QFile
{
    Q_OBJECT

    std::unique_ptr<KazvFilePrivate> m_d;

public:
    KazvFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes, QObject *parent);
    KazvFile(std::optional<Kazv::AES256CTRDesc> aes, QObject *parent);
    KazvFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes);
    KazvFile(std::optional<Kazv::AES256CTRDesc> aes);
    ~KazvFile();

    QByteArray hash() const;

protected:
    qint64 readData(char *data, qint64 len) override;
};

struct KazvSaveFilePrivate;

class KazvSaveFile : public QSaveFile {
    Q_OBJECT

    std::unique_ptr<KazvSaveFilePrivate> m_d;

public:
    KazvSaveFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes, QObject *parent);
    KazvSaveFile(std::optional<Kazv::AES256CTRDesc> aes, QObject *parent);
    KazvSaveFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes);
    ~KazvSaveFile();

    QByteArray hash() const;

protected:
    qint64 writeData(const char *data, qint64 len) override;
};
