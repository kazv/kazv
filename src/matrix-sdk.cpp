/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <fstream>
#include <filesystem>
#include <chrono>

#include <QMutex>
#include <QMutexLocker>
#include <QtConcurrent>
#include <QThreadPool>

#include <KConfig>
#include <KConfigGroup>

#include <eventemitter/lagerstoreeventemitter.hpp>
#include <client/sdk.hpp>
#include <client/notification-handler.hpp>
#include <crypto/base64.hpp>

#include <zug/util.hpp>
#include <lager/event_loop/qt.hpp>

#include "matrix-sdk.hpp"
#include "matrix-room-list.hpp"
#include "matrix-promise.hpp"
#include "matrix-event.hpp"
#include "helper.hpp"
#include "kazv-path-config.hpp"
#include "kazv-version.hpp"
#include "qt-json.hpp"
#include "qt-rand-adapter.hpp"
#include "qt-promise-handler.hpp"
#include "qt-job-handler.hpp"
#include "device-mgmt/matrix-device-list.hpp"
#include "matrix-sticker-pack-list.hpp"
#include "matrix-sticker-pack-list-p.hpp"
#include "matrix-user-given-attrs-map.hpp"
#include "kazv-log.hpp"
#include "matrix-utils.hpp"
#include "kazv-session-lock-guard.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

static const std::string clientName = "kazv";

// Sdk with qt event loop, identity transform and no enhancers
using SdkT =
    decltype(makeSdk(
              SdkModel{},
              detail::declref<JobInterface>(),
              detail::declref<EventInterface>(),
              QtPromiseHandler(detail::declref<QObject>()),
              zug::identity,
              withRandomGenerator(detail::declref<RandomInterface>())));

struct QtEventLoop
{
    QObject *m_obj;

    template<class Fn>
    void async(Fn &&) { throw std::runtime_error{"not implemented!"}; }

    template<class Fn>
    void post(Fn &&fn)
    {
        QMetaObject::invokeMethod(
            m_obj, std::forward<Fn>(fn), Qt::QueuedConnection
        );
    }

    void finish() {}
    void pause() { throw std::runtime_error{"not implemented!"}; }
    void resume() { throw std::runtime_error{"not implemented!"}; }
};

std::filesystem::path sessionDirForUserAndDeviceId(std::filesystem::path userDataDir, std::string userId, std::string deviceId)
{
    auto encodedUserId = encodeBase64(userId, Base64Opts::urlSafe);
    auto sessionDir = userDataDir / "sessions"
        / encodedUserId / deviceId;
    return sessionDir;
}

struct MatrixSdkPrivate
{
    MatrixSdkPrivate(MatrixSdk *q, bool testing, std::unique_ptr<KazvSessionLockGuard> lockGuard);
    MatrixSdkPrivate(MatrixSdk *q, bool testing, SdkModel model, std::unique_ptr<KazvSessionLockGuard> lockGuard);
    bool testing;
    std::string userDataDir;
    std::unique_ptr<KazvSessionLockGuard> lockGuard;
    RandomInterface randomGenerator;
    QThread *thread;
    QObject *obj;
    QtJobHandler *jobHandler;
    LagerStoreEventEmitter ee;
    LagerStoreEventEmitter::Watchable watchable;
    SdkT sdk;
    QTimer saveTimer;

    using SecondaryRootT = decltype(sdk.createSecondaryRoot(std::declval<lager::with_qt_event_loop>()));
    SecondaryRootT secondaryRoot;
    Client clientOnSecondaryRoot;
    NotificationHandler notificationHandler;
    void runIoContext() {
        thread->start();
    }
    void stopIoContext() {
        thread->quit();
    }

    void maybeSerialize()
    {
        if (!testing) {
            serializeClientToFile(clientOnSecondaryRoot);
        }
    }

    void serializeClientToFile(Client c);
};

// Cleaning up notes:
// 0. Callback functions may store the context for an indefinite time
// 1. The QThread event loop can be stopped
// 2. QtJobHandler::submit() should only happen in the primary event loop thread
// 3. QtJobHandler lives in the primary event loop thread
// 4. QtJobs live in the primary event loop thread
// 5. Job callbacks are called in the primary event loop thread
// 6. QtPromise::then() callbacks are called in the primary event loop thread
// 7. When the QThread event loop stops, no more callbacks will be executed (there is nothing to post to)
// 8. The QThread should stop before obj is deleted
class CleanupHelper : public QObject
{
    Q_OBJECT
public:
    explicit CleanupHelper(std::unique_ptr<MatrixSdkPrivate> d)
        : oldD(std::move(d))
    {
    }

    std::unique_ptr<MatrixSdkPrivate> oldD;

    void cleanup()
    {
        qCInfo(kazvLog) << "start to clean up everything";
        oldD->clientOnSecondaryRoot.stopSyncing()
            .then([obj=oldD->obj, thread=oldD->thread](auto &&) {
                qCDebug(kazvLog) << "stopped syncing";
                QMetaObject::invokeMethod(obj, [thread]() {
                    thread->quit();
                });
            });
        oldD->thread->wait();
        oldD->thread->deleteLater();
        // After the thread's event loop is finished, we can delete the root object
        oldD->obj->deleteLater();
        this->deleteLater();
        qCInfo(kazvLog) << "thread is done";
    }
};

void MatrixSdkPrivate::serializeClientToFile(Client c)
{
    using namespace Kazv::CursorOp;
    auto userId = +c.userId();
    auto deviceId = +c.deviceId();
    if (userId.empty() || deviceId.empty()) {
        qDebug() << "Not logged in, nothing to serialize";
        return;
    }

    using StdPath = std::filesystem::path;
    auto userDataDir = StdPath(this->userDataDir);
    auto sessionDir = sessionDirForUserAndDeviceId(userDataDir, userId, deviceId);

    auto storeFile = sessionDir / "store";
    auto storeFileNew = sessionDir / "store.new";

    auto metadataFile = sessionDir / "metadata";

    qDebug() << "storeFile=" << QString::fromStdString(storeFile.string());

    std::error_code err;
    if ((! std::filesystem::create_directories(sessionDir, err))
        && err) {
        qDebug() << "Unable to create sessionDir";
        return;
    }

    if (!lockGuard) {
        try {
            lockGuard = std::make_unique<KazvSessionLockGuard>(sessionDir);
        } catch (const std::runtime_error &e) {
            qCWarning(kazvLog) << "Error locking session: " << e.what();
            return;
        }
    }

    try {
        auto storeStream = std::ofstream(storeFileNew);
        if (! storeStream) {
            qCWarning(kazvLog) << "Unable to open storeFile";
            return;
        }
        using OAr = boost::archive::text_oarchive;
        auto archive = OAr{storeStream};
        c.serializeTo(archive);
    } catch (const std::exception &e) {
        qCWarning(kazvLog) << "Cannot write to store file: " << e.what();
        return;
    }

    err.clear();
    std::filesystem::rename(storeFileNew, storeFile, err);
    if (err) {
        qCWarning(kazvLog) << "Cannot move storeFile into place: " << QString::fromStdString(err.message());
        return;
    }
    qDebug() << "Serialization done";


    // store metadata
    {
        KConfig metadata(QString::fromStdString(metadataFile.string()));
        KConfigGroup mdGroup(&metadata, u"Metadata"_s);
        mdGroup.writeEntry("kazvVersion", QString::fromStdString(kazvVersionString()));
        mdGroup.writeEntry("archiveFormat", "text");
    }
}

MatrixSdkPrivate::MatrixSdkPrivate(MatrixSdk *q, bool testing, std::unique_ptr<KazvSessionLockGuard> lockGuard)
    : testing(testing)
    , userDataDir{kazvUserDataDir().toStdString()}
    , lockGuard(std::move(lockGuard))
    , randomGenerator(QtRandAdapter{})
    , thread(new QThread())
    , obj(new QObject())
    , jobHandler(new QtJobHandler(obj))
    , ee{QtEventLoop{obj}}
    , watchable(ee.watchable())
    , sdk(makeDefaultSdkWithCryptoRandom(
        randomGenerator.generateRange<std::string>(makeDefaultSdkWithCryptoRandomSize()),
        static_cast<JobInterface &>(*jobHandler),
        static_cast<EventInterface &>(ee),
        QtPromiseHandler(*obj),
        zug::identity,
        withRandomGenerator(randomGenerator)))
    , secondaryRoot(sdk.createSecondaryRoot(QtEventLoop{q}))
    , clientOnSecondaryRoot(sdk.clientFromSecondaryRoot(secondaryRoot))
    , notificationHandler(clientOnSecondaryRoot.notificationHandler())
{
    obj->moveToThread(thread);
}

MatrixSdkPrivate::MatrixSdkPrivate(MatrixSdk *q, bool testing, SdkModel model, std::unique_ptr<KazvSessionLockGuard> lockGuard)
    : testing(testing)
    , userDataDir{kazvUserDataDir().toStdString()}
    , lockGuard(std::move(lockGuard))
    , randomGenerator(QtRandAdapter{})
    , thread(new QThread())
    , obj(new QObject())
    , jobHandler(new QtJobHandler(obj))
    , ee{QtEventLoop{obj}}
    , watchable(ee.watchable())
    , sdk(makeSdk(
        model,
        static_cast<JobInterface &>(*jobHandler),
        static_cast<EventInterface &>(ee),
        QtPromiseHandler(*obj),
        zug::identity,
        withRandomGenerator(randomGenerator)))
    , secondaryRoot(sdk.createSecondaryRoot(QtEventLoop{q}, std::move(model)))
    , clientOnSecondaryRoot(sdk.clientFromSecondaryRoot(secondaryRoot))
    , notificationHandler(clientOnSecondaryRoot.notificationHandler())
{
    obj->moveToThread(thread);
}

MatrixSdk::MatrixSdk(std::unique_ptr<MatrixSdkPrivate> d, QObject *parent)
    : QObject(parent)
    , m_userDataDir(kazvUserDataDir())
    , m_d(std::move(d))
{
    init();
    connect(this, &MatrixSdk::trigger,
            this, [](KazvEvent e) {
                      qDebug() << "receiving trigger:";
                      if (std::holds_alternative<LoginSuccessful>(e)) {
                          qDebug() << "Login successful";
                      }
                  });
}

void MatrixSdk::init()
{
    LAGER_QT(serverUrl) = m_d->clientOnSecondaryRoot.serverUrl().xform(strToQt); Q_EMIT serverUrlChanged(serverUrl());
    LAGER_QT(userId) = m_d->clientOnSecondaryRoot.userId().xform(strToQt); Q_EMIT userIdChanged(userId());
    LAGER_QT(token) = m_d->clientOnSecondaryRoot.token().xform(strToQt); Q_EMIT tokenChanged(token());
    LAGER_QT(deviceId) = m_d->clientOnSecondaryRoot.deviceId().xform(strToQt); Q_EMIT deviceIdChanged(deviceId());

    m_d->watchable.afterAll(
        [this](KazvEvent e) {
            Q_EMIT this->trigger(e);
        });

    m_d->watchable.after<LoginSuccessful>(
        [this](LoginSuccessful e) {
            Q_EMIT this->loginSuccessful(e);
        });

    m_d->watchable.after<LoginFailed>(
        [this](LoginFailed e) {
            Q_EMIT this->loginFailed(
                QString::fromStdString(e.errorCode),
                QString::fromStdString(e.error)
            );
        });

    m_d->watchable.after<ReceivingRoomTimelineEvent>(
        [this](ReceivingRoomTimelineEvent e) {
            Q_EMIT this->receivedMessage(
                QString::fromStdString(e.roomId),
                QString::fromStdString(e.event.id())
            );
        });

    connect(&m_d->saveTimer, &QTimer::timeout, &m_d->saveTimer, [m_d=m_d.get()]() {
        m_d->maybeSerialize();
    });
    const int saveIntervalMs = 1000 * 60 * 5;
    m_d->saveTimer.start(std::chrono::milliseconds{saveIntervalMs});
}

MatrixSdk::MatrixSdk(QObject *parent)
    : MatrixSdk(std::make_unique<MatrixSdkPrivate>(this, /* testing = */ false, std::unique_ptr<KazvSessionLockGuard>()), parent)
{
}

MatrixSdk::MatrixSdk(SdkModel model, bool testing, QObject *parent)
    : MatrixSdk(std::make_unique<MatrixSdkPrivate>(this, testing, std::move(model), std::unique_ptr<KazvSessionLockGuard>()), parent)
{
}

static void cleanupDPointer(std::unique_ptr<MatrixSdkPrivate> oldD)
{
    oldD->saveTimer.disconnect();
    oldD->saveTimer.stop();

    auto helper = new CleanupHelper(std::move(oldD));
    helper->cleanup();
}

MatrixSdk::~MatrixSdk()
{
    if (m_d) {
        serializeToFile();
        cleanupDPointer(std::move(m_d));
    }
}

QString MatrixSdk::mxcUriToHttp(QString mxcUri) const
{
    return QString::fromStdString(m_d->clientOnSecondaryRoot.mxcUriToHttp(mxcUri.toStdString()));
}

MatrixDeviceList *MatrixSdk::devicesOfUser(QString userId) const
{
    return new MatrixDeviceList(m_d->clientOnSecondaryRoot.devicesOfUser(userId.toStdString()));
}

std::string MatrixSdk::validateHomeserverUrl(const QString &url)
{
    if (url.isEmpty()) {
        return std::string();
    }

    auto u = QUrl::fromUserInput(url);
    if (!u.isValid()) {
        return std::string();
    }

    if (u.scheme() == u"http"_s) {
        qCInfo(kazvLog) << "url" << u << "is http. Force switching to https.";
        u.setScheme(u"https"_s);
    } else if (u.scheme() != u"https"_s) {
        qCWarning(kazvLog) << "url" << u << "is not http/https.";
        return std::string();
    }

    return u.toString().toStdString();
}

void MatrixSdk::login(const QString &userId, const QString &password, const QString &homeserverUrl)
{
    auto loginFunc = [userId, password](const Client &client, const std::string &serverUrl) {
        client.passwordLogin(
            serverUrl,
            userId.toStdString(),
            password.toStdString(),
            clientName
        );
    };

    auto validated = validateHomeserverUrl(homeserverUrl);
    if (!validated.empty()) {
        loginFunc(m_d->clientOnSecondaryRoot, validated);
    } else {
        m_d->clientOnSecondaryRoot
            .autoDiscover(userId.toStdString())
            .then([
                this,
                client=m_d->clientOnSecondaryRoot.toEventLoop(),
                userId,
                password,
                loginFunc
            ](auto res) {
                if (!res.success()) {
                    // FIXME use real error codes and msgs when available in libkazv
                    Q_EMIT this->discoverFailed(u""_s, u""_s);
                    return res;
                }

                auto serverUrl = res.dataStr("homeserverUrl");

                loginFunc(client, serverUrl);

                return res;
            });
    }

    m_d->runIoContext();
}
void MatrixSdk::logout()
{
    m_d->clientOnSecondaryRoot.logout()
        .then([&] (EffectStatus stat) {
            if (stat.success()) {
                m_d->stopIoContext();
                Q_EMIT this->logoutSuccessful();
            } else {
                Q_EMIT this->logoutFailed(QString::fromStdString(stat.dataStr("errorCode")), QString::fromStdString(stat.dataStr("error")));
            }
        });
}

MatrixRoomList *MatrixSdk::roomList() const
{
    return new MatrixRoomList(m_d->clientOnSecondaryRoot);
}

void MatrixSdk::emplace(std::optional<SdkModel> model, std::unique_ptr<KazvSessionLockGuard> lockGuard)
{
    auto testing = m_d->testing;
    auto userDataDir = m_d->userDataDir;

    if (m_d) {
        cleanupDPointer(std::move(m_d));
    }

    m_d = (model.has_value()
           ? std::make_unique<MatrixSdkPrivate>(this, testing, std::move(model.value()), std::move(lockGuard))
           : std::make_unique<MatrixSdkPrivate>(this, testing, std::move(lockGuard)));
    m_d->userDataDir = userDataDir;

    // Re-initialize lager-qt cursors and watchable connections
    init();

    m_d->runIoContext();

    m_d->clientOnSecondaryRoot.startSyncing();

    Q_EMIT sessionChanged();
}

QStringList MatrixSdk::allSessions() const
{
    using StdPath = std::filesystem::path;
    auto userDataDir = StdPath(m_d->userDataDir);
    auto allSessionsDir = userDataDir / "sessions";

    QStringList sessionNames;

    try {
        for (const auto &p : std::filesystem::directory_iterator(allSessionsDir)) {
            if (p.is_directory()) {
                auto maybeEncodedUserId = p.path().filename().string();
                auto userId = decodeBase64(maybeEncodedUserId, Base64Opts::urlSafe);
                if (userId.empty() || userId[0] != '@') {
                    continue;
                }
                for (const auto &q : std::filesystem::directory_iterator(p.path())) {
                    auto path = q.path();
                    auto deviceId = path.filename().string();
                    std::error_code err;
                    if (std::filesystem::exists(path / "store", err)) {
                        sessionNames.append(QString::fromStdString(userId + "/" + deviceId));
                    }
                }
            }
        }
    } catch (const std::filesystem::filesystem_error &) {
        qDebug() << "sessionDir not available, ignoring";
    }

    return sessionNames;
}

void MatrixSdk::serializeToFile() const
{
    m_d->maybeSerialize();
}

auto MatrixSdk::loadSession(QString sessionName) -> LoadSessionResult
{
    using StdPath = std::filesystem::path;
    std::unique_ptr<KazvSessionLockGuard> lockGuard;
    auto loadFromSession = [this, sessionName, &lockGuard](StdPath sessionDir) {
        auto storeFile = sessionDir / "store";

        auto metadataFile = sessionDir / "metadata";

        if (! std::filesystem::exists(storeFile)) {
            qDebug() << "storeFile does not exist, skip loading session " << sessionName;
            return SessionNotFound;
        }

        if (std::filesystem::exists(metadataFile)) {
            KConfig metadata(QString::fromStdString(metadataFile.string()));
            KConfigGroup mdGroup(&metadata, u"Metadata"_s);
            auto format = mdGroup.readEntry(u"archiveFormat"_s);
            if (format != QStringLiteral("text")) {
                qDebug() << "Unknown archive format:" << format;
                return SessionFormatUnknown;
            }
            auto version = mdGroup.readEntry(u"kazvVersion"_s);
            auto curVersion = kazvVersionString();
            if (version != QString::fromStdString(curVersion)) {
                qDebug() << "A different version from the current one, making a backup";
                std::error_code err;
                auto now = std::chrono::system_clock::now();
                auto backupName =
                    std::to_string(std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count());
                auto backupDir = sessionDir / "backup" / backupName;
                if (! std::filesystem::create_directories(backupDir, err)
                    && err) {
                    qDebug() << "Cannot create backup directory";
                    return SessionCannotBackup;
                }
                std::filesystem::copy_file(storeFile, backupDir / "store");
                std::filesystem::copy_file(metadataFile, backupDir / "metadata");
            }
        }

        SdkModel model;
        try {
            auto storeStream = std::ifstream(storeFile);
            if (! storeStream) {
                qDebug() << "Unable to open storeFile";
                return SessionCannotOpenFile;
            }
            using IAr = boost::archive::text_iarchive;
            auto archive = IAr{storeStream};
            archive >> model;
            qDebug() << "Finished loading session";
        } catch (const std::exception &e) {
            qDebug() << "Error when loading session:" << QString::fromStdString(e.what());
            return SessionDeserializeFailed;
        }
        emplace(std::move(model), std::move(lockGuard));
        return SessionLoadSuccess;
    };

    qDebug() << "in loadSession(), sessionName=" << sessionName;
    auto userDataDir = StdPath(m_d->userDataDir);
    auto parts = sessionName.split(u'/');
    if (parts.size() == 2) {
        auto userId = parts[0].toStdString();
        auto deviceId = parts[1].toStdString();
        auto sessionDir = sessionDirForUserAndDeviceId(userDataDir, userId, deviceId);
        try {
            lockGuard = std::make_unique<KazvSessionLockGuard>(sessionDir);
        } catch (const std::runtime_error &e) {
            qCWarning(kazvLog) << "Error locking session: " << e.what();
            return SessionLockFailed;
        }
        return loadFromSession(sessionDir);
    }
    qDebug(kazvLog) << "no session found for" << sessionName;
    return SessionNotFound;
}

bool MatrixSdk::deleteSession(QString sessionName) {
    using StdPath = std::filesystem::path;
    qDebug() << "in deleteSession(), sessionName=" << sessionName;
    auto userDataDir = StdPath(kazvUserDataDir().toStdString());
    auto parts = sessionName.split(u'/');
    if (parts.size() == 2) {
        auto userId = parts[0].toStdString();
        auto deviceId = parts[1].toStdString();
        auto sessionDir = sessionDirForUserAndDeviceId(userDataDir, userId, deviceId);
        if (std::filesystem::exists(sessionDir)) {
            qCDebug(kazvLog) << "new path works";
            return std::filesystem::remove_all(sessionDir);
        }
        qCDebug(kazvLog) << "trying legacy path";
        auto legacySessionDir = userDataDir / "sessions" / userId / deviceId;
        if (std::filesystem::exists(legacySessionDir)) {
            qCDebug(kazvLog) << "legacy path works";
            return std::filesystem::remove_all(legacySessionDir);
        }
    }
    qDebug(kazvLog) << "no session found for" << sessionName;
    return false;
}

bool MatrixSdk::startNewSession()
{
    emplace(std::nullopt, std::unique_ptr<KazvSessionLockGuard>());
    return true;
}

static std::optional<std::string> optMaybe(QString s)
{
    if (s.isEmpty()) {
        return std::nullopt;
    } else {
        return s.toStdString();
    }
}

MatrixPromise *MatrixSdk::createRoom(
    bool isPrivate,
    const QString &name,
    const QString &alias,
    const QStringList &invite,
    bool isDirect,
    bool allowFederate,
    const QString &topic,
    const QJsonValue &powerLevelContentOverride,
    CreateRoomPreset preset,
    bool encrypted
)
{
    immer::array<Event> initialState;

    if (encrypted) {
        initialState = {Event{json{
            {"type", "m.room.encryption"},
            {"state_key", ""},
            {"content", {
                {"algorithm", "m.megolm.v1.aes-sha2"},
            }},
        }}};
    }

    return new MatrixPromise(m_d->clientOnSecondaryRoot.createRoom(
        isPrivate ? Kazv::RoomVisibility::Private : Kazv::RoomVisibility::Public,
        optMaybe(name),
        optMaybe(alias),
        qStringListToStdF(invite),
        isDirect,
        allowFederate,
        optMaybe(topic),
        nlohmann::json(powerLevelContentOverride),
        static_cast<Kazv::CreateRoomPreset>(preset),
        initialState
    ));
}

MatrixPromise *MatrixSdk::joinRoom(const QString &idOrAlias, const QStringList &servers)
{
    return new MatrixPromise(m_d->clientOnSecondaryRoot.joinRoom(
        idOrAlias.toStdString(),
        qStringListToStdF(servers)
    ));
}

MatrixPromise *MatrixSdk::setDeviceTrustLevel(QString userId, QString deviceId, QString trustLevel)
{
    return new MatrixPromise(
        m_d->clientOnSecondaryRoot.setDeviceTrustLevel(
            userId.toStdString(),
            deviceId.toStdString(),
            qStringToTrustLevelFunc(trustLevel)
        )
    );
}

MatrixPromise *MatrixSdk::getSelfProfile()
{
    return new MatrixPromise(
        m_d->clientOnSecondaryRoot.getProfile(userId().toStdString())
    );
}

MatrixPromise *MatrixSdk::setDisplayName(QString displayName)
{
    return new MatrixPromise(
        m_d->clientOnSecondaryRoot.setDisplayName(
            displayName.isEmpty() ? std::nullopt : std::optional<std::string>(displayName.toStdString())
        )
    );
}

MatrixPromise *MatrixSdk::setAvatarUrl(QString avatarUrl)
{
    return new MatrixPromise(
        m_d->clientOnSecondaryRoot.setAvatarUrl(
            avatarUrl.isEmpty() ? std::nullopt : std::optional<std::string>(avatarUrl.toStdString())
        )
    );
}

void MatrixSdk::startThread()
{
    m_d->runIoContext();
}

RandomInterface &MatrixSdk::randomGenerator() const
{
    return m_d->randomGenerator;
}

bool MatrixSdk::shouldNotify(MatrixEvent *event) const
{
    // Do not notify own event
    if (event->sender() == userId()) {
        return false;
    }
    return m_d->notificationHandler.handleNotification(event->underlyingEvent()).shouldNotify;
}

bool MatrixSdk::shouldPlaySound(MatrixEvent *event) const
{
    return m_d->notificationHandler.handleNotification(event->underlyingEvent()).sound.has_value();
}

MatrixStickerPackList *MatrixSdk::stickerPackList() const
{
    return new MatrixStickerPackList(m_d->clientOnSecondaryRoot);
}

MatrixEvent *MatrixSdk::stickerRoomsEvent() const
{
    return new MatrixEvent(m_d->clientOnSecondaryRoot.accountData()[imagePackRoomsEventType][lager::lenses::or_default]);
}

MatrixPromise *MatrixSdk::updateStickerPack(MatrixStickerPackSource source)
{
    if (source.source == MatrixStickerPackSource::AccountData) {
        auto eventJson = std::move(source.event).raw().get();
        eventJson["type"] = source.eventType;
        return sendAccountDataImpl(Event(std::move(eventJson)));
    } else if (source.source == MatrixStickerPackSource::RoomState) {
        auto eventJson = std::move(source.event).raw().get();
        eventJson["type"] = source.eventType;
        eventJson["state_key"] = source.stateKey;
        return new MatrixPromise(
            m_d->clientOnSecondaryRoot
            .room(source.roomId)
            .sendStateEvent(Event(std::move(eventJson))));
    } else {
        return 0;
    }
}

MatrixUserGivenAttrsMap *MatrixSdk::userGivenNicknameMap() const
{
    return new MatrixUserGivenAttrsMap(
        userGivenNicknameMapFor(m_d->clientOnSecondaryRoot),
        [client=m_d->clientOnSecondaryRoot](json content) {
            return client.setAccountData(json{
                {"type", USER_GIVEN_NICKNAME_EVENT_TYPES[0]},
                {"content", std::move(content)},
            });
        }
    );
}

MatrixPromise *MatrixSdk::sendAccountData(const QString &type, const QJsonObject &content)
{
    Event e = json{
        {"type", type.toStdString()},
        {"content", content},
    };
    return sendAccountDataImpl(std::move(e));
}

MatrixPromise *MatrixSdk::sendAccountDataImpl(Event event)
{
    return new MatrixPromise(m_d->clientOnSecondaryRoot.setAccountData(event));
}

void MatrixSdk::setUserDataDir(const std::string &userDataDir)
{
    m_d->userDataDir = userDataDir;
}

#include "matrix-sdk.moc"
