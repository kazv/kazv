/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <base/event.hpp>

struct MatrixStickerPackSource
{
    enum Source
    {
        AccountData,
        RoomState,
    };

    Source source;
    std::string eventType;
    Kazv::Event event;
    /// the room id, only applicable when source is RoomState
    std::string roomId;
    /// the state key, only applicable when source is RoomState
    std::string stateKey;
};

bool operator==(const MatrixStickerPackSource &a, const MatrixStickerPackSource &b);

bool operator!=(const MatrixStickerPackSource &a, const MatrixStickerPackSource &b);
