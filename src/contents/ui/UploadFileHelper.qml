/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import moe.kazv.mxc.kazv 0.0 as MK
import '.' as Kazv

QtObject {
  id: uploadFileHelper

  function chooseFileAndUpload () {
    fileDialog.open();
  }

  property var job: null
  property var available: !!job

  property var fileDialog: Kazv.FileDialogAdapter {
    onAccepted: {
      uploadFileHelper.job = kazvIOManager.startNewRoomlessUploadJob(matrixSdk.serverUrl, fileUrl, matrixSdk.token)
    }
  }

  signal uploaded(string mxcUri)
  signal failed(int ec)

  property var conn: Connections {
    target: uploadFileHelper.job

    function onResult(ec, mxcUri) {
      if (ec === MK.KazvIOBaseJob.NoError) {
        uploadFileHelper.uploaded(mxcUri);
      } else {
        uploadFileHelper.failed(ec);
      }
    }
  }

  function cleanup() {
    if (uploadFileHelper.job) {
      kazvIOManager.deleteRoomlessUploadJob(uploadFileHelper.job);
      uploadFileHelper.job = null;
    }
  }
}
