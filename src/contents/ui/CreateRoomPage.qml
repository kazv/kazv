/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

import moe.kazv.mxc.kazv 0.0 as MK

Kazv.ClosableScrollablePage {
  id: createRoomPage
  title: l10n.get('create-room-page-title')

  property var creatingRoom: false
  property var inviteUserIds: []
  property var encryptedChangedOnce: false

  ButtonGroup {
    id: roomTypeButtons
  }

  Kirigami.FormLayout {
    anchors.fill: parent

    ColumnLayout {
      Kirigami.FormData.label: l10n.get('create-room-page-type-prompt')
      RadioButton {
        id: typePublic
        objectName: 'typePublicRadio'
        checked: true
        onCheckedChanged: {
          if (!createRoomPage.encryptedChangedOnce) {
            encrypted.checked = !checked;
          }
        }
        text: l10n.get('create-room-page-type-public')
        ButtonGroup.group: roomTypeButtons
      }
      RadioButton {
        id: typePrivate
        objectName: 'typePrivateRadio'
        text: l10n.get('create-room-page-type-private')
        ButtonGroup.group: roomTypeButtons
      }
      RadioButton {
        id: typeDirect
        objectName: 'typeDirectRadio'
        text: l10n.get('create-room-page-type-direct')
        ButtonGroup.group: roomTypeButtons
      }
    }

    TextField {
      id: roomName
      objectName: 'roomNameInput'
      Kirigami.FormData.label: l10n.get('create-room-page-name-prompt')
      placeholderText: l10n.get('create-room-page-name-placeholder')
      Layout.fillWidth: true
    }

    TextField {
      id: roomAlias
      objectName: 'roomAliasInput'
      Kirigami.FormData.label: l10n.get('create-room-page-alias-prompt')
      placeholderText: l10n.get('create-room-page-alias-placeholder')
      Layout.fillWidth: true
    }

    TextField {
      id: roomTopic
      objectName: 'roomTopicInput'
      Kirigami.FormData.label: l10n.get('create-room-page-topic-prompt')
      placeholderText: l10n.get('create-room-page-topic-placeholder')
      Layout.fillWidth: true
    }

    CheckBox {
      id: allowFederate
      checked: true
      text: l10n.get('create-room-page-allow-federate-prompt')
      Layout.fillWidth: true
    }

    CheckBox {
      id: encrypted
      objectName: 'encryptedInput'
      text: l10n.get('create-room-page-encrypted-prompt')
      Layout.fillWidth: true
      onToggled: createRoomPage.encryptedChangedOnce = true
    }

    Item {
      Kirigami.FormData.isSection: true
      Kirigami.FormData.label: l10n.get('create-room-invite-userids-prompt')
    }

    ColumnLayout {
      Repeater {
        model: createRoomPage.inviteUserIds.length
        delegate: Kirigami.SwipeListItem {
          objectName: 'inviteUserItem_' + index
          Layout.fillWidth: true
          Layout.minimumWidth: Kirigami.Units.gridUnit * 10
          contentItem: RowLayout {
            Label {
              Layout.fillWidth: true
              objectName: 'inviteUserIdLabel'
              text: createRoomPage.inviteUserIds[index]
            }
          }

          actions: [
            Kirigami.Action {
              objectName: 'removeInviteAction'
              icon.name: 'list-remove-symbolic'
              text: l10n.get('create-room-page-action-remove-invite')
              onTriggered: {
                createRoomPage.inviteUserIds.splice(index, 1);
                // trigger reactivity
                createRoomPage.inviteUserIds = createRoomPage.inviteUserIds;
              }
            }
          ]
        }
      }
    }
    TextField {
      id: newInviteUserId
      Kirigami.FormData.label: l10n.get('create-room-page-add-invite-prompt')
      placeholderText: l10n.get('create-room-page-add-invite-placeholder')
      objectName: 'newInviteUserInput'
      text: ''
      Layout.fillWidth: true
    }
    Button {
      objectName: 'addInviteUserButton'
      text: l10n.get('create-room-page-action-add-invite')
      Layout.fillWidth: true
      onClicked: {
        createRoomPage.inviteUserIds.push(newInviteUserId.text);
        // trigger reactivity
        createRoomPage.inviteUserIds = createRoomPage.inviteUserIds;
        newInviteUserId.text = '';
      }
    }


    Button {
      Layout.fillWidth: true
      objectName: 'createRoomButton'
      text: l10n.get('create-room-page-action-create-room')
      onClicked: createRoomPage.createRoom.call()
    }
  }

  property var createRoom: Kazv.AsyncHandler {
    trigger: () => matrixSdk.createRoom(
      /* isPrivate = */ typePrivate.checked || typeDirect.checked,
      roomName.text,
      roomAlias.text,
      createRoomPage.inviteUserIds,
      /* isDirect = */ typeDirect.checked && createRoomPage.inviteUserIds.length === 1,
      /* allowFederate = */ allowFederate.checked,
      roomTopic.text,
      /* powerLevelContentOverride = */ {},
      /* preset = */ typeDirect.checked
        ? MK.MatrixSdk.TrustedPrivateChat
        : typePrivate.checked
        ? MK.MatrixSdk.PrivateChat
        : MK.MatrixSdk.PublicChat,
      /* encrypted = */ encrypted.checked,
    )

    onResolved: {
      if (success) {
        showPassiveNotification(l10n.get('create-room-page-success-prompt'));
        pageStack.removePage(createRoomPage);
      } else {
        showPassiveNotification(l10n.get('create-room-page-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }
}
