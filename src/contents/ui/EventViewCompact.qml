/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

Kazv.EventView {
  event: props.event
  sender: props.sender
  stateKeyUser: props.stateKeyUser
  isGapped: props.isGapped
  compactMode: true
  Layout.fillWidth: true
}
