/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

RowLayout {
  property var room

  onRoomChanged: {
    textArea.text = room.localDraft
  }

  TextArea {
    id: textArea
    placeholderText: l10n.get('send-message-box-input-placeholder')
    Layout.fillWidth: true
    wrapMode: TextEdit.Wrap
    onTextChanged: {
      room.localDraft = text
    }
  }
  Kirigami.Action {
    id: sendAction
    iconName: "document-send"
    onTriggered: {
      room.sendTextMessage(textArea.text)
      textArea.text = ""
    }
  }
  ToolButton {
    icon.name: "document-send"
    onClicked: sendAction.trigger()
  }
}
