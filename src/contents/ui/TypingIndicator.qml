/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import '.' as Kazv
import 'matrix-helpers.js' as Helpers

Label {
  id: typingIndicator
  property var typingUsers: null
  property var count: typingUsers ? typingUsers.count : 0
  property var firstTypingUser: typingUsers && typingUsers.at(0)
  property var secondTypingUser: typingUsers && typingUsers.at(1)
  property var nameProvider: Kazv.UserNameProvider {}

  visible: count
  text: getText()

  function getName(user) {
    return nameProvider.getName(user);
  }

  function getText() {
    console.log('getText', count);
    return l10n.get('typing-indicator', {
      otherNum: count - 1,
      typingUser: getName(firstTypingUser),
      secondTypingUser: getName(secondTypingUser),
    })
  }
}
