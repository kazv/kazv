/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv

ColumnLayout {
  id: stickerPicker
  property var stickerPackList
  spacing: 0
  property var currentIndex: 0

  signal sendMessageRequested(var eventJson)

  TabBar {
    id: packListView
    Layout.fillWidth: true
    Layout.minimumHeight: Kirigami.Units.gridUnit * 2

    Repeater {
      model: stickerPackList
      delegate: TabButton {
        id: packDelegate
        objectName: `stickerPack${index}`
        property var pack: stickerPackList.at(index)
        property var packNameProvider: Kazv.StickerPackNameProvider {
          pack: packDelegate.pack
        }
        icon.name: 'smiley'
        text: packNameProvider.name
        checked: stickerPicker.currentIndex === index
        onClicked: stickerPicker.currentIndex = index
      }
    }
  }

  property var currentPack: stickerPackList.at(currentIndex)

  property var stickerSize: Kirigami.Units.iconSizes.enormous
  property var stickerMargin: Kirigami.Units.smallSpacing

  GridView {
    id: stickersGridView
    Layout.fillWidth: true
    Layout.minimumHeight: stickerPicker.stickerSize * 5
    Layout.preferredHeight: stickersGridView.height
    model: currentPack
    cellWidth: stickerPicker.stickerSize + stickerPicker.stickerMargin
    cellHeight: stickerPicker.stickerSize + stickerPicker.stickerMargin
    delegate: MouseArea {
      objectName: `sticker${index}`
      property var sticker: currentPack.at(index)
      height: stickerPicker.stickerSize + stickerPicker.stickerMargin
      width: stickerPicker.stickerSize + stickerPicker.stickerMargin

      Kazv.ImageAdapter {
        anchors.centerIn: parent
        fillMode: Image.PreserveAspectFit
        height: stickerPicker.stickerSize
        width: stickerPicker.stickerSize
        mxcUri: sticker.mxcUri
      }

      Rectangle {
        visible: hoverHandler.hovered
        z: -1
        anchors.fill: parent
        color: Kirigami.Theme.activeBackgroundColor
      }

      HoverHandler {
        id: hoverHandler
      }

      onClicked: stickerPicker.sendMessageRequested(sticker.makeEventJson())
    }
  }

  Component.onCompleted: {
    console.log('stickerpicker finished', stickerPackList.count);
  }
}
