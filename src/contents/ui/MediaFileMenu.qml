/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt.labs.platform 1.1 as Platform

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK
import '.' as Kazv

QtObject {
  id: mediaFileMenu

  required property var eventContent

  property var jobManager: kazvIOManager
  property var mtxSdk: matrixSdk

  signal startDownload

  property var fileHandler: FileHandler {
    id: fileHandler
    autoCache: false
    eventContent: mediaFileMenu.eventContent
    kazvIOManager: mediaFileMenu.jobManager
    matrixSdk: mediaFileMenu.mtxSdk
  }

  property var viewAction: Kirigami.Action {
    text: l10n.get('media-file-menu-option-view')
    onTriggered: {
      fileHandler.cacheFile()
      Qt.openUrlExternally(fileHandler.cachedFile)
    }
  }

  property var saveAction: Kirigami.Action {
    text: l10n.get('media-file-menu-option-save-as')
    onTriggered: {
      fileDialog.open()
    }
  }

  property var optionMenu: [viewAction, saveAction]

  property var fileDialog: Kazv.FileDialogAdapter {
    id: fileDialog
    folder: Platform.StandardPaths.writableLocation(Platform.StandardPaths.DownloadLocation)
    onAccepted: {
      mediaFileMenu.fileHandler.downloadFile(fileUrl)
    }

    Component.onCompleted: {
      if (MK.KazvUtil.kfQtMajorVersion === 5) {
        fileDialog.selectExisting = false;
      } else {
        fileDialog.fileMode = Kazv.FileDialogAdapter.SaveFile;
      }
    }
  }

  Component.onCompleted: {
    mediaFileMenu.fileHandler.startDownload.connect(mediaFileMenu.startDownload)
  }
}
