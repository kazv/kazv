/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv

ColumnLayout {
  id: upper
  spacing: 0
  width: parent.width
  property var maxHeight: Kirigami.Units.gridUnit * 10
  implicitHeight: Math.min(listView.contentHeight, upper.maxHeight)
  property var members

  signal mentionUserRequested(string userId)

  ScrollView {
    Layout.fillWidth: true
    Layout.preferredHeight: contentHeight
    Layout.maximumHeight: upper.maxHeight

    ListView {
      id: listView
      objectName: 'completionListView'
      anchors.fill: parent
      model: members
      delegate: ItemDelegate {
        objectName: `completionItem${index}`
        property var member: members.at(index)
        property var nameProvider: Kazv.UserNameProvider {
          user: member
        }
        width: ListView.view.width

        onClicked: mentionUserRequested(member.userId)

        contentItem: RowLayout {
          id: completionItemLayout
          Kazv.AvatarAdapter {
            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
            name: nameProvider.name
            mxcUri: member.avatarMxcUri
          }
          ColumnLayout {
            Label {
              Layout.fillWidth: true
              text: nameProvider.name
            }
            Label {
              objectName: 'userIdLabel'
              Layout.fillWidth: true
              text: member.userId
            }
          }
        }
      }
    }
  }
}
