/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15

import 'matrix-helpers.js' as Helpers
import '.' as Kazv

QtObject {
  id: notifier
  property var notificationComp: Component {
    Kazv.MessageNotification {
      autoDelete: true
    }
  }

  property var nameProvider: Kazv.UserNameProvider {}
  property var roomNameProvider: Kazv.RoomNameProvider {}

  property var conn: Connections {
    target: matrixSdk
    function onReceivedMessage(roomId, eventId) {
      const roomList = matrixSdk.roomList();
      const room = roomList.room(roomId);
      const event = room.messageById(eventId);
      const sender = room.member(event.sender);
      const senderName = nameProvider.getName(sender);
      if (matrixSdk.shouldNotify(event)) {
        console.debug('Push rules say we should notify this');
        const notification = notificationComp.createObject(
          notifier,
          {
            roomId,
          });
        notification.eventId = matrixSdk.shouldPlaySound(event) ? 'message' : 'messageWithoutSound';
        notification.title = roomNameProvider.getName(room);
        const message = event.content.body;
        notification.text = message
          ? l10n.get('notification-message', { user: senderName, message })
          : l10n.get('notification-message-no-content', { user: senderName });
        notification.sendEvent();
      } else {
        console.debug('Push rules say we should not notify this');
      }
    }
  }
}
