/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQml.Models

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv
import 'emoji' as Emoji

Kazv.SelfDestroyableOverlaySheet {
  id: popup
  signal accepted(string text)

  title: l10n.get('event-react-popup-title')

  property var acceptAction: Kirigami.Action {
    text: l10n.get('event-react-accept-action')
    onTriggered: {
      accepted(reactionTextInput.text);
      popup.close();
    }
  }

  property var cancelAction: Kirigami.Action {
    text: l10n.get('event-react-cancel-action')
    onTriggered: {
      popup.close();
    }
  }

  property var emojiModel: Emoji.SmileysAndEmotion {}
  function setEmojiModel(model) {
    popup.emojiModel = Qt.createComponent(model).createObject(popup)
  }

  ColumnLayout {
    RowLayout {
      spacing: Kirigami.Units.largeSpacing
      Label {
        text: l10n.get('event-react-with-prompt')
      }

      TextField {
        objectName: 'reactionTextInput'
        id: reactionTextInput
        Layout.fillWidth: true
      }
    }

    TabBar {
      Layout.fillWidth: true
      TabButton {
        text: l10n.get('emoji-smileys-and-emotion')
        onClicked: setEmojiModel("emoji/SmileysAndEmotion.qml")
      }
      TabButton {
        text: l10n.get('emoji-people-and-body')
        onClicked: setEmojiModel("emoji/PeopleAndBody.qml")
      }
      TabButton {
        text: l10n.get('emoji-animals-and-nature')
        onClicked: setEmojiModel("emoji/AnimalsAndNature.qml")
      }
      TabButton {
        text: l10n.get('emoji-food-and-drink')
        onClicked: setEmojiModel("emoji/FoodAndDrink.qml")
      }
      TabButton {
        text: l10n.get('emoji-travel-and-places')
        onClicked: setEmojiModel("emoji/TravelAndPlaces.qml")
      }
      TabButton {
        text: l10n.get('emoji-activities')
        onClicked: setEmojiModel("emoji/Activities.qml")
      }
      TabButton {
        text: l10n.get('emoji-objects')
        onClicked: setEmojiModel("emoji/Objects.qml")
      }
      TabButton {
        text: l10n.get('emoji-symbols')
        onClicked: setEmojiModel("emoji/Symbols.qml")
      }
      TabButton {
        text: l10n.get('emoji-flags')
        onClicked: setEmojiModel("emoji/Flags.qml")
      }
    }

    ColumnLayout {
      clip: true
      GridView {
        objectName: 'emojiGrid'
        model: popup.emojiModel
        Layout.fillWidth: true
        height: Kirigami.Units.iconSizes.medium * 5
        cellWidth: Kirigami.Units.iconSizes.medium
        cellHeight: Kirigami.Units.iconSizes.medium
        delegate: ItemDelegate {
          required property string str
          width: Kirigami.Units.iconSizes.medium
          height: Kirigami.Units.iconSizes.medium
          onClicked: reactionTextInput.text = str
          contentItem: Label {
            anchors.fill: parent
            text: '<span style="font-family: emoji, sans-serif;">' + str + '</span>'
            textFormat: Qt.RichText
            font.pixelSize: parent.width
          }
        }
      }}

    RowLayout {
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignHCenter
      Button {
        action: acceptAction
      }

      Button {
        action: cancelAction
      }
    }
  }
}
