/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import moe.kazv.mxc.kazv 0.0 as MK
import '.'
// in KF6, this will override our own NotificationAction.qml
import org.kde.notification 1.0

Notification {
  id: notification
  componentName: 'kazv'
  eventId: 'message'
  text: 'test text'
  title: 'test title'
  property var roomId
  property var defaultActionText: l10n.get('notification-open')
  property var defaultActionV6: NotificationAction {
    label: defaultActionText
  }
  defaultAction: MK.KazvUtil.kfQtMajorVersion === 5 ? defaultActionText : defaultActionV6

  function switchToRoom() {
    switchToRoomRequested(roomId);
  }

  property var conn: Connections {
    target: MK.KazvUtil.kfQtMajorVersion === 5 ? notification : notification.defaultAction

    function onActivated() {
      switchToRoom();
    }

    function onDefaultActivated() {
      switchToRoom();
    }
  }
}
