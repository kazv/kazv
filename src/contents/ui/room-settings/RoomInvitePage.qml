/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.20 as Kirigami

import '..' as Kazv
import '../matrix-helpers.js' as Helpers

Kazv.ClosableScrollablePage {
  id: roomInvitePage

  property var room
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomInvitePage.room
  }
  property var roomDisplayName: roomNameProvider.name
  property var inviting: false

  title: l10n.get('room-invite-page-title', { room: roomDisplayName })

  property var inviteUser: Kazv.AsyncHandler {
    trigger: () => {
      roomInvitePage.inviting = true;
      return room.inviteUser(inviteUserIdInput.text);
    }
    onResolved: {
      roomInvitePage.inviting = false;
      if (!success) {
        showPassiveNotification(l10n.get('room-invite-page-invite-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      } else {
        pageStack.removePage(roomInvitePage);
      }
    }
  }

  Kirigami.FormLayout {
    TextField {
      id: inviteUserIdInput
      objectName: 'inviteUserIdInput'
      placeholderText: l10n.get('room-invite-invitee-matrix-id-placeholder')
      Kirigami.FormData.label: l10n.get('room-invite-invitee-matrix-id-prompt')
    }

    Button {
      objectName: 'inviteUserButton'
      text: l10n.get('room-invite-page-invite-button')
      enabled: !roomInvitePage.inviting
      Layout.fillWidth: true
      onClicked: {
        roomInvitePage.inviteUser.call();
      }
    }
  }
}
