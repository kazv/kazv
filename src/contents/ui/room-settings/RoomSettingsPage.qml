/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.20 as Kirigami

import moe.kazv.mxc.kazv 0.0 as MK

import '../matrix-helpers.js' as Helpers

import '..' as Kazv
import '.' as RoomSettings

Kazv.ClosableScrollablePage {
  id: roomSettingsPage

  property var room
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomSettingsPage.room
  }
  property var roomDisplayName: roomNameProvider.name
  property var customTagIds: room.tagIds.filter(k => k.startsWith('u.'))
  property var editingName: false
  property var submittingName: false
  property var submitName: Kazv.AsyncHandler {
    trigger: () => {
      roomSettingsPage.submittingName = true;
      return roomSettingsPage.room.setName(roomNameEdit.text);
    }
    onResolved: (success, data) => {
      roomSettingsPage.submittingName = false;
      if (success) {
        roomSettingsPage.editingName = false;
      } else {
        showPassiveNotification(l10n.get('room-settings-set-name-failed-prompt', {
          room: room.roomId,
          errorCode: data.errorCode,
          errorMsg: data.error,
        }));
      }
    }
  }

  property var editingTopic: false
  property var submittingTopic: false
  property var submitTopic: Kazv.AsyncHandler {
    trigger: () => {
      roomSettingsPage.submittingTopic = true;
      return roomSettingsPage.room.setTopic(roomTopicEdit.text);
    }
    onResolved: (success, data) => {
      roomSettingsPage.submittingTopic = false;
      if (success) {
        roomSettingsPage.editingTopic = false;
      } else {
        showPassiveNotification(l10n.get('room-settings-set-topic-failed-prompt', {
          room: room.roomId,
          errorCode: data.errorCode,
          errorMsg: data.error,
        }));
      }
    }
  }

  title: l10n.get('room-settings-page-title', { room: roomDisplayName })

  function tagIdToName(tagId) {
    return tagId.slice(2);
  }

  property var encryptionPopup: Kirigami.PromptDialog {
    parent: roomSettingsPage.overlay
    objectName: 'encryptionPopup'
    title: l10n.get('room-settings-enable-encryption-prompt-dialog-title')
    standardButtons: Kirigami.Dialog.Ok | Kirigami.Dialog.Cancel

    ColumnLayout {
      Label {
        Layout.fillWidth: true
        text: l10n.get('room-settings-enable-encryption-prompt-dialog-prompt')
        wrapMode: Text.Wrap
      }
    }

    onAccepted: enableEncryption.call()
  }

  property var enablingEncryption: false
  property var enableEncryption: Kazv.AsyncHandler {
    trigger: () => {
      roomSettingsPage.enablingEncryption = true;
      return room.sendStateEvent({
        type: 'm.room.encryption',
        state_key: '',
        content: {
          algorithm: 'm.megolm.v1.aes-sha2',
        },
      });
    }

    onResolved: (success, data) => {
      roomSettingsPage.enablingEncryption = false;
      if (success) {
        showPassiveNotification(l10n.get('room-settings-encryption-enabled-notification'));
      } else {
        showPassiveNotification(l10n.get('room-settings-encryption-failed-to-enable-notification', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  property var tagHandler: RoomSettings.RoomTagHandler {
    room: roomSettingsPage.room
  }
  property var members: room.members()

  property var avatarCount: 4

  ColumnLayout {
    RowLayout {
      Layout.alignment: Qt.AlignHCenter
      Repeater {
        objectName: 'roomMembersAvatarsRepeater'
        model: Math.min(members.count, roomSettingsPage.avatarCount)
        Kazv.AvatarAdapter {
          property var member: members.at(index)
          property var nameProvider: Kazv.UserNameProvider {
            user: member
          }
          Layout.preferredHeight: Kirigami.Units.iconSizes.huge
          Layout.preferredWidth: Kirigami.Units.iconSizes.huge
          mxcUri: member.avatarMxcUri
          name: nameProvider.name
        }
      }
      Kirigami.Icon {
        objectName: 'roomMembersAvatarsMore'
        Layout.preferredHeight: Kirigami.Units.iconSizes.huge
        Layout.preferredWidth: Kirigami.Units.iconSizes.huge
        source: 'go-next-skip'
        visible: members.count > roomSettingsPage.avatarCount
      }
    }
    ColumnLayout {
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignHCenter
      Label {
        objectName: 'roomIdLabel'
        Layout.alignment: Qt.AlignHCenter
        font: Kirigami.Theme.smallFont
        text: room.roomId
      }
    }
    RowLayout {
      visible: !roomSettingsPage.editingName
      Kazv.SelectableText {
        objectName: 'roomNameLabel'
        text: room.name || l10n.get('room-settings-name-missing')
        wrapMode: Text.Wrap
        Kirigami.Theme.colorGroup: !!room.name ? Kirigami.Theme.Normal : Kirigami.Theme.Inactive
        Layout.fillWidth: true
      }
      Button {
        Layout.alignment: Qt.AlignTop
        objectName: 'editNameButton'
        text: l10n.get('room-settings-edit-name-action')
        onClicked: {
          roomNameEdit.text = room.name;
          roomSettingsPage.editingName = true;
        }
      }
    }
    RowLayout {
      visible: roomSettingsPage.editingName
      TextArea {
        id: roomNameEdit
        objectName: 'roomNameEdit'
        Layout.fillWidth: true
        wrapMode: Text.Wrap
      }
      Button {
        Layout.alignment: Qt.AlignTop
        objectName: 'saveNameButton'
        enabled: !roomSettingsPage.submittingName
        text: l10n.get('room-settings-save-name-action')
        onClicked: roomSettingsPage.submitName.call()
      }
      Button {
        Layout.alignment: Qt.AlignTop
        objectName: 'discardNameButton'
        enabled: !roomSettingsPage.submittingName
        text: l10n.get('room-settings-discard-name-action')
        onClicked: roomSettingsPage.editingName = false
      }
    }
    RowLayout {
      visible: !roomSettingsPage.editingTopic
      Kazv.SelectableText {
        objectName: 'roomTopicLabel'
        text: room.topic || l10n.get('room-settings-topic-missing')
        font.italic: !room.topic
        wrapMode: Text.Wrap
        Kirigami.Theme.colorGroup: !!room.topic ? Kirigami.Theme.Normal : Kirigami.Theme.Inactive
        Layout.fillWidth: true
      }
      Button {
        Layout.alignment: Qt.AlignTop
        objectName: 'editTopicButton'
        text: l10n.get('room-settings-edit-topic-action')
        onClicked: {
          roomTopicEdit.text = room.topic;
          roomSettingsPage.editingTopic = true;
        }
      }
    }
    RowLayout {
      visible: roomSettingsPage.editingTopic
      TextArea {
        id: roomTopicEdit
        objectName: 'roomTopicEdit'
        Layout.fillWidth: true
        wrapMode: Text.Wrap
      }
      Button {
        Layout.alignment: Qt.AlignTop
        objectName: 'saveTopicButton'
        enabled: !roomSettingsPage.submittingTopic
        text: l10n.get('room-settings-save-topic-action')
        onClicked: roomSettingsPage.submitTopic.call()
      }
      Button {
        Layout.alignment: Qt.AlignTop
        objectName: 'discardTopicButton'
        enabled: !roomSettingsPage.submittingTopic
        text: l10n.get('room-settings-discard-topic-action')
        onClicked: roomSettingsPage.editingTopic = false
      }
    }
    Button {
      text: l10n.get('room-settings-members-action')
      icon.name: 'im-user'
      Layout.fillWidth: true
      onClicked: pageStack.push(Qt.resolvedUrl('RoomMemberListPage.qml'), { room: room, members: room.members() })
    }

    Button {
      text: l10n.get('room-settings-banned-members-action')
      icon.name: 'im-kick-user'
      icon.color: Kirigami.Theme.negativeTextColor
      Layout.fillWidth: true
      onClicked: pageStack.push(Qt.resolvedUrl('RoomMemberListPage.qml'), { room: room, members: room.bannedMembers() })
    }

    Label {
      objectName: 'encryptionIndicator'
      Layout.fillWidth: true
      text: !!room.encrypted ? l10n.get('room-settings-encrypted') : l10n.get('room-settings-not-encrypted')
    }

    Button {
      objectName: 'enableEncryptionButton'
      Layout.fillWidth: true
      visible: !room.encrypted
      enabled: !roomSettingsPage.enablingEncryption
      text: l10n.get('room-settings-enable-encryption-action')
      onClicked: encryptionPopup.open()
    }

    RowLayout {
      Layout.fillWidth: true
      Label {
        text: l10n.get('room-settings-tags')
        Layout.fillWidth: true
      }
      CheckBox {
        Layout.alignment: Qt.AlignRight
        text: l10n.get('room-settings-favourited')
        checkable: true
        checked: tagHandler.hasTag('m.favourite')
        enabled: tagHandler.available
        onToggled: tagHandler.toggleTag('m.favourite')
      }
    }

    ListView {
      model: customTagIds.length
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.minimumHeight: childrenRect.height
      delegate: Kirigami.SwipeListItem {
        required property var index
        contentItem: RowLayout {
          Label {
            Layout.fillWidth: true
            text: tagIdToName(customTagIds[index])
          }
        }

        actions: [
          Kirigami.Action {
            enabled: tagHandler.available
            icon.name: 'list-remove-symbolic'
            text: l10n.get('room-settings-remove-tag')
            onTriggered: tagHandler.toggleTag(customTagIds[index])
          }
        ]
      }
    }

    RowLayout {
      TextField {
        id: newTagName
        enabled: tagHandler.available
        text: ''
        Layout.fillWidth: true
      }
      Button {
        enabled: tagHandler.available
        text: l10n.get('room-settings-add-tag')
        icon.name: 'list-add'
        onClicked: {
          tagHandler.toggleTag('u.' + newTagName.text)
        }
      }
      Connections {
        target: tagHandler
        function onTagAdded(tagId) {
          if (tagId === 'u.' + newTagName.text) {
            newTagName.text = '';
          }
        }
      }
    }

    Button {
      objectName: 'stickerPacksButton'
      text: l10n.get('room-sticker-packs-action')
      icon.name: 'smiley'
      Layout.fillWidth: true
      onClicked: {
        activateRoomStickerPacksPage(roomSettingsPage.room);
      }
    }

    Button {
      text: l10n.get('room-invite-action')
      icon.name: 'list-add-user'
      Layout.fillWidth: true
      onClicked: {
        activateRoomInvitePage(roomSettingsPage.room);
      }
    }

    Button {
      objectName: 'leaveRoomButton'
      text: l10n.get('room-leave-action')
      icon.name: 'im-ban-kick-user'
      icon.color: Kirigami.Theme.negativeTextColor
      Layout.fillWidth: true
      onClicked: {
        confirmLeaveOverlay.open();
      }
    }

    Button {
      objectName: 'forgetRoomButton'
      text: l10n.get('room-forget-action')
      Layout.fillWidth: true
      visible: room.membership === MK.MatrixRoom.Leave
      onClicked: {
        confirmForgetOverlay.open();
      }
    }
  }

  property var confirmLeaveOverlay: Kazv.ConfirmationOverlay {
    objectName: 'confirmLeaveRoomPopup'
    parent: roomSettingsPage.overlay
    title: l10n.get('room-leave-confirm-popup-title')
    message: l10n.get('room-leave-confirm-popup-message')
    confirmActionText: l10n.get('room-leave-confirm-popup-confirm-action')
    cancelActionText: l10n.get('room-leave-confirm-popup-cancel-action')

    onAccepted: leaveRoomHandler.call()
  }

  property var leaveRoomHandler: Kazv.AsyncHandler {
    trigger: () => room.leaveRoom()
    onResolved: {
      if (success) {
        showPassiveNotification(l10n.get('leave-room-success-prompt', { room: room.roomId }));
        inviteOverlay.close();
      } else {
        showPassiveNotification(l10n.get('leave-room-failed-prompt', { room: room.roomId, errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  property var confirmForgetOverlay: Kazv.ConfirmationOverlay {
    objectName: 'confirmForgetRoomPopup'
    parent: roomSettingsPage.overlay
    title: l10n.get('room-forget-confirm-popup-title')
    message: l10n.get('room-forget-confirm-popup-message')
    confirmActionText: l10n.get('room-forget-confirm-popup-confirm-action')
    cancelActionText: l10n.get('room-forget-confirm-popup-cancel-action')

    onAccepted: forgetRoomHandler.call()
  }

  property var forgetRoomHandler: Kazv.AsyncHandler {
    trigger: () => room.forgetRoom()
    onResolved: (success, data) => {
      if (success) {
        showPassiveNotification(l10n.get('forget-room-success-prompt', { room: room.roomId }));
      } else {
        showPassiveNotification(l10n.get('forget-room-failed-prompt', { room: room.roomId, errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }
}
