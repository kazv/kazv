/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '../matrix-helpers.js' as Helpers

import '..' as Kazv

Kazv.ClosableScrollablePage {
  id: roomSettingsPage

  property var room
  required property var members
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomSettingsPage.room
  }
  property var roomDisplayName: roomNameProvider.name
  title: l10n.get('room-member-list-page-title', { room: roomDisplayName })

  Kazv.RoomMemberListView {
    room: roomSettingsPage.room
    members: roomSettingsPage.members
  }

  Component.onCompleted: room.refreshState()
}
