/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.delegates 1.0 as KAD
import moe.kazv.mxc.kazv as MK
import '..' as Kazv
import '.' as RoomSettings
import '../matrix-helpers.js' as Helpers

Kirigami.SwipeListItem {
  id: upper
  property var stickerPack
  property var stickerRoomsEvent

  text: stickerPack.packName || stickerPack.stateKey
  contentItem: KAD.SubtitleContentItem {
    itemDelegate: upper
    subtitle: stickerPack.packName ? stickerPack.stateKey : ''
  }

  actions: [
    Kirigami.Action {
      objectName: 'usePackAction'
      text: l10n.get('room-sticker-packs-use-action')
      checkable: true
      checked: upper.getIsUsingPack()
      icon.name: checked ? 'starred-symbolic' : 'non-starred-symbolic'
      onTriggered: checked ? upper.usePack() : upper.disusePack()
    }
  ]

  function getIsUsingPack() {
    return Object.entries(stickerRoomsEvent.content.rooms || {})
      .some(([roomId, packs]) =>
        roomId === stickerPack.roomId &&
        Object.entries(packs).some(([stateKey, _]) =>
          stateKey === stickerPack.stateKey
        )
      );
  }

  function usePack() {
    const content = stickerRoomsEvent.content;
    content.rooms = content.rooms || {};
    content.rooms[stickerPack.roomId] = content.rooms[stickerPack.roomId] || {};
    content.rooms[stickerPack.roomId][stickerPack.stateKey] = {};
    send(content);
  }

  function disusePack() {
    const content = stickerRoomsEvent.content;
    if (!stickerPack.stateKey in content.rooms?.[stickerPack.roomId]) {
      return;
    }
    delete content.rooms[stickerPack.roomId][stickerPack.stateKey];
    send(content);
  }

  property var sendAccountData: Kazv.AsyncHandler {
    trigger: () => matrixSdk.sendAccountData(Helpers.imagePackRoomsEventType, content);
    property var content
    onResolved: (success, data) => {
      if (!success) {
        showPassiveNotification(l10n.get('room-sticker-packs-use-failed', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  function send(content) {
    sendAccountData.content = content;
    sendAccountData.call();
  }
}
