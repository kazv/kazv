/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '../matrix-helpers.js' as Helpers

import '..' as Kazv

QtObject {
  id: roomTagHandler
  property var room
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomTagHandler.room
  }
  property var roomDisplayName: roomNameProvider.name
  property var tagIds: room.tagIds
  property var available: true
  property var tagIdInProgress: ''

  signal tagAdded(string tagId)

  function hasTag(tagId) {
    return tagIds.includes(tagId);
  }

  function toggleTag(tagId) {
    if (!tagId) {
      console.error('toggleTag: tagId cannot be empty');
      return;
    }
    available = false;
    tagIdInProgress = tagId;

    if (hasTag(tagId)) {
      removeTag.call();
    } else {
      addTag.call();
    }
  }

  function tagIdToName(tagId) {
    if (tagId.startsWith('u.')) {
      return tagId.slice(2);
    } else {
      return tagId;
    }
  }

  function getL10nKey(tagId, action, success) {
    let actionString;
    if (tagId === 'm.favourite') {
      if (action === 'add') {
        actionString = 'fav';
      } else {
        actionString = 'unfav';
      }
    } else {
      if (action === 'add') {
        actionString = 'add-tag';
      } else {
        actionString = 'remove-tag';
      }
    }

    const maybeFailed = success ? '' : '-failed';

    return `room-tags-${actionString}-action-notification${maybeFailed}`;
  }

  property var addTag: Kazv.AsyncHandler {
    trigger: () => room.addOrSetTag(tagIdInProgress);
    onResolved: {
      available = true;
      if (success) {
        tagAdded(tagIdInProgress);
      }

      showPassiveNotification(l10n.get(getL10nKey(tagIdInProgress, 'add', success), {
        name: roomDisplayName,
        tag: tagIdToName(tagIdInProgress),
        errorCode: data.errorCode || '',
        errorMsg: data.error || '',
      }));
    }
  }

  property var removeTag: Kazv.AsyncHandler {
    trigger: () => room.removeTag(tagIdInProgress);
    onResolved: {
      available = true;
      showPassiveNotification(l10n.get(getL10nKey(tagIdInProgress, 'remove', success), {
        name: roomDisplayName,
        tag: tagIdToName(tagIdInProgress),
        errorCode: data.errorCode || '',
        errorMsg: data.error || '',
      }));
    }
  }

}
