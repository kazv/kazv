/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import org.kde.kirigami as Kirigami
import moe.kazv.mxc.kazv as MK
import '..' as Kazv
import '.' as RoomSettings
import '../matrix-helpers.js' as Helpers

Kazv.ClosableScrollablePage {
  id: roomStickerPacksPage

  property var room
  property var stickerPackList: room.stickerPackList()
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomStickerPacksPage.room
  }

  title: l10n.get('room-sticker-packs-page-title', {
    room: roomNameProvider.name,
  })

  ListView {
    model: roomStickerPacksPage.stickerPackList
    delegate: RoomSettings.RoomStickerPackItemDelegate {
      required property int index
      stickerRoomsEvent: matrixSdk.stickerRoomsEvent()
      stickerPack: stickerPackList.at(index)
    }
  }

  actions: [
    Kirigami.Action {
      objectName: 'addStickerPackAction'
      text: l10n.get('room-sticker-packs-page-add-action')
      icon.name: 'list-add'
      onTriggered: addStickerPackPopup.open()
    }
  ]

  property var addStickerPack: Kazv.AsyncHandler {
    id: addStickerPack
    trigger: () => {
      const type = Helpers.roomImagePackEventType;
      const stateKey = stickerPackStateKey.text;
      const state = room.state(type, stateKey);
      const content = state.content;
      content.pack = content.pack || {};
      content.pack.display_name = stickerPackName.text;
      return roomStickerPacksPage.room.sendStateEvent({
        type,
        state_key: stateKey,
        content,
      });
    }

    onResolved: (success, data) => {
      if (success) {
        showPassiveNotification(l10n.get('room-sticker-packs-page-add-success'));
      } else {
        showPassiveNotification(l10n.get('room-sticker-packs-page-add-failed', { errorMsg: data.error, errorCode: data.errorCode }));
      }
    }
  }

  property var addStickerPackPopup: Kirigami.PromptDialog {
    id: addStickerPackPopup
    title: l10n.get('room-sticker-packs-page-add-popup-title')
    standardButtons: Kirigami.Dialog.Ok | Kirigami.Dialog.Cancel
    onAccepted: addStickerPack.call()
    Kirigami.FormLayout {
      TextField {
        id: stickerPackName
        objectName: 'stickerPackName'
        Kirigami.FormData.label: l10n.get('room-sticker-packs-page-add-popup-name-prompt')
      }
      TextField {
        id: stickerPackStateKey
        objectName: 'stickerPackStateKey'
        Kirigami.FormData.label: l10n.get('room-sticker-packs-page-add-popup-state-key-prompt')
      }
    }
  }
}
