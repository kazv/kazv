/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import '.' as Kazv

Kazv.SelfDestroyableOverlaySheet {
  id: confirmationOverlay
  property var message
  property var confirmActionText
  property var cancelActionText

  default property alias contentData: itemsLayout.data

  signal accepted()

  ColumnLayout {
    Label {
      Layout.fillWidth: true
      text: message
      wrapMode: Text.Wrap
    }

    ColumnLayout {
      id: itemsLayout
    }

    RowLayout {
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignHCenter
      Button {
        objectName: 'confirmButton'
        text: confirmActionText
        onClicked: {
          accepted();
          confirmationOverlay.close();
        }
      }

      Button {
        objectName: 'cancelButton'
        text: cancelActionText
        onClicked: confirmationOverlay.close();
      }
    }
  }
}
