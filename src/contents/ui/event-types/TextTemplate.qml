/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Types
import '..' as Kazv
import '../matrix-helpers.js' as Helpers

Types.Simple {
  id: upper
  property var text
  default property var children
  property alias textFormat: label.textFormat

  property var linkToActivate
  property var selectedUserId
  property var ensureMemberEvent: Kazv.AsyncHandler {
    trigger: () => room.ensureStateEvent('m.room.member', upper.selectedUserId)
    onResolved: (success, data) => {
      if (success) {
        activateUserPage(room.member(upper.selectedUserId), room);
      } else {
        // TODO: This opens the matrix.to url directly.
        // In a future version this should take you to a window that
        // gives you the option to create a DM with that user.
        openLink(upper.linkToActivate);
      }
    }
  }

  Kazv.Bubble {
    id: bubble
    Layout.fillWidth: true
    RowLayout {
      Layout.fillWidth: true

      property var label: Kazv.SelectableText {
        objectName: 'textEventContent'
        id: label
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        text: upper.text
        onLinkActivated: (link) => {
          const userId = MK.KazvUtil.matrixLinkUserId(link);
          if (userId) {
            upper.selectedUserId = userId;
            upper.linkToActivate = link;
            ensureMemberEvent.call();
          } else {
            openLink(link);
          }
        }
        onHoveredLinkChanged: (link) => {
          if (link) {
            // first give it text, then make it visible
            label.ToolTip.text = link;
            label.ToolTip.visible = true;
          } else {
            // first make it invisible, then remove the text
            label.ToolTip.visible = false;
            label.ToolTip.text = '';
          }
        }
        ToolTip.delay: Kirigami.Units.toolTipDelay
        ToolTip.timeout: Helpers.toolTipTimeout
      }

      data: [
        ...(Array.isArray(upper.children) ? upper.children :
            upper.children ? [upper.children] : []),
        label
      ]
    }
  }

  property var isHtmlFormatted: event.content.format === 'org.matrix.custom.html' && event.content.formatted_body

  function getMaybeFormattedText() {
    if (isHtmlFormatted) {
      const stylesheet = `
        <style>
          del {
            text-decoration: line-through;
          }

          a[href^="https://matrix.to/#/@"] {
            background-color: ${Kirigami.Theme.positiveBackgroundColor};
          }
        </style>
      `;
      const formattedBody = event.content.formatted_body;
      if (event.replyingToEventId && formattedBody.startsWith('<mx-reply>')) {
        const index = formattedBody.indexOf('</mx-reply>');
        return stylesheet + formattedBody.slice(index + '</mx-reply>'.length);
      } else {
        return stylesheet + formattedBody;
      }
    } else {
      return event.content.body;
    }
  }

  function openLink(link) {
    Qt.openUrlExternally(link);
  }
}
