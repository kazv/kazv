/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

ColumnLayout {
  property var event
  property var sender

  default property var children

  property var contentMaxWidth: {
    (parent.width
     // avatar size and margins
     - iconSize
     - 2 * Kirigami.Units.largeSpacing
     - 2 * Kirigami.Units.largeSpacing
    )
  }

  id: layout
  RowLayout {
    width: parent.width
    Layout.leftMargin: Kirigami.Units.largeSpacing
    Layout.rightMargin: Kirigami.Units.largeSpacing
    Layout.topMargin: Kirigami.Units.largeSpacing

    Kirigami.Avatar {
      //width: iconSize
      //height: iconSize
      id: avatar
      Layout.alignment: Qt.AlignTop
      sourceSize.width: iconSize
      sourceSize.height: iconSize
      source: sender.avatarMxcUri ? matrixSdk.mxcUriToHttp(sender.avatarMxcUri) : ''
      name: sender.name || sender.userId
    }

    ColumnLayout {
      Layout.fillWidth: true
      Layout.leftMargin: Kirigami.Units.largeSpacing

      Label {
        text: sender.name || sender.userId
      }

      RowLayout {
        id: container
        Layout.fillWidth: true
        data: layout.children
      }
    }
  }
}
