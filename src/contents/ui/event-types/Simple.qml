/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '..' as Kazv

ColumnLayout {
  objectName: 'eventViewMainItem'
  property var event
  property var sender

  default property alias children: container.data
  property var isSelected
  /// The item shown after the username, in compact mode
  property var summaryItem: Item {}

  property var nameProvider: Kazv.UserNameProvider {
    user: sender
  }
  property var senderNickname: nameProvider.name

  property var contentMaxWidth: {
    (parent.width
     // avatar size and margins
     - iconSize
     - 2 * Kirigami.Units.largeSpacing
     - 2 * Kirigami.Units.largeSpacing
    )
  }
  property var shouldCollapseSender: (
    !compactMode
      && prevEventType
      && prevEventType !== 'ignore'
      && (
        event.sender === prevEvent.sender
        // local echo does not have a sender, will use the current user
        || (!!event.isLocalEcho && !!prevEvent.isLocalEcho)
        || (!!event.isLocalEcho && prevEvent.sender === matrixSdk.userId)
      )
  )

  id: layout
  RowLayout {
    // width: parent.width
    Layout.fillWidth: true
    Layout.leftMargin: Kirigami.Units.largeSpacing
    Layout.rightMargin: Kirigami.Units.largeSpacing
    Layout.topMargin: shouldCollapseSender ? 0 : Kirigami.Units.largeSpacing
    Layout.bottomMargin: 0

    Kazv.AvatarAdapter {
      id: avatar
      objectName: 'senderAvatar'
      Layout.alignment: Qt.AlignTop
      mxcUri: sender.avatarMxcUri
      name: senderNickname
      visible: !compactMode && !shouldCollapseSender

      TapHandler {
        onTapped: {
          activateUserPage(sender, room, event.sender);
        }
      }
    }

    Item {
      objectName: 'senderCollapsedPlaceholder'
      visible: shouldCollapseSender
      Layout.preferredWidth: iconSize
      Layout.preferredHeight: 1
    }

    ColumnLayout {
      Layout.fillWidth: true
      Layout.leftMargin: Kirigami.Units.largeSpacing

      RowLayout {
        visible: !shouldCollapseSender
        Kazv.AvatarAdapter {
          id: avatarCompact
          Layout.preferredWidth: Kirigami.Units.iconSizes.sizeForLabels
          Layout.preferredHeight: Kirigami.Units.iconSizes.sizeForLabels
          mxcUri: sender.avatarMxcUri
          name: senderNickname
          visible: compactMode
        }

        Label {
          id: userNicknameText
          objectName: 'userNicknameText'
          text: senderNickname
          Layout.fillWidth: !compactMode

          property var reasonableWidth: Math.max(contentWidth, Kirigami.Units.gridUnit)

          TapHandler {
            enabled: !compactMode
            onTapped: {
              if (point.position.x <= userNicknameText.reasonableWidth) {
                mentionUserRequested(sender.userId);
              }
            }
          }
        }

        RowLayout {
          objectName: 'compactModeSummary'
          visible: compactMode
          data: [summaryItem]
        }
      }

      RowLayout {
        id: container
        Layout.fillWidth: true
      }
    }
  }
}
