/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import moe.kazv.mxc.kazv 0.0 as MK

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types
import '..' as Kazv

Simple {
  id: upper

  property var gender: 'neutral'
  property var body: event.content.body
  property var curEvent: event

  property var thumbnailFile: imageInfo.thumbnail_file || {}
  property var thumbnailInfo: imageInfo.thumbnail_info || {}
  property var thumbnailWidth: thumbnailInfo.w
  property var thumbnailHeight: thumbnailInfo.h

  property var imageInfo: event.content.info || {}
  property var imageFile: event.content.file || {}
  property var imageWidth: imageInfo.w
  property var imageHeight: imageInfo.h
  property var imageMaxHeight: compactMode ? Kirigami.Units.gridUnit * 5 : Infinity

  property var imageMxcUri: fileHandler.encryptedFileMxcUri || fileHandler.unencryptedFileMxcUri
  property var innerContentWidth: upper.contentMaxWidth - bubble.bubbleSpacing

  property var jobManager: kazvIOManager
  property var mtxSdk: matrixSdk
  property var isSticker: event.type === 'm.sticker'

  summaryItem: Label {
    objectName: 'summaryLabel'
    Layout.alignment: Qt.AlignLeft
    Layout.fillWidth: true
    elide: Text.ElideRight
    text: l10n.get(isSticker ? 'event-summary-sticker-sent' : 'event-summary-image-sent', { gender, body})
  }

  Kazv.FileHandler {
    id: fileHandler
    eventContent: event.content
    autoCache: true
    kazvIOManager: upper.jobManager
    matrixSdk: upper.mtxSdk
  }

  Types.MediaBubble {
    id: bubble

    eventContent: event.content

    property var label: Kazv.SelectableText {
      objectName: 'bodyLabel'
      Layout.fillWidth: true
      wrapMode: Text.Wrap

      text: l10n.get(isSticker ? 'event-message-sticker-sent' : 'event-message-image-sent', { gender, body})
      visible: !compactMode
    }

    property var image: Image {
      id: image
      objectName: 'mainImage'
      Layout.fillWidth: true
      property double imageWidth: upper.thumbnailWidth || upper.imageWidth || image.implicitWidth || 1
      property double imageHeight: upper.thumbnailHeight || upper.imageHeight || image.implicitHeight || 1
      property double privHeight: Math.min(imageMaxHeight, imageHeight)
      property double privWidth: privHeight / imageHeight * imageWidth
      Layout.preferredWidth: privWidth
      Layout.preferredHeight: width < privWidth ? privHeight / privWidth * width : privHeight
      source: fileHandler.kazvIOJob ? "" : fileHandler.cachedFile
      horizontalAlignment: Image.AlignLeft
      fillMode: Image.PreserveAspectFit
    }

    property var canAddSticker: !!event.content.url
    property var addStickerPopupComp: Component {
      Kazv.AddStickerPopup {
        parent: applicationWindow().overlay
        shouldSelfDestroy: true
        stickerPackList: matrixSdk.stickerPackList()
        event: upper.curEvent
      }
    }

    property var addStickerAction: Kirigami.Action {
      text: l10n.get('media-file-menu-add-sticker-action')
      onTriggered: bubble.addStickerPopupComp.createObject(applicationWindow().overlay).open()
    }

    additionalMenuContent: canAddSticker ? [addStickerAction] : []

    ColumnLayout {
      id: layout
      Layout.fillWidth: true
      data: [
        bubble.mediaFileMenu,
        bubble.label,
        bubble.image,
        bubble.progressBar
      ]
    }
  }
}
