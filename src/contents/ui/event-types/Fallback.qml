/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types

Types.TextTemplate {
  id: upper
  text: getText()

  Kirigami.Icon {
    objectName: 'fallbackIcon'
    source: 'emblem-question'
    Layout.preferredHeight: inlineBadgeSize
    Layout.preferredWidth: inlineBadgeSize
  }

  function getText() {
    if (event.type === 'm.room.message') {
      if (event.content.msgtype === 'moe.kazv.mxc.cannot.decrypt' ||
         event.content.msgtype === 'xyz.tusooa.kazv.not.yet.decrypted') {
        return l10n.get('event-cannot-decrypt-text');
      } else {
        return l10n.get('event-msgtype-fallback-text', {
          msgtype: event.content.msgtype,
        });
      }
    } else {
      return l10n.get('event-fallback-text', {
        type: event.type,
      });
    }
  }
}
