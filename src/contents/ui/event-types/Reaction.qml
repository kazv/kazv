/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types

import moe.kazv.mxc.kazv 0.0 as MK

Types.TextTemplate {
  id: upper
  text: getText()
  textFormat: Qt.RichText

  Kirigami.Icon {
    source: 'star-new-symbolic'
    Layout.preferredHeight: inlineBadgeSize
    Layout.preferredWidth: inlineBadgeSize
  }

  function getText() {
    const key = event.content['m.relates_to']
          ? event.content['m.relates_to'].key
          : (event.originalSource.content['m.relates_to'] ? event.originalSource.content['m.relates_to'].key : '') || '';
    // Qt cannot render coloured emojis by default
    // https://bugreports.qt.io/browse/QTBUG-85744
    return l10n.get('event-reacted-with', { key: '<span style="font-family: emoji, sans-serif;">' + MK.KazvUtil.escapeHtml(key) + '</span>' });
  }
}
