/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import QtMultimedia 5.15

import org.kde.kirigami 2.13 as Kirigami

import '..' as Kazv

Simple {
  id: upper

  property var gender: 'neutral'
  property var body: event.content.body
  property var mxcUri: event.content.url
  property var videoUri: matrixSdk.mxcUriToHttp(mxcUri)

  property var videoInfo: event.content.info || {}
  property var videoResolution: video.metaData.resolution
  property var videoWidth: videoResolution && videoResolution.width || (videoInfo.w || 1)
  property var videoHeight: videoResolution && videoResolution.height || (videoInfo.h || 1)
  property var thumbnailInfo: videoInfo.thumbnail_info || {}
  property var thumbnailMxcUri: videoInfo.thumbnail_url
  property var thumbnailUri: matrixSdk.mxcUriToHttp(thumbnailMxcUri)
  property var hasThumbnail: !! thumbnailMxcUri

  property var innerContentWidth: upper.contentMaxWidth - bubble.bubbleSpacing

  Kazv.Bubble {
    id: bubble
    ColumnLayout {
      id: layout
      Label {
        Layout.fillWidth: false
        Layout.maximumWidth: innerContentWidth
        wrapMode: Text.Wrap
        text: l10n.get('event-message-video-sent', { gender, body })
      }
      Video {
        id: video
        source: videoUri
        autoLoad: false
        autoPlay: false

        loops: MediaPlayer.Infinite

        Layout.minimumHeight: Kirigami.Units.gridUnit * 10
        Layout.minimumWidth: Kirigami.Units.gridUnit * 10
        Layout.maximumWidth: innerContentWidth
        Layout.preferredWidth: videoWidth
        Layout.preferredHeight: videoHeight / videoWidth * width
        fillMode: VideoOutput.PreserveAspectFit

        MouseArea {
          anchors.fill: parent
          onClicked: {
            video.playOrPause();
          }
        }

        Image {
          anchors.fill: parent
          source: thumbnailUri
          visible: video.playbackState == MediaPlayer.StoppedState && hasThumbnail
          fillMode: Image.PreserveAspectFit
        }

        Rectangle {
          anchors.fill: parent
          color: Kirigami.Theme.negativeBackgroundColor //Kirigami.ColorUtils.tintWithAlpha(color, Kirigami.Theme.textColor, 0.15)
          visible: {
            video.playbackState == MediaPlayer.StoppedState &&
              ! hasThumbnail
          }
        }

        onStatusChanged: {
          console.log('status = ', video.status);
        }

        onPlaybackStateChanged: {
          console.log('playback state = ', video.playbackState);
        }

        function playOrPause() {
          console.log('playback state ==', video.playbackState);
          if (video.playbackState == MediaPlayer.PlayingState) {
            video.pause();
          } else {
            if (video.playbackState == MediaPlayer.StoppedState) {
              video.seek(0);
            }
            video.play();
          }
        }
      }
    }
  }
}
