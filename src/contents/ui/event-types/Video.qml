/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtMultimedia
import moe.kazv.mxc.kazv 0.0 as MK

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types
import '..' as Kazv

Simple {
  id: upper

  property var gender: 'neutral'
  property var body: event.content.body

  property var videoInfo: event.content.info || {}
  property var videoResolution: video.metaData.value(MediaMetaData.Resolution)
  property var videoWidth: videoResolution && videoResolution.width || (videoInfo.w || 1)
  property var videoHeight: videoResolution && videoResolution.height || (videoInfo.h || 1)
  property var videoMaxHeight: compactMode ? Kirigami.Units.gridUnit * 5 : Infinity
  property var thumbnailInfo: videoInfo.thumbnail_info || {}
  property var thumbnailMxcUri: videoInfo.thumbnail_url
  property var thumbnailUri: matrixSdk.mxcUriToHttp(thumbnailMxcUri)
  property var hasThumbnail: !! thumbnailMxcUri

  property var innerContentWidth: upper.contentMaxWidth - bubble.bubbleSpacing

  property var jobManager: kazvIOManager
  property var mtxSdk: matrixSdk

  summaryItem: Label {
    objectName: 'summaryLabel'
    Layout.alignment: Qt.AlignLeft
    Layout.fillWidth: true
    elide: Text.ElideRight
    text: l10n.get('event-summary-video-sent', { gender, body })
  }

  Kazv.FileHandler {
    id: fileHandler
    eventContent: event.content
    autoCache: true
    kazvIOManager: upper.jobManager
    matrixSdk: upper.mtxSdk
  }

  Types.MediaBubble {
    id: bubble
    Layout.fillWidth: true

    eventContent: event.content

    ColumnLayout {
      id: layout
      property var label: Kazv.SelectableText {
        objectName: 'bodyLabel'
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        text: l10n.get('event-message-video-sent', { gender, body })
        visible: !compactMode
      }
      property var video: Video {
        id: video
        objectName: 'video'
        source: fileHandler.kazvIOJob ? "" : fileHandler.cachedFile
        autoPlay: !hasAudio
        loops: MediaPlayer.Infinite
        Layout.fillWidth: true
        property double videoWidth: upper.thumbnailWidth || upper.videoWidth || video.implicitWidth || 1
        property double videoHeight: upper.thumbnailHeight || upper.videoHeight || video.implicitHeight || 1
        property double privHeight: Math.min(videoMaxHeight, videoHeight)
        property double privWidth: privHeight / videoHeight * videoWidth
        Layout.preferredWidth: privWidth
        Layout.preferredHeight: width < privWidth ? privHeight / privWidth * width : privHeight
        fillMode: VideoOutput.PreserveAspectFit

        TapHandler {
          onSingleTapped: {
            video.playOrPause();
          }
        }

        Image {
          anchors.fill: parent
          source: thumbnailUri
          visible: video.playbackState == MediaPlayer.StoppedState && hasThumbnail
          fillMode: Image.PreserveAspectFit
          data: [bubble.mediaFileMenu]
        }

        Rectangle {
          anchors.fill: parent
          color: Kirigami.Theme.negativeBackgroundColor //Kirigami.ColorUtils.tintWithAlpha(color, Kirigami.Theme.textColor, 0.15)
          visible: {
            video.playbackState == MediaPlayer.StoppedState &&
              ! hasThumbnail
          }
        }

        Control {
          anchors.centerIn: parent
          height: Math.min(video.height, video.width, Kirigami.Units.iconSizes.large)
          width: Math.min(video.height, video.width, Kirigami.Units.iconSizes.large)
          visible: video.playbackState !== MediaPlayer.PlayingState

          background: Rectangle {
            radius: width / 2
            color: Qt.alpha(Kirigami.Theme.backgroundColor, 0.4)
          }

          contentItem: Kirigami.Icon {
            source: 'media-playback-start'
          }
        }

        function playOrPause() {
          console.log('playback state ==', video.playbackState);
          if (video.playbackState == MediaPlayer.PlayingState) {
            video.pause();
          } else {
            if (video.playbackState == MediaPlayer.StoppedState) {
              video.seek(0);
            }
            video.play();
          }
        }
      }
      data: [
        label,
        video,
        bubble.progressBar
      ]
    }
  }
}
