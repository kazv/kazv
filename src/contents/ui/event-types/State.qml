/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types

Types.TextTemplate {
  id: upper
  text: getText()

  property var stateKeyUser
  property var gender: 'neutral'
  property var stateKeyUserGender: 'neutral'

  Kirigami.Icon {
    source: getIcon()
    Layout.preferredHeight: inlineBadgeSize
    Layout.preferredWidth: inlineBadgeSize
  }

  function getIcon() {
    switch (event.type) {
    case 'm.room.member':
      const newState = event.content.membership;
      // If there were no previous content, we consider that the room
      // never knew this member.
      const prevContent = event.unsignedData.prev_content || {};
      const oldState = prevContent.membership || 'leave';
      // Someone changing their own member state
      const isSelfSent = event.stateKey === event.sender;

      if (newState === 'join' && oldState !== 'join') {
        return 'list-add-user';
      } else if (newState === 'leave' || newState === 'ban') {
        return isSelfSent ? 'list-remove-user' : 'im-kick-user';
      } else {
        return 'im-user';
      }

    default:
      return 'emblem-information';
    }
  }

  function getText() {
    switch (event.type) {
    case 'm.room.member':
      return getMemberChange();

    case 'm.room.create':
      return l10n.get('state-room-created', { gender });

    case 'm.room.canonical_alias':
      return l10n.get('state-room-alias-changed', { gender });
    case 'm.room.join_rules':
      return l10n.get('state-room-join-rules-changed', { gender });
    case 'm.room.power_levels':
      return l10n.get('state-room-power-levels-changed', { gender });
    case 'm.room.name':
      return l10n.get('state-room-name-changed', { gender, newName: event.content.name });

    case 'm.room.topic':
      return l10n.get('state-room-topic-changed', { gender, newTopic: event.content.topic });

    case 'm.room.avatar':
      return l10n.get('state-room-avatar-changed', { gender });

    case 'm.room.pinned_events':
      return l10n.get('state-room-pinned-events-changed', { gender });

    case 'm.room.encryption':
      return l10n.get('state-room-encryption-activated', { gender });

    default:
      return event.content.body;
    }
  }

  function getMemberChange() {
    const newState = event.content.membership;
    // If there were no previous content, we consider that the room
    // never knew this member.
    const prevContent = event.unsignedData.prev_content || {};
    const oldState = prevContent.membership || 'leave';
    // Someone changing their own member state
    const isSelfSent = event.stateKey === event.sender;

    if (newState === 'join') {
      if (oldState !== 'join') {
        return l10n.get('member-state-joined-room', { gender });
      } else {
        const nameChanged = event.content.displayname !== prevContent.displayname;
        const avatarChanged = event.content.avatar_url !== prevContent.avatar_url;
        if (nameChanged && avatarChanged) {
          return l10n.get('member-state-changed-name-and-avatar', { gender });
        } else if (nameChanged) {
          return l10n.get('member-state-changed-name', { gender });
        } else if (avatarChanged) {
          return l10n.get('member-state-changed-avatar', { gender });
        } else {
          return ''; // stayed in the room without name/avatar change. should not happen
        }
      }
    } else if (newState === 'invite') {
      return l10n.get('member-state-invited', { gender,
                                                stateKeyUser: stateKeyUser.displayname,
                                                stateKeyUserGender
                                              });
    } else if (newState === 'left') {
      if (oldState === 'ban') {
        return l10n.get('member-state-unbanned', { gender,
                                                   stateKeyUser: stateKeyUser.displayname,
                                                   stateKeyUserGender
                                                 });
      } else if (isSelfSent) {
        return l10n.get('member-state-left', { gender });
      } else {
        return l10n.get('member-state-kicked', { gender,
                                                 stateKeyUser: stateKeyUser.displayname,
                                                 stateKeyUserGender
                                               });
      }
    } else if (newState === 'ban') {
      return l10n.get('member-state-banned', { gender,
                                               stateKeyUser: stateKeyUser.displayname,
                                               stateKeyUserGender
                                             });
    }

    return ''; // not implemented
  }
}
