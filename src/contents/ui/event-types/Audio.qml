/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import moe.kazv.mxc.kazv 0.0 as MK

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types
import '..' as Kazv

Simple {
  id: upper

  property var gender: 'neutral'
  property var body: event.content.body
  property var mxcUri: event.content.url
  property var audioUri: matrixSdk.mxcUriToHttp(mxcUri)

  property var innerContentWidth: upper.contentMaxWidth - bubble.bubbleSpacing

  Types.MediaBubble {
    id: bubble

    eventContent: event.content

    ColumnLayout {
      property var msgLabel: Kazv.SelectableText {
        Layout.fillWidth: true
        wrapMode: Text.Wrap

        text: l10n.get('event-message-audio-sent', { gender, body })
      }

      data: [
        msgLabel,
        bubble.mediaFileMenu,
        bubble.progressBar
      ]
    }
  }
}
