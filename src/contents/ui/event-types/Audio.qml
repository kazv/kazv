/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import QtMultimedia 5.15

import org.kde.kirigami 2.13 as Kirigami

import '..' as Kazv

Simple {
  id: upper

  property var gender: 'neutral'
  property var body: event.content.body
  property var mxcUri: event.content.url
  property var audioUri: matrixSdk.mxcUriToHttp(mxcUri)

  property var innerContentWidth: upper.contentMaxWidth - bubble.bubbleSpacing

  Kazv.Bubble {
    id: bubble
    ColumnLayout {
      Label {
        Layout.fillWidth: false
        Layout.maximumWidth: innerContentWidth
        wrapMode: Text.Wrap

        text: l10n.get('event-message-audio-sent', { gender, body })
      }
      Audio {
        id: audio
        source: audioUri
        autoLoad: false
        autoPlay: false
      }
      Label {
        Layout.fillWidth: true
        Layout.maximumWidth: innerContentWidth
        wrapMode: Text.Wrap

        text: l10n.get('event-message-audio-play-action')
        MouseArea {
          anchors.fill: parent
          onClicked: {
            audio.play()
          }
        }
      }
    }
  }
}
