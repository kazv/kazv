/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import moe.kazv.mxc.kazv 0.0 as MK

import '..' as Kazv

Kazv.Bubble {
  id: bubble

  required property var eventContent

  property var mediaFileMenu: Kazv.MediaFileMenu {
    eventContent: bubble.eventContent
  }

  menuContent: mediaFileMenu.optionMenu.concat(additionalMenuContent)

  property var additionalMenuContent: []

  property var progressBar: Kazv.KazvIOMenu {
    id: progressBar
    kazvIOJob: mediaFileMenu.fileHandler.kazvIOJob
    jobId: bubble.mediaFileMenu.fileHandler.mediaId
    isUpload: false
    width: parent.width
  }
}
