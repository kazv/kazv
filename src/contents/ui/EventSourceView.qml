/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

ColumnLayout {
  property var event

  Label {
    text: l10n.get('event-source-decrypted')
    visible: event.encrypted
  }

  TextArea {
    objectName: 'decryptedSourceView'
    text: stringify(event.decryptedSource)
    readOnly: true
    font.family: 'monospace'
    visible: event.encrypted
    Layout.fillWidth: true
    wrapMode: TextEdit.Wrap
  }

  Label {
    text: l10n.get('event-source-original')
  }

  TextArea {
    objectName: 'originalSourceView'
    text: stringify(event.originalSource)
    readOnly: true
    font.family: 'monospace'
    Layout.fillWidth: true
    wrapMode: TextEdit.Wrap
  }

  function stringify(json) {
    return JSON.stringify(json, null, 2);
  }
}
