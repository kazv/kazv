/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.13 as Kirigami

import moe.kazv.mxc.kazv 0.0 as MK

ColumnLayout {
  id: kazvIOProgressBar

  required property var kazvIOJob
  required property var jobId
  required property bool isUpload

  property var progress: kazvIOJob ? kazvIOJob.progress : 0

  visible: kazvIOJob ? true : false

  onKazvIOJobChanged: {
    if (kazvIOJob == null) {
      progressBarLayout.visible = true
      resultMsgLayout.visible = false
      return
    }
    if (kazvIOJob.isResulted()) {
      jobSlots.onResult(kazvIOJob.error())
      return
    }
    if (kazvIOJob.isSuspended()) {
      pauseAction.suspended = true
    }
  }
  Connections {
    id: jobSlots
    target: kazvIOJob
    function onResult(ec) {
      progressBarLayout.visible = false
      resultMsgLayout.visible = true

      if (ec == MK.KazvIOBaseJob.NoError) {
        if (isUpload) {
          return
        }
        resultMsg.text = l10n.get('kazv-io-download-success-prompt')
        return
      }

      let resultL10nId = isUpload ? 'kazv-io-upload-failure-prompt' : 'kazv-io-download-failure-prompt'
      let detailL10nId = new Map([
        [MK.KazvIOBaseJob.UserCancel, 'kazv-io-failure-detail-user-cancel'],
        [MK.KazvIOBaseJob.OpenFileError, 'kazv-io-failure-detail-open-file-error'],
        [MK.KazvIOBaseJob.WriteFileError, 'kazv-io-failure-detail-write-file-error'],
        [MK.KazvIOBaseJob.KIOError, 'kazv-io-failure-detail-network-error'],
        [MK.KazvIOBaseJob.HashError, 'kazv-io-failure-detail-hash-error'],
        [MK.KazvIOBaseJob.ResponseError, 'kazv-io-failure-detail-response-error'],
        [MK.KazvIOBaseJob.KazvError, 'kazv-io-failure-detail-kazv-error']
      ])
      resultMsg.text = l10n.get(resultL10nId, {detail: l10n.get(detailL10nId.get(ec))})
    }
  }

  Label {
    objectName: 'fileNamePrompt'
    property string l10nId: isUpload ? 'kazv-io-uploading-prompt' : 'kazv-io-downloading-prompt'
    text: kazvIOJob ? l10n.get(l10nId, {fileName: kazvIOJob.fileName()}) : ""
    wrapMode: Text.Wrap
    Layout.preferredWidth: parent.width
  }

  RowLayout {
    id: progressBarLayout
    objectName: 'progressLayout'
    visible: true
    Layout.preferredWidth: parent.width
    ProgressBar {
      id: progressBar
      objectName: 'progressBar'
      Layout.fillWidth: true
      value: progress
    }
    Label {
      id: progressLabel
      objectName: 'progressText'
      text: l10n.get("kazv-io-progress", { progress: Math.floor(progress * 100) })
    }
    Kirigami.Action {
      id: pauseAction
      text: suspended ? l10n.get("kazv-io-resume") : l10n.get("kazv-io-pause")
      icon.name: suspended ? "media-playback-start" : "media-playback-pause"
      property var suspended: false
      onTriggered: {
        if (pauseAction.suspended) {
          kazvIOJob.resume()
        } else {
          kazvIOJob.suspend()
        }
        suspended = !suspended
      }
    }
    Kirigami.Action {
      id: cancelAction
      text: l10n.get("kazv-io-cancel")
      icon.name: "dialog-cancel"
      onTriggered: {
        pauseAction.suspended = false
        kazvIOJob.cancel()
      }
    }
    RoundButton {
      id: pauseBtn
      objectName: 'pauseBtn'
      Accessible.name: pauseAction.suspended ? l10n.get('kazv-io-resume') : l10n.get('kazv-io-pause')
      icon.name: pauseAction.suspended ? "media-playback-start" : "media-playback-pause"
      onClicked: pauseAction.trigger()
    }
    RoundButton {
      objectName: 'cancelBtn'
      icon.name: "dialog-cancel"
      Accessible.name: l10n.get('kazv-io-cancel')
      onClicked: cancelAction.trigger()
    }
  }

  ColumnLayout {
    id: resultMsgLayout
    objectName: 'resultLayout'
    visible: false
    width: parent.width
    Label {
      id: resultMsg
      objectName: 'resultMsg'
      wrapMode: Text.Wrap
      Layout.preferredWidth: parent.width
    }
    Button {
      objectName: 'closeBtn'
      text: l10n.get('kazv-io-prompt-close')
      onClicked: {
        if (isUpload) {
          kazvIOManager.deleteUploadJob(jobId, kazvIOJob)
        } else {
          kazvIOManager.deleteDownloadJob(jobId)
          resultMsgLayout.visible = false
          progressBarLayout.visible = true
          resultMsg.text = null
        }
      }
    }
  }
}
