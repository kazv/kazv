/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.15
import org.kde.kirigami 2.8 as Kirigami

Kirigami.Page {
  id: loginPage
  title: l10n.get('login-page-title')
  property var isSwitchingAccount: false

  ColumnLayout {
    width: parent.width
    spacing: Kirigami.Units.largeSpacing

    ColumnLayout {
      id: restoreSessionPart
      width: parent.width
      spacing: Kirigami.Units.largeSpacing
      property var sessions: matrixSdk.allSessions().filter(s => s !== sessionNameFor(matrixSdk.userId, matrixSdk.deviceId))

      Label {
        text: l10n.get('login-page-existing-sessions-prompt')
      }

      ComboBox {
        id: sessionChooser
        Layout.fillWidth: true
        model: restoreSessionPart.sessions
        currentIndex: -1
      }

      Button {
        id: restoreButton
        text: l10n.get('login-page-restore-session-button')
        Layout.fillWidth: true
        enabled: sessionChooser.currentIndex != -1
        onClicked: {
          const sess = restoreSessionPart.sessions[sessionChooser.currentIndex];
          console.log('Selected session ', sess);
          loadSession(sess);
        }
      }

      Label {
        text: l10n.get('login-page-alternative-password-login-prompt')
      }
      visible: sessions.length > 0
    }

    Kirigami.FormLayout {
      TextField {
        id: userIdField
        objectName: 'userIdField'
        Layout.fillWidth: true
        placeholderText: l10n.get('login-page-userid-input-placeholder')
        Kirigami.FormData.label: l10n.get('login-page-userid-prompt')
      }

      Kirigami.PasswordField {
        id: passwordField
        objectName: 'passwordField'
        Layout.fillWidth: true
        Kirigami.FormData.label: l10n.get('login-page-password-prompt')
      }

      TextField {
        id: serverUrlField
        objectName: 'serverUrlField'
        Layout.fillWidth: true
        placeholderText: l10n.get('login-page-server-url-placeholder')
        Kirigami.FormData.label: l10n.get('login-page-server-url-prompt')
      }

      RowLayout {
        Layout.fillWidth: true

        property var loginButton: Button {
          id: loginButton
          objectName: 'loginButton'
          text: l10n.get('login-page-login-button')
          Layout.fillWidth: true
          onClicked: {
            console.log('Trying to login...')
            if (isSwitchingAccount) {
              matrixSdk.startNewSession();
            }
            matrixSdk.login(userIdField.text, passwordField.text, serverUrlField.text);
            console.log('Login job sent')
          }
        }

        property var closeButton: Button {
          id: closeButton
          text: l10n.get('login-page-close-button')
          Layout.fillWidth: true
          onClicked: {
            pageStack.removePage(loginPage);
          }
        }

        data: isSwitchingAccount ? [closeButton, loginButton] : [loginButton]
      }
    }
  }
}
