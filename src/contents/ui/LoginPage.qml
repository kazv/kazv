/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.15
import org.kde.kirigami 2.8 as Kirigami

Kirigami.Page {
  id: loginPage
  title: l10n.get('login-page-title')
  property var isSwitchingAccount: false

  ColumnLayout {
    width: parent.width
    spacing: Kirigami.Units.largeSpacing

    ColumnLayout {
      id: restoreSessionPart
      width: parent.width
      spacing: Kirigami.Units.largeSpacing
      property var sessions: matrixSdk.allSessions().filter(s => s !== sessionNameFor(matrixSdk.userId, matrixSdk.deviceId))

      Label {
        text: l10n.get('login-page-existing-sessions-prompt')
      }

      ComboBox {
        id: sessionChooser
        Layout.fillWidth: true
        model: restoreSessionPart.sessions
        currentIndex: -1
      }

      Button {
        id: restoreButton
        text: l10n.get('login-page-restore-session-button')
        Layout.fillWidth: true
        enabled: sessionChooser.currentIndex != -1
        onClicked: {
          const sess = restoreSessionPart.sessions[sessionChooser.currentIndex];
          console.log('Selected session ', sess);
          loadSession(sess);
        }
      }

      Label {
        text: l10n.get('login-page-alternative-password-login-prompt')
      }
      visible: sessions.length > 0
    }
    Label {
      text: l10n.get('login-page-userid-prompt')
    }
    TextField {
      id: userIdField
      Layout.fillWidth: true
      placeholderText: l10n.get('login-page-userid-input-placeholder')
    }
    Label {
      text: l10n.get('login-page-password-prompt')
    }
    Kirigami.PasswordField {
      id: passwordField
      Layout.fillWidth: true
//      onAccepted: // check if passwordField.text is valid
    }
    RowLayout {
      Layout.fillWidth: true

      property var loginButton: Button {
        id: loginButton
        text: l10n.get('login-page-login-button')
        Layout.fillWidth: true
        onClicked: {
          console.log('Trying to login...')
          if (isSwitchingAccount) {
            matrixSdk.startNewSession();
          }
          matrixSdk.login(userIdField.text, passwordField.text)
          console.log('Login job sent')
        }
      }
      property var closeButton: Button {
        id: closeButton
        text: l10n.get('login-page-close-button')
        Layout.fillWidth: true
        onClicked: {
          pageStack.removePage(loginPage);
        }
      }

      data: isSwitchingAccount ? [closeButton, loginButton] : [loginButton]
    }
  }
}
