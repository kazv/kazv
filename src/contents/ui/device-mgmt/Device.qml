/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

Kirigami.SwipeListItem {
  id: device
  property var item
  property var userId

  contentItem: RowLayout {
    ColumnLayout {
      Label {
        objectName: 'deviceIdLabel'
        text: item.deviceId
      }
      Label {
        objectName: 'displayNameLabel'
        text: item.displayName
        font: Kirigami.Theme.smallFont
        visible: !!item.displayName
      }
      Layout.fillWidth: true
    }

    RowLayout {
      Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
      Layout.rightMargin: Kirigami.Units.largeSpacing

      Kirigami.Icon {
        source: device.getTrustLevelIcon(item.trustLevel)
        Layout.preferredHeight: Kirigami.Units.iconSizes.sizeForLabels
        Layout.preferredWidth: Kirigami.Units.iconSizes.sizeForLabels
        Accessible.ignored: true
      }

      Label {
        objectName: 'trustLevelLabel'
        text: l10n.get(`device-trust-level-${item.trustLevel}`)
      }
    }
  }

  property var sheet: Kirigami.OverlaySheet {
    id: popup
    title: l10n.get('device-set-trust-level-dialog-title')
    parent: applicationWindow().overlay

    property var setTrustLevelPromise: null
    property var conn: Connections {
      target: popup.setTrustLevelPromise
      function onResolved(success, data) {
        if (!success) {
          console.log('Error setting trust level:', JSON.stringify(data));
        }
        popup.setTrustLevelPromise = null;
        popup.close();
      }
    }

    ColumnLayout {
      Label {
        text: l10n.get('device-set-trust-level-dialog-name-label', { name: item.displayName })
      }
      Label {
        text: l10n.get('device-set-trust-level-dialog-id-label', { id: item.deviceId })
      }
      Label {
        text: l10n.get('device-set-trust-level-dialog-ed25519-key-label', { key: item.ed25519Key })
      }
      Label {
        text: l10n.get('device-set-trust-level-dialog-curve25519-key-label', { key: item.curve25519Key })
      }

      ButtonGroup {
        buttons: trustLevelChoices.children
      }

      ColumnLayout {
        id: trustLevelChoices
        objectName: 'trustLevelChoices'
        Layout.fillHeight: true
        Repeater {
          model: ['blocked', 'unseen', 'seen', 'verified']
          delegate: RadioButton {
            objectName: 'trustLevelChoice_' + modelData
            property var level: modelData
            autoExclusive: true
            checked: item.trustLevel === modelData
            text: l10n.get(`device-trust-level-${modelData}`)
            icon.name: device.getTrustLevelIcon(modelData)
          }
        }
      }

      RowLayout {
        Button {
          text: l10n.get('device-set-trust-level-dialog-save')
          onClicked: {
            const current = popup.getNewTrustLevel();
            popup.setTrustLevelPromise = matrixSdk.setDeviceTrustLevel(userId, item.deviceId, current);
          }
        }

        Button {
          text: l10n.get('device-set-trust-level-dialog-cancel')
          onClicked: {
            popup.close();
          }
        }
      }
    }

    function getNewTrustLevel() {
      for (let i = 0; i < trustLevelChoices.children.length; ++i) {
        const curItem = trustLevelChoices.children[i];
        if (curItem.checked) {
          return curItem.level;
        }
      }
      return 'unknown';
    }
  }

  actions: [
    Kirigami.Action {
      text: l10n.get('device-set-trust-level')
      icon.name: 'settings-configure'
      onTriggered: {
        popup.open();
      }
    }
  ]

  function getTrustLevelIcon(level)
  {
    switch (level) {
    case 'blocked':
      return 'emblem-error';
    case 'unseen':
      return 'emblem-warning';
    case 'seen':
      return 'emblem-added';
    case 'verified':
      return 'emblem-checked';
    default:
      return 'emblem-question';
    }
  }
}
