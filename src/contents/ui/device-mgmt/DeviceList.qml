/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Controls 2.15

import '.' as KazvDM

ListView {
  id: deviceList
  property var devices
  property var userId

  model: devices ? devices.count : 0

  delegate: KazvDM.Device {
    userId: deviceList.userId
    item: devices.at(index)
  }
}
