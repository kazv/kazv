/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 nannanko <nannanko@kazv.moe>
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import org.kde.kirigami 2.13 as Kirigami

import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import '..' as Kazv

RowLayout {
  id: cacheSettings

  Label {
    text: l10n.get('settings-cache-directory')
  }

  TextField {
    Layout.fillWidth: true
    id: cacheDirectoryEntry
    text: kazvConfig.cacheDirectory
  }

  Kirigami.Action {
    id: selectCacheDirectory
    icon.name: "inode-directory"
    text: 'settings-cache-directory'
    onTriggered: {
      folderDialog.open()
    }
  }

  ToolButton {
    icon.name: 'inode-directory'
    onClicked: {
      selectCacheDirectory.trigger()
    }
  }

  Kazv.FolderDialogAdapter {
    id: folderDialog
    onAccepted: {
      cacheDirectoryEntry.text = folder
    }
  }

  function save() {
    if (cacheDirectoryEntry.text !== kazvConfig.cacheDirectory) {
      kazvConfig.cacheDirectory = cacheDirectoryEntry.text
      kazvIOManager.cacheDirectory = cacheDirectoryEntry.text
    }
  }
}
