/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '..' as Kazv

ColumnLayout {
  id: profileSettings
  property var avatarUrl: ''
  property var displayName: ''
  property var getSelfProfilePromise: undefined
  property var loaded: false

  Kazv.AvatarAdapter {
    id: avatar
    objectName: 'avatar'
    Layout.alignment: Qt.AlignHCenter
    Layout.preferredHeight: Kirigami.Units.iconSizes.enormous
    Layout.preferredWidth: Kirigami.Units.iconSizes.enormous
    mxcUri: profileSettings.avatarUrl
  }

  property var saveAvatarUrl: Kazv.AsyncHandler {
    trigger: () => {
      profileSettings.loaded = false;
      return matrixSdk.setAvatarUrl(profileSettings.avatarUrl);
    }
    onResolved: {
      if (!success) {
        showPassiveNotification(l10n.get('settings-profile-save-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
      profileSettings.loaded = true;
    }
  }

  property var uploadFileHelper: Kazv.UploadFileHelper {
    onUploaded: {
      profileSettings.avatarUrl = mxcUri;
      saveAvatarUrl.call();
    }
    onFailed: showPassiveNotification(l10n.get('kazv-io-upload-failure-prompt'))
  }

  Button {
    Layout.fillWidth: true
    text: l10n.get('settings-profile-change-avatar')
    enabled: profileSettings.loaded
    onClicked: profileSettings.uploadFileHelper.chooseFileAndUpload()
  }

  RowLayout {
    Layout.fillWidth: true
    Label {
      text: l10n.get('settings-profile-display-name')
    }

    TextField {
      Layout.fillWidth: true
      id: displayNameEntry
      objectName: 'displayNameEntry'
      text: profileSettings.displayName
      enabled: profileSettings.loaded
    }
  }


  property var getSelfPromise: Kazv.AsyncHandler {
    trigger: () => matrixSdk.getSelfProfile()
    onResolved: {
      if (!success) {
        showPassiveNotification(l10n.get('settings-profile-load-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      } else {
        profileSettings.displayName = data.displayName;
        profileSettings.avatarUrl = data.avatarUrl;
        profileSettings.loaded = true;
      }
    }
  }

  Component.onCompleted: {
    getSelfPromise.call();
  }

  property var saveDisplayName: Kazv.AsyncHandler {
    trigger: () => {
      profileSettings.loaded = false;
      return matrixSdk.setDisplayName(displayNameEntry.text);
    }
    onResolved: {
      if (!success) {
        showPassiveNotification(l10n.get('settings-profile-save-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
      profileSettings.loaded = true;
    }
  }

  function save() {
    if (displayNameEntry.text !== profileSettings.displayName) {
      saveDisplayName.call();
    }
  }
}
