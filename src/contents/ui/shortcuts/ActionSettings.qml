/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2022 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15

import '.' as KazvShortcuts

ListView {
  id: actionSettings
  property var actions
  /// shortcutsConfig: {
  ///   QStringList shortcutForAction(QString actionName)
  ///   void setShortcutForAction(QString actionName, QStringList shortcut)
  /// }
  property var shortcutsConfig

  model: actions.length

  delegate: KazvShortcuts.ActionItem {
    width: actionSettings.width
    action: actions[index]
  }

  function shortcutToSequence(str) {
    return str.split(',');
  }

  function findConflicts(shortcut) {
    const sequence = shortcutToSequence(shortcut);
    return actions.filter(a => {
      const thoseShortcuts = shortcutsConfig.shortcutForAction(a.objectName);
      for (let i = 0; i < thoseShortcuts.length; ++i) {
        const thatShortcut = thoseShortcuts[i];
        if (hasConflicts(sequence, shortcutToSequence(thatShortcut))) {
          return true;
        }
      }
      return false;
    });
  }

  function hasConflicts(first, second) {
    const smallerLen = Math.min(first.length, second.length);
    for (let i = 0; i < smallerLen; ++i) {
      if (first[i] !== second[i]) {
        return false;
      }
    }
    return true;
  }

  function setShortcutFor(action, shortcut) {
    action.shortcut = shortcut;
    shortcutsConfig.setShortcutForAction(action.objectName, [action.shortcut]);
  }

  function purgeShortcutForActions(actions, shortcut) {
    actions.forEach(a => setShortcutFor(a, ""));
  }
}
