/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2022 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQml 2.15

QtObject {
  id: currentCollection

  default property var data
  /// Children of the action collection. Will be used for action lookup.
  property var children: []
  /// Name of the group.
  property var actionGroupName: ''
  /// The actions of this ActionCollection.
  property var actions: []

  property var shortcutsConfig

  function action(name) {
    return findActionImpl(currentCollection, name);
  }

  function findActionImpl(collection, name) {
    if (!collection || !collection.actions) {
      return null
    }
    for (let i = 0; i < collection.actions.length; ++i) {
      const action = collection.actions[i];
      if (action && action.objectName === name) {
        return action;
      }
    }

    if (!collection.children) {
      return null
    }

    for (let i = 0; i < collection.children.length; ++i) {
      const child = collection.children[i];
      const tentativeRes = findActionImpl(child, name);
      if (tentativeRes) {
        return tentativeRes;
      }
    }

    return null;
  }

  function allActions() {
    return getActionsImpl(currentCollection);
  }

  function getActionsImpl(collection) {
    const res = [];

    if (collection && collection.actions) {
      for (let i = 0; i < collection.actions.length; ++i) {
        const action = collection.actions[i];
        if (action) {
          res.push(action);
        }
      }
    }

    if (collection && collection.children) {
      for (let i = 0; i < collection.children.length; ++i) {
        const child = collection.children[i];
        if (child) {
          res.push(getActionsImpl(child));
        }
      }
    }

    return res.reduce((a, c) => a.concat(c), []);
  }

  function setupShortcuts() {
    allActions().forEach((action) => {
      const name = action.objectName;
      const shortcuts = shortcutsConfig.shortcutForAction(name);
      action.shortcut = shortcuts[0] || '';
    });
  }
}
