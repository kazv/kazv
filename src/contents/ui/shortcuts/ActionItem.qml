/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.13 as Kirigami
import '.' as KazvShortcuts

Control {
  id: actionItem
  property var action

  implicitHeight: layout.implicitHeight
  implicitWidth: layout.implicitWidth

  background: Rectangle {
    color: actionItem.hovered ? Kirigami.Theme.hoverColor : Kirigami.Theme.backgroundColor
  }

  property var sheet: Kirigami.OverlaySheet {
    id: popup
    property var shortcut: ''
    property var conflictsName: []
    property var conflictingActions: []
    title: l10n.get('action-settings-shortcut-conflict-modal-title')

    ColumnLayout {
      Label {
        text: l10n.get('action-settings-shortcut-conflict', {
          shortcut: popup.shortcut,
          conflictingAction: popup.conflictsName.join('<br>'),
        })
        Layout.fillWidth: true
      }

      RowLayout {
        Button {
          text: l10n.get('action-settings-shortcut-conflict-continue')
          onClicked: {
            actionItem.setShortcut(popup.shortcut);
            actionSettings.purgeShortcutForActions(popup.conflictingActions);
            popup.close();
          }
        }

        Button {
          text: l10n.get('action-settings-shortcut-conflict-cancel')
          onClicked: {
            popup.close();
          }
        }
      }
    }
  }

  RowLayout {
    id: layout
    anchors.fill: parent
    Kirigami.Icon {
      source: action.icon.name || action.icon.source
    }

    ColumnLayout {
      Layout.alignment: Qt.AlignLeft
      Layout.fillWidth: true
      Label {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft
        text: action.text
      }

      Label {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignLeft
        text: l10n.get(
          'action-settings-shortcut-prompt', {
            shortcut: action.shortcut || l10n.get('action-settings-shortcut-none')
          }
        )
      }
    }

    RowLayout {
      Layout.alignment: Qt.AlignRight
      Layout.fillWidth: false
      Button {
        id: editButton
        objectName: 'editButton'
        text: checked ? shortcutInput.partialResult : l10n.get('action-settings-shortcut-edit-action')
        checkable: true
        onClicked: {
          if (checked) {
            shortcutInput.startRecording();
          } else {
            shortcutInput.cancel();
          }
        }
      }

      Button {
        objectName: 'removeShortcutButton'
        text: l10n.get('action-settings-shortcut-remove-action')
        onClicked: {
          setShortcut("");
        }
      }
    }

    KazvShortcuts.ShortcutInput {
      id: shortcutInput

      onFinalResult: {
        editButton.checked = false;
        const conflicts = actionSettings
              .findConflicts(sequence)
              .filter(a => a.objectName !== action.objectName);
        if (!conflicts.length) {
          setShortcut(sequence);
        } else {
          popup.shortcut = sequence;
          popup.conflictingActions = conflicts;
          popup.conflictsName = conflicts.map(a => a.text);
          popup.open();
        }
      }
    }
  }

  function setShortcut(shortcut) {
    actionSettings.setShortcutFor(action, shortcut);
  }
}
