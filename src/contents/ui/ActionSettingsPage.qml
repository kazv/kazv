/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2022 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv
import 'shortcuts' as KazvShortcuts

Kazv.ClosableScrollablePage {
  id: page
  property alias actionsToConfigure: settings.actions
  title: l10n.get('action-settings-page-title')

  KazvShortcuts.ActionSettings {
    id: settings
    shortcutsConfig: kazvConfig
  }
}
