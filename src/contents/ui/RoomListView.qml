/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import 'matrix-helpers.js' as Helpers

import '.' as Kazv
import 'room-settings' as RoomSettings

ListView {
  id: roomListView
  property var roomList
  property var roomIds: roomList.roomIds

  property var iconSize: Kirigami.Units.iconSizes.large

 //matrixSdk.currentRoomId

  Layout.fillHeight: true
  model: roomList
  Layout.minimumHeight: childrenRect.height

  currentIndex: roomIds.indexOf(sdkVars.currentRoomId)

  delegate: Kazv.RoomListViewItemDelegate {
    required property var index
    item: roomList.at(index)
    iconSize: roomListView.iconSize
  }
}
