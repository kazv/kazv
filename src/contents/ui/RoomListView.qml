/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

ListView {
  id: roomListView
  property var roomList
  property var roomIds: roomList.roomIds

  property var iconSize: Kirigami.Units.iconSizes.large

 //matrixSdk.currentRoomId

  width: parent.width
  Layout.fillHeight: true
  model: roomList.count
  Layout.minimumHeight: childrenRect.height

  currentIndex: roomIds.indexOf(sdkVars.currentRoomId)

  /* onModelChanged: currentIndex = Qt.binding(function() { */
  /*   print("current room id is: `%1`".arg(matrixSdk.currentRoomId)) */
  /*   print("index is: ", roomIds.indexOf(matrixSdk.currentRoomId)) */
  /*   return roomIds.indexOf(matrixSdk.currentRoomId) */
  /* }) */

  /* onCurrentIndexChanged: { */
  /*   root.roomPage.roomId = roomList.roomIdAt(currentIndex) */
  /* } */


  delegate: Kirigami.SwipeListItem {
    property var item: roomList.at(index)
    onClicked: {
      sdkVars.currentRoomId = item.roomId
    }
    checkable: true
    checked: sdkVars.currentRoomId == item.roomId
    autoExclusive: true

    RowLayout {
      width: parent.width

      Kirigami.Avatar {
        sourceSize.width: iconSize
        sourceSize.height: iconSize
        source: item.avatarMxcUri ? matrixSdk.mxcUriToHttp(item.avatarMxcUri) : ''
        name: item.name || item.roomId
      }

      Label {
        text: l10n.get('room-list-view-room-item-title',
                       { name: item.name, roomId: item.roomId })
        Layout.fillWidth: true
      }
    }
    actions: [
      Kirigami.Action {
        text: l10n.get("room-list-view-room-item-fav-action")
        iconName: "non-starred-symbolic"
        onTriggered: showPassiveNotification(l10n.get('room-list-view-room-item-fav-action-notification',
                                                      { name: item.name }))
      }
    ]
  }
}
