/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0 as Controls
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv

import 'l10n.js' as L10n

Kirigami.ApplicationWindow {
  id: root

  property var matrixSdk: MK.MatrixSdk {
    onLoginSuccessful: {
      switchToMainPage();
      recordLastSession();
    }
    onLoginFailed: {
      console.log("Login Failed");
      showPassiveNotification(l10n.get(errorMessage));
    }
    onSessionChanged: {
      console.log('session changed');
      reloadSdkVariables();
    }
  }

  property var sdkVars: QtObject {
    property var roomList: matrixSdk.roomList()
    property string currentRoomId: ''
  }

  property var l10nProvider: MK.L10nProvider {
  }

  property var kazvConfig: MK.KazvConfig {
  }

  function initializeL10n () {
    const availableLocales = l10nProvider.availableLocaleCodes();
    console.log('available locales:', availableLocales);
    const desiredLanguages = [Qt.uiLanguage];
    console.log('ui language is: ', desiredLanguages);
    const defaultLocale = 'en';

    const wantedLocales = L10n.negotiateLanguages(desiredLanguages, availableLocales, defaultLocale);

    console.log('wanted locales:', wantedLocales);

    const bundles = L10n.generateBundles(wantedLocales, l10nProvider.getFtlData(wantedLocales));
    const provider = new L10n.FluentProvider(bundles);

    return provider;
  }

  property var l10n: initializeL10n();

  title: l10n.get('app-title')

  globalDrawer: Kirigami.GlobalDrawer {
    title: l10n.get('global-drawer-title')
    titleIcon: "applications-graphics"
    actions: [
      Kirigami.Action {
        text: l10n.get('global-drawer-action-switch-account')
        iconName: 'system-switch-user'
        onTriggered: switchAccount()
      },
      Kirigami.Action {
        text: l10n.get('global-drawer-action-hard-logout')

        onTriggered: hardLogout()
      },
      Kirigami.Action {
        text: "Switch to main page" /// XXX debug only, should not be in production build
        iconName: "view-list-icons"

        onTriggered: switchToMainPage()
      },
      Kirigami.Action {
        text: 'Take last session'
        onTriggered: loadLastSession()
      },
      Kirigami.Action {
        text: 'Record last session'
        onTriggered: kazvConfig.lastSession = '@foobar:server/devid'
      },
      Kirigami.Action {
        text: 'Serialize session to file'
        onTriggered: matrixSdk.serializeToFile()
      }
    ]
  }

  contextDrawer: Kirigami.ContextDrawer {
    id: contextDrawer
  }

  pageStack.initialPage: Qt.resolvedUrl("LoginPage.qml")

  property var mainPage: Kazv.MainPage {
    id: mainPage
  }

  property var roomPage: Kazv.RoomPage {
    id: roomPage
  }

  property var emptyPage: Kirigami.Page {
    title: l10n.get('empty-room-page-title')
    Controls.Label {
      text: l10n.get('empty-room-page-description')
    }
  }

  function activateRoomPage() {
    roomPage.roomId = root.sdkVars.currentRoomId
    pageStack.push(roomPage)
  }

  function switchToMainPage() {
    pageStack.replace([mainPage, emptyPage])
    pageStack.currentIndex = 0
  }

  function switchToLoginPage() {
    pageStack.replace([Qt.resolvedUrl("LoginPage.qml")])
    pageStack.currentIndex = 0
  }

  function pushLoginPage() {
    pageStack.push(Qt.resolvedUrl("LoginPage.qml"), {
      isSwitchingAccount: true,
    });
  }

  function sessionNameFor(userId, deviceId) {
    return userId + '/' + deviceId;
  }

  function loadSession(sessionName) {
    const succ = matrixSdk.loadSession(sessionName);

    if (succ) {
      console.log('load session successful');
      switchToMainPage();
      recordLastSession();
    } else {
      console.log('load session failed');
    }
    return succ;
  }

  function loadLastSession() {
    console.log('last session is:', kazvConfig.lastSession);

    return loadSession(kazvConfig.lastSession);
  }

  function recordLastSession() {
    kazvConfig.lastSession = sessionNameFor(matrixSdk.userId, matrixSdk.deviceId);
  }

  function reloadSdkVariables() {
    sdkVars.roomList = matrixSdk.roomList();
    sdkVars.currentRoomId = '';
  }

  Connections {
    target: sdkVars
    function onCurrentRoomIdChanged() {
      if (sdkVars.currentRoomId.length) {
        activateRoomPage()
      }
    }
  }

  function switchAccount() {
    pushLoginPage();
  }

  function hardLogout() {
  }

  Component.onCompleted: {
    loadLastSession();
  }
}
