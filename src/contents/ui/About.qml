/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.coreaddons as CoreAddons
import moe.kazv.mxc.kazv as MK
import 'matrix-helpers.js' as Helpers

FormCard.FormCardPage {
  id: aboutPage
  title: l10n.get('about-page-title')
  property var aboutData: CoreAddons.AboutData

  property var authors: [
    {
      name: 'tusooa',
      task: l10n.get('about-author-task-maintainer'),
      emailAddress: 'tusooa@kazv.moe',
      website: 'https://blog.tusooa.xyz',
    },
    {
      name: 'nannanko',
      task: l10n.get('about-author-task-developer'),
      emailAddress: 'nannanko@kazv.moe',
    },
    {
      name: 'April Simone',
      task: l10n.get('about-author-task-developer'),
      emailAddress: 'apr3vau@outlook.com',
      website: 'https://apr3vau.github.io',
    },
  ]

  property var libraries: [
    {
      name: 'libkazv',
      website: 'https://lily-is.land/kazv/libkazv',
    },
    {
      name: 'cmark',
      website: 'https://github.com/commonmark/cmark',
    },
    {
      name: 'nlohmann::json',
      website: 'https://github.com/nlohmann/json',
    },
    {
      name: 'zug',
      website: 'https://github.com/arximboldi/zug',
    },
    {
      name: 'immer',
      website: 'https://github.com/arximboldi/immer',
    },
    {
      name: 'lager',
      website: 'https://github.com/arximboldi/lager',
    },
    {
      name: 'crypto++',
      website: 'https://github.com/weidai11/cryptopp',
    },
  ]

  FormCard.FormCard {
    Layout.topMargin: Kirigami.Units.gridUnit

    FormCard.FormTextDelegate {
      text: l10n.get('about-display-name', { version: aboutData.version })
    }

    FormCard.FormButtonDelegate {
      text: aboutData.homepage
      onClicked: Qt.openUrlExternally(aboutData.homepage)
    }

    FormCard.FormTextDelegate {
      text: l10n.get('about-short-description')
    }

    FormCard.FormTextDelegate {
      text: l10n.get('about-copyright')
    }

    FormCard.FormButtonDelegate {
      text: l10n.get('about-license')
      onClicked: Qt.openUrlExternally('https://www.gnu.org/licenses/agpl-3.0.html')
    }
  }

  FormCard.FormHeader {
    Layout.topMargin: Kirigami.Units.gridUnit
    title: l10n.get('about-used-libraries')
  }

  FormCard.FormCard {
    Repeater {
      model: Kirigami.Settings.information
      delegate: FormCard.FormTextDelegate {
        required property string modelData
        text: modelData
      }
    }

    Repeater {
      model: aboutPage.libraries
      delegate: FormCard.FormButtonDelegate {
        required property string name
        required property string website
        text: name
        description: website
        onClicked: Qt.openUrlExternally(website)
      }
    }
  }

  FormCard.FormHeader {
    Layout.topMargin: Kirigami.Units.gridUnit

    title: l10n.get('about-authors')
  }

  FormCard.FormCard {
    objectName: 'authorsCard'
    Repeater {
      model: aboutPage.authors
      delegate: FormCard.FormTextDelegate {
        id: author
        required property string name
        required property string task
        required property var modelData
        required property int index
        objectName: `authors${index}`
        text: name
        description: task
        trailing: RowLayout {
          ToolButton {
            objectName: 'emailButton'
            visible: !!author.modelData.emailAddress
            display: AbstractButton.IconOnly
            icon.name: 'mail-send'
            text: l10n.get('about-author-email-action')
            ToolTip.text: author.modelData.emailAddress
            ToolTip.delay: Kirigami.Units.toolTipDelay
            ToolTip.timeout: Helpers.toolTipTimeout
            ToolTip.visible: hovered
            onClicked: Qt.openUrlExternally('mailto:' + author.modelData.emailAddress)
          }

          ToolButton {
            objectName: 'websiteButton'
            visible: !!author.modelData.website
            display: AbstractButton.IconOnly
            icon.name: 'internet-services'
            text: l10n.get('about-author-website-action')
            ToolTip.text: author.modelData.website
            ToolTip.delay: Kirigami.Units.toolTipDelay
            ToolTip.timeout: Helpers.toolTipTimeout
            ToolTip.visible: hovered
            onClicked: Qt.openUrlExternally(author.modelData.website)
          }
        }
      }
    }
  }
}
