/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv
import 'matrix-helpers.js' as Helpers

ItemDelegate {
  id: upper
  objectName: 'bubble'
  default property var children
  property var currentEvent: event
  property var menuContent: []
  property var shouldDisplayTime: displayTime || (!compactMode && !event.isLocalEcho)
  property var eventPinned: event.eventId && pinnedEvents.eventIds.hasOwnProperty(event.eventId)
  readonly property var eventReactions: event.reactions()

  topPadding: 0
  bottomPadding: 0
  readonly property var bubbleSpacing: leftPadding + rightPadding
  readonly property double replyMaxHeight: Kirigami.Units.gridUnit * 7
  Layout.fillWidth: true

  property var backgroundItem: Rectangle {
    anchors.fill: parent
    color: (hovered || isSelected) ? Kirigami.Theme.activeBackgroundColor : Kirigami.Theme.backgroundColor
  }
  background: compactMode ? null : backgroundItem

  property var eventSourcePopupComp: Component {
    Kazv.SelfDestroyableOverlaySheet {
      shouldSelfDestroy: true
      title: l10n.get('event-source-popup-title')

      Kazv.EventSourceView {
        Layout.preferredWidth: Math.min(Kirigami.Units.gridUnit * 40, Window.width)
        event: upper.currentEvent
      }
    }
  }

  property var reactionPopupComp: Component {
    Kazv.ReactToEventPopup {
      shouldSelfDestroy: true
      onAccepted: eventView.reactWith(text)
    }
  }

  function getIsEditable(event) {
    return event.sender === matrixSdk.userId
      && event.type === 'm.room.message'
      && event.content.msgtype === 'm.text';
  }

  property var menuComp: Component {
    Menu {
      objectName: 'bubbleContextMenu'
      property list<QtObject> staticItems: [
        Kirigami.Action {
          objectName: 'replyToMenuItem'
          text: l10n.get('event-reply-action')
          onTriggered: setDraftRelation('m.in_reply_to', currentEvent.eventId)
          enabled: event && !event.redacted
        },
        Kirigami.Action {
          objectName: 'editMenuItem'
          text: l10n.get('event-edit-action')
          onTriggered: {
            setDraftRelation('m.replace', currentEvent.eventId);
            replaceDraftRequested(Helpers.getEventBodyForEditing(event));
          }
          enabled: event && !event.redacted && !event.isLocalEcho && getIsEditable(event)
        },
        Kirigami.Action {
          objectName: 'deleteMenuItem'
          text: l10n.get('event-delete')
          onTriggered: deleteEventRequested(event.isLocalEcho ? event.txnId : event.eventId, event.isLocalEcho)
          enabled: event && !event.redacted
        },
        Kirigami.Action {
          objectName: 'reactMenuItem'
          text: l10n.get('event-react-action')
          onTriggered: reactionPopupComp.createObject(applicationWindow().overlay).open()
          enabled: event && !event.redacted
        },
        Kirigami.Action {
          objectName: 'pinUnpinMenuItem'
          enabled: !event.isLocalEcho
          text: eventPinned ? l10n.get('event-unpin-action') : l10n.get('event-pin-action')
          onTriggered: eventPinned ? unpinEventRequested(event.eventId) : pinEventRequested(event.eventId)
        },
        MenuSeparator {},
        Kirigami.Action {
          text: l10n.get('event-view-source')
          onTriggered: eventSourcePopupComp.createObject(applicationWindow().overlay).open()
        },
        Kirigami.Action {
          objectName: 'viewHistoryMenuItem'
          text: l10n.get('event-view-history')
          enabled: event && !!event.isEdited
          onTriggered: viewEventHistoryRequested(event.eventId)
        }
      ]

      contentData: Array.from(staticItems).concat(upper.menuContent)
      onClosed: destroy()
    }
  }

  function shouldPopupMenu() {
    return !compactMode
  }

  function maybePopupMenu() {
    if (shouldPopupMenu()) {
      menuComp.createObject(parent).popup(parent)
    }
  }

  contentItem: RowLayout {
    id: bubbleRootLayout
    property var replyToOrAnnotatedEventId: currentEvent.replyingToEventId || (currentEvent.relationType === 'm.annotation' ? currentEvent.relatedEventId : '')

    ColumnLayout {
      Layout.fillWidth: true

      MouseArea {
        visible: bubbleRootLayout.replyToOrAnnotatedEventId && !compactMode
        Layout.fillWidth: true
        Layout.margins: 0
        Layout.preferredHeight: inReplyToLayout.implicitHeight
        Layout.maximumHeight: upper.replyMaxHeight
        clip: true

        onClicked: eventListView.goToEvent(bubbleRootLayout.replyToOrAnnotatedEventId)

        RowLayout {
          id: inReplyToLayout
          anchors {
            top: parent.top
            left: parent.left
            right: parent.right
          }
          Rectangle {
            Layout.fillHeight: true
            Layout.preferredWidth: Kirigami.Units.smallSpacing
            color: Kirigami.Theme.activeTextColor
            clip: true
          }

          Loader {
            id: inReplyToLoader
            clip: true
            source: Qt.resolvedUrl('EventViewCompact.qml')
            active: bubbleRootLayout.replyToOrAnnotatedEventId && !compactMode
            asynchronous: true
            Layout.fillWidth: true

            property var event: room.messageById(bubbleRootLayout.replyToOrAnnotatedEventId)
            property var props: ({
              event,
              sender: room.member(event.sender || matrixSdk.userId),
              stateKeyUser: event.stateKey ? room.member(event.stateKey) : {},
              isGapped: timeline.gaps.hasOwnProperty(event.eventId),
            })
          }
        }
      }

      ColumnLayout {
        id: children
        data: upper.children
      }

      Kazv.EventReactions {
        objectName: 'eventReactions'
        visible: !compactMode
        Layout.fillWidth: true
        reactions: upper.eventReactions
      }
    }

    property var popupAction: Kirigami.Action {
      id: popupAction
      text: l10n.get('event-popup-action')
      icon.name: 'view-more-horizontal-symbolic'
      onTriggered: maybePopupMenu()
    }

    RowLayout {
      Layout.alignment: Qt.AlignBottom
      Kirigami.Icon {
        source: 'emblem-encrypted-locked'
        visible: !compactMode && event && event.encrypted
        Layout.preferredHeight: inlineBadgeSize
        Layout.preferredWidth: inlineBadgeSize
        Accessible.role: Accessible.StaticText
        Accessible.name: l10n.get('event-encrypted')
        ToolTip.text: l10n.get('event-encrypted')
        ToolTip.delay: Kirigami.Units.toolTipDelay
        ToolTip.timeout: Helpers.toolTipTimeout
        ToolTip.visible: hoverHandlerEncIcon.hovered

        HoverHandler { id: hoverHandlerEncIcon }
      }

      ToolButton {
        objectName: 'localEchoIndicator'
        visible: !!event.isLocalEcho
        hoverEnabled: true
        enabled: event.isFailed
        icon.name: event.isSending ? 'state-sync' : 'state-warning'
        Layout.preferredHeight: Kirigami.Units.iconSizes.medium
        Layout.preferredWidth: Kirigami.Units.iconSizes.medium
        Accessible.role: event.isSending ? Accessible.StaticText : Accessible.Button
        Accessible.name: event.isSending ? l10n.get('event-sending') : l10n.get('event-resend')
        ToolTip.text: event.isSending ? l10n.get('event-sending') : l10n.get('event-send-failed')
        ToolTip.delay: Kirigami.Units.toolTipDelay
        ToolTip.timeout: Helpers.toolTipTimeout
        ToolTip.visible: hovered

        onClicked: {
          room.resendMessage(event.txnId);
        }
      }

      Label {
        objectName: 'timeIndicator'
        visible: shouldDisplayTime
        text: event.formattedTime
        ToolTip.text: event.formattedDateTime
        ToolTip.delay: Kirigami.Units.toolTipDelay
        ToolTip.timeout: Helpers.toolTipTimeout
        ToolTip.visible: hoverHandlerTime.hovered
        HoverHandler { id: hoverHandlerTime }
        font: Kirigami.Theme.smallFont
      }

      Label {
        objectName: 'editedIndicator'
        visible: !compactMode && event.isEdited
        text: l10n.get('event-edited-indicator')
        font: Kirigami.Theme.smallFont
      }

      Kazv.EventReadIndicator {
        objectName: 'eventReadIndicator'
        shouldShow: !compactMode && !event.isLocalEcho
        model: event.readers()
      }

      ToolButton {
        id: moreButton
        objectName: 'moreButton'
        visible: !compactMode
        action: popupAction
        display: AbstractButton.IconOnly
      }
    }
  }
}
