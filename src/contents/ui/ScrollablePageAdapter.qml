/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ScrollablePage {
  id: scrollablePage
  property alias contextualActions: scrollablePage.actions
}
