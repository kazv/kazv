/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 6.6
import QtQuick.Controls 2.15
import moe.kazv.mxc.kazv 0.0 as MK

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

Kirigami.ScrollablePage {
  title: l10n.get('main-page-title', { userId: matrixSdk.userId })

  property var currentTagId: ''

  onCurrentTagIdChanged: sdkVars.roomList.setTagId(currentTagId)

  header: ColumnLayout {
    spacing: 0

    TextField {
      text: sdkVars.roomList.filter
      onTextChanged: sdkVars.roomList.filter = text
      placeholderText: l10n.get('main-page-room-filter-prompt')
      Layout.fillWidth: true
    }

    TabBar {
      Layout.fillWidth: true
      TabButton {
        icon.name: 'clock'
        text: l10n.get('main-page-recent-tab-title')
        onClicked: currentTagId = ''
        checkable: true
        checked: currentTagId === ''
      }

      TabButton {
        icon.name: 'non-starred-symbolic'
        text: l10n.get('main-page-favourites-tab-title')
        onClicked: currentTagId = 'm.favourite'
        checkable: true
        checked: currentTagId === 'm.favourite'
      }
    }
  }

  Kazv.RoomListView {
    roomList: sdkVars.roomList
    anchors.fill: parent
  }
}
