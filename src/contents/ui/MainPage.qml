/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

Kazv.TabView {
  controlAtBottom: false
  title: l10n.get('main-page-title', { userId: matrixSdk.userId })

  Kazv.Tab {
    iconName: "clock"
    title: l10n.get('main-page-recent-tab-title')
    width: parent.width
    height: parent.height
    Kazv.RoomListView {
      roomList: sdkVars.roomList
      anchors.fill: parent
    }
  }
  Kazv.Tab {
    iconName: "user-identity"
    title: l10n.get('main-page-people-tab-title')
    Label {
      text: "people"
    }
  }
  Kazv.Tab {
    iconName: "comment-symbolic"
    title: l10n.get('main-page-rooms-tab-title')
    Label {
      text: "rooms"
    }
  }
}
