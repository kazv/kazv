/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import '.' as Kazv

ListView {
  id: roomTimelineView
  objectName: 'roomTimelineView'
  property var timeline

  property string selectedEventId

  signal pinEventRequested(string eventId)
  signal unpinEventRequested(string eventId)
  signal deleteEventRequested(eventIdOrTxnId: string, isLocalEcho: bool)
  signal deleteEventRequestedNoConfirm(eventIdOrTxnId: string, isLocalEcho: bool)

  property var pinEvent: Kazv.AsyncHandler {
    property var eventIds
    trigger: () => room.pinEvents(eventIds)
    onResolved: (success, data) => {
      if (success) {
        showPassiveNotification(l10n.get('event-pin-success-prompt'));
      } else {
        showPassiveNotification(l10n.get('event-pin-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  property var unpinEvent: Kazv.AsyncHandler {
    property var eventIds
    trigger: () => room.unpinEvents(eventIds)
    onResolved: (success, data) => {
      if (success) {
        showPassiveNotification(l10n.get('event-unpin-success-prompt'));
      } else {
        showPassiveNotification(l10n.get('event-unpin-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  property var deleteEvent: Kazv.AsyncHandler {
    property var eventId
    property var reason
    trigger: () => room.redactEvent(eventId, reason)
    onResolved: (success, data) => {
      if (!success) {
        showPassiveNotification(l10n.get('event-delete-failed', { errorMsg: data.error, errorCode: data.errorCode }));
      }
    }
  }

  property var deleteLocalEcho: Kazv.AsyncHandler {
    property var txnId
    trigger: () => room.removeLocalEcho(txnId)
    onResolved: (success, data) => {
      if (!success) {
        showPassiveNotification(l10n.get('event-delete-failed', { errorMsg: data.error, errorCode: data.errorCode }));
      }
    }
  }

  onPinEventRequested: (eventId) => {
    pinEventPopupComp.createObject(Overlay.overlay, { eventId }).open()
  }

  onUnpinEventRequested: (eventId) => {
    unpinEventPopupComp.createObject(Overlay.overlay, { eventId }).open()
  }

  onDeleteEventRequested: (eventIdOrTxnId, isLocalEcho) => {
    confirmDeletionPopupComp.createObject(Overlay.overlay, { eventIdOrTxnId, isLocalEcho }).open()
  }

  onDeleteEventRequestedNoConfirm: (eventIdOrTxnId, isLocalEcho) => {
    doDeleteEvent(eventIdOrTxnId, isLocalEcho);
  }

  function doDeleteEvent(eventIdOrTxnId, isLocalEcho) {
    if (isLocalEcho) {
      deleteLocalEcho.txnId = eventIdOrTxnId;
      deleteLocalEcho.call();
    } else {
      deleteEvent.eventId = eventIdOrTxnId;
      deleteEvent.call();
    }
  }

  property var pinEventPopupComp: Component {
    Kazv.ConfirmationOverlay {
      objectName: 'pinEventPopup'
      property var eventId
      shouldSelfDestroy: true
      title: l10n.get('event-pin-confirmation-title')
      message: l10n.get('event-pin-confirmation')
      confirmActionText: l10n.get('event-pin-confirm-action')
      cancelActionText: l10n.get('event-pin-cancel-action')
      onAccepted: {
        pinEvent.eventIds = [eventId];
        pinEvent.call();
      }

      EventViewWrapper {
        Layout.fillWidth: true
        event: room.messageById(eventId)
        compactMode: true
      }
    }
  }

  property var unpinEventPopupComp: Component {
    Kazv.ConfirmationOverlay {
      objectName: 'unpinEventPopup'
      property var eventId
      shouldSelfDestroy: true
      title: l10n.get('event-unpin-confirmation-title')
      message: l10n.get('event-unpin-confirmation')
      confirmActionText: l10n.get('event-unpin-confirm-action')
      cancelActionText: l10n.get('event-unpin-cancel-action')
      onAccepted: {
        unpinEvent.eventIds = [eventId];
        unpinEvent.call();
      }

      EventViewWrapper {
        Layout.fillWidth: true
        event: room.messageById(eventId)
        compactMode: true
      }
    }
  }

  property var confirmDeletionPopupComp: Component {
    Kazv.ConfirmationOverlay {
      objectName: 'confirmDeletionPopup'
      property string eventIdOrTxnId
      property bool isLocalEcho
      shouldSelfDestroy: true
      title: l10n.get('confirm-deletion-popup-title')
      message: l10n.get('confirm-deletion-popup-message')
      confirmActionText: l10n.get('confirm-deletion-popup-confirm-action')
      cancelActionText: l10n.get('confirm-deletion-popup-cancel-action')
      onAccepted: doDeleteEvent(eventIdOrTxnId, isLocalEcho)

      EventViewWrapper {
        id: eventView
        Layout.fillWidth: true
        event: isLocalEcho ? room.localEchoById(eventIdOrTxnId) : room.messageById(eventIdOrTxnId)
        compactMode: true
      }
    }
  }

  spacing: 0

  model: timeline

  currentIndex: -1

  verticalLayoutDirection: ListView.BottomToTop

  delegate: EventViewWrapper {
    property var eventListView: ListView.view
    prevEvent: index < timeline.count - 1 ? timeline.at(index + 1) : undefined
    event: timeline.at(index)
    width: ListView.view.width
    isSelected: roomTimelineView.selectedEventId && event.eventId === roomTimelineView.selectedEventId
  }

  function goToEvent(eventId) {
    selectedEventId = eventId;
    const index = timeline.indexOfEvent(eventId);
    positionViewAtIndex(index, ListView.Center);
  }
}
