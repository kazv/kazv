/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

ListView {
  id: roomTimelineView
  property var timeline

  property var eventIds: timeline.eventIds

  property string selectedEventId

  spacing: Kirigami.Units.largeSpacing

  //width: parent.width
  //Layout.fillHeight: true
  anchors.fill: parent
  model: timeline.count
  //Layout.minimumHeight: childrenRect.height

  currentIndex: -1
  //currentIndex: eventIds.indexOf(selectedEventId)

  //onModelChanged: currentIndex = Qt.binding(function() { return eventIds.indexOf(selectedEventId) })

  delegate: EventView {
    event: timeline.at(index)
    sender: room.member(event.sender)
    stateKeyUser: event.stateKey ? room.member(event.stateKey) : {}
  }


/*   Kirigami.BasicListItem { */
/*     property var item: timeline.at(index) */
/*     //onClicked: roomTimelineView.selectedEventId = item.eventId */
/*     onClicked: {} */
/*     checkable: false */
/*     checked: false */
/*     //onClicked: print("clicked room %1".arg(index)) */
/*     //checkable: true */
/*     //checked: roomTimelineView.selectedEventId == item.eventId */
/*     //autoExclusive: true */
/*     Label { */
/*       text: i18n("Message from %1 of type %2: %3", item.sender, item.type, item.content) */
/*     } */
/* /\*    actions: [ */
/*       Kirigami.Action { */
/*         text: i18n("Set as favourite") */
/*         iconName: "non-starred-symbolic" */
/*         onTriggered: showPassiveNotification(i18n("Set %1 as favourite", item.name)) */
/*       } */
/*     ]*\/ */
  /* } */
}
