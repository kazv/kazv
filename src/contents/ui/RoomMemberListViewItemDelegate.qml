/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

Kirigami.SwipeListItem {
  id: roomMemberListViewItemDelegate
  property var member
  property var nameProvider: Kazv.UserNameProvider {
    user: member
  }
  property var displayName: nameProvider.name
  property var iconSize

  contentItem: RowLayout {
    Kazv.AvatarAdapter {
      mxcUri: member.avatarMxcUri
      name: displayName
      Layout.preferredWidth: iconSize
    }

    ColumnLayout {
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignLeft
      Label {
        text: displayName
        Layout.alignment: Qt.AlignLeft
        Layout.fillWidth: true
      }
      Label {
        text: member.userId
        Layout.alignment: Qt.AlignLeft
        Layout.fillWidth: true
      }
    }
  }

  onClicked: {
    activateUserPage(member, room);
  }
}
