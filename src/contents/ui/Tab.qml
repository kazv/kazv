/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv

Rectangle {
  property string iconName
  property string title
  color: Kirigami.Theme.backgroundColor
  anchors.fill: parent
}
