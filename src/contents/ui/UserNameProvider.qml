/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import 'matrix-helpers.js' as Helpers

QtObject {
  property var user
  property var overridedName: getOverridedName(user)
  property var name: getName(user)

  function getName(u) {
    if (!u) {
      return '';
    }

    return Helpers.userDisplayName(
      u.name,
      u.userId,
      sdkVars.userGivenNicknameMap.map,
      l10n,
    );
  }

  function getOverridedName(u) {
    if (!u) {
      return '';
    }
    return sdkVars.userGivenNicknameMap.map[u.userId] || '';
  }
}
