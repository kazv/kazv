/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Kazv
import 'settings' as Settings

Kazv.ClosableScrollablePage {
  id: settingsPage
  title: l10n.get('settings-page-title')

  contextualActions: [
    Kirigami.Action {
      icon.name: 'document-save'
      text: l10n.get('settings-save')
      onTriggered: { settingsPage.saveRecursively() }
    }
  ]

  ColumnLayout {
    Settings.ProfileSettings {
      id: profileSettings
      Layout.fillWidth: true
    }
    Settings.CacheSettings {
      id: cacheSettings
      Layout.fillWidth: true
    }
  }

  function saveRecursively() {
    profileSettings.save();
    cacheSettings.save();
  }
}
