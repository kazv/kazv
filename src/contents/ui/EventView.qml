/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import Qt.labs.qmlmodels 1.0

import org.kde.kirigami 2.13 as Kirigami

import 'event-types' as Types

Item {
  id: eventView
  property var event
  property var sender
  property var stateKeyUser

  property var messageType: getMessageType(event)

  property var iconSize: Kirigami.Units.iconSizes.large
  property var inlineBadgeSize: Kirigami.Units.iconSizes.smallMedium
  property var senderNameSize: Kirigami.Units.gridUnit * 1.2

  height: loader.item.implicitHeight
  anchors.left: parent.left
  anchors.right: parent.right
  //padding: Kirigami.Units.largeSpacing

  Component {
    id: textEV
    Types.Text {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: emoteEV
    Types.Emote {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: noticeEV
    Types.Notice {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: stateEV
    Types.State {
      event: eventView.event
      sender: eventView.sender
      stateKeyUser: eventView.stateKeyUser
    }
  }

  Component {
    id: imageEV
    Types.Image {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: fileEV
    Types.File {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: videoEV
    Types.Video {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: audioEV
    Types.Audio {
      event: eventView.event
      sender: eventView.sender
    }
  }

  Component {
    id: unknownEV
    Label {
      text: 'unknown event'
    }
  }

  /* ColumnLayout { */
  /*   id: layout */
  /*   anchors.left: eventView.left */
  /*   anchors.right: eventView.right */
  /* } */

  Loader {
    sourceComponent: getSource(messageType)
    id: loader
    /* onLoaded: { */
    /*   item.parent = layout; */
    /* } */
    anchors.left: eventView.left
    anchors.right: eventView.right
  }


  function getSource(t) {
    switch (t) {
    case 'text':
      return textEV;

    case 'emote':
      return emoteEV;

    case 'notice':
      return noticeEV;

    case 'state':
      return stateEV;

    case 'image':
      return imageEV;

    case 'file':
      return fileEV;

    case 'video':
      return videoEV;

    case 'audio':
      return audioEV;

    default:
      return unknownEV;
    }
  }

  function getMessageType(e) {
    if (e.isState) {
      return 'state';
    }
    switch (e.type) {
    case 'm.room.message':
      switch (e.content.msgtype) {
      case 'm.text':
        console.log('msg type=text');
        return 'text';

      case 'm.emote':
        return 'emote';

      case 'm.notice':
        return 'notice';

      case 'm.image':
        return 'image';

      case 'm.file':
        return 'file';

      case 'm.audio':
        return 'audio';

      case 'm.video':
        return 'video';

      case 'm.location':
        return 'location';

      default:
        console.log('msg type=unknown');
        return 'unknown';
      }

    default:
      console.log('msg type=unknown');
      return 'unknown';
    }
  }
}
