/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami


import '.' as Kazv


Kirigami.ScrollablePage {
  id: page
  background: Rectangle {
    anchors.fill: parent
    color: Kirigami.Theme.backgroundColor
  }
  default property list<Item> tabs

  property var controlAtBottom: true

  /*StackLayout {
    id: stackLayout
    Layout.fillHeight: true
    anchors.fill: parent
    currentIndex: bar.currentIndex
    Repeater {
      model: page.tabs
      delegate: Item {
        Layout.fillHeight: true
        data: modelData
      }
    }
  }*/
  contentChildren: tabs[bar.currentIndex]

  readonly property var control: RowLayout {
    id: bar
    Layout.fillWidth: true
    Layout.fillHeight: false
    property int currentIndex: 0

    property Component delegate: ToolButton {
      Layout.fillWidth: true
      text: modelData.title
      icon.name: modelData.iconName
      autoExclusive: true
      onClicked: {
        bar.currentIndex = index
      }
      display: AbstractButton.TextUnderIcon
      checkable: true
      checked: index == bar.currentIndex
    }

    Repeater {
      model: tabs
      delegate: bar.delegate
    }
  }


  footer: page.controlAtBottom ? control : null
  header: page.controlAtBottom ? null : control
}
