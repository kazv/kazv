/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami


import '.' as Kazv


Kirigami.ScrollablePage {
  id: page
  background: Rectangle {
    anchors.fill: parent
    color: Kirigami.Theme.backgroundColor
  }
  default property list<Item> tabs

  property var controlAtBottom: true

  /*StackLayout {
    id: stackLayout
    Layout.fillHeight: true
    anchors.fill: parent
    currentIndex: bar.currentIndex
    Repeater {
      model: page.tabs
      delegate: Item {
        Layout.fillHeight: true
        data: modelData
      }
    }
  }*/
  contentChildren: tabs[bar.currentIndex]

  readonly property var control: RowLayout {
    id: bar
    Layout.fillWidth: true
    Layout.fillHeight: false
    property int currentIndex: 0

    property Component delegate: ToolButton {
      Layout.fillWidth: true
      text: modelData.title
      icon.name: modelData.iconName
      autoExclusive: true
      onClicked: {
        bar.currentIndex = index
      }
      display: AbstractButton.TextUnderIcon
      checkable: true
      checked: index == bar.currentIndex
    }

    Repeater {
      model: tabs
      delegate: bar.delegate
    }
  }


  footer: page.controlAtBottom ? control : null
  header: page.controlAtBottom ? null : control
}
