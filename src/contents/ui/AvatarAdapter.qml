/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import org.kde.kirigamiaddons.components 1.0 as KAC
import '.' as Kazv

KAC.Avatar {
  id: avatar
  property string mxcUri
  readonly property var manager: kazvIOManager
  readonly property var sdk: matrixSdk
  source: fileHandler?.localFile || ''

  property var fileHandlerComp: Component {
    Kazv.FileHandler {
      autoCache: true
      kazvIOManager: avatar.manager
      matrixSdk: avatar.sdk
    }
  }
  property var fileHandler: null

  onMxcUriChanged: {
    if (fileHandler) {
      fileHandler.destroy();
      fileHandler = null;
    }

    fileHandler = fileHandlerComp.createObject(avatar, {
      eventContent: ({ url: avatar.mxcUri }),
    });
  }
}
