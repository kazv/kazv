/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import '.' as Kazv

Kazv.ClosableScrollablePage {
  id: roomPinnedEventsPage

  property var room
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomPinnedEventsPage.room
  }
  property var pinnedEvents: room.pinnedEventsTimeline()

  title: l10n.get('room-pinned-events-page-title', { room: roomNameProvider.name })

  RoomTimelineView {
    timeline: roomPinnedEventsPage.pinnedEvents
  }
}
