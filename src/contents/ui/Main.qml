/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import org.kde.kirigami as Kirigami
import QtQuick.Layouts
import QtQuick.Controls
import moe.kazv.mxc.kazv as MK

import '.' as Kazv
import 'shortcuts' as KazvShortcuts

import 'l10n.js' as L10n

Kirigami.ApplicationWindow {
  id: root

  property var kazvIOManager: MK.KazvIOManager {
    cacheDirectory: kazvConfig.cacheDirectory
  }

  property var matrixSdk: MK.MatrixSdk {
    onLoginSuccessful: {
      switchToMainPage();
      recordLastSession();
    }
    onLoginFailed: {
      console.log("Login Failed");
      showPassiveNotification(l10n.get('login-page-request-failed-prompt', { errorCode, errorMsg }));
    }
    onDiscoverFailed: {
      console.log("Discover Failed");
      showPassiveNotification(l10n.get('login-page-discover-failed-enter-prompt', { errorCode, errorMsg }));
    }
    onSessionChanged: {
      console.log('session changed');
      reloadSdkVariables();
    }
    onLogoutSuccessful: {
      root.loggedIn = false;
      root.mainPage = Qt.createComponent("MainPage.qml").createObject(root);
      pageStack.clear();
      pushLoginPage();
    }
    onLogoutFailed: {
      console.warn('Logout failed');
      showPassiveNotification(l10n.get('logout-failed-prompt', { errorCode, errorMsg }));
    }
  }

  property var loggedIn: !!matrixSdk.token

  property var sdkVars: QtObject {
    property var roomList: matrixSdk.roomList()
    property var userGivenNicknameMap: matrixSdk.userGivenNicknameMap()
    property string currentRoomId: ''
  }

  property var l10nProvider: MK.L10nProvider {
  }

  property var kazvConfig: MK.KazvConfig {
  }

  function initializeL10n () {
    const availableLocales = l10nProvider.availableLocaleCodes();
    console.log('available locales:', availableLocales);
    const desiredLanguages = [Qt.uiLanguage];
    console.log('ui language is: ', desiredLanguages);
    const defaultLocale = 'en';

    const wantedLocales = L10n.negotiateLanguages(desiredLanguages, availableLocales, defaultLocale);

    console.log('wanted locales:', wantedLocales);

    const bundles = L10n.generateBundles(wantedLocales, l10nProvider.getFtlData(wantedLocales));
    const provider = new L10n.FluentProvider(bundles);

    return provider;
  }

  property var l10n: initializeL10n();

  property var confirmLogoutPopup: Kirigami.OverlaySheet {
    id: confirmLogoutPopup
    title: l10n.get('confirm-logout-popup-title')
    ColumnLayout {
      Label {
        Layout.fillWidth: true
        text: l10n.get('confirm-logout-popup-prompt')
      }
      RowLayout {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignCenter
        Button {
          text: l10n.get('confirm-logout-popup-accept-button')
          onClicked: {
            hardLogout();
            confirmLogoutPopup.close();
          }
        }
        Button {
          text: l10n.get('confirm-logout-popup-cancel-button')
          onClicked: confirmLogoutPopup.close();
        }
      }
    }
  }

  title: l10n.get('app-title')

  globalDrawer: Kirigami.GlobalDrawer {
    title: l10n.get('global-drawer-title')
    titleIcon: "applications-graphics"
    property var actionGroupName: 'global-drawer-actions'
    actions: [
      Kirigami.Action {
        objectName: 'switch-account'
        text: l10n.get('global-drawer-action-switch-account')
        icon.name: 'system-switch-user'
        onTriggered: switchAccount()
      },
      Kirigami.Action {
        objectName: 'hard-logout'
        text: l10n.get('global-drawer-action-hard-logout')
        icon.name: 'application-exit'
        onTriggered: confirmLogoutPopup.open()
      },
      Kirigami.Action {
        objectName: 'save'
        text: l10n.get('global-drawer-action-save-session')
        icon.name: 'document-save'
        onTriggered: {
          recordLastSession()
          matrixSdk.serializeToFile()
        }
      },
      Kirigami.Action {
        objectName: 'configure-shortcuts'
        text: l10n.get('global-drawer-action-configure-shortcuts')
        icon.name: 'configure-shortcuts'
        onTriggered: {
          pushActionSettingsPage();
        }
      },
      Kirigami.Action {
        objectName: 'settings'
        text: l10n.get('global-drawer-action-settings')
        icon.name: 'configure'
        onTriggered: {
          pushSettingsPage();
        }
      },
      Kirigami.Action {
        objectName: 'create-room'
        enabled: root.loggedIn
        icon.name: 'contact-new'
        text: l10n.get('global-drawer-action-create-room')
        onTriggered: {
          pushCreateRoomPage();
        }
      },
      Kirigami.Action {
        objectName: 'join-room'
        enabled: root.loggedIn
        text: l10n.get('global-drawer-action-join-room')
        icon.name: 'list-add-user'
        onTriggered: {
          pushJoinRoomPage();
        }
      },
      Kirigami.Action {
        objectName: 'aboutAction'
        text: l10n.get('global-drawer-action-about')
        icon.name: 'help-about'
        onTriggered: pageStack.pushDialogLayer(Qt.resolvedUrl('About.qml'))
      }
    ]
  }

  contextDrawer: Kirigami.ContextDrawer {
    id: contextDrawer
  }

  property var actionCollection: KazvShortcuts.ActionCollection {
    id: globalActionCollection
    children: [
      root.globalDrawer,
      root.contextDrawer,
    ]
    shortcutsConfig: kazvConfig
  }

  property var notifier: Kazv.Notifier {
  }

  pageStack.initialPage: Qt.resolvedUrl("LoginPage.qml")

  property var mainPage: Kazv.MainPage {}

  property var emptyPage: Kirigami.Page {
    title: l10n.get('empty-room-page-title')
    Label {
      text: l10n.get('empty-room-page-description')
    }
  }

  signal switchToRoomRequested(string roomId)
  property var pageManager: Kazv.PageManager {
    pageStack: root.pageStack
    sdkVars: root.sdkVars
    main: root
  }

  function activateRoomSettingsPage(room) {
    pageStack.push(Qt.resolvedUrl("room-settings/RoomSettingsPage.qml"), {
      room
    });
  }

  function activateRoomPinnedEventsPage(room) {
    pageStack.push(Qt.resolvedUrl('RoomPinnedEventsPage.qml'), {
      room
    });
  }

  function activateRoomInvitePage(room) {
    pageStack.push(Qt.resolvedUrl("room-settings/RoomInvitePage.qml"), {
      room
    });
  }

  function activateRoomStickerPacksPage(room) {
    pageStack.push(Qt.resolvedUrl("room-settings/RoomStickerPacksPage.qml"), {
      room
    });
  }

  function activateUserPage(user, room, userId) {
    pageStack.push(Qt.resolvedUrl("UserPage.qml"), {
      userId: userId || user.userId,
      user,
      room
    });
  }

  function switchToMainPage() {
    pageStack.replace([root.mainPage])
    pageStack.currentIndex = 0
  }

  function switchToLoginPage() {
    pageStack.replace([Qt.resolvedUrl("LoginPage.qml")])
    pageStack.currentIndex = 0
  }

  function pushLoginPage() {
    pageStack.push(Qt.resolvedUrl("LoginPage.qml"), {
      isSwitchingAccount: true,
    });
  }

  function pushActionSettingsPage() {
    pageStack.push(Qt.resolvedUrl("ActionSettingsPage.qml"), {
      actionsToConfigure: globalActionCollection.allActions(),
    });
  }

  function pushSettingsPage() {
    pageStack.push(Qt.resolvedUrl("SettingsPage.qml"));
  }

  function pushCreateRoomPage() {
    pageStack.push(Qt.resolvedUrl("CreateRoomPage.qml"), {});
  }

  function pushJoinRoomPage() {
    pageStack.push(Qt.resolvedUrl("JoinRoomPage.qml"), {});
  }

  function sessionNameFor(userId, deviceId) {
    return userId + '/' + deviceId;
  }

  function getSessionLoadError(res, sessionName) {
    switch (res) {
    case MK.MatrixSdk.SessionNotFound:
      return l10n.get('session-load-failure-not-found', { sessionName });

    case MK.MatrixSdk.SessionFormatUnknown:
      return l10n.get('session-load-failure-format-unknown', { sessionName });

    case MK.MatrixSdk.SessionCannotBackup:
      return l10n.get('session-load-failure-cannot-backup', { sessionName });

    case MK.MatrixSdk.SessionLockFailed:
      return l10n.get('session-load-failure-lock-failed', { sessionName });

    case MK.MatrixSdk.SessionCannotOpenFile:
      return l10n.get('session-load-failure-cannot-open-file', { sessionName });

    case MK.MatrixSdk.SessionDeserializeFailed:
      return l10n.get('session-load-failure-deserialize-failed', { sessionName });

    default:
      console.error('loadSession: Encountered an unknown error code');
    }
  }

  function loadSession(sessionName) {
    const res = matrixSdk.loadSession(sessionName);

    if (res === MK.MatrixSdk.SessionLoadSuccess) {
      console.log('load session successful');
      switchToMainPage();
      recordLastSession();
    } else {
      showPassiveNotification(getSessionLoadError(res, sessionName));
    }
    return res;
  }

  function loadLastSession() {
    console.log('last session is:', kazvConfig.lastSession);

    return loadSession(kazvConfig.lastSession);
  }

  function recordLastSession() {
    kazvConfig.lastSession = sessionNameFor(matrixSdk.userId, matrixSdk.deviceId);
  }

  function reloadSdkVariables() {
    sdkVars.roomList = matrixSdk.roomList();
    sdkVars.userGivenNicknameMap = matrixSdk.userGivenNicknameMap();
    sdkVars.currentRoomId = '';
  }

  function switchAccount() {
    pushLoginPage();
  }

  function hardLogout() {
    matrixSdk.logout();
  }

  Component.onCompleted: {
    actionCollection.setupShortcuts();
    loadLastSession();
  }

  onClosing: {
    kazvIOManager.clearJobs()
  }
}
