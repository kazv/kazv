/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.20 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv

Kazv.SelfDestroyableOverlaySheet {
  id: addStickerPopup

  property var stickerPackList
  property var event

  readonly property var availablePacks: stickerPackList.packs
  readonly property var packNameProvider: Kazv.StickerPackNameProvider {
  }
  readonly property var availablePackNames: availablePacks.map(k => packNameProvider.getName(k))
  readonly property alias packIndex: packChooser.currentIndex
  property var currentPack: stickerPackList.at(packIndex)
  property var stickerSize: Kirigami.Units.iconSizes.enormous
  property var addingSticker: false

  title: l10n.get('add-sticker-popup-title')

  ColumnLayout {
    Kirigami.FormLayout {
      Layout.fillWidth: true

      Kirigami.Icon {
        Layout.preferredHeight: addStickerPopup.stickerSize
        Layout.preferredWidth: addStickerPopup.stickerSize
        height: addStickerPopup.stickerSize
        width: addStickerPopup.stickerSize
        source: matrixSdk.mxcUriToHttp(event.content.url)
      }

      ComboBox {
        id: packChooser
        objectName: 'packChooser'
        Layout.fillWidth: true
        model: addStickerPopup.availablePackNames
        currentIndex: 0
        Kirigami.FormData.label: l10n.get('add-sticker-popup-pack-prompt')
      }

      TextField {
        Layout.fillWidth: true
        id: shortCodeInput
        objectName: 'shortCodeInput'
        readOnly: addStickerPopup.addingSticker
        Kirigami.FormData.label: l10n.get('add-sticker-popup-short-code-prompt')
      }

      Label {
        objectName: 'shortCodeExistsWarning'
        visible: currentPack.hasShortCode(shortCodeInput.text)
        text: l10n.get('add-sticker-popup-short-code-exists-warning')
      }
    }

    RowLayout {
      Layout.alignment: Qt.AlignRight
      Button {
        objectName: 'addStickerButton'
        text: l10n.get('add-sticker-popup-add-sticker-button')
        onClicked: addStickerPopup.updateStickerPack.call()
        enabled: !addStickerPopup.addingSticker
      }

      Button {
        objectName: 'cancelButton'
        text: l10n.get('add-sticker-popup-cancel-button')
        onClicked: addStickerPopup.close()
      }
    }
  }

  property var updateStickerPack: Kazv.AsyncHandler {
    trigger: () => {
      addStickerPopup.addingSticker = true;
      return matrixSdk.updateStickerPack(
        addStickerPopup.currentPack.addSticker(shortCodeInput.text, addStickerPopup.event)
      );
    }
    onResolved: (success, data) => {
      addStickerPopup.addingSticker = false;
      if (!success) {
        showPassiveNotification(l10n.get('add-sticker-popup-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      } else {
        addStickerPopup.close();
      }
    }
  }
}
