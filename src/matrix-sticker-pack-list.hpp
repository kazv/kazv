/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QJsonValue>
#include <immer/flex_vector.hpp>

#include <lager/reader.hpp>
#include <lager/extra/qt.hpp>

#include <client/client.hpp>
#include <base/event.hpp>
#include "kazv-abstract-list-model.hpp"
#include "matrix-sticker-pack-source.hpp"
Q_MOC_INCLUDE("matrix-sticker-pack.hpp")

class MatrixStickerPack;

class MatrixStickerPackList : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<immer::flex_vector<MatrixStickerPackSource>> m_events;

public:
    /// gets a list of sticker packs available by user settings
    explicit MatrixStickerPackList(Kazv::Client client, QObject *parent = 0);
    /// gets a list of sticker packs defined in a room
    explicit MatrixStickerPackList(Kazv::Room room, QObject *parent = 0);
    ~MatrixStickerPackList() override;

    Q_INVOKABLE MatrixStickerPack *at(int index) const;

    LAGER_QT_READER(QJsonValue, packs);

    Q_INVOKABLE MatrixStickerPack *packFor(const QJsonValue &desc) const;
};
