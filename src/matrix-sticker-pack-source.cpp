/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "matrix-sticker-pack-source.hpp"

bool operator==(const MatrixStickerPackSource &a, const MatrixStickerPackSource &b)
{
    return a.source == b.source
        && b.eventType == b.eventType
        && a.event == b.event;
}

bool operator!=(const MatrixStickerPackSource &a, const MatrixStickerPackSource &b)
{
    return !(a == b);
}
