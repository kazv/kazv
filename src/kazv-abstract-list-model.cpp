/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QTimer>
#include <lager/constant.hpp>
#include "kazv-abstract-list-model.hpp"

KazvAbstractListModel::KazvAbstractListModel(lager::reader<int> countArg, InitMethod initMethod, QObject *parent)
    : QAbstractListModel(parent)
    , m_internalCount(0)
{
    connect(this, &KazvAbstractListModel::countChanged, this, &KazvAbstractListModel::updateInternalCount);

    initCountCursor(countArg, initMethod);
}

KazvAbstractListModel::KazvAbstractListModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_internalCount(0)
    , LAGER_QT(count)(lager::make_constant(0))
{
    connect(this, &KazvAbstractListModel::countChanged, this, &KazvAbstractListModel::updateInternalCount);
}

KazvAbstractListModel::~KazvAbstractListModel() = default;

void KazvAbstractListModel::initCountCursor(lager::reader<int> countArg, InitMethod initMethod)
{
    LAGER_QT(count) = countArg;

    if (initMethod == InitNow) {
        m_internalCount = count();
    } else {
        // The following is taken from MatrixRoomTimeline.
        // HACK: If internally the count is set to its full count at the beginning,
        // ListView will instantiate everything in the timeline.
        // I don't know why it is the case, so I keep the internal count as 0
        // when it is constructed, and update it in the next tick.
        QTimer::singleShot(0, this, [this] { Q_EMIT countChanged(count()); });
    }
}

QVariant KazvAbstractListModel::data(const QModelIndex &/* index */, int /* role */) const
{
    return QVariant();
}

int KazvAbstractListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    } else {
        return m_internalCount;
    }
}

void KazvAbstractListModel::updateInternalCount()
{
    auto curCount = count();
    auto oldCount = m_internalCount;
    auto diff = std::abs(curCount - oldCount);

    if (curCount > oldCount) {
        beginInsertRows(QModelIndex(), 0, diff - 1);
        m_internalCount = curCount;
        endInsertRows();
    } else if (curCount < oldCount) {
        beginRemoveRows(QModelIndex(), 0, diff - 1);
        m_internalCount = curCount;
        endRemoveRows();
    }
}
