/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <QUrl>
#include <QMimeDatabase>

#include "matrix-link.hpp"
#include "kazv-util.hpp"

KazvUtil::KazvUtil(QObject *parent)
    : QObject(parent)
    , m_kfQtMajorVersion(KAZV_KF_QT_MAJOR_VERSION)
{}

KazvUtil::~KazvUtil() = default;

QString KazvUtil::escapeHtml(const QString &orig) const
{
    return orig.toHtmlEscaped();
}

QString KazvUtil::mimeTypeForUrl(const QUrl &url) const
{
    return QMimeDatabase().mimeTypeForUrl(url).name();
}

QString KazvUtil::matrixLinkUserId(const QString &url) const
{
    MatrixLink link = QUrl(url);
    if (link.isUser()) {
        return link.identifiers()[0];
    }
    return QStringLiteral("");
}
