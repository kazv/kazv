/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <memory>

#include <QObject>
#include <QNetworkAccessManager>

#include <jobinterface.hpp>

using namespace Kazv;

class QtJob : public QObject
{
    Q_OBJECT

public:
    QtJob(QObject *parent, QNetworkAccessManager *manager, BaseJob job, std::function<void(Response)> callback);
    ~QtJob() override;

Q_SIGNALS:
    void finished();

public Q_SLOTS:
    void injectData();
    void callCallback();

private:
    struct Private;
    std::unique_ptr<Private> m_d;
};
