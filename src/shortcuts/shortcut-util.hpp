/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <QObject>
#include <QQmlEngine>
#include <QString>
#include <QPair>

class ShortcutUtil : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public Q_SLOTS:
    bool isModifier(int key, int modifiers) const;
    QString keyToString(int key, int modifiers) const;
};
