/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QString>

#include <lager/extra/qt.hpp>

#include <client.hpp>

class MatrixDevice : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::DeviceKeyInfo> m_device;

public:
    explicit MatrixDevice(lager::reader<Kazv::DeviceKeyInfo> device, QObject *parent = 0);
    ~MatrixDevice() override;

    LAGER_QT_READER(QString, deviceId);
    LAGER_QT_READER(QString, ed25519Key);
    LAGER_QT_READER(QString, curve25519Key);
    LAGER_QT_READER(QString, displayName);
    LAGER_QT_READER(QString, trustLevel);
};
