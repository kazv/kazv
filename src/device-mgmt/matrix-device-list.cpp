/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <lager/lenses/optional.hpp>

#include "matrix-device.hpp"

#include "matrix-device-list.hpp"

using namespace Kazv;

MatrixDeviceList::MatrixDeviceList(lager::reader<immer::flex_vector<DeviceKeyInfo>> devices, QObject *parent)
    : QObject(parent)
    , m_devices(devices)
    , LAGER_QT(count)(m_devices.map([](const auto &vec) -> int { return vec.size(); }))
{
}

MatrixDeviceList::~MatrixDeviceList() = default;

MatrixDevice *MatrixDeviceList::at(int index) const
{
    return new MatrixDevice(m_devices.map([index](const auto &devices) {
        if (index < static_cast<int>(devices.size())) {
            return devices[index];
        } else {
            return DeviceKeyInfo{};
        }
    }));
}

MatrixDevice *MatrixDeviceList::deviceById(QString deviceId) const
{
    return new MatrixDevice(m_devices.map([deviceId=deviceId.toStdString()](const auto &devices) {
        auto it = std::find_if(devices.begin(), devices.end(), [deviceId](const auto &dev) {
            return dev.deviceId == deviceId;
        });
        if (it != devices.end()) {
            return *it;
        } else {
            return DeviceKeyInfo{};
        }
    }));
}
