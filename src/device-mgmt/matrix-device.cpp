/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <lager/lenses/optional.hpp>

#include "helper.hpp"

#include "matrix-device.hpp"

using namespace Kazv;

MatrixDevice::MatrixDevice(lager::reader<DeviceKeyInfo> device, QObject *parent)
    : QObject(parent)
    , m_device(device)
    , LAGER_QT(deviceId)(m_device[&DeviceKeyInfo::deviceId].xform(strToQt))
    , LAGER_QT(ed25519Key)(m_device[&DeviceKeyInfo::ed25519Key].xform(strToQt))
    , LAGER_QT(curve25519Key)(m_device[&DeviceKeyInfo::curve25519Key].xform(strToQt))
    , LAGER_QT(displayName)(m_device[&DeviceKeyInfo::displayName][lager::lenses::or_default].xform(strToQt))
    , LAGER_QT(trustLevel)(m_device[&DeviceKeyInfo::trustLevel].xform(trustLevelToQString))
{
}

MatrixDevice::~MatrixDevice() = default;
