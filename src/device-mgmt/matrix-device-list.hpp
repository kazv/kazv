/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QUrl>

#include <lager/extra/qt.hpp>

#include <client.hpp>
Q_MOC_INCLUDE("matrix-device.hpp")

class MatrixDevice;

class MatrixDeviceList : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<immer::flex_vector<Kazv::DeviceKeyInfo>> m_devices;

public:
    explicit MatrixDeviceList(lager::reader<immer::flex_vector<Kazv::DeviceKeyInfo>> devices, QObject *parent = 0);
    ~MatrixDeviceList() override;

    LAGER_QT_READER(int, count);

    Q_INVOKABLE MatrixDevice *at(int index) const;
    Q_INVOKABLE MatrixDevice *deviceById(QString deviceId) const;
};
