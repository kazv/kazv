/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "matrix-event-reaction.hpp"

MatrixEventReaction::MatrixEventReaction(lager::reader<Kazv::EventList> events, lager::reader<QString> key, QObject *parent)
    : MatrixEventList(events, parent)
    , LAGER_QT(key)(key)
{
}

MatrixEventReaction::~MatrixEventReaction() = default;
