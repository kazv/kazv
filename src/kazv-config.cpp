/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "kazv-config.hpp"

#include <KSharedConfig>
#include <KConfigGroup>

using namespace Qt::Literals::StringLiterals;

struct KazvConfigPrivate
{
    KSharedConfigPtr sessionrc{KSharedConfig::openConfig(u"kazvsessionrc"_s)};
    KConfigGroup sessionGroup{sessionrc, u"Sessions"_s};

    KSharedConfigPtr shortcutsrc{KSharedConfig::openConfig(u"kazv/shortcutsrc"_s)};
    KConfigGroup shortcutsGroup{shortcutsrc, u"Shortcuts"_s};

    KSharedConfigPtr cacheDirectoryrc{KSharedConfig::openConfig(u"kazv/cacheDirectoryrc"_s)};
    KConfigGroup cacheDirectoryGroup{cacheDirectoryrc, u"CacheDirectory"_s};
};

KazvConfig::KazvConfig(QObject *parent)
    : QObject(parent)
    , m_d(new KazvConfigPrivate)
{
}

KazvConfig::~KazvConfig() = default;

QString KazvConfig::lastSession() const
{
    return m_d->sessionGroup.readEntry("lastSession", QString());
}

void KazvConfig::setLastSession(QString session)
{
    m_d->sessionGroup.writeEntry("lastSession", session);
}

QStringList KazvConfig::shortcutForAction(QString actionName) const
{
    return m_d->shortcutsGroup.readEntry(actionName, QStringList());
}

void KazvConfig::setShortcutForAction(QString actionName, QStringList shortcut)
{
    m_d->shortcutsGroup.writeEntry(actionName, shortcut);
}

QString KazvConfig::cacheDirectory() const
{
    return m_d->cacheDirectoryGroup.readEntry("cacheDirectory", QString());
}

void KazvConfig::setCacheDirectory(QString cacheDirectory)
{
    m_d->cacheDirectoryGroup.writeEntry("cacheDirectory", cacheDirectory);
}
