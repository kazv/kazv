/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "kazv-config.hpp"

#include <KSharedConfig>
#include <KConfigGroup>

struct KazvConfigPrivate
{
    KSharedConfigPtr sessionrc{KSharedConfig::openConfig("kazvsessionrc")};
    KConfigGroup sessionGroup{sessionrc, "Sessions"};
};

KazvConfig::KazvConfig(QObject *parent)
    : QObject(parent)
    , m_d(new KazvConfigPrivate)
{
}

KazvConfig::~KazvConfig() = default;

QString KazvConfig::lastSession() const
{
    return m_d->sessionGroup.readEntry("lastSession", QString());
}

void KazvConfig::setLastSession(QString session)
{
    m_d->sessionGroup.writeEntry("lastSession", session);
}
