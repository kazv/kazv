/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include "kazv-io-job.hpp"

#include <QAbstractListModel>
#include <QtQml>
#include <QVariant>
#include <QModelIndex>
#include <QHash>
#include <QByteArray>

#include <memory>

struct UploadJobModelPrivate;

class UploadJobModel : public QAbstractListModel {
    Q_OBJECT
    QML_ELEMENT

    std::unique_ptr<UploadJobModelPrivate> m_d;

public:
    enum Roles {
        JobRole = Qt::UserRole + 1,
    };

    UploadJobModel(QObject *parent = nullptr);
    ~UploadJobModel() override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;

    void addJob(QPointer<KazvIOUploadJob> job);
    void removeJob(QPointer<KazvIOUploadJob> job);

    void clearJobs();
};
