/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QString>

#include <memory>
#include <filesystem>

#include <lager/extra/qt.hpp>

#include <sdk-model.hpp>
#include <random-generator.hpp>

#include "meta-types.hpp"
Q_MOC_INCLUDE("matrix-room-list.hpp")
Q_MOC_INCLUDE("matrix-device-list.hpp")
Q_MOC_INCLUDE("matrix-promise.hpp")
Q_MOC_INCLUDE("matrix-event.hpp")
Q_MOC_INCLUDE("matrix-sticker-pack-list.hpp")
Q_MOC_INCLUDE("matrix-user-given-attrs-map.hpp")

class MatrixRoomList;
class MatrixDeviceList;
class MatrixPromise;
class MatrixSdkTest;
class MatrixSdkSessionsTest;
class MatrixEvent;
class MatrixStickerPackList;
class MatrixUserGivenAttrsMap;
class KazvSessionLockGuard;

struct MatrixSdkPrivate;

std::filesystem::path sessionDirForUserAndDeviceId(std::filesystem::path userDataDir, std::string userId, std::string deviceId);

class MatrixSdk : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    QString m_userDataDir;
    std::unique_ptr<MatrixSdkPrivate> m_d;

    /// @param d A dynamically allocated d-pointer, whose ownership
    /// will be transferred to this MatrixSdk.
    explicit MatrixSdk(std::unique_ptr<MatrixSdkPrivate> d, QObject *parent);

    void init();

public:
    enum CreateRoomPreset {
        PrivateChat = Kazv::CreateRoomPreset::PrivateChat,
        PublicChat = Kazv::CreateRoomPreset::PublicChat,
        TrustedPrivateChat = Kazv::CreateRoomPreset::TrustedPrivateChat,
    };

    Q_ENUM(CreateRoomPreset);

    enum LoadSessionResult {
        /// Successfully loaded the session
        SessionLoadSuccess,
        /// There is no store file
        SessionNotFound,
        /// The format of the store file is not supported
        SessionFormatUnknown,
        /// The store file cannot be backed up
        SessionCannotBackup,
        /// Cannot grab the lock on the session file
        SessionLockFailed,
        /// Cannot open store file
        SessionCannotOpenFile,
        /// Cannot deserialize the store file
        SessionDeserializeFailed,
    };

    Q_ENUM(LoadSessionResult);

    explicit MatrixSdk(QObject *parent = 0);
    ~MatrixSdk() override;

    LAGER_QT_READER(QString, serverUrl);
    LAGER_QT_READER(QString, userId);
    LAGER_QT_READER(QString, token);
    LAGER_QT_READER(QString, deviceId);

    Q_INVOKABLE MatrixRoomList *roomList() const;

    Q_INVOKABLE QString mxcUriToHttp(QString mxcUri) const;

    Q_INVOKABLE MatrixDeviceList *devicesOfUser(QString userId) const;

    Kazv::RandomInterface &randomGenerator() const;

private:
    // Replaces the store with another one
    void emplace(std::optional<Kazv::SdkModel> model, std::unique_ptr<KazvSessionLockGuard> lockGuard);

    static std::string validateHomeserverUrl(const QString &url);

Q_SIGNALS:
    void trigger(Kazv::KazvEvent e);

    void loginSuccessful(Kazv::KazvEvent e);
    void loginFailed(QString errorCode, QString errorMsg);
    void discoverFailed(QString errorCode, QString errorMsg);
    void logoutSuccessful();
    void logoutFailed(QString errorCode, QString errorMsg);

    void receivedMessage(QString roomId, QString eventId);

    void sessionChanged();

public Q_SLOTS:
    void login(const QString &userId, const QString &password, const QString &homeserverUrl);
    void logout();

    /**
     * Serialize data to <AppDataDir>/sessions/<userid>/<deviceid>/
     *
     * If not logged in, do nothing.
     */
    void serializeToFile() const;

    /**
     * Load session at <AppDataDir>/sessions/<sessionName> .
     *
     * @param sessionName A string in the form of <userid>/<deviceid> .
     *
     * @return true if successful, false otherwise.
     */
    LoadSessionResult loadSession(QString sessionName);

    /**
     * Delete session at <AppDataDir>/sessions/<sessionName> .
     *
     * @param sessionName A string in the form of <userid>/<deviceid> .
     *
     * @return true if successful, false otherwise.
     */
    bool deleteSession(QString sessionName);

    /**
     * Start an empty session.
     *
     * The new session is not logged in, and need to call login().
     *
     * @return true if successful, false otherwise.
     */
    bool startNewSession();

    /**
     * Get all saved sessions.
     *
     * @return A list of session names in the form of <userid>/<deviceid> .
     */
    QStringList allSessions() const;

    /**
     * Create a new room.
     *
     * @param isPrivate Whether the room is private.
     * @param name The room's name.
     * @param alias The alias of the room.
     * @param invite List of matrix ids of users to invite.
     * @param isDirect Whether it is a direct message room.
     * @param allowFederate Whether to allow users on other servers to join.
     * @param topic The topic of the room.
     * @param powerLevelContentOverride The content to override m.room.power_levels event.
     * @param preset The preset to create the room with.
     * @param encrypted Whether to enable encryption for this room.
     */
    MatrixPromise *createRoom(
        bool isPrivate,
        const QString &name,
        const QString &alias,
        const QStringList &invite,
        bool isDirect,
        bool allowFederate,
        const QString &topic,
        const QJsonValue &powerLevelContentOverride,
        CreateRoomPreset preset,
        bool encrypted
    );

    /**
     * Join a room.
     * @param idOrAlias The id or alias of the room to join.
     * @param servers The servers to use when joining the room.
     */
    MatrixPromise *joinRoom(
        const QString &idOrAlias,
        const QStringList &servers
    );

    /**
     * Change the trust level of a device.
     *
     * @param userId The user id that owns the device.
     * @param deviceId The device id to set the trust level.
     * @param trustLevel The trust level.
     *
     * @return A MatrixPromise representing the progress.
     */
    MatrixPromise *setDeviceTrustLevel(QString userId, QString deviceId, QString trustLevel);

    /**
     * Get the profile of the current user.
     *
     * @return A MatrixPromise representing the progress.
     */
    MatrixPromise *getSelfProfile();

    /**
     * Set the display name of the current user.
     *
     * @return A MatrixPromise representing the progress.
     */
    MatrixPromise *setDisplayName(QString displayName);

    /**
     * Set the avatar url of the current user.
     *
     * @return A MatrixPromise representing the progress.
     */
    MatrixPromise *setAvatarUrl(QString avatarUrl);

    /**
     * Check if an event should be notified.
     *
     * @param event The event to check.
     * @return Whether `event` should be notified.
     */
    bool shouldNotify(MatrixEvent *event) const;

    /**
     * Check if an event should be notified with sound.
     *
     * You should only call this method when `shouldNotify(event)`
     * returns true.
     *
     * @param event The event to check.
     * @return Whether `event` should be notified with sound.
     */
    bool shouldPlaySound(MatrixEvent *event) const;

    /**
     * Get the sticker pack list for the current account.
     *
     * @return A list of sticker packs associated with the current account.
     */
    MatrixStickerPackList *stickerPackList() const;

    /**
     * Get the sticker rooms account data event for the current account.
     *
     * @return A MatrixEvent representing the sticker rooms account data event.
     */
    MatrixEvent *stickerRoomsEvent() const;

    /**
     * Update the sticker pack from source.
     *
     * @param source The source of the sticker pack to update.
     * @return A promise that resolves when the sticker pack is updated,
     * or when there is an error.
     */
    MatrixPromise *updateStickerPack(MatrixStickerPackSource source);

    MatrixUserGivenAttrsMap *userGivenNicknameMap() const;

    MatrixPromise *sendAccountData(const QString &type, const QJsonObject &content);

private:
    MatrixPromise *sendAccountDataImpl(Kazv::Event event);

private: // Testing
    friend MatrixSdkTest;
    friend MatrixSdkSessionsTest;
    friend MatrixSdk *makeTestSdk(Kazv::SdkModel model);

    void setUserDataDir(const std::string &userDataDir);

    explicit MatrixSdk(Kazv::SdkModel model, bool testing = false, QObject *parent = 0);
    void startThread();
};
