/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <libkazv-config.hpp>
#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QObject>
#include <QQmlEngine>
#include <QString>

#include <memory>

#include <lager/extra/qt.hpp>

#include <sdk-model.hpp>

#include "meta-types.hpp"


class MatrixRoomList;

struct MatrixSdkPrivate;

class MatrixSdk : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    std::unique_ptr<MatrixSdkPrivate> m_d;

    /// @param d A dynamically allocated d-pointer, whose ownership
    /// will be transferred to this MatrixSdk.
    explicit MatrixSdk(std::unique_ptr<MatrixSdkPrivate> d, QObject *parent);

    void init();

public:
    explicit MatrixSdk(QObject *parent = 0);
    explicit MatrixSdk(Kazv::SdkModel model, QObject *parent = 0);
    ~MatrixSdk() override;

    LAGER_QT_READER(QString, userId);
    LAGER_QT_READER(QString, token);
    LAGER_QT_READER(QString, deviceId);

    Q_INVOKABLE MatrixRoomList *roomList() const;

    Q_INVOKABLE QString mxcUriToHttp(QString mxcUri) const;

private:
    /// Replaces the store with another one
    void emplace(std::optional<Kazv::SdkModel> model);

Q_SIGNALS:
    void trigger(Kazv::KazvEvent e);

    void loginSuccessful(Kazv::KazvEvent e);
    void loginFailed(QString errorMessage);

    void sessionChanged();

public Q_SLOTS:
    void login(const QString &userId, const QString &password);

    /**
     * Serialize data to <AppDataDir>/sessions/<userid>/<deviceid>/
     *
     * If not logged in, do nothing.
     */
    void serializeToFile() const;

    /**
     * Load session at <AppDataDir>/sessions/<sessionName> .
     *
     * @param sessionName A string in the form of <userid>/<deviceid> .
     *
     * @return true if successful, false otherwise.
     */
    bool loadSession(QString sessionName);

    /**
     * Start an empty session.
     *
     * The new session is not logged in, and need to call login().
     *
     * @return true if successful, false otherwise.
     */
    bool startNewSession();

    /**
     * Get all saved sessions.
     *
     * @return A list of session names in the form of <userid>/<deviceid> .
     */
    QStringList allSessions() const;
};
