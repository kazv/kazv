/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <cursorutil.hpp>
#include "helper.hpp"
#include "matrix-sticker.hpp"
#include "matrix-event.hpp"
#include "matrix-sticker-pack.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

MatrixStickerPack::MatrixStickerPack(lager::reader<MatrixStickerPackSource> source, QObject *parent)
    : KazvAbstractListModel(parent)
    , m_source(source)
    , m_event(m_source[&MatrixStickerPackSource::event])
    , m_images(m_event.xform(
        eventContent
        | zug::map([](const JsonWrap &content) {
            if (content.get().contains("images")
                && content.get()["images"].is_object()) {
                auto size = content.get()["images"].size();
                auto items = content.get()["images"].items();
                auto array = json(size, json::object());
                std::transform(
                    items.begin(), items.end(),
                    array.begin(), [](const auto &it) {
                        return json::array({it.key(), it.value()});
                    }
                );
                return array;
            } else {
                return json::array();
            }
        })))
    , LAGER_QT(packName)(m_event.xform(eventContent | jsonAtOr("/pack/display_name"_json_pointer, std::string()) | strToQt))
    , LAGER_QT(isAccountData)(m_source.map([](const auto &source) {
        return source.source == MatrixStickerPackSource::AccountData;
    }))
    , LAGER_QT(isState)(m_source.map([](const auto &source) {
        return source.source == MatrixStickerPackSource::RoomState;
    }))
    , LAGER_QT(roomId)(m_source[&MatrixStickerPackSource::roomId].xform(strToQt))
    , LAGER_QT(stateKey)(m_source[&MatrixStickerPackSource::stateKey].xform(strToQt))
{
    initCountCursor(m_images.map([](const JsonWrap &images) -> int {
        return images.get().size();
    }));
}

MatrixStickerPack::~MatrixStickerPack() = default;

MatrixSticker *MatrixStickerPack::at(int index) const
{
    using namespace nlohmann::literals;
    auto image = m_images.map([index](const json &j) {
        if (j.size() > std::size_t(index)) {
            return j[index];
        } else {
            return json::array();
        }
    }).make();
    return new MatrixSticker(
        image.xform(jsonAtOr("/0"_json_pointer, json(std::string())) | zug::map([](const json &j) {
            return j.template get<std::string>();
        })),
        image.xform(jsonAtOr("/1"_json_pointer, json::object()))
    );
}

bool MatrixStickerPack::hasShortCode(const QString &shortCode) const
{
    return m_event.map([shortCode=shortCode.toStdString()](const Event &e) {
        auto content = e.content();
        return content.get().contains("images")
            && content.get()["images"].is_object()
            && content.get()["images"].contains(shortCode);
    })
        .make().get();
}

QVariant MatrixStickerPack::addSticker(const QString &shortCode, MatrixEvent *event) const
{
    if (!event) {
        return QVariant::fromValue(m_source.get());
    }

    auto source = m_source.get();

    auto eventJson = source.event.raw().get();

    eventJson.merge_patch(json{
        {"content", {
            {"images", {
                {shortCode.toStdString(), json::object()},
            }},
        }},
    });
    auto &sticker = eventJson["content"]["images"][shortCode.toStdString()];

    auto content = event->content();
    auto body = event->content()[u"body"_s];
    if (!body.isUndefined()) {
        sticker["body"] = body;
    }
    auto url = event->content()[u"url"_s];
    if (!url.isUndefined()) {
        sticker["url"] = url;
    }
    auto info = event->content()[u"info"_s];
    if (!info.isUndefined()) {
        sticker["info"] = info;
    }

    source.event = Event(eventJson);

    return QVariant::fromValue(source);
}
