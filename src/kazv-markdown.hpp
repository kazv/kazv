/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <string>

#include <immer/flex_vector.hpp>

struct KazvRichTextParseResult
{
    std::string html;
    immer::flex_vector<std::string> mentions;
};

KazvRichTextParseResult markdownToHtml(const std::string &markdown);
