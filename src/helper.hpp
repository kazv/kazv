/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */


#pragma once
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QString>

#include <zug/transducer/map.hpp>

namespace Kazv
{
    namespace detail
    {
        struct StrToQtT
        {
            template<class T>
            auto operator()(T &&x) {
                return QString::fromStdString(std::forward<T>(x));
            }
        };

        struct ContainerSizeT
        {
            template<class T>
            auto operator()(T &&x) -> int {
                return std::forward<T>(x).size();
            }
        };
    }
}

constexpr auto strToQt = zug::map(::Kazv::detail::StrToQtT{});

constexpr auto containerSize = zug::map(::Kazv::detail::ContainerSizeT{});
