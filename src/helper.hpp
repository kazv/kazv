/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


#pragma once
#include <kazv-defs.hpp>
#include <immer/array.hpp>

#include <QString>
#include <QStringList>

#include <zug/transducer/map.hpp>

#include <cursorutil.hpp>
#include <device-list-tracker.hpp>

namespace Kazv
{
    namespace detail
    {
        struct StrToQtT
        {
            template<class T>
            auto operator()(T &&x) {
                return QString::fromStdString(std::forward<T>(x));
            }
        };

        struct QStringToStdT
        {
            template<class T>
            auto operator()(T &&x) {
                return std::forward<T>(x).toStdString();
            }
        };

        struct ContainerSizeT
        {
            template<class T>
            auto operator()(T &&x) -> int {
                return std::forward<T>(x).size();
            }
        };
    }
}

constexpr auto strToQt = zug::map(::Kazv::detail::StrToQtT{});

constexpr auto strListToQtF = [](auto container) {
    return zug::into(QStringList{}, strToQt, std::move(container));
};

constexpr auto strListToQt = zug::map(strListToQtF);

constexpr auto qStringToStd = zug::map(::Kazv::detail::QStringToStdT{});

constexpr auto qStringListToStdF = [](auto container) {
    return Kazv::intoImmer(immer::array<std::string>{}, qStringToStd, std::move(container));
};

constexpr auto containerSize = zug::map(::Kazv::detail::ContainerSizeT{});

QString trustLevelToQStringFunc(Kazv::DeviceTrustLevel trustLevel);

Kazv::DeviceTrustLevel qStringToTrustLevelFunc(QString trustLevel);

constexpr auto trustLevelToQString = zug::map(trustLevelToQStringFunc);

constexpr auto qStringToTrustLevel = zug::map(qStringToTrustLevelFunc);

inline auto roomMemberToAvatar = Kazv::eventContent | Kazv::jsonAtOr(std::string("avatar_url"), std::string(""));
