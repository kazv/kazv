/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <lager/constant.hpp>
#include <lager/lenses/optional.hpp>

#include "matrix-room-list.hpp"
#include "matrix-room.hpp"

#include "helper.hpp"

MatrixRoomList::MatrixRoomList(Kazv::Client client, QObject *parent)
    : QObject(parent)
    , m_client(client)
    , m_roomIds(m_client.roomIds())
    , LAGER_QT(count)(m_roomIds.xform(containerSize))
    , LAGER_QT(roomIds)(m_roomIds.xform(zug::map(
                                            [](auto container) {
                                                return zug::into(QStringList{}, strToQt, std::move(container));
                                            })))
{
}

MatrixRoomList::~MatrixRoomList() = default;

MatrixRoom *MatrixRoomList::at(int index) const
{
    qDebug() << "Room at index " << index << " requested";
    return new MatrixRoom(
        m_client.roomByCursor(
            lager::with(m_roomIds, lager::make_constant(index))
            .xform(zug::map([](auto ids, auto i) {
                                try {
                                    return ids.at(i);
                                } catch (const std::out_of_range &) {
                                    return std::string{};
                                }
                            }))));
}

QString MatrixRoomList::roomIdAt(int index) const
{
    using namespace Kazv::CursorOp;
    return +m_roomIds[index][lager::lenses::or_default].xform(strToQt);
}

MatrixRoom *MatrixRoomList::room(QString roomId) const
{
    return new MatrixRoom(m_client.room(roomId.toStdString()));
}
