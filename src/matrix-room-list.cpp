/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <zug/into_vector.hpp>
#include <zug/transducer/cat.hpp>
#include <lager/commit.hpp>
#include <lager/constant.hpp>
#include <lager/lenses/optional.hpp>

#include "matrix-room-list.hpp"
#include "matrix-room.hpp"
#include "matrix-user-given-attrs-map.hpp"
#include "matrix-utils.hpp"
#include "helper.hpp"

using namespace Kazv;

static Timestamp latestEventTimestamp(const RoomModel &room)
{
    if (room.timeline.empty()) {
        return 0;
    }
    auto latestEventId = room.timeline[room.timeline.size() - 1];
    auto latestEvent = room.messages[latestEventId];
    return latestEvent.originServerTs();
}

MatrixRoomList::MatrixRoomList(Kazv::Client client, QString tagId, QString filter, QObject *parent)
    : KazvAbstractListModel(parent)
    , m_client(client)
    , m_tagId(tagId)
    , m_tagIdCursor(lager::make_sensor([this] { return m_tagId.toStdString(); }))
    , m_filter(lager::make_state(filter.toStdString(), lager::automatic_tag{}))
    , m_userGivenNicknameMap(userGivenNicknameMapFor(m_client).map(&Event::content))
    , m_roomIds(lager::with(m_tagIdCursor, m_client.rooms(), m_filter, m_client.roomIdsByTagId(), m_userGivenNicknameMap)
        .map([](const auto &tagIdStdStr, const auto &allRooms, const auto &filter, const auto &roomsByTagMap, const auto &userGivenNicknameMap) {
            auto toId = zug::map([](const auto &pair) {
                return pair.first;
            });

            auto roomName = [](const auto &room) {
                auto content = room.stateEvents[{"m.room.name", ""}].content().get();
                if (content.contains("name") && content["name"].is_string()) {
                    return content["name"].template get<std::string>();
                }
                return std::string();
            };

            auto roomHeroNames = [m=userGivenNicknameMap](const auto &room) {
                auto heroEvents = room.heroMemberEvents();
                return intoImmer(
                    immer::flex_vector<std::string>(),
                    zug::map([m](const Event &ev) {
                        auto userId = ev.stateKey();
                        auto content = ev.content().get();
                        auto res = std::vector<std::string>{};
                        if (content.contains("displayname") && content["displayname"].is_string()) {
                            res.push_back(content["displayname"].template get<std::string>());
                        }
                        if (m.get().contains(userId)
                            && m.get()[userId].is_string()) {
                            res.push_back(m.get()[userId].template get<std::string>());
                        }
                        return res;
                    })
                    | zug::cat,
                    heroEvents
                );
            };

            auto applyFilter = zug::filter([&filter, &allRooms, &roomName, &roomHeroNames](const auto &id) {
                if (filter.empty()) {
                    return true;
                }

                const auto &room = allRooms[id];
                // Use exact match for room id
                if (room.roomId == filter) {
                    return true;
                }

                auto name = roomName(room);
                if (!name.empty()) {
                    // Use substring match for name search
                    return name.find(filter) != std::string::npos;
                }

                // The room has no name, use hero names for the search
                auto heroes = roomHeroNames(room);
                // If any of the room hero matches the filter, consider it a match
                return std::any_of(heroes.begin(), heroes.end(),
                    [&filter](const auto &name) {
                        return name.find(filter) != std::string::npos;
                    })
                    || std::any_of(room.heroIds.begin(), room.heroIds.end(),
                        [&filter](const auto &id) {
                            return id.find(filter) != std::string::npos;
                        });
            });

            auto sortByTimestampDesc = [allRooms](std::vector<std::string> container) {
                std::sort(
                    container.begin(),
                    container.end(),
                    [allRooms](const std::string &idA, const std::string &idB) {
                        const auto &roomA = allRooms[idA];
                        const auto &roomB = allRooms[idB];
                        auto aIsInvite = roomA.membership == Invite;
                        auto bIsInvite = roomB.membership == Invite;
                        auto aIsFavourite = roomA.tags().count("m.favourite") == 1;
                        auto bIsFavourite = roomB.tags().count("m.favourite") == 1;
                        if (aIsInvite != bIsInvite) {
                            /**
                             * if my membership in A is invite,
                             * then the membership in B is not invite,
                             * so A should come first;
                             * otherwise B should come first
                             **/
                            return aIsInvite;
                        } else if (aIsFavourite != bIsFavourite) {
                            return aIsFavourite;
                        } else {
                            return latestEventTimestamp(roomA)
                                > latestEventTimestamp(roomB);
                        }
                    }
                );
                return immer::flex_vector<std::string>(container.begin(), container.end());
            };

            if (tagIdStdStr.empty()) {
                return sortByTimestampDesc(zug::into_vector(
                    toId | applyFilter,
                    allRooms
                ));
            } else {
                return sortByTimestampDesc(zug::into_vector(
                    toId | applyFilter,
                    roomsByTagMap[tagIdStdStr]
                ));
            }
        }))
    , LAGER_QT(filter)(m_filter.xform(strToQt, qStringToStd))
    , LAGER_QT(roomIds)(m_roomIds.xform(zug::map(
                                            [](auto container) {
                                                return zug::into(QStringList{}, strToQt, std::move(container));
                                            })))
{
    initCountCursor(m_roomIds.xform(containerSize));
}

MatrixRoomList::~MatrixRoomList() = default;

void MatrixRoomList::setTagId(QString tagId)
{
    m_tagId = tagId;
    lager::commit(m_tagIdCursor);
}

MatrixRoom *MatrixRoomList::at(int index) const
{
    qDebug() << "Room at index " << index << " requested";
    return new MatrixRoom(
        m_client.roomByCursor(
            lager::with(m_roomIds, lager::make_constant(index))
            .xform(zug::map([](auto ids, auto i) {
                                try {
                                    return ids.at(i);
                                } catch (const std::out_of_range &) {
                                    return std::string{};
                                }
                            }))),
        m_client.userId());
}

QString MatrixRoomList::roomIdAt(int index) const
{
    using namespace Kazv::CursorOp;
    return +m_roomIds[index][lager::lenses::or_default].xform(strToQt);
}

MatrixRoom *MatrixRoomList::room(QString roomId) const
{
    return new MatrixRoom(
        m_client.room(roomId.toStdString()),
        m_client.userId(),
        userGivenNicknameMapFor(m_client)
    );
}
