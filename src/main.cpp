/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>

#include "matrix-sdk.hpp"
#include "matrix-room-list.hpp"
#include "matrix-room.hpp"
#include "matrix-room-timeline.hpp"
#include "matrix-room-member.hpp"
#include "matrix-event.hpp"
#include "l10n-provider.hpp"

#include "kazv-config.hpp"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("project-kazv");
    QCoreApplication::setOrganizationDomain("mxc.kazv.moe");
    QCoreApplication::setApplicationName("kazv");

    QQmlApplicationEngine engine;

    qmlRegisterType<MatrixSdk>("moe.kazv.mxc.kazv", 0, 0, "MatrixSdk");
    qmlRegisterType<L10nProvider>("moe.kazv.mxc.kazv", 0, 0, "L10nProvider");
    qmlRegisterType<KazvConfig>("moe.kazv.mxc.kazv", 0, 0, "KazvConfig");
    qmlRegisterUncreatableType<MatrixRoomList>("moe.kazv.mxc.kazv", 0, 0, "MatrixRoomList", "");
    qmlRegisterUncreatableType<MatrixRoom>("moe.kazv.mxc.kazv", 0, 0, "MatrixRoom", "");
    qmlRegisterUncreatableType<MatrixRoomMember>("moe.kazv.mxc.kazv", 0, 0, "MatrixRoomMember", "");
    qmlRegisterUncreatableType<MatrixRoomTimeline>("moe.kazv.mxc.kazv", 0, 0, "MatrixRoomTimeline", "");
    qmlRegisterUncreatableType<MatrixEvent>("moe.kazv.mxc.kazv", 0, 0, "MatrixEvent", "");

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
