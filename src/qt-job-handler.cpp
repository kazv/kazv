/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <unordered_map>

#include <QNetworkAccessManager>
#include <QByteArray>
#include <QThreadPool>
#include <QTimer>

#include <zug/transducer/filter.hpp>
#include <zug/into.hpp>

#include "qt-job-handler.hpp"
#include "qt-job.hpp"
#include "kazv-log.hpp"

struct QtJobHandler::Private
{
    QNetworkAccessManager *manager;
};

QtJobHandler::QtJobHandler(QObject *parent)
    : QObject(parent)
    , m_d(new Private)
{
    m_d->manager = new QNetworkAccessManager(this);
}

QtJobHandler::~QtJobHandler() = default;

void QtJobHandler::async(std::function<void()> func)
{
    QThreadPool::globalInstance()->start(func);
}

void QtJobHandler::setTimeout(std::function<void()> func, int ms,
    std::optional<std::string> timerId)
{
    Q_UNUSED(timerId);
    QTimer::singleShot(ms, this, func);
}

void QtJobHandler::setInterval(std::function<void()> func, int ms,
    std::optional<std::string> timerId)
{
    Q_UNUSED(func);
    Q_UNUSED(ms);
    Q_UNUSED(timerId);
    qCCritical(kazvLog) << "QtJobHandler::setInterval: unimplemented";
}

void QtJobHandler::cancel(std::string timerId)
{
    Q_UNUSED(timerId);
    qCCritical(kazvLog) << "QtJobHandler::cancel: unimplemented";
}

void QtJobHandler::submit(BaseJob job,
    std::function<void(Response)> callback)
{
    new QtJob(this, m_d->manager, std::move(job), std::move(callback));
}

void QtJobHandler::stop()
{
}
