/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "qt-promise-handler.hpp"

QtPromiseSignalTrigger::QtPromiseSignalTrigger(QObject *parent)
    : QObject(parent)
{}

QtPromiseSignalTrigger::~QtPromiseSignalTrigger() = default;
