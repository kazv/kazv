/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include "matrix-room.hpp"
#include "matrix-room-list.hpp"
#include "kazv-file.hpp"

#include <aes-256-ctr.hpp>

#include <QObject>
#include <QPointer>
#include <QString>
#include <QUrl>

#include <KIO/TransferJob>

#include <memory>
#include <optional>

struct KazvIOBaseJobPrivate;

class KazvIOBaseJob : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    Q_PROPERTY(float progress READ progress NOTIFY progressChanged)

    std::unique_ptr<KazvIOBaseJobPrivate> m_d;

public:
    enum ErrorCode {
        /**
         * No Error or the signal result has not been emitted yet.
         */
        NoError = 0,
        /**
         * The user actively canceled the job.
         */
        UserCancel,
        /**
         * Open File Error.
         */
        OpenFileError,
        /**
         * Write File Error
         */
        WriteFileError,
        /**
         * Errors caused by KIO, such network error.
         */
        KIOError,
        /**
         * Error caused by hash error
         */
        HashError,
        /**
         * Error caused by invalid response.
         * KIO reported no error, but got an invalid JSON response.
         */
        ResponseError,
        /**
         * Errors caused by bad code, means the bug is somewhere.
         */
        KazvError
    };
    Q_ENUM(ErrorCode)

    KazvIOBaseJob(std::optional<Kazv::AES256CTRDesc> aes = std::nullopt, QObject *parent = 0);
    ~KazvIOBaseJob();

    float progress();

    Q_INVOKABLE void suspend();
    Q_INVOKABLE void resume();
    Q_INVOKABLE void cancel();
    Q_INVOKABLE bool isSuspended();
    Q_INVOKABLE bool isResulted();
    /**
     * Only use it after emitted the result signal
     */
    Q_INVOKABLE ErrorCode error();

Q_SIGNALS:
    void jobChanged();
    void progressChanged();
    /**
     * @param data the mxcUri if this is an upload job.
     */
    void result(ErrorCode ec, QString data);

protected Q_SLOTS:
    virtual void connectJob();

protected:
    QPointer<KIO::TransferJob> job();
    void setJob(QPointer<KIO::TransferJob> job);
    std::optional<Kazv::AES256CTRDesc> aes();
    void setAes(Kazv::AES256CTRDesc);

    void emitResult(ErrorCode ec, QString data = QString());
};

struct KazvIODownloadJobPrivate;

class KazvIODownloadJob : public KazvIOBaseJob
{
    Q_OBJECT

    std::unique_ptr<KazvIODownloadJobPrivate> m_d;

    bool setFile(QString fileName);

public:
    KazvIODownloadJob(const QString &hash, std::optional<Kazv::AES256CTRDesc> aes = std::nullopt, QObject *parent = 0);
    KazvIODownloadJob(const QString &fileName, const QUrl &serverUrl, const bool showProgressBar, const QString &hash,
        std::optional<Kazv::AES256CTRDesc> aes = std::nullopt, QObject *parent = 0);
    ~KazvIODownloadJob();

    Q_INVOKABLE QString fileName();

private Q_SLOTS:
    void writeFile(KJob *job, const QByteArray &data);
    void closeFile(KJob *job);

protected Q_SLOTS:
    void connectJob() override;
};

struct KazvIOUploadJobPrivate;

class KazvIOUploadJob : public KazvIOBaseJob
{
    Q_OBJECT

    std::unique_ptr<KazvIOUploadJobPrivate> m_d;

    bool setFile(const QString fileName);

public:
    KazvIOUploadJob(std::optional<Kazv::AES256CTRDesc> aes = std::nullopt,
        const QString &relType = QStringLiteral(""),
        const QString &relatedTo = QStringLiteral(""), QObject *parent = 0);
    KazvIOUploadJob(const QString fileName, const QUrl serverUrl,
        const bool showProgressBar, MatrixRoomList *roomList,
        const QString &roomId, const QString token,
        std::optional<Kazv::AES256CTRDesc> aes = std::nullopt,
        const QString &relType = QStringLiteral(""),
        const QString &relatedTo = QStringLiteral(""), QObject *parent = 0);
    ~KazvIOUploadJob();

    Q_INVOKABLE QString fileName();

private Q_SLOTS:
    void handleResult(KJob *job);

protected Q_SLOTS:
    void connectJob() override;
};
