/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <tuple>
#include <vector>
#include <QObject>
#include <QStringList>

class QUrl;

/**
 * A link that represents a Matrix object.
 */
class MatrixLink
{
    Q_GADGET
    Q_PROPERTY(bool isUser READ isUser CONSTANT)
    Q_PROPERTY(bool isRoom READ isRoom CONSTANT)
    Q_PROPERTY(bool isEvent READ isEvent CONSTANT)
    Q_PROPERTY(QStringList identifiers READ identifiers CONSTANT)
    Q_PROPERTY(QStringList routingServers READ routingServers CONSTANT)

public:
    enum Type {
        None,
        User,
        RoomId,
        RoomAlias,
        Event,
    };

    MatrixLink(const QUrl &url);

    bool isValid() const;
    bool isUser() const;
    bool isRoom() const;
    bool isEvent() const;
    QStringList identifiers() const;
    QStringList routingServers() const;

private:
    std::vector<std::pair<Type, QString>> m_identifiers;
    QStringList m_routingServers;
};
