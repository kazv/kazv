/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QJsonValue>
#include "matrix-promise.hpp"
#include "qt-json.hpp"
#include "matrix-user-given-attrs-map.hpp"

using namespace Kazv;

MatrixUserGivenAttrsMap::MatrixUserGivenAttrsMap(
    lager::reader<Event> event,
    std::function<Client::PromiseT(json)> setter,
    QObject *parent
)
    : QObject(parent)
    , m_event(event)
    , m_setter(setter)
    , LAGER_QT(map)(m_event.map([](Event e) {
        auto content = e.content().get();
        return content.is_object()
            ? content.template get<QJsonObject>()
            : QJsonObject();
    }))
{
}

MatrixUserGivenAttrsMap::~MatrixUserGivenAttrsMap() = default;

Kazv::json MatrixUserGivenAttrsMap::set(const QString &id, const QJsonValue &data) const
{
    auto content = m_event.get().content().get();
    if (!content.is_object()) {
        content = json::object();
    }
    auto idStd = id.toStdString();
    if (data.isNull()) {
        if (content.contains(idStd)) {
            content.erase(idStd);
        }
    } else {
        content[idStd] = json(data);
    }
    return content;
}

MatrixPromise *MatrixUserGivenAttrsMap::setAndUpload(const QString &id, const QJsonValue &data)
{
    return new MatrixPromise(m_setter(set(id, data)));
}
