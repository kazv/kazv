/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <QObject>
#include <QQmlEngine>
#include <QJsonObject>
#include "matrix-room-timeline.hpp"

class MatrixRoomPinnedEventsTimeline : public MatrixRoomTimeline
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

public:
    explicit MatrixRoomPinnedEventsTimeline(Kazv::Room room, QObject *parent = 0);
    ~MatrixRoomPinnedEventsTimeline() override;

    LAGER_QT_READER(QJsonObject, eventIds);
};
