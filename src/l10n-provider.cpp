/*
 * Copyright (C) 2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "l10n-provider.hpp"

#include <optional>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStringBuilder>
#include <stdexcept>

#include "kazv-path-config.hpp"

static QString l10nConfigFile()
{
    static QString file = KazvPathConfigDetail::pathJoin(kazvL10nDir(), QString("config.json"));
    return file;
}

static QString dirForLocale(QString locale)
{
    return KazvPathConfigDetail::pathJoin(kazvL10nDir(), locale);
}

static QString readWholeFile(QString path)
{
    QFile f(path);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw std::runtime_error("Cannot open " + path.toStdString() + " for reading.");
    }
    QTextStream stream(&f);
    return stream.readAll();
}

struct L10nProviderPrivate
{
    std::optional<QJsonObject> cachedLocaleToNameMap;
    QMap<QString, QString> cachedFtlData;

    void cacheAvailableLocales();
    void cacheFtlDataFor(QString locale);
};

void L10nProviderPrivate::cacheAvailableLocales()
{
    if (cachedLocaleToNameMap.has_value()) {
        return;
    } else {
        auto fn = l10nConfigFile();
        auto content = readWholeFile(fn);
        auto j = QJsonDocument::fromJson(content.toUtf8());
        auto availableLocales = j["availableLocales"].toObject();

        cachedLocaleToNameMap = std::move(availableLocales);
    }
}

void L10nProviderPrivate::cacheFtlDataFor(QString locale)
{
    if (cachedFtlData.contains(locale)) {
        return;
    }

    auto dn = dirForLocale(locale);
    auto files = QDir(dn).entryInfoList(QStringList{QStringLiteral("*.ftl")}, QDir::NoFilter, QDir::Name);

    QString s;
    const QString newline = "\n";

    for (auto f : files) {
        auto fn = f.absoluteFilePath();
        s.append(readWholeFile(fn));
        s.append(newline);
    }

    cachedFtlData[locale] = s;
}

L10nProvider::L10nProvider(QObject *parent)
    : QObject(parent)
    , m_d(new L10nProviderPrivate)
{
}

L10nProvider::~L10nProvider() = default;

QStringList L10nProvider::availableLocaleCodes()
{
    m_d->cacheAvailableLocales();
    return m_d->cachedLocaleToNameMap.value().keys();
}

QJsonObject L10nProvider::availableLocaleToNameMap()
{
    m_d->cacheAvailableLocales();
    return m_d->cachedLocaleToNameMap.value();
}

QJsonObject L10nProvider::getFtlData(QStringList desiredLocales)
{
    QJsonObject data;
    for (auto locale : desiredLocales) {
        m_d->cacheFtlDataFor(locale);
        data[locale] = m_d->cachedFtlData[locale];
    }
    return data;
}
