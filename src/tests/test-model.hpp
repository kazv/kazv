/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <sdk-model.hpp>

class MatrixSdk;

Kazv::SdkModel makeTestModel();

MatrixSdk *makeTestSdk(Kazv::SdkModel model);

Kazv::Event makeRoomMember(std::string userId, std::string displayName = "", std::string avatarUrl = "");
