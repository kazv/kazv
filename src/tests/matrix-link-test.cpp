/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <vector>
#include <QtTest>

#include "matrix-link.hpp"

using namespace Qt::Literals::StringLiterals;

class MatrixLinkTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testInvalidLink();
    void testUserLink();
    void testRoomLink();
    void testEventLink();
    void testRoutingServers();
};

void MatrixLinkTest::testInvalidLink()
{
    std::vector<QUrl> links = {
        QUrl(u"https://matrix.to/#"_s),
        QUrl(u"https://matrix.to/#/"_s),
        QUrl(u"https://matrix.to/#//"_s),
        QUrl(u"https://matrix.to/#///"_s),
        QUrl(u"http://matrix.to/#/%40alice%3Aexample.org"_s),
        QUrl(u"https://lol.lol/#/%40alice%3Aexample.org"_s),
        QUrl(u"https://matrix.to/#%40alice%3Aexample.org"_s),
    };
    for (auto link : links) {
        MatrixLink ml(link);
        QVERIFY(!ml.isValid());
    }
}

void MatrixLinkTest::testUserLink()
{
    std::vector<QUrl> links = {
        QUrl(u"https://matrix.to/#/%40alice%3Aexample.org"_s),
        QUrl(u"https://matrix.to/#/@alice%3Aexample.org"_s),
        QUrl(u"https://matrix.to/#/%40alice:example.org"_s),
        QUrl(u"https://matrix.to/#/@alice:example.org"_s),
    };
    for (auto link : links) {
        MatrixLink ml(link);
        QVERIFY(ml.isUser());
        QCOMPARE(ml.identifiers(), QStringList{u"@alice:example.org"_s});
    }
}

void MatrixLinkTest::testRoomLink()
{
    std::vector<std::tuple<QUrl, QString>> links = {
        {QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org"_s), u"#somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/%23somewhere:example.org"_s), u"#somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/#somewhere%3Aexample.org"_s), u"#somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/#somewhere:example.org"_s), u"#somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/!somewhere%3Aexample.org?via=elsewhere.ca"_s), u"!somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/!somewhere:example.org?via=elsewhere.ca"_s), u"!somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/!somewhere%3Aexample.org"_s), u"!somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/!somewhere:example.org"_s), u"!somewhere:example.org"_s},
    };
    for (auto [link, pointed] : links) {
        MatrixLink ml(link);
        QVERIFY(ml.isRoom());
        QCOMPARE(ml.identifiers(), QStringList{pointed});
    }
}

void MatrixLinkTest::testEventLink()
{
    std::vector<std::tuple<QUrl, QString>> links = {
        {QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org/%24event%3Aexample.org"_s), u"#somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org/%24event:example.org"_s), u"#somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/!somewhere%3Aexample.org/%24event%3Aexample.org?via=elsewhere.ca"_s), u"!somewhere:example.org"_s},
        {QUrl(u"https://matrix.to/#/!somewhere%3Aexample.org/$event:example.org?via=elsewhere.ca"_s), u"!somewhere:example.org"_s},
    };
    for (auto [link, pointed] : links) {
        MatrixLink ml(link);
        QVERIFY(ml.isRoom());
        QCOMPARE(ml.identifiers(), (QStringList{pointed, u"$event:example.org"_s}));
    }
}

void MatrixLinkTest::testRoutingServers()
{
    QCOMPARE(MatrixLink(QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org/%24event:example.org"_s)).routingServers(), (QStringList{}));

    QCOMPARE(MatrixLink(QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org/%24event:example.org?via=a.org"_s)).routingServers(), (QStringList{u"a.org"_s}));

    QCOMPARE(MatrixLink(QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org/%24event:example.org?via=a.org&via=b.org"_s)).routingServers(), (QStringList{u"a.org"_s, u"b.org"_s}));

    QCOMPARE(MatrixLink(QUrl(u"https://matrix.to/#/%23somewhere%3Aexample.org/%24event:example.org?foo=bar&via=a.org&via=b.org"_s)).routingServers(), (QStringList{u"a.org"_s, u"b.org"_s}));
}

QTEST_MAIN(MatrixLinkTest)

#include "matrix-link-test.moc"
