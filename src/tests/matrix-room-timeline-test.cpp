/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>

#include <factory.hpp>

#include <matrix-room-timeline.hpp>
#include <matrix-sdk.hpp>
#include <matrix-room-list.hpp>
#include <matrix-room.hpp>
#include <matrix-event.hpp>
#include <matrix-event-reader-list-model.hpp>
#include <matrix-event-reader.hpp>

#include "test-model.hpp"
#include "test-utils.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;
using namespace Kazv::Factory;

class MatrixRoomTimelineTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testLocalEcho();
    void testReadReceipts();
    void testEdits();
    void testIndexOfEvent();
};

void MatrixRoomTimelineTest::testLocalEcho()
{
    auto model = makeTestModel();
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(u"!foo:tusooa.xyz"_s));
    auto timeline = toUniquePtr(room->timeline());

    QCOMPARE(timeline->count(), 2);
    auto first = toUniquePtr(timeline->at(1)); // reverse order
    QCOMPARE(first->isSending(), true);
    QCOMPARE(first->isLocalEcho(), true);
    QCOMPARE(first->isFailed(), false);
    QCOMPARE(first->txnId(), u"some-txn-id"_s);
    auto second = toUniquePtr(timeline->at(0));
    QCOMPARE(second->isSending(), false);
    QCOMPARE(second->isLocalEcho(), true);
    QCOMPARE(second->isFailed(), true);
    QCOMPARE(second->txnId(), u"some-other-txn-id"_s);
}

void MatrixRoomTimelineTest::testReadReceipts()
{
    auto r = makeRoom(withRoomTimeline({
        makeEvent(withEventId("$1")),
        makeEvent(withEventId("$2")),
    }));
    r.eventReadUsers = {{"$1", {"@a:example.com", "@b:example.com"}}};
    r.readReceipts = {{"@a:example.com", {"$1", 1234}}, {"@b:example.com", {"$2", 5678}}};
    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());
    {
        auto event = toUniquePtr(timeline->at(1));
        QCOMPARE(event->eventId(), u"$1"_s);
        auto readers = toUniquePtr(event->readers());
        QCOMPARE(readers->count(), 2);
        auto reader1 = toUniquePtr(readers->at(0));
        QCOMPARE(reader1->userId(), u"@a:example.com"_s);
        QCOMPARE(reader1->timestamp(), 1234);
        auto reader2 = toUniquePtr(readers->at(1));
        QCOMPARE(reader2->userId(), u"@b:example.com"_s);
        QCOMPARE(reader2->timestamp(), 5678);
    }

    {
        auto event = toUniquePtr(timeline->at(0));
        QCOMPARE(event->eventId(), u"$2"_s);
        auto readers = toUniquePtr(event->readers());
        QCOMPARE(readers->count(), 0);
    }
}

void MatrixRoomTimelineTest::testEdits()
{
    auto r = makeRoom(withRoomTimeline({
        makeEvent(withEventId("$0")),
        makeEvent(withEventId("$1") | withEventRelationship("moe.kazv.mxc.some-rel", "$0")),
        makeEvent(withEventId("$2") | withEventContent(json{{"body", "first"}}) | withEventRelationship("m.replace", "$1")),
        // valid
        makeEvent(withEventId("$3") | withEventContent(json{{"m.new_content", {{"body", "second"}}}}) | withEventRelationship("m.replace", "$1")),
        // also valid
        makeEvent(withEventId("$4") | withEventContent(json{{"m.new_content", {{"body", "third"}}}}) | withEventRelationship("m.replace", "$1")),
        // invalid, changed event type
        makeEvent(withEventId("$5") | withEventContent(json{{"m.new_content", {{"body", "fourth"}}}}) | withEventRelationship("m.replace", "$1") | withEventType("moe.kazv.mxc.some-other-type")),
        // invalid, changed event sender
        makeEvent(withEventId("$6") | withEventContent(json{{"m.new_content", {{"body", "fifth"}}}}) | withEventRelationship("m.replace", "$1") | withEventType("moe.kazv.mxc.some-other-type") | withEventSenderId("@othersender:example.com")),
        // invalid, has state key
        makeEvent(withEventId("$7") | withEventContent(json{{"m.new_content", {{"body", "sixth"}}}}) | withEventRelationship("m.replace", "$1") | withEventType("moe.kazv.mxc.some-other-type") | withStateKey("")),
        // invalid, no m.new_content
        makeEvent(withEventId("$8") | withEventContent(json{{"body", "seventh"}}) | withEventRelationship("m.replace", "$1") | withEventType("moe.kazv.mxc.some-other-type")),
    }));
    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());

    auto event = toUniquePtr(timeline->at(7));
    QCOMPARE(event->eventId(), u"$1"_s);
    // the content is taken from the newest valid edit
    qDebug() << event->content();
    QCOMPARE(event->content(), (QJsonObject{{u"body"_s, u"third"_s}}));
    // the relation is taken from the original content
    QCOMPARE(event->relationType(), u"moe.kazv.mxc.some-rel"_s);
    QCOMPARE(event->relatedEventId(), u"$0"_s);
    QVERIFY(event->isEdited());
}

void MatrixRoomTimelineTest::testIndexOfEvent()
{
    // reverse index
    // 4   3   2   1    0
    // event/txnId
    // $1  $2  $3  txn1 txn2
    auto r = makeRoom(withRoomTimeline({
        makeEvent(withEventId("$1") | withEventKV("/origin_server_ts"_json_pointer, 1000)),
        makeEvent(withEventId("$2") | withEventKV("/origin_server_ts"_json_pointer, 2000)),
        makeEvent(withEventId("$3") | withEventKV("/origin_server_ts"_json_pointer, 3000)),
    }));
    r.localEchoes = {
        LocalEchoDesc{"txn1", makeEvent(withEventId("")), LocalEchoDesc::Sending},
        LocalEchoDesc{"txn2", makeEvent(withEventId("")), LocalEchoDesc::Sending},
    };

    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());

    QCOMPARE(timeline->indexOfEvent(u"$1"_s), 4);
    QCOMPARE(timeline->indexOfEvent(u"$2"_s), 3);
    QCOMPARE(timeline->indexOfEvent(u"$3"_s), 2);
    QCOMPARE(timeline->indexOfEvent(u"$4"_s), -1);
}

QTEST_MAIN(MatrixRoomTimelineTest)

#include "matrix-room-timeline-test.moc"
