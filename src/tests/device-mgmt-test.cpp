/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <lager/state.hpp>

#include <device-list-tracker.hpp>

#include <matrix-device-list.hpp>
#include <matrix-device.hpp>

#include "tests.hpp"

#include "device-mgmt-test.hpp"

using namespace Kazv;

static const immer::flex_vector<DeviceKeyInfo> devices{
    {"device1", "device1ed25519", "device1curve25519", "device1name", Unseen},
    {"device2", "device2ed25519", "device2curve25519", "device2name", Seen},
    {"device3", "device3ed25519", "device3curve25519", "device3name", Verified},
    {"device4", "device4ed25519", "device4curve25519", "device4name", Blocked},
};

void DeviceMgmtTest::testDeviceList()
{
    auto state = lager::make_state(devices, lager::automatic_tag{});

    auto deviceList = MatrixDeviceList(state);

    QVERIFY(deviceList.count() == 4);

    auto dev = std::unique_ptr<MatrixDevice>(deviceList.at(0));
    QVERIFY(dev);

    dev = std::unique_ptr<MatrixDevice>(deviceList.at(6));
    QVERIFY(dev);
}

void DeviceMgmtTest::testDevice()
{
    auto state = lager::make_state(devices, lager::automatic_tag{});
    auto deviceList = MatrixDeviceList(state);

    auto dev = std::unique_ptr<MatrixDevice>(deviceList.at(0));
    QVERIFY(dev->deviceId() == QStringLiteral("device1"));
    QVERIFY(dev->ed25519Key() == QStringLiteral("device1ed25519"));
    QVERIFY(dev->curve25519Key() == QStringLiteral("device1curve25519"));
    QVERIFY(dev->displayName() == QStringLiteral("device1name"));
    QVERIFY(dev->trustLevel() == QStringLiteral("unseen"));

    dev = std::unique_ptr<MatrixDevice>(deviceList.at(1));
    QVERIFY(dev->trustLevel() == QStringLiteral("seen"));

    dev = std::unique_ptr<MatrixDevice>(deviceList.at(2));
    QVERIFY(dev->trustLevel() == QStringLiteral("verified"));

    dev = std::unique_ptr<MatrixDevice>(deviceList.at(3));
    QVERIFY(dev->trustLevel() == QStringLiteral("blocked"));
}

QTEST_MAIN(DeviceMgmtTest)
