/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>

#include <lager/constant.hpp>

#include <matrix-room-member-list-model.hpp>
#include <matrix-room-member.hpp>

#include "test-model.hpp"
#include "test-utils.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

class MatrixRoomMemberListModelTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testMembers();
    void testFilter();
};

static auto data = Kazv::EventList{
    makeRoomMember("@t1:tusooa.xyz", "user 1"),
    makeRoomMember("@t2:tusooa.xyz"),
};


void MatrixRoomMemberListModelTest::testMembers()
{
    MatrixRoomMemberListModel model(lager::make_constant(data));

    QCOMPARE(model.count(), 2);
    auto first = toUniquePtr(model.at(0));
    QCOMPARE(first->name(), u"user 1"_s);
    QCOMPARE(first->userId(), u"@t1:tusooa.xyz"_s);
    auto second = toUniquePtr(model.at(1));
    QVERIFY(second->name().isEmpty());
    QCOMPARE(second->userId(), u"@t2:tusooa.xyz"_s);

}

void MatrixRoomMemberListModelTest::testFilter()
{
    MatrixRoomMemberListModel model(
        lager::make_constant(data),
        QString(),
        lager::make_constant(Event(json{
            {"content", {{"@t1:tusooa.xyz", "mewmew"}}},
        }))
    );

    QCOMPARE(model.count(), 2);

    model.setfilter(u"@t"_s);
    QCOMPARE(model.count(), 2);

    {
        model.setfilter(u"user 1"_s);
        QCOMPARE(model.count(), 1);
        auto first = toUniquePtr(model.at(0));
        QCOMPARE(first->userId(), u"@t1:tusooa.xyz"_s);
    }

    {
        model.setfilter(u"t2"_s);
        QCOMPARE(model.count(), 1);
        auto first = toUniquePtr(model.at(0));
        QCOMPARE(first->userId(), u"@t2:tusooa.xyz"_s);
    }

    {
        model.setfilter(u"mewmew"_s);
        QCOMPARE(model.count(), 1);
        auto first = toUniquePtr(model.at(0));
        QCOMPARE(first->userId(), u"@t1:tusooa.xyz"_s);
    }
}

QTEST_MAIN(MatrixRoomMemberListModelTest)

#include "matrix-room-member-list-model-test.moc"
