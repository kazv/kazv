/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QtTest>

class QtJobHandlerTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testBaseJob();
    void testSetTimeout();
};
