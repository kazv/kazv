/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QtTest>
#include <QSignalSpy>
#include <lager/state.hpp>
#include <testfixtures/factory.hpp>
#include <matrix-room-timeline.hpp>
#include <matrix-sdk.hpp>
#include <matrix-room-list.hpp>
#include <matrix-room.hpp>
#include <matrix-event-list.hpp>
#include <matrix-event.hpp>
#include <matrix-event-reaction-list-model.hpp>
#include <matrix-event-reaction.hpp>
#include "test-model.hpp"
#include "test-utils.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;
using namespace Kazv::Factory;

class MatrixEventTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testEdits();
    void testEncryptedEdits();
    void testReactions();
};

void MatrixEventTest::testEdits()
{
    auto r = makeRoom(withRoomTimeline({
        makeEvent(withEventId("$0")),
        makeEvent(withEventId("$1") | withEventRelationship("moe.kazv.mxc.some-rel", "$0") | withEventKV("/origin_server_ts"_json_pointer, 1720751684500)),
        // this one is invalid because there is no m.new_content
        makeEvent(withEventId("$2") | withEventContent(json{{"body", "first"}}) | withEventRelationship("m.replace", "$1") | withEventKV("/origin_server_ts"_json_pointer, 1720751684700)),
        makeEvent(withEventId("$3") | withEventContent(json{{"m.new_content", {{"body", "second"}}}}) | withEventRelationship("m.replace", "$1") | withEventKV("/origin_server_ts"_json_pointer, 1720751684800)),
        makeEvent(withEventId("$4") | withEventContent(json{{"m.new_content", {{"body", "third"}}}}) | withEventRelationship("m.replace", "$1") | withEventKV("/origin_server_ts"_json_pointer, 1720751684900)),
    }));

    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());

    auto event = toUniquePtr(timeline->at(3));
    QCOMPARE(event->eventId(), u"$1"_s);
    auto history = toUniquePtr(event->history());
    // three versions including the original one
    QCOMPARE(history->count(), 3);

    QCOMPARE(toUniquePtr(history->at(0))->underlyingEvent(), event->underlyingEvent());

    auto v1 = toUniquePtr(history->at(1));
    QCOMPARE(v1->content()[u"body"_s], QStringLiteral("second"));
    QCOMPARE(v1->eventId(), QStringLiteral("$3"));
    QCOMPARE(v1->underlyingEvent().originServerTs(), 1720751684800);
    QCOMPARE(v1->relationType(), QStringLiteral("moe.kazv.mxc.some-rel"));

    auto v2 = toUniquePtr(history->at(2));
    QCOMPARE(v2->content()[u"body"_s], QStringLiteral("third"));
    QCOMPARE(v2->eventId(), QStringLiteral("$4"));
    QCOMPARE(v2->underlyingEvent().originServerTs(), 1720751684900);
    QCOMPARE(v2->relationType(), QStringLiteral("moe.kazv.mxc.some-rel"));
}

void MatrixEventTest::testEncryptedEdits()
{
    auto r = makeRoom();
    auto events = EventList{
        makeEvent(withEventId("$0") | withEventType("m.room.encrypted")).setDecryptedJson(json{
            {"room_id", r.roomId},
            {"type", "m.room.message"},
            {"content", {{"body", "first"}}},
        }, Event::Decrypted),
        makeEvent(withEventId("$1") | withEventType("m.room.encrypted") | withEventRelationship("m.replace", "$0")).setDecryptedJson(json{
            {"room_id", r.roomId},
            {"type", "m.room.message"},
            {"content", {{"m.new_content", {{"body", "second"}}}}},
        }, Event::Decrypted),
    };
    withRoomTimeline(events)(r);

    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());

    auto event = toUniquePtr(timeline->at(1));
    QCOMPARE(event->eventId(), u"$0"_s);
    auto history = toUniquePtr(event->history());
    // three versions including the original one
    QCOMPARE(history->count(), 2);

    QCOMPARE(toUniquePtr(history->at(0))->underlyingEvent(), event->underlyingEvent());

    auto v1 = toUniquePtr(history->at(1));
    QCOMPARE(v1->sender(), QString::fromStdString(events[1].sender()));
    QCOMPARE(v1->content()[u"body"_s], QStringLiteral("second"));
    QCOMPARE(v1->eventId(), QStringLiteral("$1"));
    QCOMPARE(v1->relationType(), QStringLiteral());
}

void MatrixEventTest::testReactions()
{
    auto r = makeRoom();
    auto events = EventList{
        makeEvent(withEventId("$0") | withEventType("m.room.encrypted")).setDecryptedJson(json{
            {"room_id", r.roomId},
            {"type", "m.room.message"},
            {"content", {{"body", "first"}}},
        }, Event::Decrypted),
        makeEvent(withEventId("$1")
            | withEventType("m.room.encrypted")
            | withEventRelationship("m.annotation", "$0")
            | withEventKV("/content/m.relates_to/key"_json_pointer, "mew")
        ).setDecryptedJson(json{
            {"room_id", r.roomId},
            {"type", "m.reaction"},
        }, Event::Decrypted),
        makeEvent(withEventId("$2")
            | withEventType("m.room.encrypted")
            | withEventRelationship("m.annotation", "$0")
            | withEventKV("/content/m.relates_to/key"_json_pointer, "mew")
        ).setDecryptedJson(json{
            {"room_id", r.roomId},
            {"type", "m.reaction"},
        }, Event::Decrypted),
        makeEvent(withEventId("$3")
            | withEventType("m.room.encrypted")
            | withEventRelationship("m.annotation", "$0")
            | withEventKV("/content/m.relates_to/key"_json_pointer, "meww")
        ).setDecryptedJson(json{
            {"room_id", r.roomId},
            {"type", "m.reaction"},
        }, Event::Decrypted),
    };
    withRoomTimeline(events)(r);

    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());

    auto event = toUniquePtr(timeline->at(events.size() - 1));
    QCOMPARE(event->eventId(), u"$0"_s);
    auto reactions = toUniquePtr(event->reactions());
    QCOMPARE(reactions->count(), 2);
    auto r0 = toUniquePtr(reactions->at(0));
    auto r1 = toUniquePtr(reactions->at(1));
    QVERIFY(r0->count() == 1 || r1->count() == 1);
    QVERIFY(r0->count() == 2 || r1->count() == 2);
    QVERIFY(r0->key() == u"mew"_s || r1->key() == u"mew"_s);
    QVERIFY(r0->key() == u"meww"_s || r1->key() == u"meww"_s);
}

QTEST_MAIN(MatrixEventTest)

#include "matrix-event-test.moc"
