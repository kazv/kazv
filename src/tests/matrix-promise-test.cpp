/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <vector>
#include <algorithm>

#include <QtTest>
#include <QSignalSpy>
#include <QTimer>
#include <QJsonValue>

#include "matrix-promise.hpp"
#include "qt-promise-handler.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

class MatrixPromiseTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testPromise();
    void testDestroyEarlier();
    void testCreateResolved();
};

template<class T, class F>
std::shared_ptr<QSignalSpy> makeSpy(T &&obj, F &&func)
{
    return std::shared_ptr<QSignalSpy>(new QSignalSpy(std::forward<T>(obj), std::forward<F>(func)));
}

void MatrixPromiseTest::testPromise()
{
    QEventLoop loop;
    QObject *obj = new QObject(&loop);
    auto ph = QtPromiseHandler(std::ref(*obj));
    auto sgph = SingleTypePromiseInterface<EffectStatus>(ph);

    auto p1 = sgph.createResolved({});
    auto p2 = p1.then([](auto) { return EffectStatus(false, json::object({{"error", "some"}, {"errorCode", "code"}})); });
    auto p3 = p2.then([](auto) { return EffectStatus(true, json::array({0, 1, 2})); });
    p3.then([&loop](auto) { loop.quit(); });

    auto mp1 = MatrixPromise(std::move(p1));
    auto mp2 = MatrixPromise(std::move(p2));
    auto mp3 = MatrixPromise(std::move(p3));

    auto calledSpies = std::vector{
        makeSpy(&mp1, &MatrixPromise::resolved),
        makeSpy(&mp1, &MatrixPromise::succeeded),
        makeSpy(&mp2, &MatrixPromise::resolved),
        makeSpy(&mp2, &MatrixPromise::failed),
        makeSpy(&mp3, &MatrixPromise::resolved),
        makeSpy(&mp3, &MatrixPromise::succeeded),
    };

    auto notCalledSpies = std::vector{
        makeSpy(&mp1, &MatrixPromise::failed),
        makeSpy(&mp2, &MatrixPromise::succeeded),
        makeSpy(&mp3, &MatrixPromise::failed),
    };

    loop.exec();

    QVERIFY(std::all_of(calledSpies.begin(), calledSpies.end(), [](auto s) { return s->count() == 1; }));
    QVERIFY(std::all_of(notCalledSpies.begin(), notCalledSpies.end(), [](auto s) { return s->count() == 0; }));

    QCOMPARE(calledSpies[0]->first().at(0), true);
    QCOMPARE(calledSpies[0]->first().at(1), QJsonValue(QJsonValue::Null));
    QCOMPARE(calledSpies[3]->first().at(0), QJsonValue(QJsonObject{{u"error"_s, u"some"_s}, {u"errorCode"_s, u"code"_s}}));
    QCOMPARE(calledSpies[5]->first().at(0), QJsonValue(QJsonArray{0, 1, 2}));
}

void MatrixPromiseTest::testDestroyEarlier()
{
    QEventLoop loop;
    QObject *obj = new QObject(&loop);
    auto ph = QtPromiseHandler(std::ref(*obj));
    auto sgph = SingleTypePromiseInterface<EffectStatus>(ph);

    auto p1 = sgph.createResolved({});
    auto p2 = p1.then([](auto) {});
    p2.then([&loop](auto) { loop.quit(); });

    auto mp1 = new MatrixPromise(std::move(p1));
    delete mp1;

    loop.exec();
}

void MatrixPromiseTest::testCreateResolved()
{
    QEventLoop loop;
    QObject *obj = new QObject(&loop);

    auto mp1 = MatrixPromise::createResolved(true, QJsonValue(1), obj);

    auto spy = makeSpy(mp1, &MatrixPromise::succeeded);
    auto spy2 = makeSpy(mp1, &MatrixPromise::failed);

    connect(mp1, &MatrixPromise::resolved, &loop, &QEventLoop::quit);

    loop.exec();

    QVERIFY(spy->count() == 1);
    QVERIFY(spy2->count() == 0);
}

QTEST_MAIN(MatrixPromiseTest)

#include "matrix-promise-test.moc"
