/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <QtTest>

#include "qt-json.hpp"

class QtJsonTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testNumericConv();
};

static auto j = nlohmann::json::parse(R"({
  "foo": 1
})");

static auto jFloat = nlohmann::json::parse(R"({
  "foo": 1.0
})");

static auto jFloat2 = nlohmann::json::parse(R"({
  "foo": 1.1
})");

void QtJsonTest::testNumericConv()
{
    QJsonObject qj = j;
    nlohmann::json j2 = qj;
    QVERIFY(j2 == j);

    qj = jFloat;
    j2 = qj;
    QVERIFY(j2 == j);
    QVERIFY(j2 == jFloat);

    qj = jFloat2;
    j2 = qj;
    QVERIFY(j2 == jFloat2);
}

QTEST_MAIN(QtJsonTest)

#include "qt-json-test.moc"
