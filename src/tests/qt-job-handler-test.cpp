/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <basejob.hpp>

#include "tests.hpp"

#include "qt-job-handler-test.hpp"
#include "qt-job-handler.hpp"

using namespace Kazv;

void QtJobHandlerTest::testBaseJob()
{
    BaseJob job(TEST_SERVER_URL, "/.well-known/matrix/client", BaseJob::Get{}, "TestJob");

    QEventLoop loop;
    auto h = new QtJobHandler(&loop);

    h->submit(
        job.withData(json{{"test", "bar"}}),
        [&loop](auto r) {
            if (r.statusCode == 200) {
                QVERIFY( isBodyJson(r.body) );

                json j = r.jsonBody().get();

                qDebug() << QString::fromStdString(j.dump());

                try {
                    QVERIFY( (j.at("m.homeserver").at("base_url") != "") );
                } catch (...) {
                    QFAIL("thrown");
                }

                QVERIFY( r.dataStr("test") == "bar" );
            }
            loop.quit();
        });
    loop.exec();
}

void QtJobHandlerTest::testSetTimeout()
{
    QEventLoop loop;
    auto h = new QtJobHandler(&loop);

    std::vector<int> v;

    h->setTimeout(
        [&v, &loop] {
            v.push_back(500);
            loop.quit();
        }, 500);

    h->setTimeout(
        [&v] {
            v.push_back(100);
        }, 100);

    loop.exec();

    QVERIFY( v.size() == 2 );
    QVERIFY( v[0] == 100 );
    QVERIFY( v[1] == 500 );
}

QTEST_MAIN(QtJobHandlerTest)
