/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>
#include <QRandomGenerator>

#include <factory.hpp>

#include <matrix-room-timeline.hpp>
#include <matrix-sdk.hpp>
#include <matrix-room-list.hpp>
#include <matrix-room.hpp>

#include "test-model.hpp"
#include "test-utils.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

Q_DECLARE_METATYPE(EventList)

static int eventListMetaType{qRegisterMetaType<EventList>("EventList")};

class MatrixRoomTimelineBenchmarkTest : public QObject
{
    Q_OBJECT

public:
    enum IndexType {
        Random,
        CloseToLatest,
        Nonexistent,
    };

    Q_ENUM(IndexType)

private Q_SLOTS:
    void testIndexOfEvent_data();
    void testIndexOfEvent();
};

void MatrixRoomTimelineBenchmarkTest::testIndexOfEvent_data()
{
    QTest::addColumn<EventList>("events");
    QTest::addColumn<IndexType>("type");

    auto events = EventList{};
    for (int i = 0; i < 10000; ++i) {
        events = std::move(events).push_back(makeEvent(withEventId("$" + std::to_string(i)) | withEventKV("/origin_server_ts"_json_pointer, i * 1000)));
    }

    QTest::addRow("random event") << events << Random;
    QTest::addRow("event close to the latest") << events << CloseToLatest;
    QTest::addRow("nonexistent event") << events << Nonexistent;
}

void MatrixRoomTimelineBenchmarkTest::testIndexOfEvent()
{
    QFETCH(EventList, events);
    QFETCH(IndexType, type);

    auto r = makeRoom(withRoomTimeline(events));
    r.localEchoes = {
        LocalEchoDesc{"txn1", makeEvent(withEventId("")), LocalEchoDesc::Sending},
        LocalEchoDesc{"txn2", makeEvent(withEventId("")), LocalEchoDesc::Sending},
    };

    auto model = SdkModel{makeClient(withRoom(r))};
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(QString::fromStdString(r.roomId)));
    auto timeline = toUniquePtr(room->timeline());

    if (type == Random) {
        QRandomGenerator g;
        const int highest = events.size();
        QBENCHMARK {
            timeline->indexOfEvent(QString::fromStdString(
                "$" + std::to_string(g.bounded(highest))));
        }
    } else if (type == CloseToLatest) {
        QBENCHMARK {
            timeline->indexOfEvent("$9940");
        }
    } else if (type == Nonexistent) {
        QBENCHMARK {
            timeline->indexOfEvent("$xxx");
        }
    }
}

QTEST_MAIN(MatrixRoomTimelineBenchmarkTest)

#include "matrix-room-timeline-benchmark-test.moc"
