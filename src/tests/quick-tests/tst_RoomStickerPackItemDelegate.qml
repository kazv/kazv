/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtTest
import moe.kazv.mxc.kazv as MK
import org.kde.kirigami as Kirigami
import '../../contents/ui/room-settings' as KazvRS
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  KazvRS.RoomStickerPackItemDelegate {
    id: packItem
  }

  TestCase {
    id: roomStickerPackItemDelegateTest
    name: 'RoomStickerPackItemDelegateTest'
    when: windowShown

    function init() {
      item.mockHelper.clearAll();

      packItem.stickerPack = {
        packName: 'some pack',
        stateKey: 'some-pack',
        roomId: '!foo:example.com',
        isRoomState: true,
      };

      packItem.stickerRoomsEvent = {
        content: {
        },
      };
    }

    function test_usePack() {
      const usePackAction = findChild(packItem, 'usePackAction');
      verify(!usePackAction.checked);
      usePackAction.trigger();
      tryVerify(() => matrixSdk.sendAccountData.calledTimes() === 1);
      verify(JsHelpers.deepEqual(
        matrixSdk.sendAccountData.lastArgs()[1],
        {
          rooms: {
            '!foo:example.com': {
              'some-pack': {},
            },
          },
        },
      ));
    }

    function test_disusePack() {
      packItem.stickerRoomsEvent = {
        content: {
          rooms: {
            '!foo:example.com': {
              'some-pack': {},
            },
          },
        },
      };

      const usePackAction = findChild(packItem, 'usePackAction');
      verify(usePackAction.checked);
      usePackAction.trigger();
      tryVerify(() => matrixSdk.sendAccountData.calledTimes() === 1);
      verify(JsHelpers.deepEqual(
        matrixSdk.sendAccountData.lastArgs()[1],
        {
          rooms: {
            '!foo:example.com': {
            },
          },
        },
      ));
    }
  }
}
