/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import moe.kazv.mxc.kazv 0.0 as MK

TestCase {
  id: notificationsTest
  name: 'NotificationsTest'
  when: true

  signal switchToRoomRequested(string roomId)

  SignalSpy {
    id: switchToRoomSpy
  }

  Kazv.MessageNotification {
    id: notification
    roomId: '!foo:example.com'
  }

  function init() {
    switchToRoomSpy.clear();
    switchToRoomSpy.target = notificationsTest;
    switchToRoomSpy.signalName = 'switchToRoomRequested';
  }

  function test_messageNotification() {
    if (MK.KazvUtil.kfQtMajorVersion === 5) {
      notification.defaultActivated();
    } else {
      notification.defaultAction.activated();
    }
    tryCompare(switchToRoomSpy, 'count', 1);
    compare(switchToRoomSpy.signalArguments[0][0], '!foo:example.com');
  }
}
