/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtTest 1.0

import '../../js/matrix-helpers.js' as Helpers

TestCase {
  id: matrixHelpersTest
  name: 'MatrixHelpersTest'
  when: true

  property var eventTestData: [
    [{
      replyingToEventId: '',
      content: {
        body: '> <@tusooa:tusooa.xyz> some text\n> some text\n\nmew\n\nmew',
      },
    }, '> <@tusooa:tusooa.xyz> some text\n> some text\n\nmew\n\nmew'],
    [{
      replyingToEventId: '$1',
      content: {
        body: '> <@tusooa:tusooa.xyz> some text\n> some text\n\nmewmew',
      },
    }, 'mewmew'],
    [{
      replyingToEventId: '$1',
      content: {
        body: '> <@tusooa:tusooa.xyz> some text\n\nmewmew',
      },
    }, 'mewmew'],
    [{
      replyingToEventId: '$1',
      content: {
        body: '> <@tusooa:tusooa.xyz> some text\nmewmew',
      },
    }, 'mewmew'],
    [{
      replyingToEventId: '$1',
      content: {
        body: 'mewmew',
      },
    }, 'mewmew'],
    [{
      replyingToEventId: '$1',
      content: {
        body: '> <@tusooa:tusooa.xyz> some text\n> some text\n\n> some\nmewmew',
      },
    }, '> some\nmewmew'],
    [{
      replyingToEventId: '$1',
      content: {
        body: '> some\n\nmewmew',
      },
    }, '> some\n\nmewmew'],
  ]

  function test_getEventBodyForEditing() {
    for (const [event, expected] of eventTestData) {
      compare(Helpers.getEventBodyForEditing(event), expected);
    }
  }
}
