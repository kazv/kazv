/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import '../../contents/ui/room-settings' as KazvRS
import moe.kazv.mxc.kazv 0.0 as MK

import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var roomUnencrypted: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    encrypted: false,
    sendStateEvent: mockHelper.promise(),
  })

  property var roomEncrypted: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    encrypted: true,
  })

  property var roomLeft: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Leave,
    forgetRoom: mockHelper.promise(),
  })

  property var roomJoined: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    leaveRoom: mockHelper.promise(),
    topic: '',
  })

  property var roomWithMembers: JsHelpers.factory.room({
    name: 'some name',
    roomId: '!someid:example.com',
    members: () => ({
      at(index) {
        return {
          userId: `@u${index}:example.com`,
          name: `user ${index}`,
        };
      },
      count: 5
    }),
    setName: mockHelper.promise(),
  })

  property var roomWithoutName: JsHelpers.factory.room({
    name: '',
    roomId: '!someid:example.com',
  })

  property var roomWithTopic: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    topic: 'some topic',
    setTopic: mockHelper.promise(),
  })

  ColumnLayout {
    KazvRS.RoomSettingsPage {
      id: pageUnencrypted
      room: item.roomUnencrypted
    }

    KazvRS.RoomSettingsPage {
      id: pageEncrypted
      room: item.roomEncrypted
    }

    KazvRS.RoomSettingsPage {
      id: pageLeft
      room: item.roomLeft
    }

    KazvRS.RoomSettingsPage {
      id: pageJoined
      room: item.roomJoined
    }

    KazvRS.RoomSettingsPage {
      id: pageWithMembers
      room: item.roomWithMembers
    }

    KazvRS.RoomSettingsPage {
      id: pageWithoutName
      room: item.roomWithoutName
    }

    KazvRS.RoomSettingsPage {
      id: pageWithTopic
      room: item.roomWithTopic
    }
  }

  TestCase {
    id: roomSettingsPageTest
    name: 'RoomSettingsPageTest'
    when: windowShown

    function initTestCase() {
      pageJoined.contentItem.clip = false;
      pageWithTopic.contentItem.clip = false;
      pageWithMembers.contentItem.clip = false;
      item.matrixSdk.userId = '@foo:example.org';
    }

    function init() {
      pageJoined.submittingTopic = false;
      pageJoined.editingTopic = false;
      pageWithTopic.submittingTopic = false;
      pageWithTopic.editingTopic = false;
      pageWithMembers.submittingName = false;
      pageWithMembers.editingName = false;
      mockHelper.clearAll();
    }

    function test_encryptionIndicator() {
      compare(findChild(pageUnencrypted, 'encryptionIndicator').text, l10n.get('room-settings-not-encrypted'));
      verify(findChild(pageUnencrypted, 'enableEncryptionButton').visible);

      compare(findChild(pageEncrypted, 'encryptionIndicator').text, l10n.get('room-settings-encrypted'));
      verify(!findChild(pageEncrypted, 'enableEncryptionButton').visible);
    }

    function test_enableEncryption() {
      pageUnencrypted.encryptionPopup.accepted();
      tryVerify(() => roomUnencrypted.sendStateEvent.lastArgs()[0].type === 'm.room.encryption');
      tryVerify(() => roomUnencrypted.sendStateEvent.lastArgs()[0].state_key === '');
      verify(!findChild(pageUnencrypted, 'enableEncryptionButton').enabled);
      roomUnencrypted.sendStateEvent.lastRetVal().resolve(true, {});
      tryVerify(() => findChild(pageUnencrypted, 'enableEncryptionButton').enabled);
    }

    function test_forgetRoom() {
      const forgetRoomButton = findChild(pageLeft, 'forgetRoomButton');
      const confirmPopup = findChild(pageLeft, 'confirmForgetRoomPopup');
      verify(forgetRoomButton.visible);
      verify(!findChild(pageJoined, 'forgetRoomButton').visible);

      confirmPopup.accepted();
      tryVerify(() => roomLeft.forgetRoom.calledTimes() === 1);

      roomLeft.forgetRoom.lastRetVal().resolve(true, {});
      tryVerify(() => item.showPassiveNotification.calledTimes() === 1);
    }

    function test_forgetRoomFailure() {
      const confirmPopup = findChild(pageLeft, 'confirmForgetRoomPopup');
      confirmPopup.accepted();
      roomLeft.forgetRoom.lastRetVal().resolve(false, {});
      tryVerify(() => item.showPassiveNotification.calledTimes() === 1);
    }

    function test_leaveRoom() {
      const leaveRoomButton = findChild(pageJoined, 'leaveRoomButton');
      const confirmPopup = findChild(pageJoined, 'confirmLeaveRoomPopup');
      verify(leaveRoomButton.visible);

      confirmPopup.accepted();
      tryVerify(() => roomJoined.leaveRoom.calledTimes() === 1);
      roomJoined.leaveRoom.lastRetVal().resolve(true, {});
      console.log(roomJoined.membership);
      tryVerify(() => item.showPassiveNotification.calledTimes() === 1);
    }

    function test_leaveRoomFailure() {
      const confirmPopup = findChild(pageJoined, 'confirmLeaveRoomPopup');
      confirmPopup.accepted();
      roomJoined.leaveRoom.lastRetVal().resolve(false, {});
      tryVerify(() => item.showPassiveNotification.calledTimes() === 1);
    }

    function test_roomMembersAvatars() {
      const repeater = findChild(pageWithMembers, 'roomMembersAvatarsRepeater');
      tryVerify(() => repeater.count === 4);
      const more = findChild(pageWithMembers, 'roomMembersAvatarsMore');
      verify(more.visible);
    }

    function test_roomWithName() {
      const nameLabel = findChild(pageWithMembers, 'roomNameLabel');
      const idLabel = findChild(pageWithMembers, 'roomIdLabel');
      verify(nameLabel.text === 'some name');
      verify(idLabel.text === '!someid:example.com');
    }

    function test_roomWithoutName() {
      const nameLabel = findChild(pageWithoutName, 'roomNameLabel');
      const idLabel = findChild(pageWithMembers, 'roomIdLabel');
      compare(nameLabel.text, l10n.get('room-settings-name-missing'));
      compare(idLabel.text, '!someid:example.com');
    }

    function test_roomWithoutTopic() {
      const topicLabel = findChild(pageJoined, 'roomTopicLabel');
      verify(topicLabel.font.italic);
      compare(topicLabel.text, l10n.get('room-settings-topic-missing'));
    }

    function test_roomWithTopic() {
      const topicLabel = findChild(pageWithTopic, 'roomTopicLabel');
      verify(!topicLabel.font.italic);
      compare(topicLabel.text, 'some topic');
    }

    function test_editRoomTopic() {
      const editButton = findChild(pageWithTopic, 'editTopicButton');
      mouseClick(editButton);
      const textArea = findChild(pageWithTopic, 'roomTopicEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some topic');
      textArea.text = 'other topic';
      const saveTopicButton = findChild(pageWithTopic, 'saveTopicButton');
      verify(saveTopicButton.visible);
      verify(saveTopicButton.enabled);
      const discardTopicButton = findChild(pageWithTopic, 'discardTopicButton');
      verify(discardTopicButton.visible);
      verify(discardTopicButton.enabled);
      mouseClick(saveTopicButton);
      tryVerify(() => roomWithTopic.setTopic.calledTimes() === 1);
      compare(roomWithTopic.setTopic.lastArgs()[0], 'other topic');
      verify(!saveTopicButton.enabled);
      verify(!discardTopicButton.enabled);

      roomWithTopic.setTopic.lastRetVal().resolve(true, {});
      tryVerify(() => editButton.visible);
      verify(!saveTopicButton.visible);
    }

    function test_editRoomTopicFailed() {
      const editButton = findChild(pageWithTopic, 'editTopicButton');
      mouseClick(editButton);
      const textArea = findChild(pageWithTopic, 'roomTopicEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some topic');
      textArea.text = 'other topic';
      const saveTopicButton = findChild(pageWithTopic, 'saveTopicButton');
      mouseClick(saveTopicButton);
      tryVerify(() => roomWithTopic.setTopic.calledTimes() === 1);
      roomWithTopic.setTopic.lastRetVal().resolve(false, {});
      verify(saveTopicButton.enabled);
      verify(textArea.visible);
    }

    function test_editRoomTopicDiscard() {
      const editButton = findChild(pageWithTopic, 'editTopicButton');
      mouseClick(editButton);
      const textArea = findChild(pageWithTopic, 'roomTopicEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some topic');
      textArea.text = 'other topic';
      const discardTopicButton = findChild(pageWithTopic, 'discardTopicButton');
      mouseClick(discardTopicButton);
      compare(roomWithTopic.setTopic.calledTimes(), 0);
      verify(!textArea.visible);
      mouseClick(editButton);
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some topic');
    }

    function test_addRoomTopic() {
      const editButton = findChild(pageJoined, 'editTopicButton');
      mouseClick(editButton);
      const textArea = findChild(pageJoined, 'roomTopicEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === '');
    }

    function test_editRoomName() {
      const editButton = findChild(pageWithMembers, 'editNameButton');
      mouseClick(editButton);
      const textArea = findChild(pageWithMembers, 'roomNameEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some name');
      textArea.text = 'other name';
      const saveNameButton = findChild(pageWithMembers, 'saveNameButton');
      verify(saveNameButton.visible);
      verify(saveNameButton.enabled);
      const discardNameButton = findChild(pageWithMembers, 'discardNameButton');
      verify(discardNameButton.visible);
      verify(discardNameButton.enabled);
      mouseClick(saveNameButton);
      tryVerify(() => roomWithMembers.setName.calledTimes() === 1);
      compare(roomWithMembers.setName.lastArgs()[0], 'other name');
      verify(!saveNameButton.enabled);
      verify(!discardNameButton.enabled);

      roomWithMembers.setName.lastRetVal().resolve(true, {});
      tryVerify(() => editButton.visible);
      verify(!saveNameButton.visible);
    }

    function test_editRoomNameFailed() {
      const editButton = findChild(pageWithMembers, 'editNameButton');
      mouseClick(editButton);
      const textArea = findChild(pageWithMembers, 'roomNameEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some name');
      textArea.text = 'other name';
      const saveNameButton = findChild(pageWithMembers, 'saveNameButton');
      mouseClick(saveNameButton);
      tryVerify(() => roomWithMembers.setName.calledTimes() === 1);
      roomWithMembers.setName.lastRetVal().resolve(false, {});
      verify(saveNameButton.enabled);
      verify(textArea.visible);
    }

    function test_editRoomNameDiscard() {
      const editButton = findChild(pageWithMembers, 'editNameButton');
      mouseClick(editButton);
      const textArea = findChild(pageWithMembers, 'roomNameEdit');
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some name');
      textArea.text = 'other name';
      const discardNameButton = findChild(pageWithMembers, 'discardNameButton');
      mouseClick(discardNameButton);
      compare(roomWithMembers.setName.calledTimes(), 0);
      verify(!textArea.visible);
      mouseClick(editButton);
      tryVerify(() => textArea.visible);
      verify(textArea.text === 'some name');
    }
  }
}
