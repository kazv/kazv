/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window
import QtQuick.Controls
import QtTest 1.0

import moe.kazv.mxc.kazv 0.0 as MK
import org.kde.kirigami 2.13 as Kirigami
import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var makeLocalEcho: (i) => ({
    eventId: '',
    sender: '',
    type: 'm.room.message',
    stateKey: '',
    content: {
      msgtype: 'm.text',
      body: 'some body' + i,
    },
    encrypted: false,
    isState: false,
    unsignedData: {},
    isLocalEcho: true,
    isSending: true,
    isFailed: false,
    txnId: 'some-txn-id' + i,
  })

  property var makeTextEvent: (i) => ({
    eventId: '$' + i,
    sender: '@foo:tusooa.xyz',
    type: 'm.room.message',
    stateKey: '',
    content: {
      msgtype: 'm.text',
      body: 'some body',
    },
    formattedTime: '4:06 P.M.',
  })

  property var room: ({
    pinEvents: mockHelper.promise(),
    unpinEvents: mockHelper.promise(),
    redactEvent: mockHelper.promise(),
    removeLocalEcho: mockHelper.promise(),
    messageById(id) {
      return { eventId: id };
    },
    localEchoById(id) {
      return { txnId: id };
    }
  })

  Kazv.RoomTimelineView {
    anchors.fill: parent
    id: roomTimelineView
    timeline: item.timeline
  }

  property var timeline: ListModel {
    ListElement {}
    ListElement {}

    function at(index) {
      if (index < 1) {
        return makeLocalEcho(index);
      } else {
        return makeTextEvent(index);
      }
    }
  }

  TestCase {
    id: roomTimelineViewTest
    name: 'RoomTimelineViewTest'
    when: windowShown

    function init() {
      mockHelper.clearAll();
    }

    function test_selected() {
      tryVerify(() => roomTimelineView.itemAtIndex(0));
      tryVerify(() => roomTimelineView.itemAtIndex(1));

      const localEcho = roomTimelineView.itemAtIndex(0);
      verify(!localEcho.isSelected);

      const sentMessage = roomTimelineView.itemAtIndex(1);
      verify(!sentMessage.isSelected);

      roomTimelineView.selectedEventId = '$1';
      verify(sentMessage.isSelected);
    }

    function test_pinSuccess() {
      roomTimelineView.pinEventRequested('$2');
      const popup = findChild(Overlay.overlay, 'pinEventPopup');
      verify(popup);
      tryVerify(() => popup.opened);
      mouseClick(findChild(popup, 'confirmButton'));
      verify(room.pinEvents.calledTimes() === 1);
      verify(JsHelpers.deepEqual(room.pinEvents.lastArgs()[0], ['$2']));
      tryVerify(() => !popup.opened);
      tryVerify(() => !findChild(Overlay.overlay, 'pinEventPopup'));
      room.pinEvents.lastRetVal().resolve(true, {});
      compare(showPassiveNotification.calledTimes(), 1);
      compare(showPassiveNotification.lastArgs()[0], l10n.get('event-pin-success-prompt'));
    }

    function test_pinFailure() {
      roomTimelineView.pinEventRequested('$2');
      const popup = findChild(Overlay.overlay, 'pinEventPopup');
      verify(popup);
      tryVerify(() => popup.opened);
      mouseClick(findChild(popup, 'confirmButton'));
      verify(room.pinEvents.calledTimes() === 1);
      verify(JsHelpers.deepEqual(room.pinEvents.lastArgs()[0], ['$2']));
      tryVerify(() => !popup.opened);
      tryVerify(() => !findChild(Overlay.overlay, 'pinEventPopup'));
      room.pinEvents.lastRetVal().resolve(false, { errorCode: 'M_FORBIDDEN', error: 'You are not allowed to do so' });
      compare(showPassiveNotification.calledTimes(), 1);
      compare(showPassiveNotification.lastArgs()[0], l10n.get('event-pin-failed-prompt', { errorCode: 'M_FORBIDDEN', errorMsg: 'You are not allowed to do so' }));
    }

    function test_unpinSuccess() {
      roomTimelineView.unpinEventRequested('$2');
      const popup = findChild(Overlay.overlay, 'unpinEventPopup');
      verify(popup);
      tryVerify(() => popup.opened);
      mouseClick(findChild(popup, 'confirmButton'));
      verify(room.unpinEvents.calledTimes() === 1);
      verify(JsHelpers.deepEqual(room.unpinEvents.lastArgs()[0], ['$2']));
      tryVerify(() => !popup.opened);
      tryVerify(() => !findChild(Overlay.overlay, 'unpinEventPopup'));
      room.unpinEvents.lastRetVal().resolve(true, {});
      compare(showPassiveNotification.calledTimes(), 1);
      compare(showPassiveNotification.lastArgs()[0], l10n.get('event-unpin-success-prompt'));
    }

    function test_unpinFailure() {
      roomTimelineView.unpinEventRequested('$2');
      const popup = findChild(Overlay.overlay, 'unpinEventPopup');
      verify(popup);
      tryVerify(() => popup.opened);
      mouseClick(findChild(popup, 'confirmButton'));
      verify(room.unpinEvents.calledTimes() === 1);
      verify(JsHelpers.deepEqual(room.unpinEvents.lastArgs()[0], ['$2']));
      tryVerify(() => !popup.opened);
      tryVerify(() => !findChild(Overlay.overlay, 'unpinEventPopup'));
      room.unpinEvents.lastRetVal().resolve(false, { errorCode: 'M_FORBIDDEN', error: 'You are not allowed to do so' });
      compare(showPassiveNotification.calledTimes(), 1);
      compare(showPassiveNotification.lastArgs()[0], l10n.get('event-unpin-failed-prompt', { errorCode: 'M_FORBIDDEN', errorMsg: 'You are not allowed to do so' }));
    }

    function test_deleteEvent() {
      roomTimelineView.deleteEventRequested("some-event-id", false);
      const popup = findChild(Overlay.overlay, 'confirmDeletionPopup');
      verify(popup);
      tryVerify(() => popup.opened);
      mouseClick(findChild(popup, 'confirmButton'));
      verify(room.redactEvent.calledTimes() === 1);
      verify(room.redactEvent.lastArgs()[0], ['some-eventId']);
      tryVerify(() => !popup.opened);
      tryVerify(() => !findChild(Overlay.overlay, 'confirmDeletionPopup'));
      room.redactEvent.lastRetVal().resolve(false, { errorCode: 'M_FORBIDDEN', error: 'You are not allowed to do so' });
      compare(showPassiveNotification.calledTimes(), 1);
      compare(showPassiveNotification.lastArgs()[0], l10n.get('event-delete-failed', { errorMsg: 'You are not allowed to do so', errorCode: 'M_FORBIDDEN' }));
    }

    function test_deleteLocalEcho() {
      roomTimelineView.deleteEventRequested("some-txnId", true);
      const popup = findChild(Overlay.overlay, 'confirmDeletionPopup');
      verify(popup);
      tryVerify(() => popup.opened);
      mouseClick(findChild(popup, 'confirmButton'));
      verify(room.removeLocalEcho.calledTimes() === 1);
      verify(room.removeLocalEcho.lastArgs()[0], ['some-txnId']);
      tryVerify(() => !popup.opened);
      tryVerify(() => !findChild(Overlay.overlay, 'confirmDeletionPopup'));
    }
  }
}
