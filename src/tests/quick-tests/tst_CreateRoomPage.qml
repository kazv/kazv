/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  Kazv.CreateRoomPage {
    anchors.fill: parent
    id: createRoomPage
  }

  TestCase {
    id: createRoomPageTest
    name: 'CreateRoomPageTest'
    when: windowShown

    function initTestCase() {
      if (MK.KazvUtil.kfQtMajorVersion === 6) {
        createRoomPage.contentItem.clip = false;
      }
      waitForRendering(createRoomPage);
    }

    function cleanup() {
      mockHelper.clearAll();
      createRoomPage.inviteUserIds = [];
      createRoomPage.creatingRoom = false;
      createRoomPage.encryptedChangedOnce = false;
    }

    function test_createRoomPublic() {
      mouseClick(findChild(createRoomPage, 'typePublicRadio'));

      findChild(createRoomPage, 'roomNameInput').text = 'some name';
      findChild(createRoomPage, 'roomAliasInput').text = 'alias';
      findChild(createRoomPage, 'roomTopicInput').text = 'some topic';
      const button = findChild(createRoomPage, 'createRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.createRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.createRoom.lastArgs(), {
        isPrivate: false,
        name: 'some name',
        alias: 'alias',
        topic: 'some topic',
        invites: [],
        isDirect: false,
        allowFederate: true,
        powerLevelContentOverride: {},
        preset: MK.MatrixSdk.PublicChat,
        encrypted: false,
      }));
    }

    function test_createRoomPrivate() {
      mouseClick(findChild(createRoomPage, 'typePrivateRadio'));

      findChild(createRoomPage, 'roomNameInput').text = 'some name';
      findChild(createRoomPage, 'roomAliasInput').text = 'alias';
      findChild(createRoomPage, 'roomTopicInput').text = 'some topic';
      const button = findChild(createRoomPage, 'createRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.createRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.createRoom.lastArgs(), {
        isPrivate: true,
        name: 'some name',
        alias: 'alias',
        topic: 'some topic',
        invites: [],
        isDirect: false,
        allowFederate: true,
        powerLevelContentOverride: {},
        preset: MK.MatrixSdk.PrivateChat,
        encrypted: true,
      }));
    }

    function test_createRoomDirect() {
      mouseClick(findChild(createRoomPage, 'typeDirectRadio'));

      findChild(createRoomPage, 'roomNameInput').text = 'some name';
      findChild(createRoomPage, 'roomAliasInput').text = 'alias';
      findChild(createRoomPage, 'roomTopicInput').text = 'some topic';
      const button = findChild(createRoomPage, 'createRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.createRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.createRoom.lastArgs(), {
        isPrivate: true,
        name: 'some name',
        alias: 'alias',
        topic: 'some topic',
        invites: [],
        isDirect: false,
        allowFederate: true,
        powerLevelContentOverride: {},
        preset: MK.MatrixSdk.TrustedPrivateChat,
        encrypted: true,
      }));
    }

    function test_createRoomDirectInviteOne() {
      mouseClick(findChild(createRoomPage, 'typeDirectRadio'));

      findChild(createRoomPage, 'roomNameInput').text = 'some name';
      findChild(createRoomPage, 'roomAliasInput').text = 'alias';
      findChild(createRoomPage, 'roomTopicInput').text = 'some topic';

      const addInviteInput = findChild(createRoomPage, 'newInviteUserInput');
      addInviteInput.text = '@foo:example.org';
      const addInviteButton = findChild(createRoomPage, 'addInviteUserButton');
      mouseClick(addInviteButton);
      tryVerify(() => addInviteInput.text === '', 1000);

      const button = findChild(createRoomPage, 'createRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.createRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.createRoom.lastArgs(), {
        isPrivate: true,
        name: 'some name',
        alias: 'alias',
        topic: 'some topic',
        invites: ['@foo:example.org'],
        isDirect: true,
        allowFederate: true,
        powerLevelContentOverride: {},
        preset: MK.MatrixSdk.TrustedPrivateChat,
        encrypted: true,
      }));
    }

    function test_createRoomDirectInviteMultiple() {
      mouseClick(findChild(createRoomPage, 'typeDirectRadio'));

      findChild(createRoomPage, 'roomNameInput').text = 'some name';
      findChild(createRoomPage, 'roomAliasInput').text = 'alias';
      findChild(createRoomPage, 'roomTopicInput').text = 'some topic';

      const addInviteInput = findChild(createRoomPage, 'newInviteUserInput');
      const addInviteButton = findChild(createRoomPage, 'addInviteUserButton');
      addInviteInput.text = '@foo:example.org';
      mouseClick(addInviteButton);
      tryVerify(() => addInviteInput.text === '', 1000);

      addInviteInput.text = '@bar:example.org';
      mouseClick(addInviteButton);
      tryVerify(() => addInviteInput.text === '', 1000);

      const button = findChild(createRoomPage, 'createRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.createRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.createRoom.lastArgs(), {
        isPrivate: true,
        name: 'some name',
        alias: 'alias',
        topic: 'some topic',
        invites: ['@foo:example.org', '@bar:example.org'],
        isDirect: false,
        allowFederate: true,
        powerLevelContentOverride: {},
        preset: MK.MatrixSdk.TrustedPrivateChat,
        encrypted: true,
      }));
    }

    function test_invite() {
      mouseClick(findChild(createRoomPage, 'typePublicRadio'));

      findChild(createRoomPage, 'roomNameInput').text = 'some name';
      findChild(createRoomPage, 'roomAliasInput').text = 'alias';
      findChild(createRoomPage, 'roomTopicInput').text = 'some topic';
      const button = findChild(createRoomPage, 'createRoomButton');
      verify(button.enabled);
      mouseClick(button);

      // add three invites
      const addInviteInput = findChild(createRoomPage, 'newInviteUserInput');
      const addInviteButton = findChild(createRoomPage, 'addInviteUserButton');
      addInviteInput.text = '@foo:example.org';
      mouseClick(addInviteButton);
      tryVerify(() => addInviteInput.text === '', 1000);
      addInviteInput.text = '@bar:example.org';
      mouseClick(addInviteButton);
      tryVerify(() => addInviteInput.text === '', 1000);
      addInviteInput.text = '@baz:example.org';
      mouseClick(addInviteButton);
      tryVerify(() => addInviteInput.text === '', 1000);

      // remove the second invite (bar)
      tryVerify(() => findChild(createRoomPage, 'inviteUserItem_1'), 1000);
      const inviteItem = findChild(createRoomPage, 'inviteUserItem_1');
      compare(findChild(inviteItem, 'inviteUserIdLabel').text, '@bar:example.org');
      findChild(inviteItem, 'removeInviteAction').trigger();

      tryVerify(() => {
        const label = findChild(
          findChild(createRoomPage, 'inviteUserItem_1'),
          'inviteUserIdLabel'
        );
        return label && label.text === '@baz:example.org'
      }, 50000);

      tryVerify(() => item.matrixSdk.createRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.createRoom.lastArgs(), {
        isPrivate: false,
        name: 'some name',
        alias: 'alias',
        topic: 'some topic',
        invites: ['@foo:example.org', '@baz:example.org'],
        isDirect: false,
        allowFederate: true,
        powerLevelContentOverride: {},
        preset: MK.MatrixSdk.PublicChat,
        encrypted: false,
      }));
    }

    function test_encryptedCheckBox() {
      const publicRadio = findChild(createRoomPage, 'typePublicRadio');
      const privateRadio = findChild(createRoomPage, 'typePrivateRadio');
      const directRadio = findChild(createRoomPage, 'typeDirectRadio')
      const encryptedInput = findChild(createRoomPage, 'encryptedInput');
      mouseClick(publicRadio);
      verify(!encryptedInput.checked);
      mouseClick(privateRadio);
      verify(encryptedInput.checked);
      mouseClick(directRadio);
      verify(encryptedInput.checked);

      mouseClick(encryptedInput);
      verify(!encryptedInput.checked);
      mouseClick(publicRadio);
      verify(!encryptedInput.checked);
      mouseClick(directRadio);
      verify(!encryptedInput.checked);
    }
  }
}
