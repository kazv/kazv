/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  property var roomTimeline: QtObject {
    property var count: 5
    function at(i) {
      if (i < 2) {
        return {
          isLocalEcho: true,
        };
      }
      if (i < 3) {
        return {
          isLocalEcho: false,
          sender: '@foo:example.org',
          eventId: `$event${i}`,
        };
      }

      return {
        isLocalEcho: false,
        sender: '@bar:example.org',
        eventId: `$event${i}`,
      };
    }
  }

  property var roomInvite: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Invite,
  })

  property var roomJoin: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    paginateBackFrom: mockHelper.promise(),
    pinnedEventsTimeline: () => ({
      count: 0,
    }),
  })

  property var roomLeave: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Leave,
  })

  property var roomWithPinned: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    paginateBackFrom: mockHelper.promise(),
    pinnedEventsTimeline: () => ({
      count: 1,
    }),
  })

  ColumnLayout {
    Kazv.RoomPage {
      id: pageInvite
      room: roomInvite
    }

    Kazv.RoomPage {
      id: pageJoin
      room: roomJoin
    }

    Kazv.RoomPage {
      id: pageLeave
      room: roomLeave
    }

    Kazv.RoomPage {
      id: pageWithPinned
      room: roomWithPinned
    }
  }

  TestCase {
    id: roomPageTest
    name: 'RoomPageTest'
    when: windowShown

    function init() {
      matrixSdk.userId = '@foo:example.org';
      mockHelper.clearAll();
    }

    function test_inviteRoom() {
      const sendMessageBox = findChild(pageInvite, 'sendMessageBox');
      verify(sendMessageBox);
      verify(!sendMessageBox.enabled);
    }

    function test_joinRoom() {
      const sendMessageBox = findChild(pageJoin, 'sendMessageBox');
      verify(sendMessageBox);
      verify(sendMessageBox.enabled);
    }

    function test_leaveRoom() {
      const sendMessageBox = findChild(pageLeave, 'sendMessageBox');
      verify(sendMessageBox);
      verify(!sendMessageBox.enabled);
    }

    function test_receiptableEventId() {
      verify(pageJoin.getLastReceiptableEventId(item.roomTimeline, item.roomTimeline.count) === '$event3');
    }

    function test_paginateBack() {
      pageJoin.paginateBackRequested('$1');
      tryVerify(() => roomJoin.paginateBackFrom.calledTimes() === 1);
      verify(roomJoin.paginateBackFrom.lastArgs()[0] === '$1');
      // one pagination under way, do not call it twice
      pageJoin.paginateBackRequested('$1');
      verify(roomJoin.paginateBackFrom.calledTimes() === 1);
      // paginating from another event is not affected
      pageJoin.paginateBackRequested('$2');
      tryVerify(() => roomJoin.paginateBackFrom.calledTimes() === 2);
      verify(roomJoin.paginateBackFrom.lastArgs()[0] === '$2');
      // after the promise resolves, we can paginate from this id again
      roomJoin.paginateBackFrom.retVals[0].resolve(true);
      pageJoin.paginateBackRequested('$1');
      tryVerify(() => roomJoin.paginateBackFrom.calledTimes() === 3);
      verify(roomJoin.paginateBackFrom.lastArgs()[0] === '$1');
    }

    function test_pinnedAction() {
      verify(!findChild(pageJoin, 'pinnedEventsAction').visible);
      verify(!findChild(pageJoin, 'pinnedEventsAction').enabled);
      verify(findChild(pageWithPinned, 'pinnedEventsAction').visible);
      verify(findChild(pageWithPinned, 'pinnedEventsAction').enabled);
    }
  }
}
