/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var event: ({
    eventId: '',
    sender: '',
    type: 'm.room.message',
    stateKey: '',
    content: {
    },
    redacted: true,
    encrypted: false,
    isState: false,
    unsignedData: {},
    isLocalEcho: false,
    isSending: false,
    isFailed: false,
    txnId: 'some-txn-id',
  })

  property var sender: ({
    membership: 'join',
    userId: '@foo:tusooa.xyz',
    name: 'foo',
    avatarMxcUri: '',
  })

  Kazv.EventView {
    id: eventView
    event: item.event
    sender: item.sender
  }

  TestCase {
    id: eventViewRedactedTest
    name: 'EventViewRedactedTest'
    when: windowShown

    function test_redacted() {
      verify(eventView.getMessageType(event) === 'redacted');
      const child = findChild(eventView, 'redactedEvent');
      verify(child);
      verify(child.text === l10n.get('event-deleted'));
    }
  }
}
