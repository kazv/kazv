/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  Kazv.JoinRoomPage {
    id: joinRoomPage
  }

  TestCase {
    id: joinRoomPageTest
    name: 'JoinRoomPageTest'
    when: windowShown

    function initTestCase() {
      if (MK.KazvUtil.kfQtMajorVersion === 6) {
        joinRoomPage.contentItem.clip = false;
      }
    }

    function cleanup() {
      mockHelper.clearAll();
    }

    function test_joinRoomWithServers() {
      findChild(joinRoomPage, 'idOrAliasInput').text = '#foo:example.com';
      findChild(joinRoomPage, 'serversInput').text = 'example.com\nexample.org';
      const button = findChild(joinRoomPage, 'joinRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.joinRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.joinRoom.lastArgs(), [
        '#foo:example.com',
        ['example.com', 'example.org'],
      ]));

      item.matrixSdk.joinRoom.lastRetVal().resolve(true, {});
      tryVerify(() => item.pageStack.removePage.calledTimes() === 1);
    }

    function test_joinRoomWithoutServers() {
      findChild(joinRoomPage, 'idOrAliasInput').text = '#foo:example.com';
      findChild(joinRoomPage, 'serversInput').text = '';
      const button = findChild(joinRoomPage, 'joinRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.joinRoom.calledTimes() === 1, 1000);
      verify(JsHelpers.deepEqual(item.matrixSdk.joinRoom.lastArgs(), [
        '#foo:example.com',
        [],
      ]));

      item.matrixSdk.joinRoom.lastRetVal().resolve(true, {});
      tryVerify(() => item.pageStack.removePage.calledTimes() === 1);
    }

    function test_joinRoomFailure() {
      findChild(joinRoomPage, 'idOrAliasInput').text = '#foo:example.com';
      findChild(joinRoomPage, 'serversInput').text = '';
      const button = findChild(joinRoomPage, 'joinRoomButton');
      verify(button.enabled);
      mouseClick(button);

      tryVerify(() => item.matrixSdk.joinRoom.calledTimes() === 1, 1000);

      item.matrixSdk.joinRoom.lastRetVal().resolve(false, {});
      tryVerify(() => item.pageStack.removePage.calledTimes() === 0);
    }
  }
}
