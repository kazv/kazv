/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import './test-helpers' as QmlHelpers
import 'test-helpers.js' as JsHelpers
import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: upper

  readonly property var gnMap: upper.sdkVars.userGivenNicknameMap

  property var powerLevelMapping: ({
    '@mew:example.com': 100,
  })

  property var room: QtObject {
    signal onPowerLevelsChanged()
    function userPowerLevel(userId) {
      return powerLevelMapping[userId];
    }
    property var membership: MK.MatrixRoom.Join
    property var setUserPowerLevel: mockHelper.promise()
    property var ensureStateEvent: mockHelper.promise()
    property var kickUser: mockHelper.promise()
    property var banUser: mockHelper.promise()
    property var unbanUser: mockHelper.promise()
    property var setSelfName: mockHelper.promise()
  }

  property var userPage: null
  property var userPageComp: Component {
    Kazv.UserPage {
      userId: '@mew:example.com'
      user: ({ userId: '@mew:example.com', name: 'mew' })
      room: upper.room
    }
  }

  property var userPageWithoutName: null
  property var userPageWithoutNameComp: Component {
    Kazv.UserPage {
      userId: '@mew:example.com'
      user: ({ userId: '@mew:example.com', name: '' })
    }
  }

  ColumnLayout {
    id: layout
  }

  TestCase {
    id: userPageTest
    name: 'UserPageTest'
    when: windowShown

    function init() {
      upper.matrixSdk.userId = '@foo:example.com';
      gnMap.map = {};
      mockHelper.clearAll();
      room.membership = MK.MatrixRoom.Join;
      userPage = userPageComp.createObject(layout);
      userPageWithoutName = userPageWithoutNameComp.createObject(layout);
      layout.children = [userPage, userPageWithoutName];
      if (MK.KazvUtil.kfQtMajorVersion === 6) {
        userPage.contentItem.clip = false;
      }
    }

    function cleanup() {
      layout.children = [];
      userPage.destroy();
      userPage = null;
    }

    function test_powerLevel() {
      verify(findChild(userPage, 'powerLevelLabel').visible);
      verify(findChild(userPage, 'powerLevelLabel').text.includes('100'));
      verify(!findChild(userPage, 'newPowerLevelInput').visible);

      const editButton = findChild(userPage, 'editPowerLevelButton');
      verify(editButton.visible);
      verify(!editButton.enabled);
      tryVerify(() => userPage.room.ensureStateEvent.calledTimes() === 2);
      tryVerify(() => userPage.room.ensureStateEvent.args[1][0] === 'm.room.power_levels', 1000);
      tryVerify(() => userPage.room.ensureStateEvent.retVals[1], 1000);
      userPage.room.ensureStateEvent.retVals[1].resolve(true, {});
      tryVerify(() => userPage.powerLevelsLoaded);
      tryVerify(() => editButton.enabled);
      tryVerify(() => editButton.visible);
      mouseClick(editButton);

      const textField = findChild(userPage, 'newPowerLevelInput');
      tryVerify(() => textField.visible);
      textField.text = '50';

      const saveButton = findChild(userPage, 'savePowerLevelButton');
      verify(saveButton.visible);
      mouseClick(saveButton);
      tryVerify(() => room.setUserPowerLevel.calledTimes(), 1000);
      tryVerify(() => room.setUserPowerLevel.lastRetVal(), 1000);
      // Text field is not closed until the request succeeded
      verify(textField.visible);
      verify(textField.readOnly);
      verify(room.setUserPowerLevel.lastArgs()[0] === '@mew:example.com');
      verify(room.setUserPowerLevel.lastArgs()[1] === 50);
      // the buttons are disabled until the request responded
      verify(!saveButton.enabled);
      verify(!findChild(userPage, 'discardPowerLevelButton').enabled);

      room.setUserPowerLevel.lastRetVal().resolve(true, {});
      tryVerify(() => !textField.visible, 1000);

      // Text field is editable when we try to edit the power level again
      mouseClick(editButton);
      tryVerify(() => textField.visible, 1000);
      verify(!textField.readOnly);
    }

    function test_powerLevelEditFailure() {
      tryVerify(() => userPage.room.ensureStateEvent.retVals[1], 1000);
      userPage.room.ensureStateEvent.retVals[1].resolve(true, {});

      const editButton = findChild(userPage, 'editPowerLevelButton');
      tryVerify(() => editButton.enabled);
      mouseClick(editButton);

      const textField = findChild(userPage, 'newPowerLevelInput');
      tryVerify(() => textField.visible, 1000);
      textField.text = '50';

      const saveButton = findChild(userPage, 'savePowerLevelButton');
      mouseClick(saveButton);
      tryVerify(() => room.setUserPowerLevel.lastRetVal(), 1000);

      // Text field is not closed until the request succeeded
      room.setUserPowerLevel.lastRetVal().resolve(false, {});

      // text field is no longer readonly if failed
      tryVerify(() => !textField.readOnly, 1000);
      // show error message
      verify(upper.showPassiveNotification.calledTimes());

      // the buttons are enabled when the request responded
      verify(saveButton.enabled);
      verify(findChild(userPage, 'discardPowerLevelButton').enabled);
    }

    function test_banUser() {
      // Ban user is only available if membership ! = "ban"
      userPage.user = ({ membership: "join", userId: '@mew:example.com' });
      const banButton = findChild(userPage, 'banUserButton');
      verify(banButton.visible);
      verify(banButton.enabled);
      const reasonDialog = findChild(userPage, 'banUserReasonDialog');
      verify(!reasonDialog.opened);
      const textField = findChild(userPage, 'banUserReasonInput');

      mouseClick(banButton);
      verify(!reasonDialog.opened);
      verify(textField.visible);
      verify(!textField.readOnly);

      textField.text = 'some reason';
      reasonDialog.accept();
      tryVerify(() => !reasonDialog.opened, 1000);
      tryVerify(() => !banButton.enabled, 1000);
      tryVerify(() => room.banUser.lastRetVal(), 1000);
      verify(room.banUser.lastArgs()[0] === '@mew:example.com');
      verify(room.banUser.lastArgs()[1] === 'some reason');

      room.banUser.lastRetVal().resolve(true, {});
      tryVerify(() => textField.text === '', 1000);
      tryVerify(() => banButton.enabled, 1000);
    }

    function test_banUserFailure() {
      // Ban user is only available if membership ! = "ban"
      userPage.user = ({ membership: "join" });
      const banButton = findChild(userPage, 'banUserButton');
      verify(banButton.visible);
      verify(banButton.enabled);
      const reasonDialog = findChild(userPage, 'banUserReasonDialog');
      verify(!reasonDialog.opened);
      const textField = findChild(userPage, 'banUserReasonInput');

      mouseClick(banButton);
      verify(!reasonDialog.opened);
      verify(textField.visible);
      verify(!textField.readOnly);

      textField.text = 'some reason';
      reasonDialog.accept();
      tryVerify(() => !reasonDialog.opened, 1000);
      tryVerify(() => !banButton.enabled, 1000);
      tryVerify(() => room.banUser.lastRetVal(), 1000);
      verify(room.banUser.lastArgs()[0] === '@mew:example.com');
      verify(room.banUser.lastArgs()[1] === 'some reason');

      room.banUser.lastRetVal().resolve(false, {});
      tryVerify(() => upper.showPassiveNotification.calledTimes(), 1000);
      // text field is not cleared when failed
      tryVerify(() => textField.text === 'some reason', 1000);
      tryVerify(() => banButton.enabled, 1000);
    }

    function test_unbanUser() {
      // Unban user is only available if membership == "ban"
      userPage.user = ({ membership: "ban" });
      const unbanButton = findChild(userPage, 'unbanUserButton');
      verify(unbanButton.visible);

      verify(unbanButton.enabled);
      mouseClick(unbanButton);
      tryVerify(() => !unbanButton.enabled, 1000);
      verify(room.unbanUser.lastArgs()[0] == '@mew:example.com');
      verify(room.unbanUser.lastRetVal());

      room.unbanUser.lastRetVal().resolve(true, {});
      tryVerify(() => unbanButton.enabled, 1000);
    }

    function test_unbanUserFailure() {
      // Unban user is only available if membership == "ban"
      userPage.user = ({ membership: "ban" });
      const unbanButton = findChild(userPage, 'unbanUserButton');
      verify(unbanButton.visible);

      verify(unbanButton.enabled);
      mouseClick(unbanButton);
      tryVerify(() => !unbanButton.enabled, 1000);
      verify(room.unbanUser.lastArgs()[0] == '@mew:example.com');
      verify(room.unbanUser.lastRetVal());

      room.unbanUser.lastRetVal().resolve(false, {});
      tryVerify(() => upper.showPassiveNotification.calledTimes(), 1000);
      tryVerify(() => unbanButton.enabled, 1000);
    }

    function test_kickUser() {
      const kickButton = findChild(userPage, 'kickUserButton');
      verify(kickButton.visible);
      verify(kickButton.enabled);

      const reasonDialog = findChild(userPage, 'kickUserReasonDialog');
      verify(!reasonDialog.opened);
      const textField = findChild(userPage, 'kickUserReasonInput');

      mouseClick(kickButton);
      verify(!reasonDialog.opened);
      verify(!textField.readOnly);

      textField.text = 'some reason';
      reasonDialog.accept();
      tryVerify(() => !reasonDialog.opened, 1000);
      tryVerify(() => !kickButton.enabled, 1000);
      tryVerify(() => room.kickUser.lastRetVal(), 1000);
      verify(room.kickUser.lastArgs()[0] === '@mew:example.com');
      verify(room.kickUser.lastArgs()[1] === 'some reason');

      room.kickUser.lastRetVal().resolve(true, {});
      tryVerify(() => textField.text === '', 1000);
      // still available only because this is a mock call, no real action
      // is performed on the user
      tryVerify(() => kickButton.enabled, 1000);
    }

    function test_overridedName() {
      const input = findChild(userPage, 'nameOverrideInput');
      compare(input.text, '');

      gnMap.map = { '@mew:example.com': 'something' };
      tryCompare(input, 'text', 'something');
    }

    function test_noOverridedNameForSelf() {
      userPage.userId = '@foo:example.com';
      verify(!findChild(userPage, 'nameOverrideInput').visible);
    }

    function test_updateOverridedName() {
      const input = findChild(userPage, 'nameOverrideInput');
      compare(input.text, '');
      verify(input.enabled);
      input.text = 'something';
      const saveButton = findChild(userPage, 'saveNameOverrideButton');
      verify(saveButton.enabled);
      mouseClick(saveButton);
      tryVerify(() => !input.enabled);
      verify(!saveButton.enabled);
      compare(gnMap.setAndUpload.calledTimes(), 1);
      verify(JsHelpers.deepEqual(gnMap.setAndUpload.lastArgs(), ['@mew:example.com', 'something']));
      gnMap.setAndUpload.lastRetVal().resolve(true, {});
      tryVerify(() => input.enabled);
      verify(saveButton.enabled);
    }

    function test_updateOverridedNameFailed() {
      const input = findChild(userPage, 'nameOverrideInput');
      compare(input.text, '');
      verify(input.enabled);
      input.text = 'something';
      const saveButton = findChild(userPage, 'saveNameOverrideButton');
      verify(saveButton.enabled);
      mouseClick(saveButton);
      tryVerify(() => !input.enabled);
      verify(!saveButton.enabled);
      compare(gnMap.setAndUpload.calledTimes(), 1);
      verify(JsHelpers.deepEqual(gnMap.setAndUpload.lastArgs(), ['@mew:example.com', 'something']));
      gnMap.setAndUpload.lastRetVal().resolve(false, {});
      tryVerify(() => input.enabled);
      verify(saveButton.enabled);
    }

    function test_removeOverridedName() {
      gnMap.map = { '@mew:example.com': 'something' };
      const input = findChild(userPage, 'nameOverrideInput');
      compare(input.text, 'something');
      input.text = '';
      const saveButton = findChild(userPage, 'saveNameOverrideButton');
      verify(saveButton.enabled);
      mouseClick(saveButton);
      tryVerify(() => !input.enabled);
      verify(!saveButton.enabled);
      compare(gnMap.setAndUpload.calledTimes(), 1);
      verify(JsHelpers.deepEqual(gnMap.setAndUpload.lastArgs(), ['@mew:example.com', null]));
      gnMap.setAndUpload.lastRetVal().resolve(true, {});
      tryVerify(() => input.enabled);
      verify(saveButton.enabled);
    }

    function test_showUserNameAndId() {
      const nameLabel = findChild(userPage, 'userNameLabel');
      const idLabel = findChild(userPage, 'userIdLabel');
      verify(nameLabel.text === 'mew');
      verify(idLabel.text === '@mew:example.com');
      verify(idLabel.visible);
    }

    function test_showUserIdOnly() {
      const nameLabel = findChild(userPageWithoutName, 'userNameLabel');
      const idLabel = findChild(userPageWithoutName, 'userIdLabel');
      verify(nameLabel.text === '@mew:example.com');
      verify(!idLabel.visible);
    }

    function test_nonSelfName() {
      const selfNameInput = findChild(userPage, 'selfNameInput');
      verify(!selfNameInput.visible);
    }

    function test_nonJoinedRoomSelfName() {
      userPage.userId = '@foo:example.com';
      userPage.user.userId = '@foo:example.com';
      upper.room.membership = MK.MatrixRoom.Invite;
      const selfNameInput = findChild(userPage, 'selfNameInput');
      verify(!selfNameInput.visible);
    }

    function test_selfName() {
      userPage.userId = '@foo:example.com';
      userPage.user.userId = '@foo:example.com';
      const selfNameInput = findChild(userPage, 'selfNameInput');
      verify(selfNameInput.visible);
      verify(selfNameInput.text === 'mew');
      selfNameInput.text = 'some others';
      const button = findChild(userPage, 'saveSelfNameButton');
      verify(button.enabled);
      mouseClick(button);
      tryVerify(() => room.setSelfName.calledTimes() === 1);
      verify(!button.enabled);
      verify(!selfNameInput.enabled);
      verify(room.setSelfName.lastArgs()[0] === 'some others');
      room.setSelfName.lastRetVal().resolve(/* succ = */ true, {});
      tryVerify(() => button.enabled);
      verify(selfNameInput.enabled);
    }

    function test_updateSelfNameFailed() {
      userPage.userId = '@foo:example.com';
      userPage.user.userId = '@foo:example.com';
      const selfNameInput = findChild(userPage, 'selfNameInput');
      selfNameInput.text = 'some others';
      const button = findChild(userPage, 'saveSelfNameButton');
      verify(button.enabled);
      mouseClick(button);
      tryVerify(() => room.setSelfName.calledTimes() === 1);
      room.setSelfName.lastRetVal().resolve(/* succ = */ false, {});
      tryVerify(() => button.enabled);
      verify(selfNameInput.enabled);
      verify(showPassiveNotification.calledTimes() === 1);
    }
  }
}
