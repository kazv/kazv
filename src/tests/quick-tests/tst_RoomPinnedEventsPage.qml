/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtTest
import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var room: ({
    name: 'some room',
    pinnedEventsTimeline() {
      return item.timeline;
    },
  })
  property var makeTextEvent: (i) => ({
    eventId: '$' + i,
    sender: '@foo:tusooa.xyz',
    type: 'm.room.message',
    stateKey: '',
    content: {
      msgtype: 'm.text',
      body: 'some body',
    },
    formattedTime: '4:06 P.M.',
  })

  property var timeline: ListModel {
    ListElement {}
    ListElement {}

    function at(index) {
      return makeTextEvent(index);
    }
  }

  Kazv.RoomPinnedEventsPage {
    anchors.fill: parent
    id: pinnedEventsPage
    room: item.room
  }

  TestCase {
    id: roomPinnedEventsPageTest
    name: 'RoomPinnedEventsPageTest'
    when: windowShown

    function initTestCase() {
      pinnedEventsPage.contentItem.clip = false;
    }

    function test_pinned() {
      const roomTimelineView = findChild(pinnedEventsPage, 'roomTimelineView');
      verify(roomTimelineView);
      tryVerify(() => roomTimelineView.itemAtIndex(0));
      tryVerify(() => roomTimelineView.itemAtIndex(1));
    }
  }
}
