/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers

TestCase {
  id: stickerPackNameProviderTest
  name: 'StickerPackNameProviderTest'
  when: true

  property var sdkVars: QtObject {
    property var userGivenNicknameMap: QtObject {
      property var map: ({})
    }
    property var roomList: QtObject {
      function room(roomId) {
        return stickerPackNameProviderTest.roomComp.createObject(stickerPackNameProviderTest, {
          roomId,
          name: `Some room ${roomId}`,
          heroEvents: [],
        });
      }
    }
  }
  property var l10n: JsHelpers.fluentMock

  property var providerFunctional: Kazv.StickerPackNameProvider {}
  property var providerReactive: Kazv.StickerPackNameProvider {}
  property var roomComp: Component {
    QtObject {
      property var name
      property var heroEvents
      property var roomId
    }
  }
  property var packComp: Component {
    QtObject {
      property var packName
      property var isAccountData
      property var isState
      property var stateKey
      property var roomId
    }
  }

  function init() {
  }

  function test_functional() {
    compare(
      providerFunctional.getName({ packName: 'some name' }),
      'some name');

    compare(providerFunctional.getName({
      packName: '',
      isAccountData: true,
    }), l10n.get('sticker-picker-user-stickers'));

    verify(
      providerFunctional.getName({
        packName: '',
        isState: true,
        stateKey: '',
      }).includes('sticker-picker-room-default-sticker-pack-name'));
  }

  function test_reactiveAgainstPack() {
    providerReactive.pack = stickerPackNameProviderTest.packComp.createObject(stickerPackNameProviderTest, {
      packName: 'some name',
      roomId: '!example:example.com',
      isState: true,
      isAccountData: false,
    });

    compare(providerReactive.name, 'some name');

    providerReactive.pack.packName = 'some other name';
    compare(providerReactive.name, 'some other name');

    providerReactive.pack.packName = '';
    verify(providerReactive.name.includes('Some room !example:example.com'));

    providerReactive.pack.isAccountData = true;
    providerReactive.pack.isState = false;
    compare(providerReactive.name, l10n.get('sticker-picker-user-stickers'));
  }

  function test_reactiveAgainstRoom() {
    providerReactive.pack = packComp.createObject(stickerPackNameProviderTest, {
      packName: '',
      roomId: '!example:example.com',
      isState: true,
      isAccountData: false,
    });

    providerReactive.roomNameProvider.room.name = 'Some other room';
    verify(providerReactive.name.includes('Some other room'));
  }
}
