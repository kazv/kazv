/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui/room-settings' as RoomSettings
import 'test-helpers' as QmlHelpers
import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  property var room: ({
    name: 'room',
    inviteUser: mockHelper.promise(),
  })

  RoomSettings.RoomInvitePage {
    id: roomInvitePage
    room: item.room
  }

  TestCase {
    id: roomInvitePageTest
    name: 'RoomInvitePageTest'
    when: windowShown

    function initTestCase() {
      if (MK.KazvUtil.kfQtMajorVersion === 6) {
        roomInvitePage.contentItem.clip = false;
      }
    }

    function cleanup() {
      mockHelper.clearAll();
    }

    function test_roomInvitePage() {
      const input = findChild(roomInvitePage, 'inviteUserIdInput');
      input.text = '@mew:example.com';

      const button = findChild(roomInvitePage, 'inviteUserButton');
      mouseClick(button);
      tryVerify(() => item.room.inviteUser.calledTimes(), 1000);
      verify(item.room.inviteUser);
      verify(item.room.inviteUser.lastArgs()[0] == '@mew:example.com');
      verify(!button.enabled);
      const promise = item.room.inviteUser.lastRetVal();
      promise.resolve(true, {});
      tryVerify(() => button.enabled, 1000);
      tryVerify(() => item.pageStack.removePage.calledTimes(), 1000);
    }

    function test_roomInvitePageFailed() {
      const input = findChild(roomInvitePage, 'inviteUserIdInput');
      input.text = '@mew:example.com';

      const button = findChild(roomInvitePage, 'inviteUserButton');
      mouseClick(button);
      tryVerify(() => item.room.inviteUser.calledTimes(), 1000);
      const promise = item.room.inviteUser.lastRetVal();
      verify(promise);
      promise.resolve(false, {});
      tryVerify(() => button.enabled, 1000);
      verify(!item.pageStack.removePage.calledTimes());
    }
  }
}
