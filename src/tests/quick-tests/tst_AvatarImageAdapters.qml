/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtTest
import '../../contents/ui' as Kazv
import './test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  ColumnLayout {
    Kazv.AvatarAdapter {
      id: avatar
      mxcUri: 'mxc://example.com/xxx'
    }
    Kazv.ImageAdapter {
      id: image
      mxcUri: 'mxc://example.com/yyy'
    }
  }

  TestCase {
    id: avatarImageAdaptersTest
    name: 'AvatarImageAdaptersTest'
    when: windowShown

    function init() {
      item.mockHelper.clearAll();
    }

    function test_avatarReactive() {
      compare(item.kazvIOManager.cacheFile.calledTimes(), 0);
      avatar.mxcUri = 'mxc://example.com/zzz';
      compare(item.kazvIOManager.cacheFile.calledTimes(), 1);
    }

    function test_imageReactive() {
      compare(item.kazvIOManager.cacheFile.calledTimes(), 0);
      image.mxcUri = 'mxc://example.com/zzz';
      compare(item.kazvIOManager.cacheFile.calledTimes(), 1);
    }
  }
}
