/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15

QtObject {
  id: managerMock
  property var startNewUploadJob: mockHelper.noop([
    'serverUrl',
    'fileUrl',
    'token',
    'roomId',
    'roomList',
    'encrypted',
    'draftRelType',
    'draftRelatedTo'
  ])
  property var deleteDownloadJob: mockHelper.noop()
  property var deleteUploadJob: mockHelper.noop()
  property var cacheFile: mockHelper.noop()

  function getDownloadJob (jobId) {
    const component = Qt.createComponent("KazvIOJobMock.qml");
    return component.createObject(managerMock);
  }
}
