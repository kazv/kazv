/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15

QtObject {
  signal result(int ec)
  function isResulted () { return false; }
  function isSuspended () { return false; }
  function fileName () { return ''; }
  property var progress: 0
}
