/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15

QtObject {
  signal resolved(var isSuccess, var data)
  signal succeeded(var data)
  signal failed(var data)

  function onResolved(isSuccess, data) {
    if (isSuccess) {
      succeeded(data);
    } else {
      failed(data);
    }
  }

  function resolve(isSuccess, data) {
    resolved(isSuccess, data);
  }
}
