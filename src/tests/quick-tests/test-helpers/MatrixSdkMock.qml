/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15

QtObject {
  property var userId: ''
  property var serverUrl: 'https://example.com'
  property var token: 'token'
  property var updateStickerPack: mockHelper.promise()
  property var createRoom: mockHelper.promise([
    'isPrivate',
    'name',
    'alias',
    'invites',
    'isDirect',
    'allowFederate',
    'topic',
    'powerLevelContentOverride',
    'preset',
    'encrypted',
  ])
  property var joinRoom: mockHelper.promise()
  property var sendAccountData: mockHelper.promise()
  property var login: mockHelper.noop()

  property var sessions: []

  function allSessions() {
    return sessions;
  }

  function mxcUriToHttp (uri) {
    console.log('mxcUriToHttp');
    return uri || '';
  }

  function devicesOfUser (userId) {
    return [];
  }
}
