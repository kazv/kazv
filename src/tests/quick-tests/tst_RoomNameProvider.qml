/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import './test-helpers' as QmlHelpers
import 'test-helpers.js' as JsHelpers

TestCase {
  id: roomNameProviderTest
  name: 'RoomNameProviderTest'
  when: true

  property var sdkVars: QtObject {
    property var userGivenNicknameMap: QtObject {
      property var map: ({})
    }
  }
  property var l10n: JsHelpers.fluentMock

  property var providerFunctional: Kazv.RoomNameProvider {}
  property var providerReactive: Kazv.RoomNameProvider {}
  property var roomComp: Component {
    QtObject {
      property var name
      property var heroEvents
      property var roomId
    }
  }
  property var heroEvents: [{
    type: 'm.room.member',
    state_key: '@foo:example.com',
    content: {
      displayname: 'foo',
    },
  }, {
    type: 'm.room.member',
    state_key: '@bar:example.com',
    content: {
      displayname: 'bar',
    },
  }]

  function init() {
    sdkVars.userGivenNicknameMap.map = {
      '@foo:example.com': 'something',
    };
  }

  function test_functional() {
    compare(
      providerFunctional.getName({ name: 'some name' }),
      l10n.get(
        'room-list-view-room-item-title-name',
        { name: 'some name' }));

    compare(providerFunctional.getName({
      heroEvents
    }), l10n.get('room-list-view-room-item-title-heroes', {
      hero: l10n.get(
        'user-name-overrided',
        { overridedName: 'something', globalName: 'foo' }),
      secondHero: 'bar',
      otherNum: 1,
    }));

    compare(
      providerFunctional.getName({ roomId: '!room:example.com' }),
      l10n.get(
        'room-list-view-room-item-title-id',
        { roomId: '!room:example.com' }));
  }

  function test_reactiveAgainstRoom() {
    providerReactive.room = roomComp.createObject(roomNameProviderTest, {
      name: 'some name',
      heroEvents,
      roomId: '!room:example.com',
    });
    compare(
      providerReactive.name,
      l10n.get(
        'room-list-view-room-item-title-name',
        { name: 'some name' }));
    providerReactive.room.name = 'other name';
    compare(
      providerReactive.name,
      l10n.get(
        'room-list-view-room-item-title-name',
        { name: 'other name' }));
    providerReactive.room.name = undefined;
    compare(
      providerReactive.name,
      l10n.get('room-list-view-room-item-title-heroes', {
        hero: l10n.get(
          'user-name-overrided',
          { overridedName: 'something', globalName: 'foo' }),
        secondHero: 'bar',
        otherNum: 1,
      }));

    providerReactive.room.heroEvents = [heroEvents[0]];
    compare(
      providerReactive.name,
      l10n.get('room-list-view-room-item-title-heroes', {
        hero: l10n.get(
          'user-name-overrided',
          { overridedName: 'something', globalName: 'foo' }),
        secondHero: undefined,
        otherNum: 0,
      }));

    providerReactive.room.heroEvents = [];
    compare(
      providerReactive.name,
      l10n.get('room-list-view-room-item-title-id', {
        roomId: '!room:example.com',
      }));

    providerReactive.room = roomComp.createObject(roomNameProviderTest, {
      name: 'some name 2',
      heroEvents,
      roomId: '!room:example.com',
    });
    compare(
      providerReactive.name,
      l10n.get(
        'room-list-view-room-item-title-name',
        { name: 'some name 2' }));
  }

  function test_reactiveAgainstOverrides() {
    providerReactive.room = roomComp.createObject(roomNameProviderTest, {
      heroEvents,
      roomId: '!room:example.com',
    });

    compare(
      providerReactive.name,
      l10n.get('room-list-view-room-item-title-heroes', {
        hero: l10n.get(
          'user-name-overrided',
          { overridedName: 'something', globalName: 'foo' }),
        secondHero: 'bar',
        otherNum: 1,
      }));
    sdkVars.userGivenNicknameMap.map = {
    };

    compare(
      providerReactive.name,
      l10n.get('room-list-view-room-item-title-heroes', {
        hero: 'foo',
        secondHero: 'bar',
        otherNum: 1,
      }));
  }
}
