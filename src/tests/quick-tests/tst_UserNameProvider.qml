/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import './test-helpers' as QmlHelpers
import 'test-helpers.js' as JsHelpers

TestCase {
  id: userNameProviderTest
  name: 'UserNameProviderTest'
  when: true

  property var sdkVars: QtObject {
    property var userGivenNicknameMap: QtObject {
      property var map: ({})
    }
  }
  property var l10n: JsHelpers.fluentMock

  property var userOverrided: ({
    name: 'foo',
    userId: '@foo:tusooa.xyz'
  })

  property var userNotOverrided: ({
    name: 'bar',
    userId: '@bar:tusooa.xyz'
  })

  property var userIdOnly: ({ userId: '@bar:tusooa.xyz' })

  property var providerFunctional: Kazv.UserNameProvider {}
  property var providerReactive: Kazv.UserNameProvider {}
  property var userComp: Component {
    QtObject {
      property var name: 'bar'
      property var userId: '@bar:tusooa.xyz'
    }
  }

  function init() {
    sdkVars.userGivenNicknameMap.map = {
      '@foo:tusooa.xyz': 'something',
    };
  }

  function test_functional() {
    compare(
      providerFunctional.getName(userOverrided),
      l10n.get(
        'user-name-overrided', { overridedName: 'something', globalName: 'foo' }));
    compare(providerFunctional.getOverridedName(userOverrided), 'something');
    compare(
      providerFunctional.getName(userNotOverrided),
      'bar'
    );
    compare(providerFunctional.getOverridedName(userNotOverrided), '');

    compare(
      providerFunctional.getName(userIdOnly),
      '@bar:tusooa.xyz'
    );
  }

  function test_reactiveAgainstUser() {
    providerReactive.user = userComp.createObject(userNameProviderTest);
    compare(providerReactive.name, 'bar');
    compare(providerReactive.overridedName, '');
    providerReactive.user.userId = '@foo:tusooa.xyz';
    compare(
      providerReactive.name,
      l10n.get(
        'user-name-overrided', { overridedName: 'something', globalName: 'bar' }));
    compare(providerReactive.overridedName, 'something');

    providerReactive.user = userComp.createObject(userNameProviderTest, {
      userId: '@foo:tusooa.xyz',
      name: 'foo',
    });
    compare(
      providerReactive.name,
      l10n.get(
        'user-name-overrided', { overridedName: 'something', globalName: 'foo' }));
    compare(providerReactive.overridedName, 'something');
  }

  function test_reactiveAgainstOverrides() {
    providerReactive.user = userComp.createObject(userNameProviderTest);
    compare(providerReactive.name, 'bar');
    compare(providerReactive.overridedName, '');
    sdkVars.userGivenNicknameMap.map = {
      '@bar:tusooa.xyz': 'something',
    };

    compare(
      providerReactive.name,
      l10n.get(
        'user-name-overrided', { overridedName: 'something', globalName: 'bar' }));
    compare(providerReactive.overridedName, 'something');
  }
}
