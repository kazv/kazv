/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var user1: ({
    name: 'some name',
    userId: '@some:tusooa.xyz',
  })

  property var user2: ({
    name: '',
    userId: '@some2:tusooa.xyz',
  })

  property var user3: ({
    name: 'xxx',
    userId: '@some3:tusooa.xyz',
  })

  property var typingUserModel: ({
    _users: [user1, user2, user3],
    at: function (index) {
      return this._users[index]
    },
    count: 3,
  })

  ColumnLayout {
    Kazv.TypingIndicator {
      id: ind0
    }

    Kazv.TypingIndicator {
      id: ind3
      typingUsers: typingUserModel
    }
  }

  TestCase {
    id: typingIndicatorTest
    name: 'typingIndicatorTest'
    when: windowShown

    function test_ind0() {
      verify(!ind0.visible);
    }

    function test_ind3() {
      verify(ind3.visible);
      verify(ind3.text === l10n.get('typing-indicator', {
        otherNum: 2,
        typingUser: user1.name,
        secondTypingUser: user2.userId,
      }))
    }
  }
}
