/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtTest 1.0
import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK
import '../../contents/ui/shortcuts' as KazvShortcuts
import 'test-helpers' as TestHelpers
import 'test-helpers.js' as JsHelpers

Item {
  id: item
  width: 800
  height: 600

  property var l10n: JsHelpers.fluentMock

  property var mockHelper: TestHelpers.MockHelper {}
  property var actionSettings: QtObject {
    property var purgeShortcutForActions: mockHelper.noop()
    property var findConflicts: mockHelper.func((seq) => {
      if (seq === 'Ctrl+X') {
        return [{ name: 'action1' }];
      }
      return [];
    })
    property var setShortcutFor: mockHelper.noop()
  }

  KazvShortcuts.ActionItem {
    id: actionItem
    action: Kirigami.Action {
      text: 'some action'
    }
  }

  TestCase {
    id: kazvshortcutsActionItemTest
    name: 'KazvshortcutsActionItemTest'
    when: windowShown

    function init() {
      actionItem.action.shortcut = '';
      mockHelper.clearAll();
    }

    function test_setShortcut() {
      const editButton = findChild(actionItem, 'editButton');
      mouseClick(editButton);
      keySequence('Ctrl+A,Ctrl+K');
      tryVerify(() => actionSettings.setShortcutFor.calledTimes(), 2000);
      compare(actionSettings.setShortcutFor.lastArgs()[1], 'Ctrl+A,Ctrl+K');
    }

    function test_eraseShortcut() {
      actionItem.action.shortcut = 'Ctrl+A,Ctrl+K';
      const button = findChild(actionItem, 'removeShortcutButton');
      mouseClick(button);
      tryVerify(() => actionSettings.setShortcutFor.calledTimes());
      compare(actionSettings.setShortcutFor.lastArgs()[1], '');
    }
  }
}
