/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtTest 1.0
import moe.kazv.mxc.kazv 0.0 as MK
import '../../contents/ui/device-mgmt' as KazvDM
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var deviceItem: ({
    deviceId: 'some-id',
    displayName: 'name',
    trustLevel: 'unseen'
  })

  KazvDM.Device {
    id: device
    item: deviceItem
  }

  TestCase {
    id: devicePopupTest
    name: 'DevicePopupTest'
    when: windowShown

    function initTestCase() {
    }

    function test_trustLevelPopup() {
      if (MK.KazvUtil.kfQtMajorVersion === 6) {
        device.sheet.parent = item;
      }
      device.sheet.open();
      tryVerify(() => device.sheet.getNewTrustLevel() === 'unseen', 1000);
      const buttons = findChild(device.sheet, 'trustLevelChoices');
      if (MK.KazvUtil.kfQtMajorVersion === 5) {
        buttons.parent = device; // force showing, the OverlaySheet does not seem to show properly
      }
      const button = findChild(buttons, 'trustLevelChoice_verified');
      mouseClick(button);
      tryVerify(() => device.sheet.getNewTrustLevel() === 'verified', 1000);
    }
  }
}
