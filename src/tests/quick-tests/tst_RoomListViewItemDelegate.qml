/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  property var roomInvite: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Invite,
    unreadNotificationCount: 0,
    tombstoned: true,
  })

  property var roomJoin: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    unreadNotificationCount: 0,
  })

  property var roomLeave: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Leave,
    unreadNotificationCount: 0,
  })

  property var roomWithUnreadNotificationCount: JsHelpers.factory.room({
    membership: MK.MatrixRoom.Join,
    unreadNotificationCount: 2,
  })

  property var roomTombstone: JsHelpers.factory.room({
    tombstoned: true,
    unreadNotificationCount: 0,
  })

  property var timelineUnread: QtObject {
    property var count: 5
    function at(i) {
      if (i < 2) {
        return {
          isLocalEcho: true,
          readers: () => ({ count: 0 }),
        };
      }
      if (i < 3) {
        return {
          isLocalEcho: false,
          sender: '@bar:example.org',
          eventId: `$event${i}`,
          readers: () => ({ count: 0 }),
        };
      }

      return {
        isLocalEcho: false,
        sender: '@foo:example.org',
        eventId: `$event${i}`,
        readers: () => ({ count: 0 }),
      };
    }
  }

  property var timelineLastSentIsMine: QtObject {
    property var count: 5
    function at(i) {
      if (i < 2) {
        return {
          isLocalEcho: true,
          readers: () => ({ count: 0 }),
        };
      }
      if (i < 3) {
        return {
          isLocalEcho: false,
          sender: '@foo:example.org',
          eventId: `$event${i}`,
          readers: () => ({ count: 0 }),
        };
      }

      return {
        isLocalEcho: false,
        sender: '@bar:example.org',
        eventId: `$event${i}`,
        readers: () => ({ count: 0 }),
      };
    }
  }

  property var timelineRead: QtObject {
    property var count: 5
    function at(i) {
      if (i < 2) {
        return {
          isLocalEcho: true,
          readers: () => ({ count: 0 }),
        };
      }
      if (i < 3) {
        return {
          isLocalEcho: false,
          sender: '@bar:example.org',
          eventId: `$event${i}`,
          readers: () => ({
            count: 1,
            at() {
              return { userId: '@foo:example.org' };
            },
          }),
        };
      }

      return {
        isLocalEcho: false,
        sender: '@foo:example.org',
        eventId: `$event${i}`,
        readers: () => ({ count: 0 }),
      };
    }
  }

  property string testRoomId: 'room-test'

  signal switchToRoomRequested(string roomId)

  SignalSpy {
    id: switchToRoomSpy
    target: item
    signalName: 'switchToRoomRequested'
  }

  ColumnLayout {
    anchors.fill: parent
    Kazv.RoomListViewItemDelegate {
      id: delegateInvite
      item: roomInvite
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateJoin
      item: roomJoin
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateLeave
      item: roomLeave
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateRoom
      item: JsHelpers.factory.room({
        roomId: testRoomId
      })
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateRoomWithName
      item: QtObject {
        property var roomId: testRoomId
        property var name: 'some name'
      }
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateRoomWithHeroes
      item: QtObject {
        property var roomId: testRoomId
        property var heroEvents: [{
          type: 'm.room.member',
          state_key: '@foo:example.com',
          content: {
            displayname: 'foo',
          },
        }, {
          type: 'm.room.member',
          state_key: '@bar:example.com',
          content: {
            displayname: 'bar',
          },
        }]
      }
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateUnread
      item: Object.assign({}, roomJoin, {
        timeline () {
          return timelineUnread;
        },
      })
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateWithUnreadNotifications
      item: Object.assign({}, roomWithUnreadNotificationCount, {
        timeline () {
          return timelineUnread;
        },
      })
    }

    Kazv.RoomListViewItemDelegate {
      id: delegateTombstone
      item: roomTombstone
    }
  }

  TestCase {
    id: roomListViewItemDelegateTest
    name: 'RoomListViewItemDelegateTest'
    when: windowShown

    function initTestCase() {
      item.matrixSdk.userId = '@foo:example.org';
      item.sdkVars.userGivenNicknameMap.map = {
        '@foo:example.com': 'something',
      };
    }

    function test_inviteIndicator() {
      const indicator = findChild(delegateInvite, 'inviteIndicator');
      verify(indicator);
      verify(indicator.visible);

      const leaveIndicator = findChild(delegateInvite, 'leaveIndicator');
      verify(leaveIndicator);
      verify(!leaveIndicator.visible);
    }

    function test_inviteIndicatorJoin() {
      const indicator = findChild(delegateJoin, 'inviteIndicator');
      verify(indicator);
      verify(!indicator.visible);

      const leaveIndicator = findChild(delegateJoin, 'leaveIndicator');
      verify(leaveIndicator);
      verify(!leaveIndicator.visible);
    }

    function test_inviteIndicatorLeave() {
      const indicator = findChild(delegateLeave, 'inviteIndicator');
      verify(indicator);
      verify(!indicator.visible);

      const leaveIndicator = findChild(delegateLeave, 'leaveIndicator');
      verify(leaveIndicator);
      verify(leaveIndicator.visible);
    }

    function test_lastUnreadMessage() {
      const room = {};
      compare(delegateJoin.getLastUnreadMessage(timelineUnread, timelineUnread.count, room).eventId, '$event2');

      compare(delegateJoin.getLastUnreadMessage(timelineLastSentIsMine, timelineLastSentIsMine.count, room), undefined);

      compare(delegateJoin.getLastUnreadMessage(timelineRead, timelineRead.count, room), undefined);

      room.localReadMarker = '$event2';
      compare(delegateJoin.getLastUnreadMessage(timelineUnread, timelineUnread.count, room), undefined);
    }

    function test_returnRoom() {
      compare(switchToRoomSpy.count, 0);
      mouseClick(delegateRoom);
      tryCompare(switchToRoomSpy, 'count', 1);
      verify(JsHelpers.deepEqual(switchToRoomSpy.signalArguments[0], [testRoomId]));
    }

    function test_roomName() {
      compare(
        findChild(delegateRoomWithName, 'roomDisplayNameLabel').text,
        l10n.get('room-list-view-room-item-title-name', { name: 'some name' }));

      compare(
        findChild(delegateRoomWithHeroes, 'roomDisplayNameLabel').text,
        l10n.get('room-list-view-room-item-title-heroes', {
          hero: l10n.get(
            'user-name-overrided',
            { overridedName: 'something', globalName: 'foo' }),
          secondHero: 'bar',
          otherNum: 1,
        }));
    }

    function test_unreadIndicators() {
      verify(!findChild(delegateWithUnreadNotifications, 'unreadIndicator').visible);
      verify(findChild(delegateWithUnreadNotifications, 'unreadNotificationCount').visible);

      verify(findChild(delegateUnread, 'unreadIndicator').visible);
      verify(!findChild(delegateUnread, 'unreadNotificationCount').visible);
    }

    function test_tombstoneIndictor() {
      const indicator = findChild(delegateTombstone, 'tombstoneIndicator');
      verify(indicator);
      verify(indicator.visible);

      const indicatorJoin = findChild(delegateJoin, 'tombstoneIndicator');
      verify(indicatorJoin);
      verify(!indicatorJoin.visible);
    }
  }
}
