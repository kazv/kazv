/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtTest

import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers
import 'test-helpers.js' as JsHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  property var kazvIOJob: QtObject {
    function isResulted() { return false; }
    function isSuspended() { return false; }
    function fileName() { return 'some-file-name'; }
    property var suspend: mockHelper.noop()
    property var cancel: mockHelper.noop()
    property var progress: 0.0
    signal result(int ec)
  }
  property var suspendedJob: QtObject {
    function isResulted() { return false; }
    function isSuspended() { return true; }
    property var resume: mockHelper.noop()
  }
  property var resultedJob: QtObject {
    function isResulted() { return true; }
    function isSuspended() { return false; }
    function error() { return MK.KazvIOBaseJob.NoError; }
  }

  Kazv.KazvIOMenu {
    id: progressBar
    kazvIOJob: null
    jobId: 'some-job-id'
    isUpload: false
  }

  TestCase {
    id: kazvIOMenuTest
    name: 'KazvIOMenuTest'
    when: windowShown

    function init() {
      progressBar.kazvIOJob = null;
      progressBar.isUpload = false;
    }

    function test_progress() {
      progressBar.kazvIOJob = kazvIOJob;
      const progress = findChild(progressBar, 'progressBar');
      const progressText = findChild(progressBar, 'progressText');
      compare(progress.value, kazvIOJob.progress);
      compare(progressText.text, l10n.get("kazv-io-progress", {
        progress: Math.floor(kazvIOJob.progress * 100)}));
      kazvIOJob.progress = Math.random();
      compare(progress.value, kazvIOJob.progress);
      compare(progressText.text, l10n.get("kazv-io-progress", {
        progress: Math.floor(kazvIOJob.progress * 100)}));
    }

    function test_progressBarNullJob() {
      progressBar.kazvIOJob = null;
      verify(!progressBar.visible);
    }

    function test_progressBarDownload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = false;
      verify(progressBar.visible);
      const prompt = findChild(progressBar, 'fileNamePrompt');
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      verify(prompt.visible);
      verify(progressLayout.visible);
      verify(!resultLayout.visible);
      compare(prompt.text, l10n.get('kazv-io-downloading-prompt', {
        fileName: kazvIOJob.fileName()}));
    }

    function test_progressBarUpload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = true;
      const prompt = findChild(progressBar, 'fileNamePrompt');
      compare(prompt.text, l10n.get('kazv-io-uploading-prompt', {
        fileName: kazvIOJob.fileName()}));
    }

    function test_resume() {
      progressBar.kazvIOJob = kazvIOJob;
      const pauseBtn = findChild(progressBar, 'pauseBtn');
      compare(pauseBtn.icon.name, 'media-playback-pause');
      mouseClick(pauseBtn);
      compare(kazvIOJob.suspend.calledTimes(), 1);
    }

    function test_pause() {
      progressBar.kazvIOJob = suspendedJob;
      const pauseBtn = findChild(progressBar, 'pauseBtn');
      compare(pauseBtn.icon.name, 'media-playback-start');
      mouseClick(pauseBtn);
      compare(suspendedJob.resume.calledTimes(), 1);
    }

    function test_cancel() {
      progressBar.kazvIOJob = kazvIOJob;
      const cancelBtn = findChild(progressBar, 'cancelBtn');
      mouseClick(cancelBtn);
      compare(kazvIOJob.cancel.calledTimes(), 1);
    }

    function test_resulted() {
      progressBar.kazvIOJob = resultedJob;
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      verify(!progressLayout.visible);
      verify(resultLayout.visible);
    }

    /**
     * ProgressBar is destroyed automatically after a successful upload,
     * so there is no need to test
     */
    function test_resultSuccDownload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = false;
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      const resultMsg = findChild(progressBar, 'resultMsg');
      kazvIOJob.result(MK.KazvIOBaseJob.NoError);
      verify(!progressLayout.visible);
      verify(resultLayout.visible);
      compare(resultMsg.text, l10n.get('kazv-io-download-success-prompt'));
    }

    function test_resultFailDownload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = false;
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      const resultMsg = findChild(progressBar, 'resultMsg');
      kazvIOJob.result(MK.KazvIOBaseJob.UserCancel);
      verify(!progressLayout.visible);
      verify(resultLayout.visible);
      const failPromptL10nId = 'kazv-io-download-failure-prompt';
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-user-cancel')}));
      kazvIOJob.result(MK.KazvIOBaseJob.OpenFileError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-open-file-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.WriteFileError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-write-file-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.KIOError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-network-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.HashError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-hash-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.ResponseError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-response-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.KazvError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-kazv-error')}));
    }

    function test_resultFailUpload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = true;
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      const resultMsg = findChild(progressBar, 'resultMsg');
      kazvIOJob.result(MK.KazvIOBaseJob.UserCancel);
      verify(!progressLayout.visible);
      verify(resultLayout.visible);
      const failPromptL10nId = 'kazv-io-upload-failure-prompt';
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-user-cancel')}));
      kazvIOJob.result(MK.KazvIOBaseJob.OpenFileError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-open-file-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.WriteFileError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-write-file-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.KIOError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-network-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.HashError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-hash-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.ResponseError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-response-error')}));
      kazvIOJob.result(MK.KazvIOBaseJob.KazvError);
      compare(resultMsg.text, l10n.get(failPromptL10nId, {
        detail: l10n.get('kazv-io-failure-detail-kazv-error')}));
    }

    function test_closeDownload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = false;
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      const closeBtn = findChild(progressBar, 'closeBtn')

      kazvIOJob.result(MK.KazvIOBaseJob.NoError);
      verify(!progressLayout.visible);
      verify(resultLayout.visible);
      mouseClick(closeBtn);
      compare(kazvIOManager.deleteDownloadJob.calledTimes(), 1);
    }

    function test_closeUpload() {
      progressBar.kazvIOJob = kazvIOJob;
      progressBar.isUpload = true;
      const progressLayout = findChild(progressBar, 'progressLayout');
      const resultLayout = findChild(progressBar, 'resultLayout');
      const closeBtn = findChild(progressBar, 'closeBtn')

      kazvIOJob.result(MK.KazvIOBaseJob.NoError);
      verify(!progressLayout.visible);
      verify(resultLayout.visible);
      mouseClick(closeBtn);
      compare(kazvIOManager.deleteUploadJob.calledTimes(), 1);
    }
  }
}
