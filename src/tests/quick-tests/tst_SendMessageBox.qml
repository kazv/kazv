/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtTest 1.0

import org.kde.kirigami 2.13 as Kirigami

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  function makeRoom () {
    return {
      timeline () {
        return {
          count: 0,
          gaps: {},
          at () { return {}; },
        };
      },
      messageById () { return {}; },
      member () { return {}; },
      localDraft: '',
      setLocalDraft (localDraft) {
        this.localDraft = localDraft;
      },
      updateLocalDraftNow () {
      },
      members () {
        return membersModel;
      },
      setTyping: mockHelper.noop(),
      sendTextMessage: mockHelper.noop(),
      roomId: '!some-event-id',
      encrypted: true,
    };
  }

  property var room: makeRoom()
  property var membersModel: ListModel {
    ListElement { userId: '@foo:example.com' }
    ListElement { userId: '@bar:example.com' }
    property string filter
    property var count: 2
    function at(index) {
      return get(index);
    }
  }

  Kazv.SendMessageBox {
    id: sendMessageBox
    y: 400
    room: item.room
  }

  TestCase {
    id: sendMessageBoxTest
    name: 'SendMessageBoxTest'
    when: windowShown

    function init() {
      membersModel.count = 2;
      findChild(sendMessageBox, 'draftMessage').text = '';
      mockHelper.clearAll();
    }

    function test_empty() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === '@foo:example.com ');
      verify(textArea.cursorPosition === textArea.text.length);
    }

    function test_cursorAtEnd() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some test';
      textArea.cursorPosition = textArea.text.length;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === 'some test @foo:example.com ');
      verify(textArea.cursorPosition === textArea.text.length);
    }

    function test_cursorAtEndSpacingBefore() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some test\n';
      textArea.cursorPosition = textArea.text.length;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === 'some test\n@foo:example.com ');
      verify(textArea.cursorPosition === textArea.text.length);
    }

    function test_cursorAtBeginning() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some test';
      textArea.cursorPosition = 0;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === '@foo:example.com some test');
      verify(textArea.cursorPosition === '@foo:example.com '.length);
    }

    function test_cursorAtBeginningSpacingAfter() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = '\nsome test';
      textArea.cursorPosition = 0;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === '@foo:example.com\nsome test');
      verify(textArea.cursorPosition === '@foo:example.com\n'.length);
    }

    function test_cursorInMiddleNoSpacing() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some\ntest';
      textArea.cursorPosition = 1;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === 's @foo:example.com ome\ntest');
      verify(textArea.cursorPosition === 's @foo:example.com '.length);
    }

    function test_cursorInMiddleSpacingBefore() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some\ntest';
      textArea.cursorPosition = 5;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === 'some\n@foo:example.com test');
      verify(textArea.cursorPosition === 'some\n@foo:example.com '.length);
    }

    function test_cursorInMiddleSpacingAfter() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some\ntest';
      textArea.cursorPosition = 4;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === 'some @foo:example.com\ntest');
      verify(textArea.cursorPosition === 'some @foo:example.com\n'.length);
    }

    function test_cursorInMiddleSpacingAround() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = 'some\n\ntest';
      textArea.cursorPosition = 5;
      sendMessageBox.mentionUser('@foo:example.com');
      tryVerify(() => textArea.text === 'some\n@foo:example.com\ntest');
      verify(textArea.cursorPosition === 'some\n@foo:example.com\n'.length);
    }

    function test_filter() {
      const data = [
        ['some\n\ntest', 5, ''],
        ['some\n\n@test\n', 11, '@test'],
        ['some\n\n@test\n', 10, '@tes'],
        ['some\n\n@test喵 ', 12, '@test喵'],
        // separated by a whitespace
        ['some\n\n@test\n', 12, ''],
        ['some\n\n@test\t', 12, ''],
        ['some\n\n@test ', 12, ''],
        // multiple @s
        ['@test@233', 9, '@233'],
      ];
      const textArea = findChild(sendMessageBox, 'draftMessage');
      data.forEach(([text, pos, expected]) => {
        textArea.text = text;
        textArea.cursorPosition = pos;
        compare(textArea.filter, expected);
      });
    }

    function test_completion() {
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = '@ex';
      textArea.cursorPosition = 3;
      const completionPopup = findChild(sendMessageBox, 'completionPopup');
      tryVerify(() => completionPopup.opened);
      const listView = findChild(completionPopup, 'completionListView');
      tryVerify(() => listView);
      tryVerify(() => findChild(listView, 'completionItem0'));
      tryVerify(() => findChild(listView, 'completionItem1'));
      const first = findChild(listView, 'completionItem0');
      const second = findChild(listView, 'completionItem1');
      verify(findChild(first, 'userIdLabel').text === '@foo:example.com');
      verify(findChild(second, 'userIdLabel').text === '@bar:example.com');
      mouseClick(second);
      tryVerify(() => !completionPopup.opened);
      const expected = '@bar:example.com ';
      tryCompare(textArea, 'text', expected);
      compare(textArea.cursorPosition, expected.length);
    }

    function test_zeroCompletion() {
      membersModel.count = 0;
      const textArea = findChild(sendMessageBox, 'draftMessage');
      textArea.text = '@ex';
      textArea.cursorPosition = 3;
      const completionPopup = findChild(sendMessageBox, 'completionPopup');
      tryVerify(() => !completionPopup.opened);
    }

    function test_emptySend() {
      mouseClick(findChild(sendMessageBox, 'sendButton'));
      verify(room.sendTextMessage.calledTimes() === 0);

      const textArea = findChild(sendMessageBox, 'draftMessage');
      mouseClick(textArea);
      tryVerify(() => textArea.activeFocus);
      keySequence('Ctrl+Enter');
      verify(room.sendTextMessage.calledTimes() === 0);
      keySequence('Ctrl+Return');
      verify(room.sendTextMessage.calledTimes() === 0);
    }

    function test_replyWithFile() {
      sendMessageBox.draftRelType = 'm.in_reply_to';
      sendMessageBox.draftRelatedTo = '!some-event-id';
      const sendFileButton = findChild(sendMessageBox, 'sendMediaFileButton');
      const fileDialog = findChild(sendMessageBox, 'fileDialog');

      verify(sendFileButton.enabled);
      verify(!fileDialog.visible);
      mouseClick(sendFileButton);
      verify(fileDialog.visible);
      fileDialog.accept();
      verify(!fileDialog.visible);

      compare(kazvIOManager.startNewUploadJob.calledTimes(), 1);
      verify(JsHelpers.deepEqual(kazvIOManager.startNewUploadJob.lastArgs(), {
        serverUrl: matrixSdk.serverUrl,
        fileUrl: Qt.url(''),
        token: matrixSdk.token,
        roomId: room.roomId,
        roomList: sdkVars.roomList,
        encrypted: room.encrypted,
        draftRelType: 'm.in_reply_to',
        draftRelatedTo: '!some-event-id'
      }));
    }
  }
}
