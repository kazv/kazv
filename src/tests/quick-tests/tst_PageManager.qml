/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  property string testRoomId: '!roomtest:example.org'
  property var pageStack: QtObject {
    property var currentIndex: 0
    function get(index) {
      const roomPageComp = Qt.createComponent(Qt.resolvedUrl("../../contents/ui/RoomPage.qml"))
      return roomPageComp.createObject(item, { roomId: testRoomId })
    }
    function goBack() {
      --currentIndex;
    }
    function goForward() {
      ++currentIndex;
    }
    signal push(var url, var args)
  }

  SignalSpy {
    id: pushSignalSpy
  }

  Kazv.PageManager {
    id: pageManager
    sdkVars: QtObject {
      property string currentRoomId: testRoomId
    }
    pageStack: item.pageStack
    main: QtObject {
      signal switchToRoomRequested(string roomId)
    }
  }

  TestCase {
    id: pageManagerTest
    name: 'PageManagerTest'

    function init() {
      pushSignalSpy.clear();
      pushSignalSpy.target = pageStack;
      pushSignalSpy.signalName = 'push';
      pageStack.currentIndex = 0;
    }

    function test_returnToRoom() {
      pageManager.main.switchToRoomRequested(testRoomId);
      compare(pageStack.currentIndex, 1);
      compare(pushSignalSpy.count, 0);
    }

    function test_switchToOtherRoom() {
      compare(pushSignalSpy.count, 0);
      pageManager.main.switchToRoomRequested('!otherroom:example.org');
      tryCompare(pushSignalSpy, 'count', 1);
      verify(JsHelpers.deepEqual(pushSignalSpy.signalArguments[0][1], { roomId: '!otherroom:example.org' }));
    }
  }
}
