/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtTest

import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var room: ({
    messageById: (_id) => item.textEvent,
    member: (_id) => ({}),
  })
  property var history: ListModel {
    ListElement {}
    ListElement {}

    function at(index) {
      return {
        type: 'm.room.message',
        content: {
          msgtype: 'm.text',
          body: `version ${index}`,
        },
        sender: '@foo:example.com',
      };
    }
  }

  Kazv.EventHistoryView {
    id: eventHistoryView
    anchors.fill: parent
    history: item.history
  }

  TestCase {
    id: eventHistoryViewTest
    name: 'EventHistoryViewTest'

    function test_history() {
      tryVerify(() => eventHistoryView.itemAtIndex(0));
      const v0 = eventHistoryView.itemAtIndex(0);
      verify(findChild(v0, 'timeIndicator').visible);
      compare(findChild(v0, 'textEventContent').text, 'version 0');
      tryVerify(() => eventHistoryView.itemAtIndex(1));
      compare(findChild(eventHistoryView.itemAtIndex(1), 'textEventContent').text, 'version 1');
    }
  }
}
