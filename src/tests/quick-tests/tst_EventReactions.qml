/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtTest
import moe.kazv.mxc.kazv 0.0 as MK
import org.kde.kirigami 2.13 as Kirigami
import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  function getReaction(i) {
    return {
      count: 2,
      key: `r${i}`,
      at(index) {
        return index === 0 ? {
          sender: `@foo${i}:example.com`,
          eventId: `$xxx${i}`,
        } : {
          sender: `@bar${i}:example.com`,
          eventId: `$yyy${i}`,
        };
      },
    };
  }
  readonly property var reactions: ListModel {
    ListElement {}
    ListElement {}

    function at(index) {
      return item.getReaction(index);
    }
  }

  readonly property var deleteEventRequestedNoConfirm: mockHelper.noop()
  readonly property var eventView: QtObject {
    readonly property var reactWith: item.mockHelper.noop()
  }

  ColumnLayout {
    Kazv.EventReactions {
      id: eventReactions
      reactions: item.reactions
    }

    Kazv.EventView {
      id: eventViewReaction
      event: ({
        type: 'm.reaction',
        content: {},
        isFailed: false,
      })
    }

    Kazv.EventView {
      id: eventViewReactionFailed
      event: ({
        type: 'm.reaction',
        content: {},
        isFailed: true,
      })
    }
  }

  TestCase {
    id: eventViewVideoTest
    name: 'EventViewVideoTest'
    when: windowShown

    function init() {
      item.mockHelper.clearAll();
      item.matrixSdk.userId = '@foo0:example.com';
    }

    function test_reactionEvent() {
      compare(eventViewReaction.messageType, 'ignore');
      compare(eventViewReactionFailed.messageType, 'reaction');
    }

    function test_reactionsReacted() {
      const reaction = findChild(eventReactions, 'reaction0');
      const text = findChild(reaction, 'reactionContentText');
      verify(text.text.includes('r0'));
      const content = findChild(reaction, 'reactionContent');
      verify(content.checked);
      compare(reaction.selfReactEventId, '$xxx0');

      mouseClick(content);
      compare(item.eventView.reactWith.calledTimes(), 0);
      compare(item.deleteEventRequestedNoConfirm.calledTimes(), 1);
      compare(item.deleteEventRequestedNoConfirm.lastArgs()[0], '$xxx0');
    }

    function test_reactionsUnreacted() {
      const reaction = findChild(eventReactions, 'reaction1');
      const text = findChild(reaction, 'reactionContentText');
      verify(text.text.includes('r1'));
      const content = findChild(reaction, 'reactionContent');
      verify(!content.checked);
      verify(!reaction.selfReactEventId);

      mouseClick(content);
      compare(item.deleteEventRequestedNoConfirm.calledTimes(), 0);
      compare(item.eventView.reactWith.calledTimes(), 1);
      compare(item.eventView.reactWith.lastArgs()[0], 'r1');
    }
  }
}
