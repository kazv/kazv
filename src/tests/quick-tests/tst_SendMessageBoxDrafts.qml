/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtTest 1.0

import org.kde.kirigami 2.13 as Kirigami

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  function makeRoom () {
    return {
      timeline () {
        return {
          count: 0,
          gaps: {},
          at () { return {}; },
        };
      },
      messageById () { return {}; },
      member () { return {}; },
      localDraft: '',
      setLocalDraft: mockHelper.noop(),
      updateLocalDraftNow: mockHelper.func(function () {
        this.localDraft = this.setLocalDraft.lastArgs()[0];
      }),
    };
  }

  property var room: makeRoom()
  property var room2: makeRoom()

  property var sendMessageBoxComp: Component {
    id: comp
    Kirigami.Page {
      id: page
      Label {
        text: 'room 1'
      }
      footer: Item {
        height: childrenRect.height
        width: parent.width
        Kazv.SendMessageBox {
          room: item.room
        }
      }
    }
  }

  property var sendMessageBoxComp2: Component {
    id: comp2
    Kirigami.Page {
      id: page
      Label {
        text: 'room 2'
      }
      footer: Item {
        height: childrenRect.height
        width: parent.width
        Kazv.SendMessageBox {
          room: item.room2
        }
      }
    }
  }

  property var mockMainPageComp: Component {
    id: mockMainPageComp
    Kirigami.Page {}
  }

  Kirigami.ApplicationWindow {
    id: window
  }

  TestCase {
    id: sendMessageBoxDraftsTest
    name: 'SendMessageBoxDraftsTest'
    when: windowShown

    function test_saveDraftsOnSwitching() {
      // simulates the MainPage when starting up kazv
      window.pageStack.push(mockMainPageComp);
      // a room page is pushed, here we simulate it with only the send
      // message box, as other things are unrelevant
      window.pageStack.push(comp);
      const textArea = findChild(window.pageStack, 'draftMessage');
      verify(textArea);
      // change text
      textArea.text = 'foo';
      // verify that the draft is saved internally but not propagated
      verify(item.room.localDraft === '');
      verify(item.room.setLocalDraft.lastArgs()[0] === 'foo');

      // go back to MainPage
      window.pageStack.goBack();
      verify(window.pageStack.currentIndex === 0);
      // push another room page
      window.pageStack.push(comp2);
      verify(window.pageStack.currentIndex === 1);
      // should be the draft for that room, so it is empty
      tryVerify(() => findChild(window.pageStack, 'draftMessage').text === '', 1000);
      // verify that the draft is propagated
      tryVerify(() => item.room.localDraft === 'foo', 1000);
      verify(item.room.updateLocalDraftNow.calledTimes() === 1);
    }
  }
}
