/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtTest
import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  Kazv.LoginPage {
    id: loginPage
  }

  TestCase {
    id: loginPageTest
    name: 'LoginPageTest'
    when: windowShown

    function init() {
      item.mockHelper.clearAll();
      findChild(loginPage, 'userIdField').text = '@foo:example.org';
      findChild(loginPage, 'passwordField').text = 'somepassword';
    }

    function test_discover() {
      findChild(loginPage, 'serverUrlField').text = '';
      mouseClick(findChild(loginPage, 'loginButton'));
      compare(item.matrixSdk.login.calledTimes(), 1);
      compare(item.matrixSdk.login.lastArgs()[2], '');
    }

    function test_noDiscover() {
      findChild(loginPage, 'serverUrlField').text = 'https://mxs.example.org';
      mouseClick(findChild(loginPage, 'loginButton'));
      compare(item.matrixSdk.login.calledTimes(), 1);
      compare(item.matrixSdk.login.lastArgs()[2], 'https://mxs.example.org');
    }
  }
}
