/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  function makeSticker(sticker) {
    return {
      type: 'm.sticker',
      content: {
        body: sticker.body,
        url: sticker.mxcUri,
        info: sticker.info,
      },
    };
  }

  property list<ListModel> stickerPacks: [
    ListModel {
      id: stickerPack0
      ListElement {
        shortCode: 'some'
        body: 'some'
        mxcUri: 'mxc://example.org/some'
        makeEventJson: () => makeSticker(stickerPack0.get(0))
      }

      ListElement {
        shortCode: 'some1'
        body: 'some1'
        mxcUri: 'mxc://example.org/some1'
        makeEventJson: () => makeSticker(stickerPack0.get(1))
      }

      function at(index) {
        return stickerPack0.get(index);
      }
    },
    ListModel {
      id: stickerPack1
      ListElement {
        shortCode: 'some2'
        body: 'some2'
        mxcUri: 'mxc://example.org/some2'
        makeEventJson: () => makeSticker(stickerPack1.get(0))
      }

      ListElement {
        shortCode: 'some3'
        body: 'some3'
        mxcUri: 'mxc://example.org/some3'
        makeEventJson: () => makeSticker(stickerPack1.get(1))
      }

      function at(index) {
        return stickerPack1.get(index);
      }
    }
  ]

  SignalSpy {
    id: sendMessageRequestedSpy
    signalName: 'sendMessageRequested'
  }

  Kazv.StickerPicker {
    id: stickerPicker
    stickerPackList: ListModel {
      id: stickerPackListModel
      ListElement {
      }
      ListElement {
      }
      function at(index) {
        return stickerPacks[index];
      }
    }
  }

  TestCase {
    id: stickerPickerTest
    name: 'StickerPickerTest'
    when: windowShown

    function init() {
      sendMessageRequestedSpy.clear();
      sendMessageRequestedSpy.target = stickerPicker;
    }

    function test_stickerPicker() {
      verify(findChild(stickerPicker, 'stickerPack0'));
      verify(findChild(stickerPicker, 'stickerPack1'));
      verify(findChild(stickerPicker, 'sticker0'));
      verify(findChild(stickerPicker, 'sticker1'));
      const stickerButton = findChild(stickerPicker, 'sticker1');
      mouseClick(stickerButton);
      tryVerify(() => sendMessageRequestedSpy.count === 1, 1000);
      verify(JsHelpers.deepEqual(sendMessageRequestedSpy.signalArguments[0][0], makeSticker(stickerPack0.get(1))));

      mouseClick(findChild(stickerPicker, 'stickerPack1'));
      tryVerify(() => findChild(stickerPicker, 'sticker0').sticker.mxcUri === 'mxc://example.org/some2');
      mouseClick(findChild(stickerPicker, 'sticker0'));
      tryVerify(() => sendMessageRequestedSpy.count === 2, 1000);
      verify(JsHelpers.deepEqual(sendMessageRequestedSpy.signalArguments[1][0], makeSticker(stickerPack1.get(0))));
    }
  }
}
