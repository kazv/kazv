/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import '../../contents/ui/room-settings' as RS
import 'test-helpers' as QmlHelpers

import moe.kazv.mxc.kazv 0.0 as MK

QmlHelpers.TestItem {
  id: item

  property var room: QtObject {
    property var refreshState: mockHelper.promise()
  }
  property var pageComp: Component {
    RS.RoomMemberListPage {
    }
  }

  TestCase {
    id: roomMemberListPageTest
    name: 'RoomMemberListPageTest'
    when: windowShown

    function init() {
      mockHelper.clearAll();
    }

    function test_loadState() {
      const page = pageComp.createObject(item, { room, members: 0 });
      tryVerify(() => room.refreshState.calledTimes());
    }
  }
}
