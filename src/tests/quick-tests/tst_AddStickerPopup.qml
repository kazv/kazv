/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0
import org.kde.kirigami 2.20 as Kirigami

import '../../contents/ui' as Kazv
import 'test-helpers.js' as Helpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  Kazv.AddStickerPopup {
    id: addStickerPopup
    event: ({
      content: {
        url: 'mxc://example.org/something',
      },
    })
    stickerPackList: ({
      at(index) {
        return {
          addSticker(shortCode, event) {
            return {
              _packIndex: index,
              event: {
                content: {
                  images: {
                    [shortCode]: event.content,
                  },
                },
              },
            };
          },
          hasShortCode(shortCode) {
            return shortCode === 'foo';
          },
        };
      },
      packs: [
        { packName: 'pack0' },
        { packName: 'pack1' },
      ],
    })
    property var close: mockHelper.noop()
  }

  TestCase {
    id: eventViewTest
    name: 'EventViewTest'
    when: windowShown

    function init() {
      addStickerPopup.contentItem.parent = item;
      findChild(addStickerPopup, 'packChooser').currentIndex = 0;
      addStickerPopup.addingSticker = false;
    }

    function cleanup() {
      item.mockHelper.clearAll();
    }

    function test_addSticker() {
      findChild(addStickerPopup, 'shortCodeInput').text = 'bar';
      verify(!findChild(addStickerPopup, 'shortCodeExistsWarning').visible);
      const button = findChild(addStickerPopup, 'addStickerButton');
      verify(button.enabled);
      mouseClick(button);
      tryVerify(() => matrixSdk.updateStickerPack.calledTimes() === 1);
      verify(Helpers.deepEqual(matrixSdk.updateStickerPack.lastArgs()[0].event.content.images, {
        'bar': {
          url: 'mxc://example.org/something',
        }
      }));

      tryVerify(() => !button.enabled);
      tryVerify(() => findChild(addStickerPopup, 'shortCodeInput').readOnly);
      verify(addStickerPopup.close.calledTimes() === 0);
      matrixSdk.updateStickerPack.lastRetVal().resolve(true, {});
      tryVerify(() => addStickerPopup.close.calledTimes() === 1);
    }

    function test_addStickerOtherPack() {
      verify(Helpers.deepEqual(addStickerPopup.availablePackNames, ['pack0', 'pack1']));
      findChild(addStickerPopup, 'packChooser').currentIndex = 1;
      findChild(addStickerPopup, 'shortCodeInput').text = 'bar';
      verify(!findChild(addStickerPopup, 'shortCodeExistsWarning').visible);
      const button = findChild(addStickerPopup, 'addStickerButton');
      verify(button.enabled);
      mouseClick(button);
      tryVerify(() => matrixSdk.updateStickerPack.calledTimes() === 1);
      compare(matrixSdk.updateStickerPack.lastArgs()[0]._packIndex, 1);
    }

    function test_addStickerFailed() {
      findChild(addStickerPopup, 'shortCodeInput').text = 'bar';
      verify(!findChild(addStickerPopup, 'shortCodeExistsWarning').visible);
      const button = findChild(addStickerPopup, 'addStickerButton');
      verify(button.enabled);
      mouseClick(button);
      tryVerify(() => matrixSdk.updateStickerPack.calledTimes() === 1);
      verify(Helpers.deepEqual(matrixSdk.updateStickerPack.lastArgs()[0].event.content.images, {
        'bar': {
          url: 'mxc://example.org/something',
        }
      }));
      matrixSdk.updateStickerPack.lastRetVal().resolve(false, {});
      tryVerify(() => button.enabled);
      verify(addStickerPopup.close.calledTimes() === 0);
    }

    function test_addStickerOverride() {
      findChild(addStickerPopup, 'shortCodeInput').text = 'foo';
      tryVerify(() => findChild(addStickerPopup, 'shortCodeExistsWarning').visible);
      const button = findChild(addStickerPopup, 'addStickerButton');
      verify(button.enabled);
      mouseClick(button);
      tryVerify(() => matrixSdk.updateStickerPack.calledTimes() === 1);
      verify(Helpers.deepEqual(matrixSdk.updateStickerPack.lastArgs()[0].event.content.images, {
        'foo': {
          url: 'mxc://example.org/something',
        }
      }));

      tryVerify(() => !button.enabled);
      tryVerify(() => findChild(addStickerPopup, 'shortCodeInput').readOnly);
      verify(addStickerPopup.close.calledTimes() === 0);
      matrixSdk.updateStickerPack.lastRetVal().resolve(true, {});
      tryVerify(() => addStickerPopup.close.calledTimes() === 1);
    }
  }
}
