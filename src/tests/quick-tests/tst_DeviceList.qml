/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtTest 1.0

import '../../contents/ui/device-mgmt' as KazvDM
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  property var deviceItems: [
    {
      deviceId: 'some-id',
      displayName: 'name',
      trustLevel: 'unseen'
    },
    {
      deviceId: 'some-id2',
      displayName: 'name2',
      trustLevel: 'unseen'
    },
  ]

  property var deviceListModel: ({
    count: 2,
    at: (index) => {
      return deviceItems[index] || {}
    },
  })

  KazvDM.DeviceList {
    id: deviceList
    anchors.fill: parent
  }

  TestCase {
    id: deviceListTest
    name: 'DeviceListTest'
    when: windowShown

    function initTestCase() {
      deviceList.devices = deviceListModel;
    }

    function test_deviceList() {
      verify(deviceList.model === 2);
      deviceList.currentIndex = 0;
      verify(deviceList.itemAtIndex(0));
      verify(findChild(deviceList.itemAtIndex(0), 'deviceIdLabel').text === 'some-id');
      deviceList.currentIndex = 1;
      tryVerify(() => findChild(deviceList.itemAtIndex(1)), 1000);
      tryVerify(() => findChild(deviceList.itemAtIndex(1), 'deviceIdLabel').text === 'some-id2', 1000);
    }
  }
}
