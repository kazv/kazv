/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var unencryptedEvent: ({
    encrypted: false,
    decrypted: false,
    originalSource: {
      event_id: '$foo',
      content: {},
    },
  })

  property var encryptedEvent: ({
    encrypted: true,
    decrypted: false,
    originalSource: {
      event_id: '$foo',
      content: {},
    },
  })

  property var decryptedEvent: ({
    encrypted: true,
    decrypted: true,
    originalSource: {
      event_id: '$foo',
      content: {},
    },
    decryptedSource: {
      content: {
        msgtype: 'm.text',
        body: 'foo',
      },
    },
  })

  ColumnLayout {
    Kazv.EventSourceView {
      id: evUnencrypted
      event: unencryptedEvent
    }

    Kazv.EventSourceView {
      id: evEncrypted
      event: encryptedEvent
    }

    Kazv.EventSourceView {
      id: evDecrypted
      event: decryptedEvent
    }
  }

  TestCase {
    id: eventSourceViewTest
    name: 'EventSourceViewTest'
    when: windowShown

    function test_unencrypted() {
      verify(!findChild(evUnencrypted, 'decryptedSourceView').visible);
      verify(findChild(evUnencrypted, 'originalSourceView').visible);
    }

    function test_encrypted() {
      verify(findChild(evEncrypted, 'decryptedSourceView').visible);
      verify(findChild(evEncrypted, 'originalSourceView').visible);
    }

    function test_decrypted() {
      verify(findChild(evDecrypted, 'decryptedSourceView').visible);
      verify(findChild(evDecrypted, 'originalSourceView').visible);
    }
  }
}
