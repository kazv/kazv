/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  function makeModel(size) {
    return {
      at(index) {
        return {
          userId: `@u${index}:example.com`,
          name: `user ${index}`,
        };
      },
      count: size,
    };
  }

  Kazv.EventReadIndicator {
    id: indicator
  }

  TestCase {
    id: eventReadIndicatorTest
    name: 'EventReadIndicatorTest'
    when: windowShown

    function test_noUser() {
      indicator.model = makeModel(0);
      verify(!indicator.visible);
    }

    function test_singleUser() {
      indicator.model = makeModel(1);
      tryVerify(() => indicator.visible);
      verify(findChild(indicator, 'readIndicatorAvatar0').name === 'user 0');
      verify(!findChild(indicator, 'moreUsersIndicator').visible);
    }

    function test_tooManyUsers() {
      indicator.model = makeModel(5);
      tryVerify(() => indicator.visible);
      verify(indicator.actualItems === indicator.maxItems);
      verify(findChild(indicator, 'readIndicatorAvatar3').name === 'user 3');
      verify(!findChild(indicator, 'readIndicatorAvatar4'));
      verify(findChild(indicator, 'moreUsersIndicator').visible);
    }
  }
}
