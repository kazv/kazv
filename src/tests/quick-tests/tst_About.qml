/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtTest
import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  Kazv.About {
    id: about
    authors: [
      {
        name: 'author 0',
        task: 'task 0',
      },
      {
        name: 'author 1',
        task: 'task 1',
        emailAddress: 'email@example.com',
      },
      {
        name: 'author 2',
        task: 'task 2',
        website: 'https://example.com',
      },
      {
        name: 'author 3',
        task: 'task 3',
        emailAddress: 'email@example.com',
        website: 'https://example.com',
      },
    ]
  }

  TestCase {
    id: aboutTest
    name: 'AboutTest'
    when: windowShown

    function test_authors() {
      const card = findChild(about, 'authorsCard');
      verify(!findChild(findChild(card, 'authors0'), 'emailButton').visible);
      verify(!findChild(findChild(card, 'authors0'), 'websiteButton').visible);

      verify(findChild(findChild(card, 'authors1'), 'emailButton').visible);
      verify(!findChild(findChild(card, 'authors1'), 'websiteButton').visible);

      verify(!findChild(findChild(card, 'authors2'), 'emailButton').visible);
      verify(findChild(findChild(card, 'authors2'), 'websiteButton').visible);

      verify(findChild(findChild(card, 'authors3'), 'emailButton').visible);
      verify(findChild(findChild(card, 'authors3'), 'websiteButton').visible);
    }
  }
}
