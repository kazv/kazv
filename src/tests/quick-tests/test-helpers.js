/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

class FluentMock
{
  constructor() {}

  get(id, args) {
    return `${id}, ${JSON.stringify(args)}`;
  }
};

const fluentMock = new FluentMock();

const factory = {
  room(props) {
    return Object.assign({
      name: 'some name',
      heroNames: [],
      tagIds: [],
      membership: 1, // Join
      timeline: 0,
    }, props);
  },
};

const isSubset = (a, b) => {
  return Object.entries(a).every(([k, v]) => {
    return k in b && deepEqual(v, b[k]);
  });
}

const deepEqual = (a, b) => {
  if (a === b) {
    return true;
  } else if (Array.isArray(a) && Array.isArray(b)) {
    if (a.length !== b.length) {
      return false;
    }
    return a.every((item, index) => deepEqual(item, b[index]));
  } else if (typeof a === 'object' && typeof b === 'object' && a && b) {
    return isSubset(a, b) && isSubset(b, a);
  } else {
    return false;
  }
};

const Mock = {
  calledTimes() {
    return this.args.length;
  },
  lastArgs() {
    return this.args[this.args.length - 1];
  },
  lastRetVal() {
    return this.retVals[this.retVals.length - 1];
  },
  clear() {
    this.args = [];
    this.retVals = [];
  },
};

const makeTransform = (t) => {
  if (t instanceof Function) {
    return t;
  } else if (Array.isArray(t)) {
    return (args) => {
      return t.reduce((acc, cur, index) => {
        acc[cur] = args[index];
        return acc;
      }, {});
    };
  } else {
    return (args) => args;
  }
};

const mockFunction = (f, t) => {
  const transform = makeTransform(t);
  function mock() {
    mock.args.push(transform([...arguments]));
    const retVal = f.apply(this, arguments);
    mock.retVals.push(retVal);
    return retVal;
  }
  Object.entries(Mock).forEach(([k, v]) => {
    mock[k] = v;
  });
  mock.clear();
  return mock;
};
