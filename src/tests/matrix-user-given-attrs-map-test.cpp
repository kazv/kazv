/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <vector>
#include <QtTest>

#include "qt-promise-handler.hpp"
#include "qt-json.hpp"
#include "matrix-promise.hpp"
#include "test-utils.hpp"
#include "matrix-user-given-attrs-map.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

class MatrixUserGivenAttrsMapTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testGet();
    void testSet();
};

static Event emptyEvent{};
// https://github.com/matrix-org/matrix-spec-proposals/pull/3865/files
static Event nicknameEvent = json{
    {"type", "work.banananet.msc3865.user_given.user.displayname"},
    {"content", {
        {"@alice:example.com", "My Wife"},
        {"@bob:example.com", "Bobby"},
        {"@eve:example.com", "My Lifelong Rival"},
    }},
};

namespace
{
    struct FakeSetter
    {
        QObject *executor{0};
        std::shared_ptr<int> called;
        std::shared_ptr<json> lastVal;

        Client::PromiseT operator()(json val)
        {
            if (called) { ++*called; }
            if (lastVal) { *lastVal = val; }
            return QtPromise<EffectStatus>(executor, EffectStatus());
        }
    };
}

void MatrixUserGivenAttrsMapTest::testGet()
{
    {
        auto map = MatrixUserGivenAttrsMap(lager::make_constant(emptyEvent), FakeSetter());
        QCOMPARE(map.map(), QJsonObject());
    }

    {
        auto map = MatrixUserGivenAttrsMap(lager::make_constant(nicknameEvent), FakeSetter());
        QCOMPARE(map.map(),
            nicknameEvent.content().get().template get<QJsonObject>());
    }
}

void MatrixUserGivenAttrsMapTest::testSet()
{
    {
        auto map = MatrixUserGivenAttrsMap(lager::make_constant(emptyEvent), FakeSetter());
        QCOMPARE(map.set(u"@a:example.com"_s, QJsonValue()), json::object());
        QCOMPARE(map.set(u"@a:example.com"_s, u"k"_s), (json{{"@a:example.com", "k"}}));
    }

    {
        auto called = std::make_shared<int>(0);
        auto lastVal = std::make_shared<json>();
        auto setter = FakeSetter{this, called, lastVal};
        auto map = MatrixUserGivenAttrsMap(lager::make_constant(nicknameEvent), setter);

        QCOMPARE(map.set(u"@alice:example.com"_s, QJsonValue()), (json{
            {"@bob:example.com", "Bobby"},
            {"@eve:example.com", "My Lifelong Rival"},
        }));

        QCOMPARE(map.set(u"@alice:example.com"_s, u"Something"_s), (json{
            {"@alice:example.com", "Something"},
            {"@bob:example.com", "Bobby"},
            {"@eve:example.com", "My Lifelong Rival"},
        }));

        QCOMPARE(*called, 0);

        auto p = toUniquePtr(map.setAndUpload(u"@alice:example.com"_s, u"Something"_s));
        QCOMPARE(*called, 1);
        QCOMPARE(*lastVal, (json{
            {"@alice:example.com", "Something"},
            {"@bob:example.com", "Bobby"},
            {"@eve:example.com", "My Lifelong Rival"},
        }));
    }
}

QTEST_MAIN(MatrixUserGivenAttrsMapTest)

#include "matrix-user-given-attrs-map-test.moc"
