/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <string>

#ifndef TEST_SERVER
#define TEST_SERVER "https://matrix.org"
#endif

const std::string TEST_SERVER_URL{TEST_SERVER};
