/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "kazv-file.hpp"
#include "qt-rand-adapter.hpp"

#include <client/random-generator.hpp>
#include <crypto/aes-256-ctr.hpp>

#include <QObject>
#include <QtTest>
#include <QTemporaryDir>

class KazvFileTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testKazvFile();
};

using namespace Kazv;

void KazvFileTest::testKazvFile()
{
    QTemporaryDir dir{};
    dir.isValid();
    auto rawContent = QByteArrayLiteral("Some test content");
    QSaveFile rawFile{dir.filePath(QStringLiteral("rawFile"))};
    rawFile.open(QIODevice::WriteOnly);
    rawFile.write(rawContent.data(), rawContent.size());
    rawFile.commit();
    Kazv::RandomInterface randomGenerator = QtRandAdapter{};
    auto aes = AES256CTRDesc::fromRandom(
        randomGenerator.generateRange<Kazv::RandomData>(
            Kazv::AES256CTRDesc::randomSize));
    KazvFile kazvFile{rawFile.fileName(), aes};
    kazvFile.open(QIODevice::ReadOnly);
    QByteArray encryptedContent = kazvFile.readAll();
    kazvFile.close();
    KazvSaveFile kazvSaveFile{
        dir.filePath(QStringLiteral("decryptedFile")), aes};
    kazvSaveFile.open(QIODevice::WriteOnly);
    kazvSaveFile.write(encryptedContent.data(),
        encryptedContent.size());
    kazvSaveFile.commit();
    QFile file{kazvSaveFile.fileName()};
    file.open(QIODevice::ReadOnly);
    auto decryptedContent = file.readAll();
    QCOMPARE(decryptedContent, rawContent);
}

QTEST_MAIN(KazvFileTest)

#include "kazv-file-test.moc"
