/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>
#include <QSet>
#include <QJsonObject>
#include <lager/state.hpp>

#include <base/event.hpp>

#include <testfixtures/factory.hpp>

#include "test-model.hpp"
#include "test-utils.hpp"
#include "matrix-sticker-pack-list.hpp"
#include "matrix-sticker-pack.hpp"
#include "matrix-sticker.hpp"
#include "matrix-event.hpp"
#include "matrix-sdk.hpp"
#include "matrix-room-list.hpp"
#include "matrix-room.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;
using namespace Kazv::Factory;

class MatrixStickerPackTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testStickerPack();
    void testStickerPackList();
    void testStickerPackListInRoom();
    void testAddToPack();
};

// https://github.com/Sorunome/matrix-doc/blob/soru/emotes/proposals/2545-emotes.md
static auto stickerPackEvent = Event{R"({
  "content": {
    "images": {
      "myemote": {
        "url": "mxc://example.org/blah"
      },
      "mysticker": {
        "body": "my sticker",
        "url": "mxc://example.org/sticker",
        "usage": ["sticker"],
        "info": {
          "mimetype": "image/png"
        }
      }
    },
    "pack": {
      "display_name": "Awesome Pack",
      "usage": ["emoticon"]
    }
  },
  "type": "im.ponies.user_emotes"
})"_json};

static const auto imagePackRoomsEvent = Event(R"({
  "content": {
    "rooms": {
      "!someroom:example.org": {
        "": {},
        "de.sorunome.mx-puppet-bridge.discord": {}
      },
      "!someotherroom:example.org": {
        "": {}
      }
    }
  },
  "type": "im.ponies.emote_rooms"
})"_json);

static Event getRoomStickersEvent(std::string stateKey, std::string name)
{
    auto j = R"({
  "content": {
    "images": {
      "myemote": {
        "url": "mxc://example.org/blah"
      },
      "mysticker": {
        "body": "my sticker",
        "url": "mxc://example.org/sticker",
        "usage": ["sticker"],
        "info": {
          "mimetype": "image/png"
        }
      }
    },
    "pack": {
      "usage": ["emoticon"]
    }
  },
  "type": "im.ponies.room_emotes"
})"_json;
    j["state_key"] = stateKey;
    j["content"]["pack"]["display_name"] = name;
    return Event(j);
}

template<class Range>
static bool hasPack(const Range &packs, const QString &name)
{
    return std::any_of(packs.begin(), packs.end(), [&name](const auto &pack) {
        return pack->packName() == name;
    });
}

void MatrixStickerPackTest::testStickerPack()
{
    auto sourceCursor = lager::make_state(MatrixStickerPackSource{
        MatrixStickerPackSource::AccountData,
        "im.ponies.user_emotes",
        stickerPackEvent,
        "",
        ""
    }, lager::automatic_tag{});

    auto stickerPack = toUniquePtr(new MatrixStickerPack(sourceCursor));

    QCOMPARE(stickerPack->rowCount(QModelIndex()), 2);
    QCOMPARE(stickerPack->count(), 2);
    QVERIFY(stickerPack->hasShortCode(QStringLiteral("myemote")));
    QVERIFY(!stickerPack->hasShortCode(QStringLiteral("lolol")));

    auto sticker0 = toUniquePtr(stickerPack->at(0));
    QCOMPARE(sticker0->shortCode(), QStringLiteral("myemote"));
    QCOMPARE(sticker0->body(), QStringLiteral("myemote"));
    QCOMPARE(sticker0->mxcUri(), QStringLiteral("mxc://example.org/blah"));
    QCOMPARE(sticker0->info(), QJsonObject());
    QCOMPARE(sticker0->makeEventJson(), (QJsonObject{
        {u"type"_s, u"m.sticker"_s},
        {u"content"_s, QJsonObject{
            {u"body"_s, u"myemote"_s},
            {u"url"_s, u"mxc://example.org/blah"_s},
            {u"info"_s, QJsonObject()},
        }},
    }));

    auto sticker1 = toUniquePtr(stickerPack->at(1));
    QCOMPARE(sticker1->shortCode(), QStringLiteral("mysticker"));
    QCOMPARE(sticker1->body(), QStringLiteral("my sticker"));
    QCOMPARE(sticker1->mxcUri(), QStringLiteral("mxc://example.org/sticker"));
    QCOMPARE(sticker1->info(), (QJsonObject{{u"mimetype"_s, u"image/png"_s}}));
    QCOMPARE(sticker1->makeEventJson(), (QJsonObject{
        {u"type"_s, u"m.sticker"_s},
        {u"content"_s, QJsonObject{
            {u"body"_s, u"my sticker"_s},
            {u"url"_s, u"mxc://example.org/sticker"_s},
            {u"info"_s, QJsonObject{{u"mimetype"_s, u"image/png"_s}}},
        }},
    }));
}

void MatrixStickerPackTest::testStickerPackList()
{
    auto model = makeClient(
        withAccountData({stickerPackEvent, imagePackRoomsEvent})
        | withRoom(makeRoom(
            withRoomId("!someroom:example.org")
            | withRoomState({
                getRoomStickersEvent("", "Pack 1"),
                getRoomStickersEvent("de.sorunome.mx-puppet-bridge.discord", "Pack 2"),
            })
        ))
        | withRoom(makeRoom(
            withRoomId("!someotherroom:example.org")
            | withRoomState({getRoomStickersEvent("", "Pack 3")})
        ))
    );
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(SdkModel{model})};
    auto stickerPackList = toUniquePtr(sdk->stickerPackList());

    QCOMPARE(stickerPackList->rowCount(QModelIndex()), 4);
    QCOMPARE(stickerPackList->count(), 4);

    auto packs = std::array<std::unique_ptr<MatrixStickerPack>, 4>{
        toUniquePtr(stickerPackList->at(0)),
        toUniquePtr(stickerPackList->at(1)),
        toUniquePtr(stickerPackList->at(2)),
        toUniquePtr(stickerPackList->at(3)),
    };

    QVERIFY(hasPack(packs, u"Awesome Pack"_s));
    QVERIFY(hasPack(packs, u"Pack 1"_s));
    QVERIFY(hasPack(packs, u"Pack 2"_s));
    QVERIFY(hasPack(packs, u"Pack 3"_s));

    auto expectedPacks = QSet<QJsonValue>{
        QJsonObject{
            {u"source"_s, MatrixStickerPackSource::AccountData},
            {u"isAccountData"_s, true},
            {u"isState"_s, false},
            {u"eventType"_s, u"im.ponies.user_emotes"_s},
            {u"roomId"_s, u""_s},
            {u"stateKey"_s, u""_s},
            {u"packName"_s, u"Awesome Pack"_s},
        },
        QJsonObject{
            {u"source"_s, MatrixStickerPackSource::RoomState},
            {u"isAccountData"_s, false},
            {u"isState"_s, true},
            {u"eventType"_s, u"im.ponies.room_emotes"_s},
            {u"roomId"_s, u"!someroom:example.org"_s},
            {u"stateKey"_s, u""_s},
            {u"packName"_s, u"Pack 1"_s},
        },
        QJsonObject{
            {u"source"_s, MatrixStickerPackSource::RoomState},
            {u"isAccountData"_s, false},
            {u"isState"_s, true},
            {u"eventType"_s, u"im.ponies.room_emotes"_s},
            {u"roomId"_s, u"!someroom:example.org"_s},
            {u"stateKey"_s, u"de.sorunome.mx-puppet-bridge.discord"_s},
            {u"packName"_s, u"Pack 2"_s},
        },
        QJsonObject{
            {u"source"_s, MatrixStickerPackSource::RoomState},
            {u"isAccountData"_s, false},
            {u"isState"_s, true},
            {u"eventType"_s, u"im.ponies.room_emotes"_s},
            {u"roomId"_s, u"!someotherroom:example.org"_s},
            {u"stateKey"_s, u""_s},
            {u"packName"_s, u"Pack 3"_s},
        },
    };

    auto packsArr = stickerPackList->packs().toArray();
    QCOMPARE(QSet<QJsonValue>(packsArr.begin(), packsArr.end()), expectedPacks);

    auto p1 = toUniquePtr(stickerPackList->packFor(QJsonObject{
        {u"source"_s, MatrixStickerPackSource::RoomState},
        {u"eventType"_s, u"im.ponies.room_emotes"_s},
        {u"roomId"_s, u"!someroom:example.org"_s},
        {u"stateKey"_s, u""_s},
    }));
    QCOMPARE(p1->packName(), u"Pack 1"_s);

    auto p2 = toUniquePtr(stickerPackList->packFor(QJsonObject{
        {u"source"_s, MatrixStickerPackSource::RoomState},
        {u"eventType"_s, u"im.ponies.room_emotes"_s},
        {u"roomId"_s, u"!someotherroom:example.org"_s},
        {u"stateKey"_s, u"somestatekey"_s},
    }));
    QVERIFY(p2->isAccountData());
}

void MatrixStickerPackTest::testStickerPackListInRoom()
{
    auto model = makeClient(
        withRoom(makeRoom(
            withRoomId("!someroom:example.org")
            | withRoomState({
                getRoomStickersEvent("", "Pack 1"),
                getRoomStickersEvent("de.sorunome.mx-puppet-bridge.discord", "Pack 2"),
            })
        ))
    );
    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(SdkModel{model})};
    auto roomList = toUniquePtr(sdk->roomList());
    auto room = toUniquePtr(roomList->room(u"!someroom:example.org"_s));
    auto stickerPackList = toUniquePtr(room->stickerPackList());
    QCOMPARE(stickerPackList->count(), 2);

    auto packs = std::array<std::unique_ptr<MatrixStickerPack>, 2>{
        toUniquePtr(stickerPackList->at(0)),
        toUniquePtr(stickerPackList->at(1)),
    };

    QVERIFY(hasPack(packs, u"Pack 1"_s));
    QVERIFY(hasPack(packs, u"Pack 2"_s));

    auto expectedPacks = QSet<QJsonValue>{
        QJsonObject{
            {u"source"_s, MatrixStickerPackSource::RoomState},
            {u"isAccountData"_s, false},
            {u"isState"_s, true},
            {u"eventType"_s, u"im.ponies.room_emotes"_s},
            {u"roomId"_s, u"!someroom:example.org"_s},
            {u"stateKey"_s, u""_s},
            {u"packName"_s, u"Pack 1"_s},
        },
        QJsonObject{
            {u"source"_s, MatrixStickerPackSource::RoomState},
            {u"isAccountData"_s, false},
            {u"isState"_s, true},
            {u"eventType"_s, u"im.ponies.room_emotes"_s},
            {u"roomId"_s, u"!someroom:example.org"_s},
            {u"stateKey"_s, u"de.sorunome.mx-puppet-bridge.discord"_s},
            {u"packName"_s, u"Pack 2"_s},
        },
    };

    auto packsArr = stickerPackList->packs().toArray();
    QCOMPARE(QSet<QJsonValue>(packsArr.begin(), packsArr.end()), expectedPacks);
}

void MatrixStickerPackTest::testAddToPack()
{
    using namespace nlohmann::literals;

    auto sourceCursor = lager::make_state(MatrixStickerPackSource{
        MatrixStickerPackSource::AccountData,
        "im.ponies.user_emotes",
        stickerPackEvent,
        "",
        "",
    }, lager::automatic_tag{});

    auto stickerPack = toUniquePtr(new MatrixStickerPack(sourceCursor));

    auto contentJson = json{
        {"url", "mxc://example.org/someotheremote"},
        {"info", {{"mimetype", "image/png"}}},
        {"body", "some other emote"},
    };
    auto eventCursor = lager::make_constant(makeEvent(
        withEventContent(contentJson)
    ));
    auto event = toUniquePtr(new MatrixEvent(eventCursor));

    auto newSource = stickerPack->addSticker(u"someotheremote"_s, event.get()).template value<MatrixStickerPackSource>();

    auto expected = sourceCursor.get().event.content().get();
    expected["images"]["someotheremote"] = contentJson;

    QVERIFY(newSource.event.content().get() == expected);
}

QTEST_MAIN(MatrixStickerPackTest)

#include "matrix-sticker-pack-test.moc"
