/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <QtTest>

class QtPromiseHandlerTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testPromise();
    void testTimer();
    void testStop();
    void testSingleTypePromise();
    void testResolveToPromise();
};
