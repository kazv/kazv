/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "test-model.hpp"

#include "matrix-sdk.hpp"

using namespace Kazv;

auto deviceKeyInfoFor(std::string deviceId)
{
    return DeviceKeyInfo{
        deviceId,
        deviceId + "ed25519",
        deviceId + "curve25519",
        deviceId + "name",
    };
}

SdkModel makeTestModel()
{
    SdkModel model;
    model.client.serverUrl = "example.com";
    model.client.userId = "@bob:example.com";
    model.client.token = "exampletoken";
    model.client.deviceId = "exampledevice";
    model.client.loggedIn = true;
    model.client.deviceLists.deviceLists = {
        {"@test1:test1.org", {
            {"device1", deviceKeyInfoFor("device1")},
            {"device2", deviceKeyInfoFor("device2")},
        }},
    };

    RoomModel room;
    room.roomId = "!foo:tusooa.xyz";
    auto event = Event{json{
        {"content", {{"foo", "bar"}}},
        {"type", "m.room.message"},
    }};
    room.localEchoes = immer::flex_vector<LocalEchoDesc>{
        {"some-txn-id", event, LocalEchoDesc::Sending},
        {"some-other-txn-id", event, LocalEchoDesc::Failed},
    };

    model.client.roomList.rooms = model.client.roomList.rooms.set(
        "!foo:tusooa.xyz",
        room
    );
    return model;
}

MatrixSdk *makeTestSdk(SdkModel model)
{
    return new MatrixSdk(model, /* testing = */ true);
}

Event makeRoomMember(std::string userId, std::string displayName, std::string avatarUrl)
{
    auto content = json::object({{"membership", "join"}});
    if (!displayName.empty()) {
        content["displayname"] = displayName;
    }

    if (!avatarUrl.empty()) {
        content["avatar_url"] = avatarUrl;
    }

    auto eventId = "$user-ev" + std::to_string(std::hash<std::string>()(userId));
    return Event{json{
        {"state_key", userId},
        {"type", "m.room.member"},
        {"content", content},
        {"event_id", eventId},
        {"room_id", "$dummy:tusooa.xyz"},
    }};
}
