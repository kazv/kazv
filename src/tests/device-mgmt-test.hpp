/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <QtTest>

#include <memory>

class DeviceMgmtTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testDeviceList();
    void testDevice();
};
