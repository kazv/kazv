/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QtTest>
#include <QSignalSpy>
#include <lager/state.hpp>
#include "kazv-abstract-list-model.hpp"

class KazvAbstractListModelTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testInit();
    void testConstruct();
    void testSignals();
};

class TestListModel : public KazvAbstractListModel
{
    Q_OBJECT

public:
    TestListModel(lager::reader<int> count, InitMethod initMethod, QObject *parent = 0)
        : KazvAbstractListModel(parent)
    {
        initCountCursor(count, initMethod);
    }
};

void KazvAbstractListModelTest::testInit()
{
    {
        auto count = lager::make_state(2, lager::automatic_tag{});
        TestListModel model(count, KazvAbstractListModel::InitNow);
        QSignalSpy spy(&model, &KazvAbstractListModel::countChanged);
        QCOMPARE(spy.count(), 0);
        QCOMPARE(model.count(), 2);
        QCOMPARE(model.rowCount(QModelIndex()), 2);

        count.set(5);
        QCOMPARE(model.rowCount(QModelIndex()), 5);
    }

    {
        auto count = lager::make_state(2, lager::automatic_tag{});
        TestListModel model(count, KazvAbstractListModel::InitLater);
        QSignalSpy spy(&model, &KazvAbstractListModel::countChanged);
        QCOMPARE(model.rowCount(QModelIndex()), 0);
        spy.wait();
        QCOMPARE(spy.count(), 1);
        QCOMPARE(model.count(), 2);
        QCOMPARE(model.rowCount(QModelIndex()), 2);

        count.set(5);
        QCOMPARE(model.rowCount(QModelIndex()), 5);
    }
}

void KazvAbstractListModelTest::testConstruct()
{
    auto count = lager::make_state(2, lager::automatic_tag{});
    KazvAbstractListModel model(count, KazvAbstractListModel::InitNow);
    QSignalSpy spy(&model, &KazvAbstractListModel::countChanged);
    QCOMPARE(spy.count(), 0);
    QCOMPARE(model.count(), 2);
    QCOMPARE(model.rowCount(QModelIndex()), 2);

    count.set(5);
    QCOMPARE(model.rowCount(QModelIndex()), 5);
}

void KazvAbstractListModelTest::testSignals()
{
    auto count = lager::make_state(2, lager::automatic_tag{});
    KazvAbstractListModel model(count, KazvAbstractListModel::InitNow);
    QSignalSpy countChangedSpy(&model, &KazvAbstractListModel::countChanged);
    QSignalSpy rowsInsertedSpy(&model, &QAbstractItemModel::rowsInserted);
    QSignalSpy rowsRemovedSpy(&model, &QAbstractItemModel::rowsRemoved);

    // 2 + 3 = 5
    count.set(5);
    QCOMPARE(countChangedSpy.count(), 1);
    QCOMPARE(rowsInsertedSpy.count(), 1);
    QCOMPARE(rowsInsertedSpy[0][1], 0);
    QCOMPARE(rowsInsertedSpy[0][2], 2);
    QCOMPARE(rowsRemovedSpy.count(), 0);

    // 5 - 4 = 1
    count.set(1);
    QCOMPARE(countChangedSpy.count(), 2);
    QCOMPARE(rowsInsertedSpy.count(), 1);
    QCOMPARE(rowsRemovedSpy.count(), 1);
    QCOMPARE(rowsRemovedSpy[0][1], 0);
    QCOMPARE(rowsRemovedSpy[0][2], 3);
}

QTEST_MAIN(KazvAbstractListModelTest)

#include "kazv-abstract-list-model-test.moc"
