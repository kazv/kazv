/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 nannanko <nannanko@kazv.moe>
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "kazv-file.hpp"

#include <QCryptographicHash>

#include <optional>

struct KazvFilePrivate
{
    std::optional<Kazv::AES256CTRDesc> aes;
    QCryptographicHash hash{QCryptographicHash::Sha256};
};

KazvFile::KazvFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes, QObject *parent)
    : QFile(name, parent)
    , m_d(new KazvFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvFile::KazvFile(std::optional<Kazv::AES256CTRDesc> aes, QObject *parent)
    : QFile(parent)
    , m_d(new KazvFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvFile::KazvFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes)
    : QFile(name)
    , m_d(new KazvFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvFile::KazvFile(std::optional<Kazv::AES256CTRDesc> aes)
    : QFile()
    , m_d(new KazvFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvFile::~KazvFile() = default;

QByteArray KazvFile::hash() const
{
    return m_d->hash.result().toBase64(QByteArray::Base64Encoding | QByteArray::OmitTrailingEquals);
}

qint64 KazvFile::readData(char *data, qint64 len)
{
    qint64 readLen = QFile::readData(data, len);
    if (m_d->aes.has_value() && readLen > 0) {
        auto [next, encrypted] = m_d->aes.value().process(std::string(data, readLen));
        m_d->aes = next;
        std::copy_n(encrypted.data(), readLen, data);
        m_d->hash.addData(data, readLen);
    }
    return readLen;
}

struct KazvSaveFilePrivate {
    std::optional<Kazv::AES256CTRDesc> aes;
    QCryptographicHash hash{QCryptographicHash::Sha256};
};

KazvSaveFile::KazvSaveFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes, QObject *parent)
    : QSaveFile(name, parent)
    , m_d(new KazvSaveFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvSaveFile::KazvSaveFile(std::optional<Kazv::AES256CTRDesc> aes, QObject *parent)
    : QSaveFile(parent)
    , m_d(new KazvSaveFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvSaveFile::KazvSaveFile(const QString &name, std::optional<Kazv::AES256CTRDesc> aes)
    : QSaveFile(name)
    , m_d(new KazvSaveFilePrivate)
{
    if (aes.has_value()) {
        m_d->aes = Kazv::AES256CTRDesc{aes.value().key(), aes.value().iv()};
    }
}

KazvSaveFile::~KazvSaveFile() = default;

QByteArray KazvSaveFile::hash() const
{
    return m_d->hash.result().toBase64(QByteArray::Base64Encoding | QByteArray::OmitTrailingEquals);
}

qint64 KazvSaveFile::writeData(const char *data, qint64 len)
{
    if (m_d->aes.has_value() && len > 0) {
        m_d->hash.addData(data, len);
        auto result = m_d->aes.value().process(std::string(data, len));
        m_d->aes = result.first;
        return QSaveFile::writeData(result.second.data(), len);
    }
    return QSaveFile::writeData(data, len);
}
