/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "matrix-event-reaction-list-model.hpp"
#include <kazv-defs.hpp>
#include <json-utils.hpp>
#include "matrix-event-reaction.hpp"

using namespace Kazv;

MatrixEventReactionListModel::MatrixEventReactionListModel(
    Room room,
    lager::reader<std::string> parentEventId,
    QObject *parent)
    : KazvAbstractListModel(parent)
    , m_reactions(room.relatedEvents(parentEventId, "m.annotation")
        .map([](const auto &events) {
            QMap<QString, Kazv::EventList> reactions;
            for (const auto &e : events) {
                auto rel = e.mRelatesTo().get();
                // This automatically excludes redacted events,
                // because redacted events' content is empty.
                if (hasAtThat(rel, "key", &json::is_string)) {
                    auto key = QString::fromStdString(rel["key"].template get<std::string>());
                    reactions[key] = reactions[key].push_back(e);
                }
            }
            return reactions;
        }))
    , m_reactionKeys(m_reactions.map([](const auto &reactions) {
        return reactions.keys();
    }))
{
    initCountCursor(m_reactions.map([](const auto &reactions) -> int {
        return reactions.size();
    }));
}

MatrixEventReactionListModel::~MatrixEventReactionListModel() = default;

MatrixEventReaction *MatrixEventReactionListModel::at(int index) const
{
    return new MatrixEventReaction(lager::with(m_reactionKeys, m_reactions)
        .map([index](const auto &keys, const auto &reactions) {
            if (index >= 0 && index < keys.size()) {
                return reactions.value(keys[index]);
            }
            return EventList{};
        }),
        m_reactionKeys.map([index](const auto &keys) {
            if (index >= 0 && index < keys.size()) {
                return keys[index];
            }
            return QString();
        }));
}
