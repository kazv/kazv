/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QJsonObject>
#include <lager/extra/qt.hpp>

#include <client/room/room.hpp>
#include "kazv-abstract-list-model.hpp"

class MatrixEvent;

class MatrixRoomTimeline : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    Kazv::Room m_room;

    lager::reader<immer::flex_vector<std::string>> m_timelineEventIds;
    lager::reader<immer::map<std::string, Kazv::Event>> m_messagesMap;
    lager::reader<immer::flex_vector<Kazv::LocalEchoDesc>> m_localEchoes;
    lager::reader<immer::map<std::string, std::string>> m_timelineGaps;

public:
    explicit MatrixRoomTimeline(Kazv::Room room, QObject *parent = 0);
    explicit MatrixRoomTimeline(lager::reader<immer::flex_vector<std::string>> eventIds, lager::reader<immer::map<std::string, Kazv::Event>> messagesMap, lager::reader<immer::flex_vector<Kazv::LocalEchoDesc>> localEchoes, lager::reader<immer::map<std::string, std::string>> timelineGaps, Kazv::Room room, QObject *parent = 0);
    ~MatrixRoomTimeline() override;

    LAGER_QT_READER(QJsonObject, gaps);

    Q_INVOKABLE MatrixEvent *at(int index) const;

    Q_INVOKABLE int indexOfEvent(const QString &eventId) const;
};
