/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>

#include <lager/extra/qt.hpp>

#include <client/room/room.hpp>

#include "qt-json.hpp"
Q_MOC_INCLUDE("matrix-event-list.hpp")
Q_MOC_INCLUDE("matrix-event-reader-list-model.hpp")
Q_MOC_INCLUDE("matrix-event-reaction-list-model.hpp")

class MatrixEventReaderListModel;
class MatrixEventList;
class MatrixEventReactionListModel;

class MatrixEvent : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<std::optional<Kazv::LocalEchoDesc>> m_localEcho;
    lager::reader<Kazv::Event> m_event;
    std::optional<Kazv::Room> m_room;
    lager::reader<std::string> m_eventIdStd;
    lager::reader<std::string> m_senderStd;
    /// the unedited content of this event
    lager::reader<QJsonObject> m_originalContent;
    lager::reader<Kazv::EventList> m_edits;

public:
    explicit MatrixEvent(lager::reader<std::variant<Kazv::Event, Kazv::LocalEchoDesc>> event, std::optional<Kazv::Room> room = std::nullopt, QObject *parent = 0);
    explicit MatrixEvent(lager::reader<Kazv::Event> event, std::optional<Kazv::Room> room = std::nullopt, QObject *parent = 0);
    ~MatrixEvent() override;

    LAGER_QT_READER(QString, eventId);
    LAGER_QT_READER(QString, sender);
    LAGER_QT_READER(QString, type);
    LAGER_QT_READER(QString, stateKey);
    LAGER_QT_READER(QJsonObject, content);
    LAGER_QT_READER(bool, encrypted);
    LAGER_QT_READER(bool, decrypted);
    LAGER_QT_READER(bool, isState);
    LAGER_QT_READER(QJsonObject, unsignedData);
    LAGER_QT_READER(bool, isLocalEcho);
    LAGER_QT_READER(bool, isSending);
    LAGER_QT_READER(bool, isFailed);
    LAGER_QT_READER(QString, txnId);
    LAGER_QT_READER(bool, redacted);
    LAGER_QT_READER(QJsonObject, originalSource);
    LAGER_QT_READER(QJsonObject, decryptedSource);
    LAGER_QT_READER(QString, replyingToEventId);
    LAGER_QT_READER(QString, relationType);
    LAGER_QT_READER(QString, relatedEventId);
    LAGER_QT_READER(QString, formattedTime);
    LAGER_QT_READER(QString, formattedDateTime);
    LAGER_QT_READER(bool, isEdited);

    Q_INVOKABLE MatrixEventReaderListModel *readers() const;

    Kazv::Event underlyingEvent() const;

    Q_INVOKABLE MatrixEventList *history() const;

    Q_INVOKABLE MatrixEventReactionListModel *reactions() const;
};
