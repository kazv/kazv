/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QObject>
#include <QQmlEngine>

#include <lager/extra/qt.hpp>

#include <client/room/room.hpp>

#include "qt-json.hpp"

class MatrixEvent : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::Event> m_event;

public:
    explicit MatrixEvent(lager::reader<Kazv::Event> event, QObject *parent = 0);
    ~MatrixEvent() override;

    LAGER_QT_READER(QString, eventId);
    LAGER_QT_READER(QString, sender);
    LAGER_QT_READER(QString, type);
    LAGER_QT_READER(QString, stateKey);
    LAGER_QT_READER(QJsonObject, content);
    LAGER_QT_READER(bool, encrypted);
    LAGER_QT_READER(bool, isState);
    LAGER_QT_READER(QJsonObject, unsignedData);
};
