/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <QObject>
#include <QQmlEngine>
#include <QAbstractListModel>
#include <lager/extra/qt.hpp>
#include <base/types.hpp>
#include "qt-json.hpp"
#include "kazv-abstract-list-model.hpp"
Q_MOC_INCLUDE("matrix-event.hpp")

class MatrixEvent;

class MatrixEventList : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::EventList> m_eventList;

public:
    explicit MatrixEventList(lager::reader<Kazv::EventList> eventList, QObject *parent = 0);
    ~MatrixEventList() override;

    Q_INVOKABLE MatrixEvent *at(int index) const;
};
