/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "matrix-room-pinned-events-timeline.hpp"
#include <QJsonValue>

using namespace Kazv;

MatrixRoomPinnedEventsTimeline::MatrixRoomPinnedEventsTimeline(Kazv::Room room, QObject *parent)
    : MatrixRoomTimeline(
        room.pinnedEvents(),
        room.messagesMap(),
        lager::make_constant(immer::flex_vector<LocalEchoDesc>()),
        lager::make_constant(immer::map<std::string, std::string>()),
        room,
        parent
    )
    , LAGER_QT(eventIds)(room.pinnedEvents().map([](const auto &ids) {
        QJsonObject res;
        for (const auto &id : ids) {
            res.insert(QString::fromStdString(id), QJsonValue(true));
        }
        return res;
    }))
{
}

MatrixRoomPinnedEventsTimeline::~MatrixRoomPinnedEventsTimeline() = default;
