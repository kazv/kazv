/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <QUrl>
#include <QUrlQuery>
#include <QDebug>
#include <QString>

#include "matrix-link.hpp"

using namespace Qt::Literals::StringLiterals;

std::pair<MatrixLink::Type, QString> parseMatrixToComp(QString comp)
{
    comp = QUrl(u"foo:#"_s + comp).fragment(QUrl::FullyDecoded);
    if (comp.startsWith(u'@')) {
        return {MatrixLink::User, comp};
    } else if (comp.startsWith(u'#')) {
        return {MatrixLink::RoomAlias, comp};
    } else if (comp.startsWith(u'!')) {
        return {MatrixLink::RoomId, comp};
    } else if (comp.startsWith(u'$')) {
        return {MatrixLink::Event, comp};
    } else {
        return {MatrixLink::None, comp};
    }
}

QStringList parseMatrixToQuery(QString query)
{
    auto q = QUrlQuery(query);
    return q.allQueryItemValues(u"via"_s);
}

MatrixLink::MatrixLink(const QUrl &url)
{
    // matrix.to link
    if (url.scheme() == u"https"_s
        && url.host() == u"matrix.to"_s) {
        auto frag = url.fragment();
        auto parts = frag.split(u'?');
        QString ids = parts[0];
        QString query = parts.size() > 1 ? parts[1] : u""_s;

        auto comps = ids.split(u'/');
        if (comps.empty() || !comps[0].isEmpty()) {
            return;
        }
        comps.pop_front();
        for (auto comp : comps) {
            m_identifiers.push_back(parseMatrixToComp(comp));
        }

        m_routingServers = parseMatrixToQuery(query);
    }
}

bool MatrixLink::isValid() const
{
    return !m_identifiers.empty()
        && m_identifiers[0].first != None
        && m_identifiers[0].first != Event
        && (m_identifiers.size() == 1 || (
            m_identifiers[1].first == Event
            && (m_identifiers[0].first == RoomId
                || m_identifiers[0].first == RoomAlias)));
}

bool MatrixLink::isUser() const
{
    return isValid() && m_identifiers[0].first == User;
}

bool MatrixLink::isRoom() const
{
    return isValid() && (
        m_identifiers[0].first == RoomId
        || m_identifiers[0].first == RoomAlias);
}

bool MatrixLink::isEvent() const
{
    return isValid() && m_identifiers.size() == 2;
}

QStringList MatrixLink::identifiers() const
{
    QStringList ret;
    for (auto [_, id] : m_identifiers) {
        ret.push_back(id);
    }
    return ret;
}

QStringList MatrixLink::routingServers() const
{
    return m_routingServers;
}
