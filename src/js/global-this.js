/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

this.globalThis = this;
this.self = this;
this.global = this;
