
function roomAvatarPlaceholderName(room)
{
  return room.name || room.heroNames[0] || room.roomId;
}

function roomNameOrHeroes(room, heroes, userNameOverrides, l10n)
{
  if (room.name) {
    return l10n.get(
      'room-list-view-room-item-title-name',
      {
        name: room.name,
      }
    );
  } else if (room.heroEvents && room.heroEvents.length) {
    const heroNames = room.heroEvents.map(ev => {
      const displayName = ev.content && ev.content.displayname || '';
      const userId = ev.state_key;
      return userDisplayName(displayName, userId, userNameOverrides, l10n);
    });

    return l10n.get(
      'room-list-view-room-item-title-heroes',
      {
        hero: heroNames[0],
        secondHero: heroNames[1],
        otherNum: heroNames.length - 1,
      }
    );
  } else {
    return l10n.get(
      'room-list-view-room-item-title-id',
      {
        roomId: room.roomId,
      }
    );
  }
}

function userNameWithId(user, l10n)
{
  if (user.name) {
    return l10n.get('user-name-with-id', { name: user.name, userId: user.userId });
  } else {
    return user.userId;
  }
}

/**
 * Check whether the event is read by a user.
 *
 * @param event The event to check.
 * @param userId The id of the user to check.
 * @return Whether @c event is read by the user whose id is @c userId.
 */
function isEventReadBy(event, userId)
{
  const readers = event.readers();
  for (let i = 0; i < readers.count; ++i) {
    const reader = readers.at(i);
    if (reader.userId === userId) {
      return true;
    }
  }
  return false;
}

/**
 * Get the event body for editing.
 *
 * This takes into account the replied-to content.
 * As Matrix spec has nothing for the source content of a message,
 * we can only use some heuristics to determine what really is
 * what the user has typed.
 *
 * @param event The event to get the body of.
 * @return The body for editing.
 */
function getEventBodyForEditing(event) {
  if (
    event.replyingToEventId
    // Element will put the quotation inside the body and it follows the
    // following format:
    // ```
    // > <@userid:domain.ltd> whatever quoted content
    // > other lines
    // > other lines
    //
    // actual content
    // ```
    && event.content.body.startsWith('> <@')
  ) {
    const body = event.content.body;
    const lines = body.split('\n');
    const lastLineOfQuote = lines.findIndex((line, index) => {
      // the current line is a quote but the next line is not
      return line.startsWith('>') && !(lines[index + 1] || '').startsWith('>');
    });
    // Considering the empty line may not exist, strip the empty line from the beginning
    const firstLineOfContent = lines[lastLineOfQuote + 1] ? lastLineOfQuote + 1 : lastLineOfQuote + 2
    return lines.slice(firstLineOfContent).join('\n');
  }
  return event.content.body;
}

/**
 * Get the display name for a user, possibly overrided.
 *
 * @param name The user's global display name.
 * @param userId The user's id.
 * @param overrides A map from user id to overrided names.
 * @param l10n The fluent object.
 * @return A calculated display name for the user.
 */
function userDisplayName(name, userId, overrides = {}, l10n)
{
  const overridedName = overrides[userId];
  const globalName = name || userId;
  if (overridedName) {
    return l10n.get('user-name-overrided', { overridedName, globalName });
  } else {
    return globalName;
  }
}

const toolTipTimeout = 5000;
const imagePackRoomsEventType = 'im.ponies.emote_rooms';
const roomImagePackEventType = 'im.ponies.room_emotes';
