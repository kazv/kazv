
.import 'global-this.js' as MakeglobalThisAvailableInGlobal

.import 'bundled-deps.js' as Deps

.import 'fluent-bundle.js' as MakeFluentBundleAvailableInGlobal
.import 'fluent-sequence.js' as MakeFluentSequenceAvailableInGlobal
.import 'fluent-langneg.js' as MakeFluentLangNegAvailableInGlobal

this.Intl = this.IntlPolyfill;

class FluentProvider
{
  constructor(bundles)
  {
    this.bundles = bundles;
  }

  get(id, args)
  {
    // contexts is a negotiated iterable of FluentBundle instances.
    let ctx = FluentSequence.mapBundleSync(this.bundles, id);

    if (ctx === null) {
      return id;
    }

    let msg = ctx.getMessage(id);
    if (msg.value) {
      return ctx.formatPattern(msg.value, args);
    } else {
      return `<No message for ${id}>`; // should not happen
    }
  }
};

function negotiateLanguages(desiredLanguages, availableLocales, defaultLocale)
{
  if (! desiredLanguages) {
    desiredLanguages = [Qt.uiLanguage];
  }

  return FluentLangNeg.negotiateLanguages(
    desiredLanguages,
    availableLocales,
    { defaultLocale }
  );
}

// console.log("Available locales: ", LOCALES_ALL);

function generateBundles(locales, messageHash) {
  return locales.map(locale => {
    const bundle = new FluentBundle.FluentBundle(locale);
    bundle.addResource(new FluentBundle.FluentResource(messageHash[locale]));
    return bundle;
  });
}

/**

const availableLocales = ftlProvider.availableLocaleCodes();
const desiredLanguages = ...; // from user input
const defaultLocale = ...; // a fallback locale for which we have all translations

const wantedLocales = negotiateLanguages(desiredLanguages, availableLocales, defaultLocale);

const bundles = generateBundles(ftlProvider.get(wantedLocales));

const provider = new FluentProvider(bundles);

const testVar = provider.get('hello-world', {name: "Anna"});

*/
