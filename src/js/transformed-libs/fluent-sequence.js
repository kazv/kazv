function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _asyncIterator(iterable) { var method; if (typeof Symbol !== "undefined") { if (Symbol.asyncIterator) method = iterable[Symbol.asyncIterator]; if (method == null && Symbol.iterator) method = iterable[Symbol.iterator]; } if (method == null) method = iterable["@@asyncIterator"]; if (method == null) method = iterable["@@iterator"]; if (method == null) throw new TypeError("Object is not async iterable"); return method.call(iterable); }

/* @fluent/sequence@0.6.0 */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) : typeof define === 'function' && define.amd ? define('@fluent/sequence', ['exports'], factory) : (global = global || self, factory(global.FluentSequence = {}));
})(this, function (exports) {
  'use strict';
  /*
   * Synchronously map an identifier or an array of identifiers to the best
   * `FluentBundle` instance(s).
   *
   * @param bundles - An iterable of bundles to sift through.
   * @param ids - An id or ids to map.
   */

  function mapBundleSync(bundles, ids) {
    if (!Array.isArray(ids)) {
      return getBundleForId(bundles, ids);
    }

    return ids.map(id => getBundleForId(bundles, id));
  }
  /*
   * Find the best `FluentBundle` with the translation for `id`.
   */


  function getBundleForId(bundles, id) {
    for (const bundle of bundles) {
      if (bundle.hasMessage(id)) {
        return bundle;
      }
    }

    return null;
  }
  /*
   * Asynchronously map an identifier or an array of identifiers to the best
   * `FluentBundle` instance(s).
   *
   * @param bundles - An iterable of bundles to sift through.
   * @param ids - An id or ids to map.
   */


  function mapBundleAsync(_x, _x2) {
    return _mapBundleAsync.apply(this, arguments);
  }

  function _mapBundleAsync() {
    _mapBundleAsync = _asyncToGenerator(function* (bundles, ids) {
      if (!Array.isArray(ids)) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;

        var _iteratorError;

        try {
          for (var _iterator = _asyncIterator(bundles), _step, _value; _step = yield _iterator.next(), _iteratorNormalCompletion = _step.done, _value = yield _step.value, !_iteratorNormalCompletion; _iteratorNormalCompletion = true) {
            const bundle = _value;

            if (bundle.hasMessage(ids)) {
              return bundle;
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return != null) {
              yield _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        return null;
      }

      const foundBundles = new Array(ids.length).fill(null);
      let remainingCount = ids.length;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;

      var _iteratorError2;

      try {
        for (var _iterator2 = _asyncIterator(bundles), _step2, _value2; _step2 = yield _iterator2.next(), _iteratorNormalCompletion2 = _step2.done, _value2 = yield _step2.value, !_iteratorNormalCompletion2; _iteratorNormalCompletion2 = true) {
          const bundle = _value2;

          for (const [index, id] of ids.entries()) {
            if (!foundBundles[index] && bundle.hasMessage(id)) {
              foundBundles[index] = bundle;
              remainingCount--;
            } // Return early when all ids have been mapped to bundles.


            if (remainingCount === 0) {
              return foundBundles;
            }
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            yield _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      return foundBundles;
    });
    return _mapBundleAsync.apply(this, arguments);
  }

  exports.mapBundleAsync = mapBundleAsync;
  exports.mapBundleSync = mapBundleSync;
  Object.defineProperty(exports, '__esModule', {
    value: true
  });
});

