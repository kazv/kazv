/*
 * This file is part of kazv-js-deps.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import assert from 'node:assert/strict';
import { exit } from 'node:process';
import {
  getLineOffset,
  parseResources,
  validate,
  diffCore,
} from '../lint-l10n-lib.mjs';

const resolve = (name) => new URL(import.meta.resolve(name)).pathname;

const testCases = [
  function testGetLineOffset() {
    const file = `012\n4567\n\nABCD\n`;
    assert.equal(getLineOffset(file, 0), '1:0');
    assert.equal(getLineOffset(file, 1), '1:1');
    assert.equal(getLineOffset(file, 3), '1:3');
    assert.equal(getLineOffset(file, 4), '2:0');
    assert.equal(getLineOffset(file, 9), '3:0');
    assert.equal(getLineOffset(file, 10), '4:0');
  },
  function testValidate() {
    assert.deepEqual(
      validate(parseResources(resolve('./fixtures/empty'))),
      []
    );

    {
      const res = validate(parseResources(resolve('./fixtures/invalid')));
      assert.equal(res.length, 1);
      assert.ok(res[0].includes('Junk at'));
      assert.ok(res[0].includes('/invalid/x-foo/0.ftl (1:0 - 2:0):'));
      assert.ok(res[0].includes("'a =\n'"));
    }

    {
      const res = validate(parseResources(resolve('./fixtures/invalid-concat-valid')));
      assert.equal(res.length, 2);
      assert.ok(res[0].includes('Junk at'));
      assert.ok(res[0].includes('/invalid-concat-valid/x-foo/0.ftl (1:0 - 2:0):'));
      assert.ok(res[0].includes("'a = {\n'"));
      assert.ok(res[1].includes('Junk at'));
      assert.ok(res[1].includes('/invalid-concat-valid/x-foo/1.ftl (1:0 - 2:0):'));
      assert.ok(res[1].includes(`'  "1" }\n'`));
    }
  },
  function testDiffCore() {
    const resources = parseResources(resolve('./fixtures/2same1diff'));
    {
      const res = diffCore(resources, ['x-a', 'x-b']);
      assert.equal(res.length, 0);
    }
    {
      const res = diffCore(resources, ['x-a', 'x-b', 'x-c']);
      // b and x differ
      assert.equal(res.length, 2);
      const missingB = res.find(e => e.includes(`Message 'b'`));
      const missingX = res.find(e => e.includes(`Message 'x'`));
      assert.ok(missingB);
      assert.ok(missingX);
    }
  },
];

// Simple test harness
Promise.all(
  testCases.map(async (func) => {
    try {
      const res = await func();
      console.log('ok', func.name);
      return { func, status: 'success', res };
    } catch (err) {
      console.log('not ok', func.name, err);
      return { func, status: 'failure', err };
    }
  })
)
  .then(results => {
    const success = results.filter(k => k.status === 'success');
    const failure = results.filter(k => k.status === 'failure');
    console.log(`${results.length} total, ${success.length} succeeded, ${failure.length} failed`);
    exit(failure.length ? 1 : 0);
  });
