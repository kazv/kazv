/*
 * This file is part of kazv-js-deps.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import { readdirSync, readFileSync } from 'node:fs';
import path from 'node:path';
import FluentSyntax from '@fluent/syntax';

/**
 * @typedef {string} LangCode
 * @typedef {{
 *   [code: LangCode]: {
 *     [fileName: string]: {
 *       resource: FluentSyntax.Resource;
 *       fileContent: string;
 *     };
 *   };
 * }} Resources
 */

/**
 * @param {string} dir
 * @param {{ onlyDirs?: boolean }}
 * @return {string[]} Entries under dir
 */
const ls = (dir, { onlyDirs = false } = {}) => {
  const entries = readdirSync(dir, { withFileTypes: true });
  const customFilter = onlyDirs ? ent => ent.isDirectory() : _ => true;
  return entries
    .filter(ent => customFilter(ent) && !ent.name.startsWith('.'))
    .map(ent => ent.name);
};

/**
 * @param {string} dir
 * @return {{ [langCode: LangCode]: { [fileName: string]: string } }}
 */
export const getL10nFilesContent = (dir) => {
  const langCodes = ls(dir, { onlyDirs: true });
  const content = {};
  langCodes.forEach(langCode => {
    const d = path.join(dir, langCode);
    const ftls = ls(d)
          .filter(name => name.endsWith('.ftl'))
          .map(name => path.join(d, name));

    content[langCode] = {};
    const fileContent = ftls
          .forEach(f => {
            content[langCode][f] = readFileSync(f, { encoding: 'UTF-8' });
          });
  });
  return content;
}

/**
 * @param {string} dir
 * @return {Resources}
 */
export const parseResources = (dir) => {
  const content = getL10nFilesContent(dir);
  const resources = {};
  Object.entries(content)
    .forEach(([langCode, fileMap]) => {
      resources[langCode] = {};
      Object.entries(fileMap)
        .forEach(([fileName, fileContent]) => {
          resources[langCode][fileName] = {
            fileContent,
            resource: FluentSyntax.parse(fileContent),
          };
        });
    });
  return resources;
};

/**
 * @param {string} fileContent
 * @param {number} plainOffset
 * @return {string}
 */
export const getLineOffset = (fileContent, plainOffset) => {
  const substr = fileContent.slice(0, plainOffset);
  const lines = substr.split('\n');
  const lastLine = lines[lines.length - 1];
  return `${lines.length}:${lastLine.length}`;
};

const formatSpan = (fileName, fileContent, span) => {
  const { start, end } = span;
  const startOffset = getLineOffset(fileContent, start);
  const endOffset = getLineOffset(fileContent, end);
  const str = fileContent.slice(start, end);
  return {
    pos: `${fileName} (${startOffset} - ${endOffset})`,
    str,
  };
};

/**
 * @param {Resources} resources
 * @return {string[]} errors
 */
export const validate = (resources) => {
  const errors = [];
  Object.entries(resources)
    .forEach(([langCode, resMap]) => {
      Object.entries(resMap).forEach(([fileName, res]) => {
        res.resource.body
          .filter(entry => entry.type === 'Junk')
          .forEach(junk => {
            const { pos, str } = formatSpan(fileName, res.fileContent, junk.span);
            errors.push(`Junk at ${pos}: '${str}'`);
          });
      });
    });
  return errors;
};

const collectMessages = resourceMap => {
  const res = new Map();
  Object.entries(resourceMap)
    .forEach(([fileName, { resource }]) => {
      const messages = resource.body.filter(e => e.type === 'Message');
      messages.forEach(entry => {
        res.set(entry.id.name, { entry, fileName });
      });
    });
  return res;
};

/**
 * @param {Resources} resources
 * @param {LangCode[]} coreLanguages
 * @return {string[]} errors
 */
export const diffCore = (resources, coreLanguages) => {
  const missingLangs = coreLanguages.filter(langCode => !resources[langCode]);
  const errors = [];
  missingLangs.forEach(langCode => {
    errors.push(`Language ${langCode} is missing`);
  });
  const existingCoreLanguages = coreLanguages.filter(langCode => resources[langCode])
  if (existingCoreLanguages.length > 1) {
    const messagesMap = {};
    existingCoreLanguages.forEach(langCode => {
      messagesMap[langCode] = collectMessages(resources[langCode]);
    });

    const allMessageIds = existingCoreLanguages.reduce(
      (acc, langCode) => acc.union(messagesMap[langCode]),
      new Set(),
    );

    allMessageIds.forEach(id => {
      const {
        found,
        notFound,
      } = Object.groupBy(existingCoreLanguages, langCode =>
        messagesMap[langCode].has(id) ? 'found' : 'notFound'
      );

      if (notFound) {
        const foundPositions = found.map(langCode => {
          const msg = messagesMap[langCode].get(id);
          const span = msg.entry.span;
          const fileName = msg.fileName;
          const content = resources[langCode][fileName].fileContent;
          const { pos } = formatSpan(fileName, content, span);
          return pos;
        });

        errors.push(`Message '${id}' is not found in: ${notFound.join(', ')}. Found in ${foundPositions.join(', ')}.`);
      }
    });
  }
  return errors;
};
