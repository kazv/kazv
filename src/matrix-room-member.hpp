/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>

#include <lager/extra/qt.hpp>

#include <base/event.hpp>

class MatrixEvent;

class MatrixRoomMember : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::Event> m_memberEvent;

public:
    explicit MatrixRoomMember(lager::reader<Kazv::Event> memberEvent, QObject *parent = 0);
    ~MatrixRoomMember() override;

    LAGER_QT_READER(QString, membership);
    LAGER_QT_READER(QString, userId);
    LAGER_QT_READER(QString, name);
    LAGER_QT_READER(QString, avatarMxcUri);

    Q_INVOKABLE MatrixEvent *toEvent() const;
};
