/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QObject>
#include <QQmlEngine>

#include <lager/extra/qt.hpp>

#include <base/event.hpp>

class MatrixRoomMember : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::Event> m_memberEvent;

public:
    explicit MatrixRoomMember(lager::reader<Kazv::Event> memberEvent, QObject *parent = 0);
    ~MatrixRoomMember() override;

    LAGER_QT_READER(QString, membership);
    LAGER_QT_READER(QString, userId);
    LAGER_QT_READER(QString, name);
    LAGER_QT_READER(QString, avatarMxcUri);
};
