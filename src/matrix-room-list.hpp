/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <lager/sensor.hpp>
#include <lager/state.hpp>
#include <lager/extra/qt.hpp>

#include <client/client.hpp>
#include "kazv-abstract-list-model.hpp"
Q_MOC_INCLUDE("matrix-user-given-attrs-map.hpp")

class MatrixRoom;
class MatrixUserGivenAttrsMap;

class MatrixRoomList : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    Kazv::Client m_client;
    QString m_tagId;
    lager::sensor<std::string> m_tagIdCursor;
    lager::state<std::string, lager::automatic_tag> m_filter;
    lager::reader<Kazv::JsonWrap> m_userGivenNicknameMap;
    lager::reader<immer::flex_vector<std::string>> m_roomIds;

public:
    explicit MatrixRoomList(Kazv::Client client, QString tagId = QString(), QString filter = QString(), QObject *parent = 0);
    ~MatrixRoomList() override;

    Q_INVOKABLE void setTagId(QString tagId);

    LAGER_QT_CURSOR(QString, filter);
    LAGER_QT_READER(QStringList, roomIds);

    Q_INVOKABLE MatrixRoom *at(int index) const;
    Q_INVOKABLE QString roomIdAt(int index) const;
    Q_INVOKABLE MatrixRoom *room(QString roomId) const;
};
