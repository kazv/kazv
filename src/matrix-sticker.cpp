/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <base/cursorutil.hpp>

#include "helper.hpp"
#include "qt-json.hpp"
#include "matrix-sticker.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

MatrixSticker::MatrixSticker(lager::reader<std::string> shortCode, lager::reader<Kazv::json> sticker, QObject *parent)
    : QObject(parent)
    , m_sticker(sticker)
    , LAGER_QT(shortCode)(shortCode.xform(strToQt))
    , LAGER_QT(body)(lager::with(shortCode, m_sticker).xform(
        zug::map([](const std::string &shortCode, const json &j) {
            if (j.contains("body") && j["body"].is_string()) {
                return j["body"].template get<std::string>();
            } else {
                return shortCode;
            }
        }) | strToQt))
    , LAGER_QT(mxcUri)(m_sticker.xform(zug::map([](const json &j) {
        if (j.contains("url") && j["url"].is_string()) {
            return j["url"].template get<std::string>();
        } else {
            return std::string();
        }
    }) | strToQt))
    , LAGER_QT(info)(m_sticker.xform(zug::map([](const json &j) {
        if (j.contains("info") && j["info"].is_object()) {
            return j["info"];
        } else {
            return json::object();
        }
    }) | zug::map([](const json &j) -> QJsonObject {
            return j.template get<QJsonObject>();
        })))
{
}

MatrixSticker::~MatrixSticker() = default;

QJsonObject MatrixSticker::makeEventJson() const
{
    return QJsonObject{
        {u"type"_s, u"m.sticker"_s},
        {u"content"_s, QJsonObject{
            {u"url"_s, mxcUri()},
            {u"body"_s, body()},
            {u"info"_s, info()},
        }},
    };
}
