/*
 * Copyright (C) 2020 Tusooa Zhu <tusooa@vista.aero>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <libkazv-config.hpp>

#include <cursorutil.hpp>

#include "matrix-room-member.hpp"
#include "helper.hpp"


using namespace Kazv;
using namespace std::literals::string_literals;

MatrixRoomMember::MatrixRoomMember(lager::reader<Kazv::Event> memberEvent, QObject *parent)
    : QObject(parent)
    , m_memberEvent(memberEvent)
    , LAGER_QT(membership)(m_memberEvent.xform(eventContent | jsonAtOr("membership"s, ""s) | strToQt))
    , LAGER_QT(userId)(m_memberEvent.xform(zug::map([](Event e) { return e.stateKey(); }) | strToQt))
    , LAGER_QT(name)(m_memberEvent.xform(eventContent | jsonAtOr("displayname"s, ""s) | strToQt))
    , LAGER_QT(avatarMxcUri)(m_memberEvent.xform(eventContent | jsonAtOr("avatar_url"s, ""s) | strToQt))
{
}

MatrixRoomMember::~MatrixRoomMember() = default;
