/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <cursorutil.hpp>

#include "matrix-room-member.hpp"
#include "helper.hpp"
#include "matrix-event.hpp"

using namespace Kazv;
using namespace std::literals::string_literals;

MatrixRoomMember::MatrixRoomMember(lager::reader<Kazv::Event> memberEvent, QObject *parent)
    : QObject(parent)
    , m_memberEvent(memberEvent)
    , LAGER_QT(membership)(m_memberEvent.xform(eventContent | jsonAtOr("membership"s, ""s) | strToQt))
    , LAGER_QT(userId)(m_memberEvent.xform(zug::map([](Event e) { return e.stateKey(); }) | strToQt))
    , LAGER_QT(name)(m_memberEvent.xform(eventContent | jsonAtOr("displayname"s, ""s) | strToQt))
    , LAGER_QT(avatarMxcUri)(m_memberEvent.xform(roomMemberToAvatar | strToQt))
{
}

MatrixRoomMember::~MatrixRoomMember() = default;

MatrixEvent *MatrixRoomMember::toEvent() const
{
    return new MatrixEvent(m_memberEvent);
}
