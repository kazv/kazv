/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "matrix-event.hpp"
#include "matrix-event-reader-list-model.hpp"
#include "matrix-event-reaction-list-model.hpp"
#include "matrix-event-list.hpp"
#include "helper.hpp"
#include "kazv-log.hpp"

using namespace Kazv;

static std::optional<LocalEchoDesc> getLocalEcho(std::variant<Event, LocalEchoDesc> event)
{
    if (std::holds_alternative<LocalEchoDesc>(event)) {
        return std::get<LocalEchoDesc>(event);
    } else {
        return std::nullopt;
    }
}

static Event getEvent(std::variant<Event, LocalEchoDesc> event)
{
    if (std::holds_alternative<LocalEchoDesc>(event)) {
        return std::get<LocalEchoDesc>(event).event;
    } else {
        return std::get<Event>(event);
    }
}

MatrixEvent::MatrixEvent(lager::reader<std::variant<Event, LocalEchoDesc>> event, std::optional<Room> room, QObject *parent)
    : QObject(parent)
    , m_localEcho(event.map(getLocalEcho))
    , m_event(event.map(getEvent))
    , m_room(room)
    , m_eventIdStd(m_event.map(&Event::id))
    , m_senderStd(m_event.map(&Event::sender))
    , m_originalContent(m_event.map([](Event e) { return e.content().get().get<QJsonObject>(); }))
    , m_edits(m_room.has_value() ? lager::reader<EventList>(lager::with(
        m_event,
        m_room->relatedEvents(m_eventIdStd, "m.replace")
    ).map([](const Event &origEvent, const EventList &edits) -> EventList {
        // https://spec.matrix.org/v1.10/client-server-api/#event-replacements
        if (!(
            // (3) no state key
            !origEvent.isState()
            // (4) original event is not an edit
            && origEvent.relationship().first != "m.replace"
        )) {
            return EventList{};
        }
        auto origType = origEvent.type();
        auto origSender = origEvent.sender();
        return intoImmer(
            EventList{},
            zug::filter([&origType, &origSender](const Event &ev) {
                return
                    // (0) same room id requirement is implicit.
                    // (1) same sender
                    origSender == ev.sender()
                    // (2) same type
                    && origType == ev.type()
                    // (3) no state key
                    && !ev.isState()
                    // (4) original event is not an edit is checked above
                    // (5) replaced event content has `m.new_content`
                    && ev.content().get().contains("m.new_content");
            }),
            edits
        );
    })) : lager::make_constant(EventList{}))
    , LAGER_QT(eventId)(m_eventIdStd.xform(strToQt))
    , LAGER_QT(sender)(m_senderStd.xform(strToQt))
    , LAGER_QT(type)(m_event.xform(zug::map([](Event e) { return e.type(); }) | strToQt))
    , LAGER_QT(stateKey)(m_event.xform(zug::map([](Event e) { return e.stateKey(); }) | strToQt))
    , LAGER_QT(content)(lager::with(m_originalContent, m_edits).map([](const QJsonObject &origContent, const EventList &edits) {
        if (edits.empty()) {
            return origContent;
        } else {
            return edits.at(edits.size() - 1).content().get().at("m.new_content").get<QJsonObject>();
        }
    }))
    , LAGER_QT(encrypted)(m_event.xform(zug::map([](Event e) { return e.encrypted(); })))
    , LAGER_QT(decrypted)(m_event.map([](Event e) { return e.decrypted(); }))
    , LAGER_QT(isState)(m_event.map([](Event e) { return e.isState(); }))
    , LAGER_QT(unsignedData)(m_event.map([](Event e) {
                                             auto j =  e.raw();
                                             if (j.get().contains("unsigned")) {
                                                 return j.get()["unsigned"].template get<QJsonObject>();
                                             } else {
                                                 return QJsonObject();
                                             }
                                         }))
    , LAGER_QT(isLocalEcho)(m_localEcho.map([](const auto &maybe) { return maybe.has_value(); }))
    , LAGER_QT(isSending)(m_localEcho.map([](const auto &maybe) {
        return maybe.has_value() && maybe.value().status == LocalEchoDesc::Sending;
    }))
    , LAGER_QT(isFailed)(m_localEcho.map([](const auto &maybe) {
        return maybe.has_value() && maybe.value().status == LocalEchoDesc::Failed;
    }))
    , LAGER_QT(txnId)(m_localEcho.map([](const auto &maybe) {
        return maybe.has_value() ? QString::fromStdString(maybe.value().txnId) : QStringLiteral("");
    }))
    , LAGER_QT(redacted)(m_event.map([](const auto &e) {
        return e.redacted();
    }))
    , LAGER_QT(originalSource)(m_event.map([](Event e) {
        return e.originalJson().get().template get<QJsonObject>();
    }))
    , LAGER_QT(decryptedSource)(m_event.map([](Event e) {
        return e.encrypted() ? e.decryptedJson().get().template get<QJsonObject>() : QJsonObject();
    }))
    , LAGER_QT(replyingToEventId)(m_event.map([](Event e) {
        return e.replyingTo();
    }).xform(strToQt))
    , LAGER_QT(relationType)(m_event.map([](Event e) {
        return e.relationship().first;
    }).xform(strToQt))
    , LAGER_QT(relatedEventId)(m_event.map([](Event e) {
        return e.relationship().second;
    }).xform(strToQt))
    , LAGER_QT(formattedTime)(m_event
        .map(&Event::originServerTs)
        .map([](Timestamp ts) {
            auto locale = QLocale::system();
            return locale.toString(
                QDateTime::fromMSecsSinceEpoch(ts).time(),
                QLocale::ShortFormat
            );
        }))
    , LAGER_QT(formattedDateTime)(m_event
        .map(&Event::originServerTs)
        .map([](Timestamp ts) {
            auto locale = QLocale::system();
            return locale.toString(
                QDateTime::fromMSecsSinceEpoch(ts),
                QLocale::ShortFormat
            );
        }))
    , LAGER_QT(isEdited)(m_edits.map([](const auto &edits) { return !edits.empty(); }))
{
}

MatrixEvent::MatrixEvent(lager::reader<Event> event, std::optional<Kazv::Room> room, QObject *parent)
    : MatrixEvent(event.map([](const auto &e) -> std::variant<Kazv::Event, Kazv::LocalEchoDesc> { return e; }), room, parent)
{
}

MatrixEvent::~MatrixEvent() = default;

MatrixEventReaderListModel *MatrixEvent::readers() const
{
    if (!m_room.has_value()) {
        qCDebug(kazvLog) << "readers(): no room!";
        return new MatrixEventReaderListModel(lager::make_constant(immer::flex_vector<std::tuple<Kazv::Event, Kazv::Timestamp>>{}));
    }

    auto eventReaders = lager::with(
        m_room.value().eventReaders(m_event.map(&Event::id)),
        m_room.value().stateEvents()
    ).map([](const auto &eventReaders, const auto &states) {
        return intoImmer(
            immer::flex_vector<std::tuple<Kazv::Event, Kazv::Timestamp>>{},
            zug::map([states](const auto &item) -> std::tuple<Kazv::Event, Kazv::Timestamp> {
                auto [userId, ts] = item;
                qCDebug(kazvLog) << "readers(): read by" << QString::fromStdString(userId);
                auto memberEvent = states.count({"m.room.member", userId})
                    ? states[{"m.room.member", userId}]
                    : Event{json{
                    {"content", json::object()},
                    {"state_key", userId},
                    {"type", "m.room.member"},
                }};
                return {memberEvent, ts};
            }),
            eventReaders
        );
    }).make();

    return new MatrixEventReaderListModel(eventReaders);
}

Kazv::Event MatrixEvent::underlyingEvent() const
{
    return m_event.get();
}

MatrixEventList *MatrixEvent::history() const
{
    return new MatrixEventList(lager::with(m_event, m_edits)
        .map([](const auto &event, const auto &edits) {
            return intoImmer(
                EventList{event},
                zug::map([event](const auto &e) {
                    auto j = event.originalJson().get();
                    auto ret = j;
                    ret["type"] = e.type();
                    ret["content"] = e.content().get().at("m.new_content");
                    ret["event_id"] = e.id();
                    ret["origin_server_ts"] = e.originServerTs();
                    if (j.contains("/content/m.relates_to"_json_pointer)) {
                        ret["content"]["m.relates_to"] = j["content"]["m.relates_to"];
                    }
                    return Event(ret);
                }),
                edits
            );
        }));
}

MatrixEventReactionListModel *MatrixEvent::reactions() const
{
    if (!m_room) {
        return nullptr;
    }
    return new MatrixEventReactionListModel(
        m_room.value(),
        m_eventIdStd
    );
}
