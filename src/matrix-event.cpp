/*
 * Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 *
 * This file is part of kazv.
 *
 * kazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * kazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with kazv.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <libkazv-config.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include "matrix-event.hpp"

#include "helper.hpp"

using namespace Kazv;

MatrixEvent::MatrixEvent(lager::reader<Event> event, QObject *parent)
    : QObject(parent)
    , m_event(event)
    , LAGER_QT(eventId)(m_event.xform(zug::map([](Event e) { return e.id(); }) | strToQt))
    , LAGER_QT(sender)(m_event.xform(zug::map([](Event e) { return e.sender(); }) | strToQt))
    , LAGER_QT(type)(m_event.xform(zug::map([](Event e) { return e.type(); }) | strToQt))
    , LAGER_QT(stateKey)(m_event.xform(zug::map([](Event e) { return e.stateKey(); }) | strToQt))
    , LAGER_QT(content)(m_event.xform(zug::map([](Event e) { return e.content().get().get<QJsonObject>(); })))
    , LAGER_QT(encrypted)(m_event.xform(zug::map([](Event e) { return e.encrypted(); })))
    , LAGER_QT(isState)(m_event.map([](Event e) { return e.isState(); }))
    , LAGER_QT(unsignedData)(m_event.map([](Event e) {
                                             auto j =  e.raw();
                                             if (j.get().contains("unsigned")) {
                                                 return j.get()["unsigned"].template get<QJsonObject>();
                                             } else {
                                                 return QJsonObject();
                                             }
                                         }))
{
}

MatrixEvent::~MatrixEvent() = default;
