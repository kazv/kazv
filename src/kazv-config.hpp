/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2021 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QString>
#include <QScopedPointer>

struct KazvConfigPrivate;

class KazvConfig : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    QScopedPointer<KazvConfigPrivate> m_d;

    Q_PROPERTY(QString lastSession READ lastSession WRITE setLastSession)
    Q_PROPERTY(QString cacheDirectory READ cacheDirectory WRITE setCacheDirectory)

public:
    explicit KazvConfig(QObject *parent = 0);
    ~KazvConfig() override;

public Q_SLOTS:
    QString lastSession() const;

    void setLastSession(QString session);

    QStringList shortcutForAction(QString actionName) const;

    void setShortcutForAction(QString actionName, QStringList shortcut);

    /**
     * If Kazv calls setCacheDirectory() correctly, then this will always return a valid path
     **/
    QString cacheDirectory() const;

    /**
     * Where call this function should make sure that the cacheDirectory passed in is a valid path,
     * otherwise the return value of cacheDirectory() will be an invalid path
     **/
    void setCacheDirectory(QString cacheDirectory);
};
