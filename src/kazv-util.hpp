/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QString>
#include <QQmlEngine>

class KazvUtil : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

    Q_PROPERTY(int kfQtMajorVersion CONSTANT MEMBER m_kfQtMajorVersion)

public:
    KazvUtil(QObject *parent = 0);
    ~KazvUtil() override;

public Q_SLOTS:
    QString escapeHtml(const QString &orig) const;
    QString mimeTypeForUrl(const QUrl &url) const;

    /// @return The user id for a given matrix link, or the empty string
    /// if @c url is not a matrix link pointing to a user
    QString matrixLinkUserId(const QString &url) const;

private:
    int m_kfQtMajorVersion;
};
