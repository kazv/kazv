/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include "matrix-room-member-list-model.hpp"
Q_MOC_INCLUDE("matrix-event-reader.hpp")

class MatrixEventReader;

class MatrixEventReaderListModel : public MatrixRoomMemberListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<immer::flex_vector<std::tuple<Kazv::Event, Kazv::Timestamp>>> m_readers;

public:
    explicit MatrixEventReaderListModel(lager::reader<immer::flex_vector<std::tuple<Kazv::Event, Kazv::Timestamp>>> readers, QObject *parent = 0);
    ~MatrixEventReaderListModel() override;

    Q_INVOKABLE MatrixEventReader *at(int index) const;
};
