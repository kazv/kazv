/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <lager/extra/qt.hpp>
#include <lager/state.hpp>
#include <client/room/room.hpp>
#include "kazv-abstract-list-model.hpp"

class MatrixRoomMember;

class MatrixRoomMemberListModel : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::state<std::string, lager::automatic_tag> m_filter;
    lager::reader<Kazv::EventList> m_members;

public:
    explicit MatrixRoomMemberListModel(lager::reader<Kazv::EventList> members, QString filter = QString(), lager::reader<Kazv::Event> userGivenNicknameEvent = lager::make_constant(Kazv::Event()), QObject *parent = 0);
    ~MatrixRoomMemberListModel() override;

    LAGER_QT_CURSOR(QString, filter);

    Q_INVOKABLE MatrixRoomMember *at(int index) const;
};
