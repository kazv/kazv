/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QMetaType>

#include <base/kazvevents.hpp>
#include "matrix-sticker-pack-source.hpp"
#include "kazv-platform.hpp"

Q_DECLARE_METATYPE(Kazv::KazvEvent)
Q_DECLARE_METATYPE(MatrixStickerPackSource)

class KazvMetaTypeRegistration
{
public:
    KazvMetaTypeRegistration();

    int m_kazvEvent;
    int m_matrixStickerPackSource;
};
