/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "kazv-log.hpp"

#include "helper.hpp"

using namespace Kazv;

QString trustLevelToQStringFunc(DeviceTrustLevel trustLevel)
{
    switch (trustLevel) {
    case Blocked:
        return QStringLiteral("blocked");

    case Unseen:
        return QStringLiteral("unseen");

    case Seen:
        return QStringLiteral("seen");

    case Verified:
        return QStringLiteral("verified");

    default:
        qCWarning(kazvLog) << "trustLevelToQStringFunc(): Unknown trust level:" << trustLevel;
        return QStringLiteral();
    }
}

DeviceTrustLevel qStringToTrustLevelFunc(QString trustLevel)
{
    if (trustLevel == QStringLiteral("blocked")) {
        return Blocked;
    } else if (trustLevel == QStringLiteral("unseen")) {
        return Unseen;
    } else if (trustLevel == QStringLiteral("seen")) {
        return Seen;
    } else if (trustLevel == QStringLiteral("verified")) {
        return Verified;
    } else {
        qCWarning(kazvLog) << "QStringToTrustLevelFunc(): Unknown trust level:" << trustLevel;
        return Blocked;
    }
}
