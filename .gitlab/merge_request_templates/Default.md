Summary | 概述:
<!--
    (Please remove the comments when submitting the merge request.)
    （在提交合并请求的时候请删掉这些注释。）
    Describe in detail what the changes are.
    详细描述改变了什么。

    Any added UI text must be translated via `l10n.get()`.
    The messages must be translated into every *core language*: en, cmn.

    任何添加的 UI 文字都要用 `l10n.get()` 翻译。
    讯息必须翻译成所有*核心语言*：en，cmn。

    The merge request creator must translate each message into one of the *core languages*.
    The others can be handled by others.

    开合并请求的人必须把每条讯息翻译成一种*核心语言*。
    别的可以给别人做。
-->

Type | 类型: <!-- add|remove|skip|security|fix -->

Test Plan | 测试计划:
<!--
    Tell reviewers how to verify the intended behaviours.
    告诉审核者怎么验证想要的表现。
-->
