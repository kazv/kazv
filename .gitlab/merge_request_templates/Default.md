
# Checklist | 检查清单

Before marking this merge request as Ready, the following must be done:

在把这个合并请求标记成就绪之前，这些东西必须完成：

- [ ]
  The code must compile. Preferably, each commit should compile.

  代码必须能编译。最好每个提交都能编译。

- [ ]
  Any added UI text must be translated via `l10n.get()`.
  The messages must be translated into every *core language*: en, cmn.

  任何添加的 UI 文字都要用 `l10n.get()` 翻译。
  讯息必须翻译成所有*核心语言*：en，cmn。

  - [ ]
    The merge request creator must translate each message into one of the *core languages*.
    The others can be handled by others.

    开合并请求的人必须把每条讯息翻译成一种*核心语言*。
    别的可以给别人做。

- [ ]
  If the merge request contains a user-visible change, it must have a changelog.

  如果这个合并请求包括对用户可见的更改，必须写个更改记录。
