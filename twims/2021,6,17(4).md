TWIM:

# kazv

[kazv](https://lily.kazv.moe/kazv/kazv) is a matrix client based on [libkazv](https://lily.kazv.moe/kazv/libkazv).
Talk to us on #kazv:tusooa.xyz .

## Updates

We got an AppImage build for x86-64 GNU/Linux systems. Feel free to try -))

https://lily.kazv.moe/kazv/kazv/-/jobs/452/artifacts/browse
