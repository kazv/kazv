TWIM:

# kazv

[kazv](https://lily.kazv.moe/kazv/kazv) is a matrix client based on [libkazv](https://lily.kazv.moe/kazv/libkazv).
Talk to us on #kazv:tusooa.xyz .

## Updates

0. @tusooa:tusooa.xyz fixed a thread-safety issue that caused crashes. https://lily.kazv.moe/kazv/kazv/-/merge_requests/6

1. We now have a new developer @nannanko:tusooa.xyz . She implemented a login failure prompt for kazv. https://lily.kazv.moe/kazv/kazv/-/merge_requests/4

You can get the current AppImage build at https://lily.kazv.moe/kazv/kazv/-/jobs/611/artifacts/browse .
