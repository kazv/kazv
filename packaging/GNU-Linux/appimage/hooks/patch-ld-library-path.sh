# A script to work around the "can't mix incompatible qt versions" problem in
# libqtquicktemplates2plugin.so

set -e

# the directory where AppRun is
thisDir="$(readlink -f "$(dirname "$(realpath "$0")")")"

libDir="$thisDir/usr/lib"

export LD_LIBRARY_PATH="$libDir"

unset thisDir libDir
