#!/bin/bash

set -x
set -e

thisDir="$(readlink -f "$(dirname "$(realpath "$0")")")"

BUILD_TYPE="$1"
if [[ ! "$BUILD_TYPE" ]]; then
    export BUILD_TYPE=Debug
fi
export JOBS="$(nproc)" DEPS_INSTALL_DIR=/opt/libkazv-deps LIBKAZV_INSTALL_DIR=/opt/libkazv KAZV_INSTALL_DIR=/usr

mkdir build && cd build && \
    export CC=gcc-9 CXX=g++-9 && \
    cmake .. -DCMAKE_INSTALL_PREFIX="$KAZV_INSTALL_DIR" \
          -DCMAKE_PREFIX_PATH="$LIBKAZV_INSTALL_DIR;$DEPS_INSTALL_DIR" -DCMAKE_BUILD_TYPE=$BUILD_TYPE && \
    make -j$JOBS && \
    make -j$JOBS DESTDIR=AppDir install

export LD_LIBRARY_PATH="$LIBKAZV_INSTALL_DIR/lib:$DEPS_INSTALL_DIR/lib"

wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
wget https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage

chmod +x linuxdeploy*.AppImage

export QML_SOURCES_PATHS=../src
./linuxdeploy-plugin-qt-x86_64.AppImage --appimage-extract
./linuxdeploy-x86_64.AppImage --appimage-extract

# https://github.com/linuxdeploy/linuxdeploy/issues/154
mv -v ./linuxdeploy-plugin-qt-x86_64.AppImage ./linuxdeploy-notplugin-qt-x86_64.AppImage

# Enable the appimage to use Plasma themes, linuxdeploy-plugin-qt will only copy the gtk3 plugin -((
mkdir -pv AppDir/usr/plugins/platformthemes/
cp /usr/lib/x86_64-linux-gnu/qt5/plugins/platformthemes/KDEPlasmaPlatformTheme.so AppDir/usr/plugins/platformthemes/

# Workaround "cannot mix incompatible qt versions" problem

mkdir -pv AppDir/apprun-hooks/
cp -v "$thisDir"/hooks/patch-ld-library-path.sh AppDir/apprun-hooks/

./squashfs-root/AppRun --appdir AppDir -i ../icons/kazvlogo-256.png --plugin qt --output appimage # -l /lib/x86_64-linux-gnu/libstdc++.so.6

cp kazv*.AppImage ../kazv-"$BUILD_TYPE".AppImage
