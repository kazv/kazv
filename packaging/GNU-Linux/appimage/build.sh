#!/bin/bash

set -x
set -e

export thisDir="$(readlink -f "$(dirname "$(realpath "$0")")")"

# Collect deps
wget https://raw.githubusercontent.com/KDE/docker-neon/Neon/stable/public.key && \
    apt-key add public.key && \
    rm public.key

DEP_PACKAGES=(
    extra-cmake-modules
    wget
    weston
    dbus-x11
    libcmark-dev
)

if [[ $KF_VER == 5 ]]; then
    echo "Building on KF5 is no longer supported."
    exit 1
else
    DEP_PACKAGES+=(
        qt6-base-dev
        qt6-declarative-dev
        qt6-multimedia-dev
        qt6-imageformats
        qml6-module-qtmultimedia
        libqt6concurrent6
        libqt6gui6
        libqt6svg6-dev
        libqt6quickcontrols2-6
        qml6-module-qtquick-controls
        qml6-module-qtquick-layouts
        qml6-module-qt-labs-qmlmodels
        kf6-kirigami2-dev
        libkf6config-dev
        qt6-gtk-platformtheme
        plasma-integration
        libkf6kio-dev
        qml6-module-qttest
        qml6-module-qt-labs-platform
        libkf6notifications-dev
        qml6-module-qtquick-dialogs
        qml6-module-org-kde-notification
        kf6-kirigami-addons-dev
        kf6-kcoreaddons-dev
        qml6-module-org-kde-qqc2desktopstyle
        kf6-qqc2-desktop-style-dev
        kf6-breeze-icon-theme-dev
        cmark
    )
    export QMAKE=qmake6
    cp -v packaging/GNU-Linux/appimage/kde-neon-jammy.list /etc/apt/sources.list.d/
fi

apt-get update && \
    apt-get -y upgrade && \
    DEBIAN_FRONTEND="noninteractive" \
    apt-get -y install aptitude && \
    aptitude -y install "${DEP_PACKAGES[@]}"

export BUILD_TYPE="$1"
if [[ ! "$BUILD_TYPE" ]]; then
    export BUILD_TYPE=Debug
fi

useradd -U builder
mkdir build
chown -R builder:builder ccache
chown builder:builder build
cd build

export JOBS=${JOBS:-$(( $(nproc) / 2))} DEPS_INSTALL_DIR=/opt/libkazv-deps LIBKAZV_INSTALL_DIR=/opt/libkazv KAZV_INSTALL_DIR=/usr

export LD_LIBRARY_PATH="$LIBKAZV_INSTALL_DIR/lib:$LIBKAZV_INSTALL_DIR/lib/x86_64-linux-gnu:$DEPS_INSTALL_DIR/lib:$DEPS_INSTALL_DIR/lib/x86_64-linux-gnu"

mkdir -pv /run/user/test
chown builder:builder /run/user/test
su builder -c "env PATH='$PATH' '$thisDir'/build-unprivileged.sh" || exit 1

wget https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage
wget https://github.com/linuxdeploy/linuxdeploy-plugin-qt/releases/download/continuous/linuxdeploy-plugin-qt-x86_64.AppImage

chmod +x linuxdeploy*.AppImage

export QML_SOURCES_PATHS=../src
./linuxdeploy-plugin-qt-x86_64.AppImage --appimage-extract
./linuxdeploy-x86_64.AppImage --appimage-extract

# https://github.com/linuxdeploy/linuxdeploy/issues/154
mv -v ./linuxdeploy-plugin-qt-x86_64.AppImage ./linuxdeploy-notplugin-qt-x86_64.AppImage

# Enable the appimage to use Plasma themes, linuxdeploy-plugin-qt will only copy the gtk3 plugin -((
mkdir -pv AppDir/usr/plugins/platformthemes/
cp /usr/lib/x86_64-linux-gnu/qt6/plugins/platformthemes/KDEPlasmaPlatformTheme6.so AppDir/usr/plugins/platformthemes/ || true
mkdir -pv AppDir/usr/qml/org/kde
cp -rv /usr/lib/x86_64-linux-gnu/qt6/qml/org/kde/desktop AppDir/usr/qml/org/kde
cp -rv /usr/lib/x86_64-linux-gnu/qt6/qml/org/kde/qqc2desktopstyle AppDir/usr/qml/org/kde
mkdir -pv AppDir/usr/plugins/kf6/kirigami/platform
cp -v /usr/lib/x86_64-linux-gnu/qt6/plugins/kf6/kirigami/platform/org.kde.desktop.so AppDir/usr/plugins/kf6/kirigami/platform/

# Workaround "cannot mix incompatible qt versions" problem

mkdir -pv AppDir/apprun-hooks/
cp -v "$thisDir"/hooks/*.sh AppDir/apprun-hooks/

./squashfs-root/AppRun --appdir AppDir -i ../icons/kazvlogo-256.png --plugin qt --output appimage # -l /lib/x86_64-linux-gnu/libstdc++.so.6

cp kazv*.AppImage ../kazv-"$BUILD_TYPE".AppImage
