#!/bin/bash

export XDG_RUNTIME_DIR=/run/user/test

cmake .. -DCMAKE_INSTALL_PREFIX="$KAZV_INSTALL_DIR" \
      -DCMAKE_PREFIX_PATH="$LIBKAZV_INSTALL_DIR;$DEPS_INSTALL_DIR" -DCMAKE_BUILD_TYPE=$BUILD_TYPE -Dkazv_KF_QT_MAJOR_VERSION=$KF_VER -Dkazv_LINK_BREEZE_ICONS=ON && \
    make -j$JOBS && \
    make -j$JOBS DESTDIR=AppDir install && \
    dbus-launch --exit-with-session -- bash "$thisDir"/test.sh
