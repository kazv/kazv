
# Change log

## 0.6.0

### Added
- Add confirm popup for delete event. https://iron.lily-is.land/D125 by nannanko
- Enable upload file for reply. https://iron.lily-is.land/D143 by nannanko
- Support emote rooms in MSC2545. https://iron.lily-is.land/D166 by tusooa
- Display list of sticker packs in a room. https://iron.lily-is.land/D170 by tusooa
- Add option to use or disuse a sticker pack. https://iron.lily-is.land/D171 by tusooa
- Show decrypted json source for undecrypted events. https://iron.lily-is.land/D174 by tusooa
- Add tests for KazvFile and KazvSaveFile. https://iron.lily-is.land/D176 by nannan
- Implement adding sticker pack to a room. https://iron.lily-is.land/D175 by tusooa
- Add test for progress bar. https://iron.lily-is.land/D181 by nannan
- Add progress text for download and upload and relevant test. https://iron.lily-is.land/D182 by nannan
- Enable edit room name and relevant tests. https://iron.lily-is.land/D183 by nannan
- Build flatpak. https://lily-is.land/kazv/kazv/-/merge_requests/113 by tusooa
- Support adding sticker to packs in room state. https://iron.lily-is.land/D188 by tusooa
- Use a temp file to save session. https://iron.lily-is.land/D189 by tusooa
- Use FileHandler for all images and avatars. https://iron.lily-is.land/D192 by tusooa
- Remove useless items and added missing items of create-room-page series. https://iron.lily-is.land/D194 by nannanko
- Add indicator for tombstoned room. https://iron.lily-is.land/D197 by nannanko
- Display reactions under the reacted event. https://iron.lily-is.land/D195 by tusooa

### Fixed
- Use Kirigami.Theme.smallFont for small fonts. https://iron.lily-is.land/D172 by tusooa
- Fix the problem that kazv cannot be closed when cached files. https://iron.lily-is.land/D177 by nannan
- Fix missing libraries in appimage build. https://iron.lily-is.land/D187 by tusooa
- Disable show KIO status notification when cache files. https://iron.lily-is.land/D200 by nannanko
- Fix reaction count text color. https://iron.lily-is.land/D201 by tusooa
- Consider local read marker when checking whether a room is read. https://iron.lily-is.land/D205 by tusooa

### Internal changes
- Fix lint ci. https://iron.lily-is.land/D167 by tusooa
- Split video event tests into its own file. https://iron.lily-is.land/D169 by tusooa
- Update documentation regarding dependencies. https://iron.lily-is.land/D179 by tusooa
- Skip reactToEventPopupTest. https://iron.lily-is.land/D180 by nannan
- Refactor common mocks into its own component. https://iron.lily-is.land/D190 by tusooa
- Add installation info for Nix and Windows. https://iron.lily-is.land/D191 by tusooa
- Use ccache for CI/CD. https://lily-is.land/kazv/kazv/-/merge_requests/114 by tusooa
- Add CI to validate ftls and ensure core languages are up-to-date. https://iron.lily-is.land/D196 by tusooa
- Fix ccache not being used. https://iron.lily-is.land/D198 by tusooa
- Cache apt files. https://iron.lily-is.land/D199 by tusooa

## 0.5.0

### Added
- Optimize image event layout in compact mode. https://iron.lily-is.land/D98 by tusooa
- Show room id on RoomSettingsPage, add avatars and icons. https://lily-is.land/kazv/kazv/-/merge_requests/100 by April Simone
- Change the visual style of reply bubble in SendMessageBox.qml. https://lily-is.land/kazv/kazv/-/merge_requests/96 by April Simone
- Show user id on UserPage, and style change. https://lily-is.land/kazv/kazv/-/merge_requests/99 by April Simone
- Add context menu for SendMessageBox text area. https://lily-is.land/kazv/kazv/-/merge_requests/106 by April Simone
- Add unread notification count to room list view. https://iron.lily-is.land/D108 by tusooa
- Add MatrixEventList class. https://iron.lily-is.land/D116 by tusooa
- Use POSIX file lock to prevent two kazvs running on the same session. https://iron.lily-is.land/D120 by tusooa
- Display error message if load session failed. https://iron.lily-is.land/D124 by tusooa
- Support displaying and setting room topic. https://iron.lily-is.land/D126 by tusooa
- Add hard logout facility. https://lily-is.land/kazv/kazv/-/merge_requests/108 by April Simone
- Add a tooltip for event date in timeline. https://iron.lily-is.land/D112 by tusooa
- Support viewing event history. https://iron.lily-is.land/D117 by tusooa
- Support displaying pinned events. https://iron.lily-is.land/D129 by tusooa
- Paste directly to upload pictures/files. https://lily-is.land/kazv/kazv/-/merge_requests/109 by April Simone
- Show favourite rooms at the top. https://iron.lily-is.land/D123 by nannanko
- Implement pinning and unpinning events. https://iron.lily-is.land/D131 by tusooa
- Support creating encrypted room. https://iron.lily-is.land/D139 by tusooa
- Add emoji picker in react dialog. https://lily-is.land/kazv/kazv/-/merge_requests/111 by April Simone
- Improve playback logic for videos. https://iron.lily-is.land/D149 by tusooa
- Add a compact mode summary for video event type. https://iron.lily-is.land/D150 by tusooa
- Add about page. https://iron.lily-is.land/D151 by tusooa
- Support set self display name in room. https://iron.lily-is.land/D155 by tusooa

### Fixed
- Fix not show passiveNotification when leave room action resolved. https://iron.lily-is.land/D92 by nannanko
- Fix styles for SwipeListItems. https://iron.lily-is.land/D94 by tusooa
- Make the ToolButtons in SendMessageBox all use `action`. https://iron.lily-is.land/D96 by tusooa
- Optimize replied-to event style. https://iron.lily-is.land/D97 by tusooa
- Downscale image in compact mode. https://iron.lily-is.land/D99 by tusooa
- Fix ItemDelegate display. https://iron.lily-is.land/D100 by tusooa
- Add "#include <lager/config.hpp>" in kazv-defs.hpp.in. https://lily-is.land/kazv/kazv/-/merge_requests/104 by April Simone
- Add qt6-imageformats as dependency. https://lily-is.land/kazv/kazv/-/merge_requests/105 by April Simone
- Change the visual style of Recent/Favourite buttons. https://lily-is.land/kazv/kazv/-/merge_requests/97 by April Simone
- Fix crash when upload failed. https://iron.lily-is.land/D104 by nannanko
- Fix local echoes being displayed as selected. https://iron.lily-is.land/D109 by tusooa
- Fixed a translation error about forgetting a room. https://iron.lily-is.land/D121 by nannanko
- Compile translation files directly into the binary. https://iron.lily-is.land/D110 by tusooa
- Prevent user from sending an empty message. https://iron.lily-is.land/D111 by tusooa
- Make the resend button a ToolButton. https://iron.lily-is.land/D122 by nannanko
- Fix file name not being displayed in ConfirmUploadPopup. https://iron.lily-is.land/D136 by tusooa
- Remove sourceSize from avatar items. https://iron.lily-is.land/D140 by tusooa
- Use Image instead of Kirigami.Icon for sticker picker. https://iron.lily-is.land/D141 by tusooa
- Make video playable. https://iron.lily-is.land/D147 by tusooa
- Make encrypted videos playable. https://iron.lily-is.land/D148 by tusooa
- Fix AppImage styles and icons. https://iron.lily-is.land/D154 by tusooa

### Removed
- Drop support for Qt5/KF5. https://iron.lily-is.land/D91 by tusooa
- Drop support for legacy sessions. https://iron.lily-is.land/D119 by tusooa

### Internal changes
- Set cmake indent to 2 in .editorconfig. https://iron.lily-is.land/D103 by tusooa
- Unify list models. https://iron.lily-is.land/D114 by tusooa
- Add tests for MatrixSdk session management. https://iron.lily-is.land/D118 by tusooa
- Delete TWIM entries. https://iron.lily-is.land/D127 by tusooa
- Unify tooltip delay and timeout. https://iron.lily-is.land/D137 by tusooa
- Convert kazvprivlib to qml modules. https://iron.lily-is.land/D132 by tusooa
- Compile qml files into module. https://iron.lily-is.land/D133 by tusooa
- Add waitForRendering() in CreateRoomPageTest. https://iron.lily-is.land/D142 by tusooa
- Port away from legacy implicit conversions. https://iron.lily-is.land/D144 by tusooa
- Reduce calls to qt_target_qml_sources(). https://iron.lily-is.land/D145 by tusooa
- Add no trailing space rule in lint. https://iron.lily-is.land/D146 by tusooa
- Fix CI binary link. https://iron.lily-is.land/D152 by tusooa

## 0.4.0

### Added
- Open user page when clicking on a matrix link to a room member. https://iron.lily-is.land/D36
- Parse user given attribute events. https://iron.lily-is.land/D37
- Display overrided user names. https://iron.lily-is.land/D38
- Support editing overrided display name for users. https://iron.lily-is.land/D39
- Display overrided room hero names for unnamed rooms. https://iron.lily-is.land/D40
- Jump to an event when clicking on a reply. https://iron.lily-is.land/D41
- Make compile with Qt6 and KF6. https://iron.lily-is.land/D44
- Port Avatar to KirigamiAddons. https://iron.lily-is.land/D47
- Build with qt6/kf6 in CI. https://iron.lily-is.land/D76
- Show date in event read indicator. https://iron.lily-is.land/D79
- Paginate back from gaps automatically. https://iron.lily-is.land/D80
- Filter rooms by custom nicknames of room heroes. https://iron.lily-is.land/D82
- Load room members when opening members page. https://iron.lily-is.land/D87
- Autocomplete mentions. https://iron.lily-is.land/D88
- Make completion filterable by overrided nickname. https://iron.lily-is.land/D89

### Fixed
- Lazily create OverlaySheets. https://iron.lily-is.land/D42
- Fix notification tests for KF6. https://iron.lily-is.land/D45
- Port away from iconName and iconSource in Kirigami.Action. https://iron.lily-is.land/D48
- Make SelfDestroyableOverlaySheet work with KF6. https://iron.lily-is.land/D49
- Make FileDialog work with Qt6. https://iron.lily-is.land/D50
- Make Video work with Qt6. https://iron.lily-is.land/D51
- Make ScrollablePage actions work with KF6. https://iron.lily-is.land/D53
- Fix OverlaySheet display on KF6. https://iron.lily-is.land/D54
- Fix CreateRoomPage, RoomInvitePage, UserPage tests. https://iron.lily-is.land/D56
- Fix DeviceListTest, DevicePopupTest and RoomListViewItemDelegateTest. https://iron.lily-is.land/D57
- Fix Accessible.role type error in RoomListViewItemDelegate. https://iron.lily-is.land/D58
- Install translation files to DATADIR/kazv/l10n. https://iron.lily-is.land/D60
- Remove audio playing. https://iron.lily-is.land/D52
- Fix ConfirmUploadPopup for KF6. https://iron.lily-is.land/D55
- Make KazvShortcuts work on KF6. https://iron.lily-is.land/D77
- Fix EventReadIndicator on KF6. https://iron.lily-is.land/D78
- Optimize RoomListViewItemDelegate style for KF6. https://iron.lily-is.land/D81
- Fix upload icon on breeze-icons 6. https://iron.lily-is.land/D83
- Make JoinRoomPage auto-closable. https://iron.lily-is.land/D84
- Fix loading room page when no such member event is in local state. https://iron.lily-is.land/D86

### Internal changes
- Use a variable to control KF/Qt version in CMake. https://iron.lily-is.land/D43
- Update metadata to pass appstreamtest. https://iron.lily-is.land/D46
- Use mock helper and signal spy for mocks. https://iron.lily-is.land/D65
- Fix some UserPageTest not being run. https://iron.lily-is.land/D66

## 0.3.0

### Added
- Support displaying read receipts. https://lily-is.land/kazv/kazv/-/merge_requests/83
- Display edited version of events. https://lily-is.land/kazv/kazv/-/merge_requests/84
- Collapse nickname and avatar of messages from the same sender. https://iron.lily-is.land/D14
- Post read receipts when the room page is visible. https://iron.lily-is.land/D17
- Open link when clicked on. https://iron.lily-is.land/D18
- Display unread indicator in room list. https://iron.lily-is.land/D19
- Display edited version of events. https://iron.lily-is.land/D5
- Display invited room at the top of RoomListView. https://iron.lily-is.land/D32
- Make all secondary pages closable. https://iron.lily-is.land/D23
- Support enabling encryption in a room. https://iron.lily-is.land/D33
- Add functionality to parse matrix.to links. https://iron.lily-is.land/D34
- Optimize style for Device.qml. https://lily-is.land/kazv/kazv/-/merge_requests/95
- Differentiate between messages with and without sound. https://iron.lily-is.land/D21
- Implement event editing. https://iron.lily-is.land/D22
- Activate the corresponding room when clicking on notification. https://iron.lily-is.land/D24

### Fixed
- Fix some type errors in ConfirmUploadPopup.qml. https://lily-is.land/kazv/kazv/-/merge_requests/88
- Fix type error in AsyncHandler.qml. https://lily-is.land/kazv/kazv/-/merge_requests/98
- Stop click leaking from sticker picker. https://iron.lily-is.land/D29

### Internal changes
- Remove explicit compiler version in CI script. https://lily-is.land/kazv/kazv/-/merge_requests/93

## 0.2.0

### Added
- Implement removing local echo. https://lily-is.land/kazv/kazv/-/merge_requests/70
- Support sending stickers. https://lily-is.land/kazv/kazv/-/merge_requests/71
- Support dragging files into send message box to upload them. https://lily-is.land/kazv/kazv/-/merge_requests/72
- Implement rich text formatting. https://lily-is.land/kazv/kazv/-/merge_requests/74
- Support mentioning user. https://lily-is.land/kazv/kazv/-/merge_requests/78
- Support filtering by room name and id. https://iron.lily-is.land/D10
- Get rid of spin-wait Promises. https://iron.lily-is.land/D12
- Support filtering unnamed rooms by heros. https://iron.lily-is.land/D11

### Fixed
- Fix image overflow in event view. https://lily-is.land/kazv/kazv/-/merge_requests/73
- Fix creates wrong subdirectory when set cache directory. https://lily-is.land/kazv/kazv/-/merge_requests/75
- Use constant time cursors for MatrixRoomTimeline. https://lily-is.land/kazv/kazv/-/merge_requests/76
- Fix room name overflow in room list. https://lily-is.land/kazv/kazv/-/merge_requests/77
- Fix join room page. https://lily-is.land/kazv/kazv/-/merge_requests/79
- Fix translations display on Windows. https://lily-is.land/kazv/kazv/-/merge_requests/80
- Fix download result bar display on upload file event. https://lily-is.land/kazv/kazv/-/merge_requests/81

### Internal changes
- Rework on code review process. https://lily-is.land/kazv/kazv/-/merge_requests/84

## 0.1.1

### Fixed
- Make kazv run under Windows. https://lily-is.land/kazv/kazv/-/merge_requests/68

## 0.1.0

### Added
- Use fluent for translations. https://lily.kazv.moe/kazv/kazv/-/tree/tusooa/3-fluent
- Support read and save client state. https://lily.kazv.moe/kazv/kazv/-/merge_requests/2
- Support common event types. https://lily.kazv.moe/kazv/kazv/-/merge_requests/3
- Add send message shortcut. https://lily.kazv.moe/kazv/kazv/-/merge_requests/7
- Support auto-discovery and provide better login error messages. https://lily.kazv.moe/kazv/kazv/-/merge_requests/9
- Add translations for Hindi(hi) https://lily-is.land/kazv/kazv/-/merge_requests/11
- Use room heroes when there is no explicit room name. https://lily-is.land/kazv/kazv/-/merge_requests/15
- Add media file menu for download. https://lily-is.land/kazv/kazv/-/merge_requests/14
- Add a shortcut editor. https://lily-is.land/kazv/kazv/-/merge_requests/17
- Add UI for sending media files, controlling pause and cancel, and display the progress in real time. https://lily-is.land/kazv/kazv/-/merge_requests/18
- Use QtNetwork for job handling instead of libkazvjob. https://lily-is.land/kazv/kazv/-/merge_requests/21
- Implement creating and joining rooms. https://lily-is.land/kazv/kazv/-/merge_requests/19
- Add the ability to see users' devices and manage trust level. https://lily-is.land/kazv/kazv/-/merge_requests/23
- Support local echo. https://lily-is.land/kazv/kazv/-/merge_requests/25
- Support redaction. https://lily-is.land/kazv/kazv/-/merge_requests/28
- Implement profile settings. https://lily-is.land/kazv/kazv/-/merge_requests/30
- Support viewing event source. https://lily-is.land/kazv/kazv/-/merge_requests/32
- Display hero avatar if it is a two-person room. https://lily-is.land/kazv/kazv/-/merge_requests/34
- Support typing status. https://lily-is.land/kazv/kazv/-/merge_requests/33
- Support room tagging. https://lily-is.land/kazv/kazv/-/merge_requests/35
- Allow paginate back in the timeline. https://lily-is.land/kazv/kazv/-/merge_requests/38
- Handle incoming invites. https://lily-is.land/kazv/kazv/-/merge_requests/39
- Support displaying notifications for incoming messages. https://lily-is.land/kazv/kazv/-/merge_requests/40
- Support leaving room. https://lily-is.land/kazv/kazv/-/merge_requests/41
- Support sending and receiving encrypted media files. https://lily-is.land/kazv/kazv/-/merge_requests/20
- Allow selecting message content. https://lily-is.land/kazv/kazv/-/merge_requests/44
- Implement room member list view. https://lily-is.land/kazv/kazv/-/merge_requests/45
- Handle message replies. https://lily-is.land/kazv/kazv/-/merge_requests/48
- Implement reactions. https://lily-is.land/kazv/kazv/-/merge_requests/50
- Use libkazv push rules to determine whether to notify for an event. https://lily-is.land/kazv/kazv/-/merge_requests/51
- Implement displaying and changing users' power levels. https://lily-is.land/kazv/kazv/-/merge_requests/52
- Add the ability to ban and unban user. https://lily-is.land/kazv/kazv/-/merge_requests/54
- Sort rooms by descending order of latest event timestamp. https://lily-is.land/kazv/kazv/-/merge_requests/57
- Implement kicking user. https://lily-is.land/kazv/kazv/-/merge_requests/58
- Improve event view layout. https://lily-is.land/kazv/kazv/-/merge_requests/62
- Install kazv logo to icon directory. https://lily-is.land/kazv/kazv/-/merge_requests/63
- Implement inviting user. https://lily-is.land/kazv/kazv/-/merge_requests/65
- Support inviting users when creating a room. https://lily-is.land/kazv/kazv/-/merge_requests/66

### Fixed
- Fix scroll-to-top when receiving new events. https://lily-is.land/kazv/kazv/-/merge_requests/26
- Fix timeline efficiency. https://lily-is.land/kazv/kazv/-/merge_requests/37
- Use proper style and l10n for event fallback. https://lily-is.land/kazv/kazv/-/merge_requests/55
- Use debounce when setting local draft. https://lily-is.land/kazv/kazv/-/merge_requests/56
- Fix AppImage build due to missing KNotification qml modules. https://lily-is.land/kazv/kazv/-/merge_requests/59
- Put primary event loop back to separate thread. https://lily-is.land/kazv/kazv/-/merge_requests/60
- Fix crash when timeline of a room is empty. https://lily-is.land/kazv/kazv/-/merge_requests/61

### Removed
- Remove useless use of tabs on main page. https://lily-is.land/kazv/kazv/-/merge_requests/34
