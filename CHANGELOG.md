
# Change log

## Unreleased

- Use fluent for translations. https://lily.kazv.moe/kazv/kazv/-/tree/tusooa/3-fluent
- Support read and save client state. https://lily.kazv.moe/kazv/kazv/-/merge_requests/2
- Support common event types. https://lily.kazv.moe/kazv/kazv/-/merge_requests/3
