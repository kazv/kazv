# License

Copyright (C) 2020-2021 Tusooa Zhu <tusooa@kazv.moe>

kazv is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

kazv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with kazv.  If not, see <https://www.gnu.org/licenses/>.

# Installation

## For Gentoo users

If you are using Gentoo, you can use [tusooa-overlay][tusooa-overlay]
to install kazv.

[tusooa-overlay]: https://gitlab.com/tusooa/tusooa-overlay

## Dependencies

- ECM
- Qt5: Core Gui Qml QuickControls2 Svg Concurrent Multimedia
- KF5: Kirigami2 KConfig
- nlohmann_json
- libkazv with kazvjob

## Build process

Normal build:

```
mkdir build && cd build
cmake ..
make install
```

Updating external JavaScript libraries (advanced, usually not needed):

You need to first have Node.js available.

```
cd src/js/Intl.js
npm install
cd ..
npm install
./transform.bash
```
