# This file is part of libkazv.
# SPDX-FileCopyrightText: 2022-2023 tusooa <tusooa@kazv.moe>
# SPDX-License-Identifier: AGPL-3.0-or-later

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "trigger"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: '$CI_COMMIT_TAG !~ /^phabricator\//'

stages:
  - prepare
  - build
  - lint
  - report

.report:
  image:
    name: 'reg.lily.kazv.moe/infra/phorge-ci-tools:servant'
  rules:
    - if: $TARGET_PHID
      when: always
    - when: never
  before_script:
    - pipelineUrl="$CI_PROJECT_URL"/-/pipelines/"$CI_PIPELINE_ID"

report-start:
  extends: .report
  stage: prepare
  script:
    - 'echo "{\"receiver\": \"$TARGET_PHID\", \"type\": \"work\", \"unit\": [{\"name\": \"GitLab CI (information only)\", \"result\": \"skip\", \"details\": \"$pipelineUrl\", \"format\": \"remarkup\"}]}" | /tools/arcanist/bin/arc call-conduit --conduit-uri https://iron.lily-is.land/ --conduit-token "$CONDUIT_TOKEN" -- harbormaster.sendmessage'

'lint':
  stage: lint
  image: ubuntu
  script:
    - "echo 'Verify that no source file contains tabs'"
    - "! egrep -R --exclude-dir=transformed-libs --exclude-dir=Intl.js --exclude-dir=node_modules $'\t' src"
    - "echo 'Verify that no source file contains trailing spaces'"
    - "! egrep -R --exclude-dir=transformed-libs --exclude-dir=Intl.js --exclude-dir=node_modules '\\s$' src"
  rules: &build-rules
    - changes: &src-chg
        - Dockerfile.deps
        - .gitlab-ci.yml
        - '**/*.cpp'
        - '**/*.hpp'
        - '**/CMakeLists.txt'
        - '**/*.in'
        - '**/*.qml'
        - 'src/js/**/*.js'
        - 'src/js/**/*.mjs'
        - 'packaging/**/*'
        - 'icons/**/*'

lint-l10n:
  stage: lint
  image: node:lts
  rules:
    - changes:
        - .gitlab-ci.yml
        - 'src/l10n/**/*.ftl'
        - 'src/js/**/*.js'
        - 'src/js/**/*.mjs'
  script:
    - cd src/js
    - npm install
    - npm run test
    - npm run lint-l10n
  cache:
    key: $CI_JOB_NAME
    paths:
      - src/js/node_modules

.build:
  stage: build
  rules: *build-rules
  # Use ccache, see https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/240/diffs
  before_script:
    # Prevent removing of downloaded archives, because we are caching it
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/991#note_126864314
    - rm -f /etc/apt/apt.conf.d/docker-clean || true
    - echo "dir::cache::archives $CI_PROJECT_DIR/apt-cache/archives; dir::state::lists $CI_PROJECT_DIR/apt-cache/lists;" > /etc/apt/apt.conf.d/gitlab-cache
    - mkdir -pv "$CI_PROJECT_DIR/apt-cache/archives/partial" "$CI_PROJECT_DIR/apt-cache/lists/partial"
    - find "$CI_PROJECT_DIR/apt-cache/" || true
    - apt-get -y update
    - DEBIAN_FRONTEND=noninteractive apt-get -y install ccache
    - export CCACHE_COMPILERCHECK=content
    - export CCACHE_BASEDIR="$CI_PROJECT_DIR"
    - export CCACHE_DIR="$CI_PROJECT_DIR/ccache"
    - export PATH="/usr/lib/ccache:$PATH"
    - ccache --zero-stats || true
    - ccache --show-stats || true
  after_script:
    - DEBIAN_FRONTEND=noninteractive apt-get -y autoclean
    - find "$CI_PROJECT_DIR/apt-cache/"
    - export CCACHE_DIR="$CI_PROJECT_DIR/ccache"
    - ccache --show-stats
  cache:
    - key: $CI_JOB_NAME
      paths:
        - ccache/
    # This can be shared between jobs because we install the same set of packages regardless of the type of the build
    - key: apt-archive-cache
      paths:
        - apt-cache/

.build-debug:
  extends: .build
  script: |
    env JOBS=2 ./packaging/GNU-Linux/appimage/build.sh
  artifacts:
    paths:
      - kazv-Debug.AppImage
    expire_in: 1 week

.build-prod:
  extends: .build
  script: |
    env JOBS=2 ./packaging/GNU-Linux/appimage/build-prod.sh
  artifacts:
    paths:
      - kazv-Release.AppImage
    expire_in: 1 week

'build:GNU/Linux-amd64-debug:kf6':
  extends: .build-debug
  before_script:
    - !reference [.build-debug, before_script]
    - export KF_VER=6
  image:
    name: 'reg.lily.kazv.moe/kazv/libkazv/libkazv:servant-ubuntu22.04-debug'

'build:GNU/Linux-amd64-prod:kf6':
  extends: .build-prod
  before_script:
    - !reference [.build-prod, before_script]
    - export KF_VER=6
  image:
    name: 'reg.lily.kazv.moe/kazv/libkazv/libkazv:servant-ubuntu22.04-prod'

report-success:
  extends: .report
  rules:
    - if: $TARGET_PHID
      when: on_success
    - when: never
  stage: report
  script:
    - 'echo "{\"receiver\": \"$TARGET_PHID\", \"type\": \"pass\"}" | /tools/arcanist/bin/arc call-conduit --conduit-uri https://iron.lily-is.land/ --conduit-token "$CONDUIT_TOKEN" -- harbormaster.sendmessage'

report-failure:
  extends: .report
  rules:
    - if: $TARGET_PHID
      when: on_failure
    - when: never
  stage: report
  script:
    - 'echo "{\"receiver\": \"$TARGET_PHID\", \"type\": \"fail\"}" | /tools/arcanist/bin/arc call-conduit --conduit-uri https://iron.lily-is.land/ --conduit-token "$CONDUIT_TOKEN" -- harbormaster.sendmessage'
